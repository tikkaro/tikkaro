<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  |--------------------------------------------------------------------------
  | Display Debug backtrace
  |--------------------------------------------------------------------------
  |
  | If set to TRUE, a backtrace will be displayed along with php errors. If
  | error_reporting is disabled, the backtrace will not display, regardless
  | of this setting
  |
 */
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
  |--------------------------------------------------------------------------
  | File and Directory Modes
  |--------------------------------------------------------------------------
  |
  | These prefs are used when checking and setting modes when working
  | with the file system.  The defaults are fine on servers with proper
  | security, but you may wish (or even need) to change the values in
  | certain environments (Apache running a separate process for each
  | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
  | always be used to set the mode correctly.
  |
 */
defined('FILE_READ_MODE') OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE') OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE') OR define('DIR_WRITE_MODE', 0755);

/*
  |--------------------------------------------------------------------------
  | File Stream Modes
  |--------------------------------------------------------------------------
  |
  | These modes are used when working with fopen()/popen()
  |
 */
defined('FOPEN_READ') OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE') OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE') OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE') OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE') OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE') OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT') OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT') OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
  |--------------------------------------------------------------------------
  | Exit Status Codes
  |--------------------------------------------------------------------------
  |
  | Used to indicate the conditions under which the script is exit()ing.
  | While there is no universal standard for error codes, there are some
  | broad conventions.  Three such conventions are mentioned below, for
  | those who wish to make use of them.  The CodeIgniter defaults were
  | chosen for the least overlap with these conventions, while still
  | leaving room for others to be defined in future versions and user
  | applications.
  |
  | The three main conventions used for determining exit status codes
  | are as follows:
  |
  |    Standard C/C++ Library (stdlibc):
  |       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
  |       (This link also contains other GNU-specific conventions)
  |    BSD sysexits.h:
  |       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
  |    Bash scripting:
  |       http://tldp.org/LDP/abs/html/exitcodes.html
  |
 */
defined('EXIT_SUCCESS') OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR') OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG') OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE') OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS') OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT') OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE') OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN') OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX') OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

define('ADMIN_LAYOUT', 'admin/layout');
define('CUSTOMER_LAYOUT', 'front/customer/layout');
define('CUSTOMER_LAYOUT_LOGIN', 'front/customer/layout_login');

define('FRONT_LAYOUT', 'front/layout');
define('USER_LAYOUT', 'front/user/layout');
define('USER_LAYOUT_LOGIN', 'front/user/layout_login');

define('ADMIN_LAYOUT_LOGIN', 'admin/layout_login');
define('DISPLAY_LOGO', 'public/assets/images/logo.png');
define('NO_IMAGE', 'public/assets/images/noImg.png');
// FILE UPLOAD
define('KEY_ACCOUNT_PROFILE_IMG', 'public/upload/keyaccountmanager');
define('ADMIN_PROFILE_PIC', 'public/upload/profile_pic');
// END FILE UPLOAD
define('CLIENT_PROFILE_IMG', 'public/upload/client_profile_pic');
define('USER_PROFILE_IMG', 'public/upload/user_profile_pic');


//define status

define('CLIENT_STATUS', json_encode(['LOW' => 'Low', 'MEDIUM' => 'Medium', 'HIGH' => 'High']));
define('PROJECT_STATUS', json_encode(['ACTIVE' => 'Active', 'PRIORITY' => 'Priority', 'WAITING' => 'Waiting', 'SCHEDULING' => 'Scheduling', 'CLOSED' => 'Closed']));
define('SUBSCRIPTION_STATUS', json_encode(['PAY-AS-YOU-GO' => 'Pay As You Go', 'UNIT-BLOCK' => 'Unit Block']));
define('SHORTLISTED_STATUS', json_encode(['EXPERTS_ATTACHED' => 'Experts Attached', 'BIOS_SENT' => 'Bios sent', 'CALLS_IN_SCHEDULING' => 'Calls in Scheduling', 'CALLS_SCHEDULED' => 'Calls Scheduled', 'CALLS_CANCELLED' => 'Calls Cancelled']));
define('SCHEDULING_STATUS', json_encode(['CLIENT_REQUEST' => 'Client Requested', 'TIME_GIVEN' => 'Time Given']));

define('REVISED_VERSION', '1.7');
define('FRONT_REVISED_VERSION', '1.2');

define('VALID_IP', json_encode(['182.70.124.75', '103.251.227.213', '43.250.164.22']));

/* sending a mail  */
define('SMTP_USER', 'postmaster@mailing.webfirminfotech.com');
define('SMTP_PASSWORD', '4970f540f85b4425f1213bb15d1dd6e2');

define('TABLE_PREFIX', 'ps_');

define('PROJECT_NAME', 'tikkaro');
//Date Formate
define('MONTHYEAR', 'yyyy/mm');
define('DBMONTHYEAR', 'Y/m');
