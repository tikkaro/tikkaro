<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    public $json_response = null;

    function __construct() {
        parent::__construct();
        date_default_timezone_set('UTC');
        $this->json_response = array('status' => 'error', 'message' => 'something went wrong!');
    }

}

class Front_Controller extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

}

class Expert_Controller extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('ExpertAuthLibrary');
        $this->load->helper('expert_auth_helper');

        if ($this->router->fetch_class() != "account" && $this->router->fetch_method() != "login") {

            if (!isExpertLogin()) {
                redirect(expert_url('login'));
            }
        } else {
            if ($this->router->fetch_class() != "account" && $this->router->fetch_method() != "logout") {
                if (isExpertLogin()) {
                    redirect(expert_url('profile'));
                }
            }
        }
        checkExpertLoggedinUser();
    }

}

class Customer_Controller extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('CustomerAuthLibrary');
        $this->load->helper('customer_auth_helper');

        if ($this->router->fetch_class() != "account" && $this->router->fetch_method() != "login") {

            if (!isCustomerLogin()) {
                redirect(customer_url('login'));
            }
        } else {
            if ($this->router->fetch_class() != "account" && $this->router->fetch_method() != "logout") {
                if (isCustomerLogin()) {
                    redirect(customer_url('my-projects'));
                }
            }
        }
        checkCustomerLoggedinUser();
    }

}

class User_Controller extends CI_Controller {

    function __construct() {
        parent::__construct();

        $this->load->library('UserAuthLibrary');
        $this->load->helper('user_auth_helper');

        if ($this->router->fetch_class() != "account" && $this->router->fetch_method() != "login") {

            if (!isUserLogin()) {
                redirect(user_url('login'));
            }
        } else {
            if ($this->router->fetch_class() != "account" && $this->router->fetch_method() != "logout") {
                if (isUserLogin()) {
                    redirect(user_url('profile'));
                }
            }
        }
        checkUserLoggedinUser();
    }

}

class Admin_Controller extends MY_Controller {

    public $user_data = null;
    public $user_type = null;
    public $user_id = null;

    function __construct() {
        parent::__construct();
        $this->load->library('AuthLibrary');


        if ($this->router->fetch_class() != "account" && $this->router->fetch_method() != "login") {

            if (!isset($this->session->userdata['valid_login_user'])) {

                redirect(admin_url('login'));
            }
            $this->user_data = $this->authlibrary->getLoginUserData();
            $this->user_type = $this->authlibrary->getLoginUserType();
            $this->user_id = $this->authlibrary->getLoginUserId();
            $this->user_data = $this->session->userdata('valid_login_user');
        }
    }

}
