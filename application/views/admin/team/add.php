<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-user"></i>Add Team </div>
    </div>
    <div class="portlet-body form">
        <form method="post" enctype="multipart/form-data" id="addTeamFrm" action="<?= admin_url('team/add'); ?>" class="form-horizontal">
            <div class="form-body">
                <div class="form-group">
                    <label class="col-md-3 control-label">Name<span class="required" aria-required="true">*</span> :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="text" id="var_team" name="var_team" placeholder="Enter Team Name" class="form-control"> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-4">
                        <button type="submit" class="btn green btn-circle">Submit</button>
                        <a class="btn default btn-circle" href="<?php echo admin_url() . 'team'; ?>">Cancel</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>