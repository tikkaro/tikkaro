<div class="modal-dialog">
    <form class="form-horizontal" id="edit_education_frm" name="edit_education_frm" method="POST"
          enctype="multipart/form-data" action="<?= admin_url('experts/addEducationHistory') ?>">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Edit Education</h4>
            </div>
            <div class="modal-body">
                <div class="form-body">

                    <div class="eduction">
                        <div class="form-group">
                            <label class="col-md-3 control-label" style="padding-right:0">College/University <span class="required"
                                                                                 aria-required="true">*</span>:</label>
                            <div class="col-md-8">
                                <div class="input-icon right">
                                    <input type="hidden" id="expert_id" name="expert_id" value="<?= $edu_data['fk_experts'] ?>"/>
                                    <input type="hidden" id="edu_id" name="edu_id" value="<?= $edu_data['id'] ?>"/>
                                    <input type="text" id="var_colleage" name="var_college"
                                           placeholder="Enter College/University Name" class="form-control" value="<?= $edu_data['var_colleges'] ?>">
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-3 control-label" style="padding-right:0">Subject<span class="required"
                                                                                    aria-required="true">*</span> :</label>
                            <div class="col-md-8">
                                <div class="input-icon right">
                                    <input type="text" id="var_certificates" name="var_certificates"
                                           placeholder="Enter Subject" class="form-control" value="<?= $edu_data['var_certificates'] ?>">
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="col-md-3 control-label" style="padding-right:0">Dates :</label>
                            <div class="col-md-4">
                                <div class="input-group  date date-picker" data-date-format="dd/mm/yyyy">
                                    <input type="text" class="form-control" name="from" value="<?= (!empty($edu_data['dt_from_respective_date']))? date('d/m/Y', strtotime($edu_data['dt_from_respective_date'])):'' ?>">
                                    <span class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                </div>
                                <span class="help-block"> From </span>
                            </div>
                            <div class="col-md-4 to">
                                <div class="input-group  date date-picker" data-date-format="dd/mm/yyyy">
                                    <input type="text" name="to" class="form-control" value="<?= (!empty($edu_data['dt_to_respective_date']))? date('d/m/Y', strtotime($edu_data['dt_to_respective_date'])):'' ?>">
                                    <span class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                </div>
                                <span class="help-block">  To </span>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn green btn-circle submitcls"> Submit</button>
            </div>
        </div>
    </form>
</div>