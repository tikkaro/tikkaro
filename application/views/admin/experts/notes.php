<script type="text/javascript">
    var expertId = "<?php echo $expertsid; ?>";
</script>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="tabbable-line boxless tabbable-reversed">

            <?php $this->load->helper('common_tab_helper'); ?>

            <?= expertsMenu('notes', $expertsid, $getExperts,$expertDataArray['var_email']) ?>
            <div class="tab-content">
                <?php $this->load->helper('comman_data_helper'); ?>
                <?= expertData($expertDataArray) ?>
                <div class="portlet light portlet-fit portlet-datatable bordered">

                    <div class="portlet-title">
                        <div class="col-md-3" style="padding-left: 0">
                            <div class="caption">
                                <i class="icon-equalizer font-blue-hoki"></i>
                                <span class="caption-subject font-green-haze sbold uppercase"> Notes</span>
                            </div>
                        </div>

                        <div class="actions">
                            <a class="btn btn-circle btn-default btn-sm expert-notes" expert-id="<?= $expertsid ?>"
                               href="javascript:;"
                               data-toggle="modal">
                                <i class="fa fa-plus"></i> Add Notes
                            </a>
                        </div>
                    </div>


                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover table-checkable" id="notes_list">
                            <thead>
                            <tr role="row" class="heading">
                                <th> Date</th>
                                <th> Author</th>
                                <th> Info</th>
                                <th> Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<!-- START EDIT NOTES MODEL -->
<div class="modal fade" id="edit_notes_model" tabindex="-1" role="basic" aria-hidden="true"
     style="z-index: 999999999 !important;">
    <div class="modal-dialog">
        <form class="form-horizontal" id="expert_note_frm" name="expert_note_frm" method="POST"
              action="<?= admin_url('experts/addExpertNote') ?>">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Add Notes</h4>
                </div>
                <div class="modal-body">
                    <div class="form-body">

                            <div class="form-group">
                                <label class="control-label col-md-2">Notes : <span class="required">  </span></label>
                                <div class="col-md-10">
                                    <input type="hidden" name="expertId" class="expert_id" value=""/>
                                    <input type="hidden" name="note_id" class="note_id" value=""/>
                                    <textarea class="form-control exp_note" rows="6" name="notes"
                                              placeholder="Enter Notes"></textarea>
                                </div>
                            </div>


                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn green btn-circle submitcls submit-btn"> Submit</button>
                </div>
            </div>
        </form>
    </div>
    <!-- /.modal-content -->
</div>
<!-- END EDIT NOTE MODEL -->