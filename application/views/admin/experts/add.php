<script>

    $(document).ready(function () {
        var area = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            prefetch: {
                url: adminurl + 'experts/getArea',
                filter: function (list) {
                    return $.map(list, function (area) {
                        return {name: area};
                    });
                }
            }
        });
        area.initialize();

        $('#var_areaofexpert').tagsinput({
            typeaheadjs: {
                name: 'areaname',
                displayKey: 'name',
                valueKey: 'name',
                source: area.ttAdapter()
            }
        });
    });

</script>
<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-user"></i>Add Experts
        </div>

    </div>
    <div class="portlet-body form">
        <form role="form" method="post" enctype="multipart/form-data" id="addExpertsFrm"
              action="<?php echo admin_url() . 'experts/add'; ?>" class="form-horizontal">
            <div class="form-body">
                <div class="form-group">
                    <label class="col-md-3 control-label">First Name<span class="required" aria-required="true">*</span> :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="hidden" name="prospectId" id="prospectId"
                                   value="<?= ($prospectData['id']) ? $prospectData['id'] : '' ?>">
                            <input type="hidden" name="projectId" id="projectId"
                                   value="<?= ($projectId) ? $projectId : '' ?>">
                            <input type="text" id="var_name" name="var_fname" placeholder="Enter First Name"
                                   class="form-control"
                                   value="<?= ($prospectData['var_fname']) ? $prospectData['var_fname'] : '' ?>">
                        </div>
                    </div>
                    <div class="col-md-1">
                        <a class="btn btn-circle btn-primary btn-sm addmoreEdution tooltips" data-placement="top"
                           data-original-title="Import From Linked in" title="Import From Linked in"
                           href="javascript:;">
                            <i class="fa fa-linkedin" aria-hidden="true"></i>
                        </a>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Last Name<span class="required" aria-required="true">*</span> :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="text" id="var_name" name="var_lname" placeholder="Enter Last Name"
                                   class="form-control"
                                   value="<?= ($prospectData['var_lname']) ? $prospectData['var_lname'] : '' ?>">
                        </div>
                    </div>

                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Email
                        :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="text" id="var_email" name="var_email"
                                   placeholder="Enter Email" class="form-control"
                                   value="<?= ($prospectData['var_email']) ? $prospectData['var_email'] : '' ?>">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Password
                        :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="password" id="var_password" name="var_password"
                                   placeholder="Enter Password" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Hourly Rate (USD):</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="text" id="var_rateperhours" name="var_rateperhours"
                                   placeholder="Enter Rate per Hours" class="form-control" maxlength="3">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Rate per Survey (USD):</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="text" id="var_ratepersurvey" name="var_ratepersurvey"
                                   placeholder="Enter Rate per Survey" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Country:</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="text" id="var_country" name="var_country"
                                   placeholder="Enter  Country Name"
                                   class="form-control">
                        </div>
                    </div>
                </div>

                <div class="form-group custom-tagsinput">
                    <label for="multiple" class="control-label col-md-3">Expertise </label>
                    <div class="col-md-8">
                        <input type="text" id="var_areaofexpert" data-role="tagsinput" name="var_areaofexpert"
                               placeholder="Enter Expertise" class="form-control">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Compliance Date :</label>
                    <div class="col-md-8">
                            <input type="text" data-date-format="dd/mm/yyyy" class="form-control date-picker" name="compliance_date">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Bio :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <textarea class="form-control" id="txt_bio" name="txt_bio"
                                      placeholder="Enter Bio Discription" rows="3"></textarea>
                        </div>
                    </div>
                </div>


                <hr/>
                <h4> Education </h4>

                <div class="form-group">
                    <label class="col-md-3 control-label">College/University
                        :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="text" id="var_colleage" name="var_college[]"
                                   placeholder="Enter College/University Name" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Subject :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="text" id="var_certificates" name="var_certificates[]"
                                   placeholder="Enter Subject" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Dates :</label>
                    <div class="col-md-3">
                        <div class="input-group  date date-picker" data-date-format="dd/mm/yyyy">
                            <input type="text" class="form-control" name="eduction_from[]">
                            <span class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                        </div>
                        <span class="help-block"> From </span>
                    </div>
                    <div class="col-md-3 to">
                        <div class="input-group  date date-picker" data-date-format="dd/mm/yyyy">
                            <input type="text" name="eduction_to[]" class="form-control">
                            <span class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                        </div>
                        <span class="help-block">  To </span>
                    </div>
                </div>

                <div class="addEdution"></div>

                <div class="col-md-offset-3">
                    <a class="btn btn-circle btn-primary btn-sm addmoreEdution" href="javascript:;">
                        <i class="fa fa-plus"></i> Add More
                    </a>
                </div>

                <hr/>
                <h4> Employment History </h4>
                <!--<div class="employment">-->
                <div class="form-group">
                    <label class="col-md-3 control-label">Company :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="text" name="var_company[]" placeholder="Enter Company Name"
                                   class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Position :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="text" id="var_position" name="var_position[]"
                                   placeholder="Enter Position"
                                   class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Geography :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="text" id="var_geography" name="var_geography[]"
                                   placeholder="Enter Geography"
                                   class="form-control">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Responsibilities :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <textarea class="form-control" id="txt_resposibility" name="txt_resposibility[]"
                                      placeholder="Enter Responsibilities" rows="3"></textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group respectiveDates">
                    <label class="control-label col-md-3">Dates :</label>
                    <div class="col-md-3">
                        <div class="input-group  date date-picker" data-date-format="dd/mm/yyyy">
                            <input type="text" class="form-control" name="history_from[]">
                            <span class="input-group-btn">
                                <button class="btn default" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                        </div>
                        <span class="help-block"> From </span>
                    </div>
                    <div class="col-md-3 to">
                        <div class="input-group  date date-picker" data-date-format="dd/mm/yyyy">
                            <input type="text" name="history_to[]" class="form-control">
                            <span class="input-group-btn">
                                <button class="btn default" type="button">
                                    <i class="fa fa-calendar"></i>
                                </button>
                            </span>
                        </div>
                        <span class="help-block">  To </span>
                    </div>

                    <label class="control-label col-md-2  present" style="display:none;"><strong>
                            -Present</strong></label>
                    <input type="hidden" class="checkVal" name="checkVal[]" value="0" />
                </div>
                <div class="form-group">
                    <div class="col-md-offset-3 md-checkbox">
                        <input type="checkbox" id="work" name="work[]" value="YES" class="md-check checkbox6">
                        <label for="work">
                            <span></span>
                            <span class="check"></span>
                            <span class="box"></span> I currently work here </label>
                    </div>
                </div>

                <!--</div>-->
                <div class="addEmployment">

                </div>
                <div class="col-md-offset-3">
                    <a class="btn btn-circle btn-primary btn-sm addmoreEmployment" href="javascript:;">
                        <i class="fa fa-plus"></i> Add More
                    </a>
                </div>
            </div>

            <div class="clearfix"></div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-4">
                        <button type="submit" class="btn green btn-circle">Submit</button>
                        <a class="btn default btn-circle"
                           href="<?php echo admin_url() . 'experts'; ?>">Cancel</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>


<div class="eduction_history" style="display: none;">
    <div class="eduction">
        <div class="form-group">
            <label class="col-md-3 control-label">College/University:</label>
            <div class="col-md-8">
                <div class="input-icon right">
                    <input type="text" id="var_colleage" name="var_college[]"
                           placeholder="Enter College/University Name" class="form-control">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Subject:</label>
            <div class="col-md-8">
                <div class="input-icon right">
                    <input type="text" id="var_certificates" name="var_certificates[]"
                           placeholder="Enter Subject" class="form-control">
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Dates :</label>
            <div class="col-md-2">
                <div class="input-group  date date-picker" data-date-format="dd/mm/yyyy">
                    <input type="text" class="form-control" name="eduction_from[]">
                    <span class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                </div>
                <span class="help-block"> From </span>
            </div>
            <div class="col-md-2 to">
                <div class="input-group  date date-picker" data-date-format="dd/mm/yyyy">
                    <input type="text" name="eduction_to[]" class="form-control">
                    <span class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                </div>
                <span class="help-block">  To </span>
            </div>
<!--            <div class="col-md-8">-->
<!--                <div class="input-group input-large date-picker input-daterange"-->
<!--                     data-date-format="dd/mm/yyyy">-->
<!--                    <input class="form-control" name="eduction_from[]" type="text">-->
<!--                    <span class="input-group-addon"> to </span>-->
<!--                    <input class="form-control" name="eduction_to[]" type="text">-->
<!--                </div>-->
<!--            </div>-->
        </div>

        <a style="margin-left: 550px; margin-bottom: 10px;" class=" btn btn-circle btn-danger btn-sm eductionRemove"
           href="javascript:;">Remove</a>


    </div>
</div>
<div class="employment_history" style="display: none;">
    <div class="employment">
        <div class="form-group">
            <label class="col-md-3 control-label">Company :</label>
            <div class="col-md-8">
                <div class="input-icon right">
                    <input type="text" name="var_company[]" placeholder="Enter Company Name" class="form-control">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Position :</label>
            <div class="col-md-8">
                <div class="input-icon right">
                    <input type="text" id="var_position" name="var_position[]" placeholder="Enter Position"
                           class="form-control">
                </div>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Geography :</label>
            <div class="col-md-8">
                <div class="input-icon right">
                    <input type="text" id="var_geography" name="var_geography[]" placeholder="Enter Geography"
                           class="form-control">
                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Responsibilities :</label>
            <div class="col-md-8">
                <div class="input-icon right">
                    <textarea class="form-control" id="txt_resposibility" name="txt_resposibility[]"
                              placeholder="Enter Responsibilities" rows="3"></textarea>
                </div>
            </div>
        </div>
        <div class="form-group respectiveDates">
            <label class="control-label col-md-3">Dates :</label>
            <div class="col-md-2">
                <div class="input-group  date date-picker" data-date-format="dd/mm/yyyy">
                    <input type="text" class="form-control" name="history_from[]">
                    <span class="input-group-btn">
                        <button class="btn default" type="button">
                            <i class="fa fa-calendar"></i>
                        </button>
                    </span>
                </div>
                <span class="help-block"> From </span>
            </div>
            <div class="col-md-2 to">
                <div class="input-group  date date-picker" data-date-format="dd/mm/yyyy">
                    <input type="text" name="history_to[]" class="form-control">
                    <span class="input-group-btn">
                        <button class="btn default" type="button">
                            <i class="fa fa-calendar"></i>
                        </button>
                    </span>
                </div>
                <span class="help-block">  To </span>
            </div>

            <label class="control-label col-md-2  present" style="display:none;"><strong> -Present</strong></label>
            <input type="hidden" class="checkVal" name="checkVal[]" value="0" />
        </div>
        <div class="form-group">
            <div class="col-md-offset-3 md-checkbox">
                <input type="checkbox" id="work_@" name="work[]" value="YES" class="md-check checkbox6">
                <label for="work_@">
                    <span></span>
                    <span class="check"></span>
                    <span class="box"></span> I currently work here </label>
            </div>
        </div>
        <a style="margin-left: 550px; margin-bottom: 10px;" class=" btn btn-circle btn-danger btn-sm employeeRemove"
           href="javascript:;">Remove</a>

    </div>
</div>
