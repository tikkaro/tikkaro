<style>
    .modal-open .colorpicker, .modal-open .datepicker, .modal-open .daterangepicker {
        z-index: 1005599999 !important;
    }

    .marginTop0 {
        margin-top: 0;
    }
</style>
<script type="text/javascript">
    var expertId = '<?php echo $expertsid ?>';

    $(document).ready(function () {

        var area = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            prefetch: {
                url: adminurl + 'experts/getArea',
                filter: function (list) {
                    return $.map(list, function (area) {
                        return {name: area};
                    });
                }
            }
        });
        area.initialize();

        $('#var_areaofexpert').tagsinput({
            typeaheadjs: {
                name: 'areaname',
                displayKey: 'name',
                valueKey: 'name',
                source: area.ttAdapter()
            }
        });
    });

</script>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="tabbable-line boxless tabbable-reversed">
            <?php $this->load->helper('common_tab_helper'); ?>
            <?= expertsMenu('bio', $expertsid, $getExperts,$expertBio[0]['var_email']) ?>
            <div class="tab-content">

                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="portlet light portlet-fit portlet-datatable bordered">
                            <div class="portlet-title">
                                <div class="col-md-3" style="padding-left: 0">
                                    <div class="caption">
                                        <i class="icon-equalizer font-blue-hoki"></i>
                                        <span class="caption-subject font-green-haze sbold uppercase"> Expert Bio</span>
                                    </div>
                                </div>
                                <div class="actions" style="padding-right: 10px;">
                                    <?php if ($expertBio[0]['enum_block'] == 'NO') { ?>
                                        <a class=" btn btn-circle btn-danger btn-sm block" href="javascript:;"
                                           data-id="<?= $expertBio[0]['id']; ?>">Block</a>
                                    <?php } else { ?>
                                        <a class=" btn btn-circle btn-primary btn-sm unblock" href="javascript:;"
                                           data-id="<?= $expertBio[0]['id']; ?>">Unblock</a>
                                    <?php } ?>
                                    <a class=" btn btn-circle btn-primary btn-sm resetPassword" href="javascript:;"
                                       data-id="<?= $expertBio[0]['id']; ?>">Reset Password</a>
                                </div>
                            </div>
                            <div class="portlet-body">

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label col-md-2">Recruited By:</label>
                                            <div class="col-md-8">
                                                <label class="control-label expertsBio">
                                                    <?= $expertBio[0]['RecruitedBy']; ?>
                                                </label>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label col-md-2">Name:</label>
                                            <div class="col-md-8">
                                                <label class="control-label expertsBio">
                                                    <a href="javascript:;" class="inline-editing-trigger"
                                                       data-title="Enter Name"
                                                       data-placeholder="Enter Name"
                                                       data-table="experts"
                                                       data-required="true"
                                                       data-field1="var_fname"
                                                       data-field2="var_lname"
                                                       data-input-class="form-control cuswidth"
                                                       data-id="<?= $expertBio[0]['id']; ?>"
                                                       data-url="<?= admin_url('commaneditable/editableFnameLname') ?>"
                                                    ><?= $expertBio[0]['var_fname'] . ' ' . $expertBio[0]['var_lname']; ?>
                                                    </a>
                                                </label>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                                <!--/row-->


                                <!--/row-->
                                <div class="row ">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label col-md-2">Email:</label>
                                            <div class="col-md-8">
                                                <label class="control-label expertsBio">
                                                    <a href="javascript:;" class="inline-editing-trigger"
                                                       data-title="Enter Email"
                                                       data-placeholder="Enter Email"
                                                       data-table="experts"
                                                       data-required="true"
                                                       data-validate-email="true"
                                                       data-field="var_email"
                                                       data-input-class="form-control cuswidth"
                                                       data-id="<?= $expertBio[0]['id']; ?>"
                                                    ><?= $expertBio[0]['var_email']; ?>
                                                    </a></label>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label col-md-2">Rate per Hour:</label>
                                            <div class="col-md-8">
                                                <label class="control-label expertsBio">
                                                    <a href="javascript:;" class="inline-editing-trigger"
                                                       data-title="Enter Hour"
                                                       data-placeholder="Enter Hour"
                                                       data-table="experts"
                                                       data-required="true"
                                                       data-field="var_rate_per_hour"
                                                       data-input-class="form-control cuswidth"
                                                       data-id="<?= $expertBio[0]['id']; ?>"
                                                    ><?= ($expertBio[0]['var_rate_per_hour']) ? $expertBio[0]['var_rate_per_hour'] . ' USD' : ''; ?>
                                                    </a>
                                                </label>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label col-md-2">Rate per Survey:</label>
                                            <div class="col-md-8">
                                                <label class="control-label expertsBio">
                                                    <a href="javascript:;" class="inline-editing-trigger"
                                                       data-title="Enter Survey"
                                                       data-placeholder="Enter Survey"
                                                       data-table="experts"
                                                       data-required="true"
                                                       data-field="var_rate_per_survey"
                                                       data-input-class="form-control cuswidth"
                                                       data-id="<?= $expertBio[0]['id']; ?>"
                                                    ><?= ($expertBio[0]['var_rate_per_survey']) ? $expertBio[0]['var_rate_per_survey'] . ' USD' : ''; ?>
                                                    </a></label>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label col-md-2">Country:</label>
                                            <div class="col-md-8">
                                                <label class="control-label expertsBio">
                                                    <a href="javascript:;" class="inline-editing-trigger"
                                                       data-title="Enter Country"
                                                       data-placeholder="Enter Country"
                                                       data-table="experts"
                                                       data-required="true"
                                                       data-field="var_country"
                                                       data-input-class="form-control cuswidth"
                                                       data-id="<?= $expertBio[0]['id']; ?>"
                                                    ><?= $expertBio[0]['var_country']; ?>
                                                    </a></label>

                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                                <!--/row-->
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label col-md-2">Expertise (keywords):</label>
                                            <div class="col-md-8">
                                                <label class="control-label">
                                                    <?php
                                                    $expertsArea = explode(',', $expertBio[0]['area_of_expert']);
                                                    $expertsArea = implode(' , ', $expertsArea);
                                                    ?>
                                                    <a href="javascript:;"
                                                       class="cusComapnine editable editable-click"> <?= (!empty($expertsArea)) ? $expertsArea : '<span style="color: #DD1144"><i>Empty</i></span>' ?></a>
                                                </label>
                                                <div class="input-icon Tagstoshow expertsBio" style="display: none;">
                                                    <input type="text" id="var_areaofexpert" name="var_areaofexpert"
                                                           placeholder="Enter Expertise" data-role="tagsinput"
                                                           class="form-control"
                                                           value="<?= $expertBio[0]['area_of_expert']; ?>">
                                                    <div class="editable-buttons cusCompanyTags">
                                                        <button type="submit"
                                                                class="btn blue editable-submit updateTags"
                                                                data-table="experts_area" data-id="<?= $expertsid; ?>">
                                                            <i class="fa fa-check"></i></button>
                                                        <button type="button"
                                                                class="btn default editable-cancel cancleTags"><i
                                                                    class="fa fa-times"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label col-md-2">Compliance Date:</label>
                                            <div class="col-md-8">
                                                <label class="control-label">
                                                    <a href="javascript:;"
                                                       class="expertsComplianceDate editable editable-click"> <?= ($expertBio[0]['dt_compliance_date']) ? date('d/m/Y', strtotime($expertBio[0]['dt_compliance_date'])) : '<span style="color: #DD1144"><i>Compliance training incomplete</i></span>'?></a>
                                                </label>
                                                <div class="input-icon dateToshow expertsBio" style="display: none;">
                                                    <input type="text" id="compliance_date" name="compliance_date"
                                                           placeholder="Enter Compliance Date"
                                                           class="form-control date-picker" data-date-format="dd/mm/yyyy"
                                                           value="<?= ($expertBio[0]['dt_compliance_date'])?date('d/m/Y', strtotime($expertBio[0]['dt_compliance_date'])): NULL; ?>">
                                                    <div class="editable-buttons cusDateValue">
                                                        <button type="submit"
                                                                class="btn blue editable-submit updateDates"
                                                                data-table="experts" data-id="<?= $expertsid; ?>">
                                                            <i class="fa fa-check"></i></button>
                                                        <button type="button"
                                                                class="btn default editable-cancel cancelDates"><i
                                                                    class="fa fa-times"></i></button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label col-md-2">Bio:</label>
                                            <div class="col-md-8">
                                                <label class="control-label expertsBio">
                                                    <a href="javascript:;" class="inline-editing-trigger"
                                                       data-title="Enter Bio"
                                                       data-placeholder="Enter Bio"
                                                       data-table="experts"
                                                       data-type="textarea"
                                                       data-required="true"
                                                       data-field="txt_bio"
                                                       data-input-class="form-control cuswidth"
                                                       data-id="<?= $expertBio[0]['id']; ?>"><?= $expertBio[0]['txt_bio']; ?></a>
                                                </label>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>

                                <hr>
                                <div class="eductionHistory">
                                    <h3 style="color: #00A0D1; margin-top:0; margin-bottom: 20px;" class="col-md-8">
                                        Education History</h3>
                                    <div class="col-md-4">
                                        <a class=" btn btn-circle btn-default btn-sm edit_education pull-right text-right"
                                           href="javascript:;">
                                            <i class="fa fa-plus"></i> Add Education History
                                        </a>
                                    </div>
                                </div>
                                <?php for ($i = 0; $i < count($expertEducation); $i++) { ?>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Colleges:</label>
                                                <div class="col-md-8">
                                                    <label class="control-label">
                                                        <?= $expertEducation[$i]['var_colleges'] ?>
                                                    </label>
                                                </div>
                                                <div class="col-md-2 text-right">
                                                    <a class=" btn btn-circle btn-primary btn-sm edit_education pull-right"
                                                       data-id="<?= $expertEducation[$i]['id']; ?>"
                                                       href="javascript:;">
                                                        <i class="fa fa-edit"></i> Edit
                                                    </a>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Certificates:</label>
                                                <div class="col-md-8">
                                                    <label class="control-label">
                                                        <?= $expertEducation[$i]['var_certificates'] ?>
                                                    </label>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Respective Date:</label>
                                                <div class="col-md-8">
                                                    <label class="control-label">
                                                        <?php
                                                        if (!empty($expertEducation[$i]['dt_from_respective_date']) && !empty($expertEducation[$i]['dt_to_respective_date'])) {
                                                            echo date('d/m/Y', strtotime($expertEducation[$i]['dt_from_respective_date'])) . ' TO' . date('d/m/Y', strtotime($expertEducation[$i]['dt_to_respective_date']));
                                                        } else {
                                                            echo "-";
                                                        }

                                                        ?>
                                                    </label>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                <?php } ?>
                                <hr>
                                <div class="employmentHistory">
                                    <h3 class="col-md-8" style="color: #00A0D1; margin-top: 0; margin-bottom: 20px;">
                                        Employment History</h3>
                                    <div class="col-md-4 text-right">
                                        <a class=" btn btn-circle btn-default btn-sm pull-right"
                                           href="#add_bio_employeement_model" data-toggle="modal">
                                            <i class="fa fa-plus"></i> Add Employment History
                                        </a>
                                    </div>
                                </div>
                                <?php
                                for ($i = 0; $i < count($expertsEmployement); $i++) { ?>

                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Company:</label>
                                                <div class="col-md-8">
                                                    <label class="control-label">
                                                        <?= $expertsEmployement[$i]['var_company']; ?>
                                                    </label>
                                                </div>
                                                <div class="col-md-2">
                                                    <a class=" btn btn-circle btn-primary btn-sm pull-right edit"
                                                       data-id="<?= $expertsEmployement[$i]['id']; ?>"
                                                       data-action="<?= admin_url('experts/editEmployHistory'); ?>"
                                                       href="javascript:;">
                                                        <i class="fa fa-edit"></i> Edit
                                                    </a>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Position:</label>
                                                <div class="col-md-8">
                                                    <label class="control-label">
                                                        <?= $expertsEmployement[$i]['var_position']; ?>
                                                    </label>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Responsibilities:</label>
                                                <div class="col-md-8">
                                                    <label class="control-label">
                                                        <?= $expertsEmployement[$i]['txt_responsibilities']; ?>
                                                    </label>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="form-group">
                                                <label class="control-label col-md-2">Respective Dates:</label>
                                                <div class="col-md-8">
                                                    <label class="control-label">
                                                        <?php
                                                        if (!empty($expertsEmployement[$i]['dt_from_respective_date'])) {
                                                            if ($expertsEmployement[$i]['present'] == 'YES') {
                                                                echo date('d/m/Y', strtotime($expertsEmployement[$i]['dt_from_respective_date'])) . ' To Present';
                                                            } else {
                                                                if (!empty($expertsEmployement[$i]['dt_to_respective_date'])) {
                                                                    echo date('d/m/Y', strtotime($expertsEmployement[$i]['dt_from_respective_date'])) . ' To ' . date('d/m/Y', strtotime($expertsEmployement[$i]['dt_to_respective_date']));
                                                                } else {
                                                                    echo "-";
                                                                }
                                                            }
                                                        } else {
                                                            echo "-";
                                                        }
                                                        ?>
                                                        <!--                                                        --><? //= date('d/m/Y', strtotime($expertsEmployement[$i]['dt_from_respective_date'])) ?>
                                                        <!--                                                        TO --><? //= ($expertsEmployement[$i]['dt_to_respective_date']) ? date('d/m/Y', strtotime($expertsEmployement[$i]['dt_to_respective_date'])) : 'Present'; ?>
                                                    </label>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <hr>
                                <?php } ?>
                            </div>
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!--add employeement history model-->
<div class="modal fade" id="add_bio_employeement_model" tabindex="-1" role="basic" aria-hidden="true"
     style="z-index: 999999999 !important;">
    <div class="modal-dialog">
        <form class="form-horizontal" id="add_employeement_frm" name="add_employeement_frm" method="POST"
              enctype="multipart/form-data" action="<?= admin_url('experts/addEmployHistory'); ?>">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Add Employment</h4>
                </div>
                <div class="modal-body">
                    <div class="form-body">

                        <div class="employment">
                            <div class="form-group">
                                <input type="hidden" value="<?= $expertsid ?>" name="expert_id" id="expert_id"/>
                                <label class="col-md-3 control-label">Company <span class="required"
                                                                                    aria-required="true">*</span>:</label>
                                <div class="col-md-8">
                                    <div class="input-icon right">
                                        <input type="text" id="var_company" name="var_company"
                                               placeholder="Enter Company Name" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Position <span class="required"
                                                                                     aria-required="true">*</span>:</label>
                                <div class="col-md-8">
                                    <div class="input-icon right">
                                        <input type="text" id="var_position" name="var_position"
                                               placeholder="Enter Position"
                                               class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Geography<span class="required"
                                                                                     aria-required="true">*</span>
                                    :</label>
                                <div class="col-md-8">
                                    <div class="input-icon right">
                                        <input type="text" id="var_geography" name="var_geography"
                                               placeholder="Enter Geography"
                                               class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Responsibilities <span class="required"
                                                                                             aria-required="true">*</span>:</label>
                                <div class="col-md-8">
                                    <div class="input-icon right">
                                        <textarea class="form-control" id="txt_resposibility" name="txt_resposibility"
                                                  placeholder="Enter Responsibilities" rows="3"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group respectiveDates">
                                <label class="control-label col-md-3">Respective Date<span class="required"
                                                                                           aria-required="true">*</span>:</label>
                                <div class="col-md-4">
                                    <div class="input-group  date date-picker" data-date-format="dd/mm/yyyy">
                                        <input type="text" class="form-control" name="history_from"
                                               value="<?= date('d/m/Y') ?>">
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                    </div>
                                    <span class="help-block"> From </span>
                                </div>
                                <div class="col-md-4 to">
                                    <div class="input-group  date date-picker" data-date-format="dd/mm/yyyy">
                                        <input type="text" name="history_to" class="form-control">
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                    </div>
                                    <span class="help-block">  To </span>
                                </div>


                                <label class="control-label col-md-2  present" style="display:none;"><strong>
                                        -Present</strong></label>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-4 md-checkbox">
                                    <input type="checkbox" id="present" name="work" class="md-check checkbox6">
                                    <label for="present">
                                        <span></span>
                                        <span class="check"></span>
                                        <span class="box"></span> I currently work here </label>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn green btn-circle submitcls"> Submit</button>
                </div>
            </div>
        </form>
    </div>
    <!-- /.modal-content -->
</div>
<!--end employeement history model-->


<!--start edit employeement history model-->
<div class="modal fade" id="edit_bio_employeement_model" tabindex="-1" role="basic" aria-hidden="true"
     style="z-index: 999999999 !important;">
    <div class="modal-dialog">
        <form class="form-horizontal" id="edit_employeement_frm" name="edit_employeement_frm" method="POST"
              enctype="multipart/form-data" action="<?= admin_url('experts/addEmployHistory'); ?>">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Edit Employment</h4>
                </div>
                <div class="modal-body">
                    <div class="form-body">

                        <div class="employment">
                            <div class="form-group">
                                <label class="col-md-3 control-label">Company <span class="required"
                                                                                    aria-required="true">*</span>:</label>
                                <div class="col-md-8">
                                    <div class="input-icon right">
                                        <input type="hidden" value="<?= $expertsid ?>" name="expert_id" id="expert_id"/>
                                        <input type="hidden" value="" name="employee_history_id" class="history_id"
                                               id="employee_history_id"/>
                                        <input type="text" id="var_company" name="var_company"
                                               placeholder="Enter Company Name" class="form-control var_company">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Position <span class="required"
                                                                                     aria-required="true">*</span>:</label>
                                <div class="col-md-8">
                                    <div class="input-icon right">
                                        <input type="text" id="var_position" name="var_position"
                                               placeholder="Enter Position"
                                               class="form-control var_position">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Geography<span class="required"
                                                                                     aria-required="true">*</span>
                                    :</label>
                                <div class="col-md-8">
                                    <div class="input-icon right">
                                        <input type="text" id="var_geography" name="var_geography"
                                               placeholder="Enter Geography"
                                               class="form-control var_geography">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Responsibilities <span class="required"
                                                                                             aria-required="true">*</span>:</label>
                                <div class="col-md-8">
                                    <div class="input-icon right">
                                        <textarea class="form-control txt_resposibility" id="txt_resposibility"
                                                  name="txt_resposibility"
                                                  placeholder="Enter Responsibilities" rows="3"></textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group respectiveDates">
                                <label class="control-label col-md-3">Respective Date:</label>
                                <div class="col-md-4">
                                    <div class="input-group  date date-picker" data-date-format="dd/mm/yyyy">
                                        <input type="text" class="form-control history_from" name="history_from">
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                    </div>
                                    <span class="help-block"> From </span>
                                </div>
                                <div class="col-md-4 to">
                                    <div class="input-group  date date-picker" data-date-format="dd/mm/yyyy">
                                        <input type="text" name="history_to" class="form-control history_to">
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                    </div>
                                    <span class="help-block">  To </span>
                                </div>

                                <label class="control-label col-md-2  present" style="display:none;"><strong>
                                        -Present</strong></label>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-4 md-checkbox">
                                    <input type="checkbox" id="check1" name="work" class="md-check checkbox6">
                                    <label for="check1">
                                        <span></span>
                                        <span class="check"></span>
                                        <span class="box"></span> I currently work here </label>
                                </div>
                            </div>

                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn green btn-circle submitcls edit-history"> Submit</button>
                </div>
            </div>
        </form>
    </div>
    <!-- /.modal-content -->
</div>
<!--end edit employeement history model-->

<!--Education Model-->
<div class="modal fade" id="edit_bio_education_model" tabindex="-1" role="basic" aria-hidden="true"
     style="z-index: 999999999 !important;">
    <div class="modal-dialog">
        <form class="form-horizontal" id="edit_education_frm" name="edit_education_frm" method="POST"
              enctype="multipart/form-data" action="<?= admin_url('experts/addEducationHistory') ?>">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Add Education</h4>
                </div>
                <div class="modal-body">
                    <div class="form-body">

                        <div class="eduction">
                            <div class="form-group">
                                <label class="col-md-3 control-label" style="padding-right:0">College/University <span class="required"
                                                                                               aria-required="true">*</span>:</label>
                                <div class="col-md-8">
                                    <div class="input-icon right">
                                        <input type="hidden" id="expert_id" name="expert_id" value="<?= $expertsid ?>"/>
                                        <input type="hidden" id="edu_id" name="edu_id" value=""/>
                                        <input type="text" id="var_colleage" name="var_college"
                                               placeholder="Enter College/University Name" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label" style="padding-right:0">Subject<span class="required"
                                                                                   aria-required="true">*</span>
                                    :</label>
                                <div class="col-md-8">
                                    <div class="input-icon right">
                                        <input type="text" id="var_certificates" name="var_certificates"
                                               placeholder="Enter Subject" class="form-control">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label" style="padding-right:0">Dates :</label>
                                <div class="col-md-4">
                                    <div class="input-group  date date-picker" data-date-format="dd/mm/yyyy">
                                        <input type="text" class="form-control" name="from">
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                    </div>
                                    <span class="help-block"> From </span>
                                </div>
                                <div class="col-md-4 to">
                                    <div class="input-group  date date-picker" data-date-format="dd/mm/yyyy">
                                        <input type="text" name="to" class="form-control">
                                        <span class="input-group-btn">
                                            <button class="btn default" type="button">
                                                <i class="fa fa-calendar"></i>
                                            </button>
                                        </span>
                                    </div>
                                    <span class="help-block">  To </span>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn green btn-circle submitcls"> Submit</button>
                </div>
            </div>
        </form>
    </div>
    <!-- /.modal-content -->
</div>
<!--End education model-->

<!--Education Model-->
<div class="modal fade" id="experts_reset_password" tabindex="-1" role="basic" aria-hidden="true"
     style="z-index: 999999999 !important;">
    <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Reset Password</h4>
                </div>
                <div class="modal-body">
                    <div class="form-body">
                            <h3>Are you sure you want to reset password?</h3>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn green btn-circle expertsResetPassword"> Submit</button>
                </div>
            </div>
    </div>
    <!-- /.modal-content -->
</div>
<!--End education model-->