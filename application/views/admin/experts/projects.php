<script>
    var expertId = '<?php echo $expertsid ?>'
</script>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="tabbable-line boxless tabbable-reversed">
            <?php $this->load->helper('common_tab_helper'); ?>

            <?= expertsMenu('projects', $expertsid, $getExperts,$expertDataArray['var_email']) ?>
            <div class="tab-content">
                <?php $this->load->helper('comman_data_helper'); ?>
                <?= expertData($expertDataArray) ?>
                <div class="portlet light portlet-fit portlet-datatable bordered">

                    <div class="portlet-title">
                        <div class="col-md-3" style="padding-left: 0">
                            <div class="caption">
                                <i class="icon-settings font-green-haze"></i>
                                <span class="caption-subject font-green-haze sbold uppercase"> Project List </span>
                            </div>
                        </div>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover table-checkable" id="projectList">
                            <thead>
                                <tr role="row" class="heading">
                                    <th>Project Name</th>
                                    <th>Description</th>
                                    <th>Client</th>
                                    <th>Client Value</th>
                                    <th>Initiated</th>
                                    <th>Deadline</th>
                                    <th>Calls Completed</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div> 

                </div>
            </div>
        </div>
    </div>
</div>