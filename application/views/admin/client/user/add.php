<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-user"></i>Add User
        </div>
    </div>
    <div class="portlet-body form">
        <form role="form" method="post" enctype="multipart/form-data" id="addUserFrm" action="<?= admin_url().'client/addUser'?>" class="form-horizontal">
            <div class="form-body">
                <div class="form-group">
                    <label class="col-md-3 control-label">Firt Name<span class="required" aria-required="true">*</span>
                        :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="hidden" id="fk_account_manager" name="fk_assign_account_manager" class="form-control" value="<?= $clientData['fk_assign_account_manager']?>">
                            <input type="hidden" id="client_id" name="client_id" class="form-control" value="<?= $clientData['id']?>">
                            <input type="hidden" id="fk_team" name="fk_team" class="form-control" value="<?= $clientData['fk_team']?>">
                            <input type="text" id="var_fname" name="var_fname" placeholder="Enter First Name" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Last Name<span class="required" aria-required="true">*</span>
                        :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="text" id="var_lname" name="var_lname" placeholder="Enter Lirst Name"
                                   class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Email:</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="text" id="var_email" name="var_email" placeholder="Enter Email Address"
                                   class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Phone:</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="text" id="bint_phone" name="bint_phone" placeholder="Enter Contact No"
                                   class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Position:</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="text" id="var_position" name="var_position" placeholder="Enter Position"
                                   class="form-control">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-4">
                        <button type="submit" class="btn green btn-circle">Submit</button>
                        <a class="btn default btn-circle" href="<?php echo admin_url() . 'client'; ?>">Cancel</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>