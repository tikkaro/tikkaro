<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="tabbable-line boxless tabbable-reversed">
            <?php $this->load->helper('common_tab_helper'); ?>
            <?= clientUserMenu('userproject', $userid) ?>
            <div class="tab-content">
                <?php $this->load->helper('comman_data_helper'); ?>
                <?= clientData($clientdata) ?>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-5"><b>User Name:</b></label>
                            <div class="col-md-7">
                                <label class="control-label"> David Smith </label>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-5"><b>Email:</b></label>
                            <div class="col-md-7">
                                <label class="control-label"> Devid@fund1.com  </label>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-5"><b>Phone Number:</b></label>
                            <div class="col-md-7">
                                <label class="control-label"> 0208 555 5555 </label>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="portlet light portlet-fit portlet-datatable bordered">

                    <div class="portlet-title">
                        <div class="col-md-3" style="padding-left: 0">
                            <div class="caption">
                                <i class="icon-equalizer font-blue-hoki"></i>
                                <span class="caption-subject font-green-haze sbold uppercase"> User Project</span>
                            </div>
                        </div>
<!--                        <div class="actions">
                            <a class="btn btn-circle btn-default btn-sm" href="#add_notes_model" data-toggle="modal">
                                <i class="fa fa-plus"></i> Add Notes 
                            </a>
                        </div>-->
                    </div>


                    <div class="portlet-body">

                        <table class="table table-striped table-bordered table-hover" id="user_project_list">
                            <thead>
                                <tr role="row" class="heading">
                                    <th> Project Name </th>
                                    <th> Project Description </th>
                                    <th> Target </th>
                                    <th> Number of calls requested </th>
                                    <th> Status </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div> 

                </div>
            </div>
        </div>
    </div>
</div>
