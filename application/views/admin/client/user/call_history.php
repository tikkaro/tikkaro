<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="tabbable-line boxless tabbable-reversed">
            <?php $this->load->helper('common_tab_helper'); ?>
            <?= clientUserMenu('callhistory', $userid) ?>
            <div class="tab-content">
                     <div class="portlet light portlet-fit portlet-datatable bordered">

                    <div class="portlet-title">
                        <div class="col-md-3" style="padding-left: 0">
                            <div class="caption">
                                <i class="icon-equalizer font-blue-hoki"></i>
                                <span class="caption-subject font-green-haze sbold uppercase"> Serach Call Histrory </span>
                            </div>
                        </div>

                    </div>
                    <div class="portlet-body">
                        <div class="col-md-12">
                            <div class="col-md-2">
                                <div class="form-group">

                                    <select class="form-control select2me" name="client" data-placeholder="Select Month">
                                        <option value=""></option>
                                        <option value="1">January</option>
                                        <option value="2">Febuary</option>
                                        <option value="3">March</option>
                                        <option value="4">April</option>
                                        <option value="5">May</option>
                                        <option value="6">June</option>
                                        <option value="7">July</option>
                                        <option value="8">August</option>
                                        <option value="9">September</option>
                                        <option value="10">Octomber</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <!--<label class="control-label">Industry</label>-->

                                    <select class="form-control select2me project" name="project" data-placeholder="Select Year">
                                        <option value=""></option>
                                        <?php for ($i = 1960; $i <= 2017; $i++) { ?>
                                            <option value="<?= $i; ?>"> <?= $i; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="control-label col-md-1 text-center">
                                <strong>To </strong>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">

                                    <select class="form-control select2me" name="client" data-placeholder="Select Month">
                                        <option value=""></option>
                                        <option value="1">January</option>
                                        <option value="2">Febuary</option>
                                        <option value="3">March</option>
                                        <option value="4">April</option>
                                        <option value="5">May</option>
                                        <option value="6">June</option>
                                        <option value="7">July</option>
                                        <option value="8">August</option>
                                        <option value="9">September</option>
                                        <option value="10">Octomber</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <!--<label class="control-label">Industry</label>-->

                                    <select class="form-control select2me project" name="project" data-placeholder="Select Year">
                                        <option value=""></option>
                                        <?php for ($i = 1960; $i <= 2017; $i++) { ?>
                                            <option value="<?= $i; ?>"> <?= $i; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="col-md-2">
                                <div class="form-group">
                                    <a href="javascript:;" class="btn btn-circle btn-primary experts_search">
                                        <i class="fa fa-search"></i> Search </a>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <hr>
                        <table class="table table-striped table-bordered table-hover" id="">

                            <tr role="row" class="heading">
                                <td> <b>Month</b> </td>
                                <td>Feb 17</td>
                                <td>Mar 17</td>
                                <td>Apr 17</td>
                                <td>May 17</td>
                                <td>Jun 17</td>
                                <td>July 17</td>
                                <td>Aug 17</td>
                                <td>Sep 17</td>
                                <td>Oct 17</td>
                            </tr>
                            <tr role="row" class="heading">
                                <td><b> Unit Number</b> </td>
                                <td>3</td>
                                <td>5</td>
                                <td>2</td>
                                <td>7</td>
                                <td>4</td>
                                <td>10</td>
                                <td>15</td>
                                <td>12</td>
                                <td>9</td>
                            </tr>



                        </table>
                    </div> 
                </div>
                
                <div class="portlet light portlet-fit portlet-datatable bordered">

                    <div class="portlet-title">
                        <div class="col-md-3" style="padding-left: 0">
                            <div class="caption">
                                <i class="icon-equalizer font-blue-hoki"></i>
                                <span class="caption-subject font-green-haze sbold uppercase"> User Call Histrory </span>
                            </div>
                        </div>

                    </div>
                    <div class="portlet-body">
                        <div class="col-md-12">
                            <div class="col-md-5">
                                <div class="form-group">

                                    <select class="form-control select2me" name="client" data-placeholder="Select Month">
                                        <option value=""></option>
                                        <option value="1">January</option>
                                        <option value="2">Febuary</option>
                                        <option value="3">March</option>
                                        <option value="4">April</option>
                                        <option value="5">May</option>
                                        <option value="6">June</option>
                                        <option value="7">July</option>
                                        <option value="8">August</option>
                                        <option value="9">September</option>
                                        <option value="10">Octomber</option>
                                        <option value="11">November</option>
                                        <option value="12">December</option>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <!--<label class="control-label">Industry</label>-->

                                    <select class="form-control select2me project" name="project" data-placeholder="Select Year">
                                        <option value=""></option>
                                        <?php for ($i = 1960; $i <= 2017; $i++) { ?>
                                            <option value="<?= $i; ?>"> <?= $i; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <a href="javascript:;" class="btn btn-circle btn-primary experts_search">
                                        <i class="fa fa-search"></i> Search </a>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                        <hr>
                        <table class="table table-striped table-bordered table-hover" id="user_call_list">
                            <thead>
                                <tr role="row" class="heading">
                                    <th> Date </th>
                                    <th> Time</th>
                                    <th> Expert</th>
                                    <th> Project</th>
                                    <th> Length</th>
                                    <th> Status</th>
                                    <th> Billing Details</th>
                                    <th> Action</th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div> 
                </div>
            </div>
        </div>
    </div>
</div>
