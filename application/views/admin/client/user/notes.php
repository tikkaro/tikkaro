<?php
$noteData = $noteData[0];

$headerData = array(
    'var_company_name' => $noteData['var_company_name'],
    'account_manager' => $noteData['account_manager'],
    'team_name' => $noteData['var_team_name'],
);
?>
<script type="text/javascript">
    var userID = '<?php echo $userId; ?>'
</script>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="tabbable-line boxless tabbable-reversed">
            <?php $this->load->helper('common_tab_helper'); ?>
            <?= clientUserMenu('usernotes', $userId) ?>
            <div class="tab-content">
                <?php $this->load->helper('comman_data_helper');?>
                <?= clientData($headerData) ?>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-5"><b>User Name:</b></label>
                            <div class="col-md-7">
                                <label class="control-label"><?= $noteData['var_fname'] . ' ' . $noteData['var_lname'] ?></label>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-5"><b>Email:</b></label>
                            <div class="col-md-7">
                                <label class="control-label"> <?= $noteData['var_email'] ?>  </label>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <div class="row ">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-5"><b>Phone Number:</b></label>
                            <div class="col-md-7">
                                <label class="control-label"> <?= $noteData['bint_phone'] ?> </label>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>

                <div class="portlet light portlet-fit portlet-datatable bordered">

                    <div class="portlet-title">
                        <div class="col-md-3" style="padding-left: 0">
                            <div class="caption">
                                <i class="icon-equalizer font-blue-hoki"></i>
                                <span class="caption-subject font-green-haze sbold uppercase"> User Notes</span>
                            </div>
                        </div>
                        <div class="actions">
                            <a class="btn btn-circle btn-default btn-sm btn-edit-note" data-action="<?= admin_url('client/addUserNote'); ?>" data-user-id="<?= $noteData['id'] ?>" data-client-id="<?= $noteData['fk_client'] ?>"  href="javascript:;" >
                                <i class="fa fa-plus"></i> Add Notes 
                            </a>
                        </div>
                    </div>


                    <div class="portlet-body">

                        <table class="table table-striped table-bordered table-hover" id="user_notes_list">
                            <thead>
                                <tr role="row" class="heading">
                                    <th> Date </th>
                                    <th> Note Left by </th>
                                    <th> Info </th>
                                    <th> Action </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div> <!-- ADD USER NOTES MODEL -->
                    <div class="modal fade" id="add_notes_model" tabindex="-1" role="basic" aria-hidden="true"
                         style="z-index: 999999999 !important;">
                        <div class="modal-dialog">
                            <form class="form-horizontal" id="add_note_frm" name="add_note_frm" method="POST" action="<?php echo admin_url('client/addUserNote'); ?>">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                                        <h4 class="modal-title">Add Notes</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-body">

                                                <div class="form-group">
                                                    <label class="control-label col-md-2">Notes : <span class="required">  </span></label>
                                                    <div class="col-md-10">
                                                        <input type="hidden" name="userId" class="user_id" value=""/>
                                                        <input type="hidden" name="clientId" class="client_id" value=""/>
                                                        <input type="hidden" name="noteId" class="note_id" value=""/>
                                                        <textarea class="form-control user_note" rows="6" name="notes"
                                                                  placeholder="Enter Notes"></textarea>
                                                    </div>
                                                </div>

                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn green btn-circle submitcls submit-btn"> Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- /.modal-content -->

                </div>
            </div>
        </div>
    </div>
</div>
