<?php
$userData = $userData[0];
$clientData = array(
    'var_fname' => $userData['clientFname'],
    'var_lname' => $userData['clientLname'],
    'account_manager' => $userData['account_manager'],
    'team_name' => $userData['var_team_name'],
    'var_email' => $userData['var_email'],
    'var_company_name' => $userData['var_company_name'],
    'id' => $userData['id'],
);
?>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="tabbable-line boxless tabbable-reversed">
            <?php $this->load->helper('common_tab_helper'); ?>
            <?= clientUserMenu('userbio', $userid) ?>
            <div class="tab-content">
                <?php $this->load->helper('comman_data_helper'); ?>
                <?= clientData($clientData) ?>
                <div class="portlet light portlet-fit portlet-datatable bordered">

                    <div class="portlet-title">
                        <div class="col-md-3" style="padding-left: 0">
                            <div class="caption">
                                <i class="icon-equalizer font-blue-hoki"></i>
                                <span class="caption-subject font-green-haze sbold uppercase"> User Bio</span>
                            </div>
                        </div>
                        <?php if(!empty($userData['var_email'])) {?>
                        <div class="actions">
                            <a class="btn btn-circle btn-primary btn-sm resetpassword" data-email="<?= $userData['var_email'];?>" data-id="<?= $userid; ?>" href="javascript:;">
                                <i class="fa fa-edit"></i> Reset Password
                            </a>
                        </div>
                        <?php } ?>
                    </div>

                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-2">User Name:</label>
                                    <div class="col-md-8">
                                        <label class="control-label userBio">
                                            <a href="javascript:;" class="inline-editing-trigger"
                                               data-title="Enter Name"
                                               data-table="user"
                                               data-required="true"
                                               data-field1="var_fname"
                                               data-field2="var_lname"
                                               data-input-class="form-control cuswidth"
                                               data-url="<?= admin_url('commaneditable/editableFnameLname') ?>"
                                               data-id="<?= $userData['id']; ?>"
                                               ><?= $userData['var_fname'] . ' ' . $userData['var_lname']; ?>
                                            </a>
                                        </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Email:</label>
                                    <div class="col-md-8">
                                        <label class="control-label userBio">
                                            <a href="javascript:;" class="inline-editing-trigger"
                                               data-title="Enter Email"
                                               data-table="user"
                                               data-required="true"
                                               data-validate-email="true"
                                               data-field="var_email"
                                               data-input-class="form-control cuswidth"
                                               data-id="<?= $userData['id']; ?>"
                                               ><?= $userData['var_email']; ?>
                                            </a>
                                        </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row ">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Phone Number:</label>
                                    <div class="col-md-8">
                                        <label class="control-label userBio">
                                            <a href="javascript:;" class="inline-editing-trigger"
                                               data-title="Enter Phone"
                                               data-table="user"
                                               data-required="true"
                                               data-validate-number="true"
                                               data-field="bint_phone"
                                               data-input-class="form-control cuswidth"
                                               data-id="<?= $userData['id']; ?>"
                                               ><?= $userData['bint_phone']; ?>
                                            </a>
                                        </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row ">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Position:</label>
                                    <div class="col-md-8">
                                        <label class="control-label userBio">
                                            <a href="javascript:;" class="inline-editing-trigger"
                                               data-title="Enter Position"
                                               data-table="user"
                                               data-required="true"
                                               data-field="var_position"
                                               data-input-class="form-control cuswidth"
                                               data-id="<?= $userData['id']; ?>"
                                               ><?= $userData['var_position']; ?>
                                            </a> </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row ">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Address:</label>
                                    <div class="col-md-8">
                                        <label class="control-label userBio">
                                            <a href="javascript:;" class="inline-editing-trigger"
                                               data-title="Enter Position"
                                               data-table="user"
                                               data-required="true"
                                               data-field="txt_address"
                                               data-input-class="form-control cuswidth"
                                               data-type="textarea"
                                               data-id="<?= $userData['id']; ?>"><?= $userData['txt_address']; ?></a></label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row ">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Birthdate:</label>
                                    <div class="col-md-8">
                                        <label class="control-label userBio">
                                            <a href="javascript:;" class="inline-editing-trigger"
                                               data-title="Enter Position"
                                               data-table="user"
                                               data-required="true"
                                               data-field="dt_dob"
                                               data-input-class = "cusDate"
                                               data-id="<?= $userData['id']; ?>"
                                               data-type="date">
                                                <?=($userData['dt_dob'] != null)?date('d/m/Y', strtotime($userData['dt_dob'])):''; ?>
                                            </a></label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row ">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Project List:</label>
                                    <div class="col-md-8">
                                        <label class="control-label"> Project List  </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row ">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Project Status:</label>
                                    <div class="col-md-8">
                                        <label class="control-label"> scheduling  </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row ">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Calls Taken on the project:</label>
                                    <div class="col-md-8">
                                        <label class="control-label"> 9 </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row ">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Calls Cancelled on the project:</label>
                                    <div class="col-md-8">
                                        <label class="control-label"> 5  </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row ">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Calls in scheduling:</label>
                                    <div class="col-md-8">
                                        <label class="control-label"> 3  </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div> 

                </div>
            </div>
        </div>
    </div>
</div>
