<script type="text/javascript">
    var clientId = "<?php echo $clientId; ?>";
</script>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="tabbable-line boxless tabbable-reversed">
            <?php $this->load->helper('common_tab_helper'); ?>
            <?= clientMenu('notes', $clientId) ?>
            <div class="tab-content">
                <?php $this->load->helper('comman_data_helper'); ?>
                <?= clientData($clientData) ?>


                <div class="portlet light portlet-fit portlet-datatable bordered">

                    <div class="portlet-title">
                        <div class="col-md-3" style="padding-left: 0">
                            <div class="caption">
                                <i class="icon-equalizer font-blue-hoki"></i>
                                <span class="caption-subject font-green-haze sbold uppercase"> Notes</span>
                            </div>
                        </div>

                        <div class="actions">
                            <a class="btn btn-circle btn-default btn-sm edit_client_notes"
                               data-client-id="<?= $clientData['id'] ?>"
                               data-assign-ac-manager="<?= $clientData['fk_assign_account_manager'] ?>"
                               href="javascript:;">
                                <i class="fa fa-plus"></i> Add Notes
                            </a>
                        </div>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover" id="notes_list">
                            <thead>
                            <tr role="row" class="heading">
                                <th> Date</th>
                                <th> Author</th>
                                <th> Info</th>
                                <th> Action</th>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="add_client_notes" tabindex="-1" role="basic" aria-hidden="true"
     style="z-index: 999999999 !important;">
    <div class="modal-dialog">
        <form class="form-horizontal" id="add_client_notes_frm" name="add_client_notes_frm" method="POST"
              enctype="multipart/form-data" action="<?= admin_url('client/addClientNote'); ?>">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Add Notes</h4>
                </div>
                <div class="modal-body">
                    <div class="form-body">

                            <div class="form-group">

                                <label class="control-label col-md-2">Notes : <span class="required">*</span></label>
                                <div class="col-md-10">
                                    <input type="hidden" id="note_id" class="form-control" name="note_id"/>
                                    <input type="hidden" id="fk_client" class="form-control" name="fk_client"/>
                                    <input type="hidden" id="fk_assign_ac_manager" class="form-control"
                                           name="fk_assign_ac_manager"/>
                                    <input type="hidden" id="fk_ac_manager" class="form-control" name="fk_ac_manager"/>
                                    <textarea class="form-control client_notes" rows="6" name="notes"
                                              placeholder="Enter Notes"></textarea>
                                </div>
                            </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn green btn-circle submitcls"> Submit</button>
                </div>
            </div>
        </form>
    </div>
    <!-- /.modal-content -->
</div>
<!-- End ADD NOTES MODEL -->