<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="tabbable-line boxless tabbable-reversed">
            <?php $this->load->helper('common_tab_helper', $clientId); ?>
            <?= clientMenu('rules', $clientid) ?>
            <div class="tab-content">
                <?php $this->load->helper('comman_data_helper'); ?>
                <?= clientData($clientData) ?>

                <div class="portlet light portlet-fit portlet-datatable bordered">

                    <div class="portlet-title">
                        <div class="col-md-3" style="padding-left: 0">
                            <div class="caption">
                                <i class="icon-equalizer font-blue-hoki"></i>
                                <span class="caption-subject font-green-haze sbold uppercase"> Client Rules </span>
                            </div>
                        </div>

                    </div>
                    <div class="portlet-body">

                        <div class="col-md-12 companyRules">
                            <?php if(!empty($clientRules)){
                                for($i=0; $i< count($clientRules); $i++){
                                ?>
                                <div class="row">
                                    <div class="form-group">
                                        <label class="control-label col-md-3">
                                            <?= $clientRules[$i]['var_name']; ?>
                                        </label>
                                        <a href="#delete_model" data-toggle="modal" class="common_delete" data-href="<?php echo admin_url('client/deleteRules')?>" data-id="<?=$clientRules[$i]['rule_id'];?>"><i class="fa fa-close"></i></a>
                                            <div class="clearfix"></div>
                                    </div>
                                </div>
                                <?php } } ?>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <select class="form-control select2me company_rules" id="company_rules" data-placeholder="Select Rules">
                                            <option value=""></option>
                                            <?php for($i=0; $i<count($masterRules); $i++) { ?>
                                            <option value="<?= $masterRules[$i]['id']?>"><?= $masterRules[$i]['var_name']?></option>
                                            <?php } ?>
                                            <!--<option value="Public Company "> Public Company </option>-->
                                            
                                        </select>
                                    </div>
                                    <label class="control-label col-md-1"> OR </label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control company_Cusrules" placeholder="Add New Rules">
                                    </div>
                                    <div class="col-md-3">
                                        <a class="btn btn-circle btn-primary  btn-sm addNewRules" data-client-id="<?= $clientData['id']?>" data-assign-account-id="<?= $clientData['fk_assign_account_manager']?>" href="javascript:;">
                                            <i class="fa fa-edit"></i> Add New Rule 
                                        </a>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

