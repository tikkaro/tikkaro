<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-user"></i>Add Client </div>
    </div>
    <div class="portlet-body form">
        <form role="form" method="post" enctype="multipart/form-data" id="addClientFrm" action="<?php echo admin_url('client/add'); ?>" class="form-horizontal">
            <div class="form-body">
                <div class="form-group">
                    <label class="col-md-3 control-label">Company Name<span class="required" aria-required="true">*</span> :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="text" id="company_name" name="company_name" placeholder="Enter Company Name" class="form-control"> 
                        </div>
                    </div>
                </div>

               <div class="form-group">
                    <label class="col-md-3 control-label">Team<span class="required" aria-required="true">*</span> :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <select class="form-control select2me" id="team" name="team" data-placeholder="Select Team">
                                <option value=""></option>
                                <?php
                                for ($i = 0; $i < count($teamData); $i++) { ?>
                                        <option value="<?= $teamData[$i]['id'] ?>"><?= $teamData[$i]['var_team_name'] ?></option>
                                    <?php  } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Account Manager<span class="required" aria-required="true">*</span> :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <select class="form-control select2me" id="ac_manager" name="ac_manager" data-placeholder="Select Account Manager">
                                <option value=""></option>
                                <?php
                                for ($i = 0; $i < count($accountManager); $i++) {
                                    ?>
                                    <option value="<?= $accountManager[$i]['id'] ?>"><?= $accountManager[$i]['var_name'] ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Priority<span class="required" aria-required="true">*</span> :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <select class="form-control select2me" id="set_priority" name="set_priority" data-placeholder="Select Priority">
                                <option value=""></option>
                                <option value="LOW">Low</option>
                                <option value="MEDIUM">Medium</option>
                                <option value="HIGH">High</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Phone :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="text" id="phone" name="phone" placeholder="Enter Contact No" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Address :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <textarea id="address" name="address" placeholder="Enter Address" class="form-control"> </textarea>
                        </div>
                    </div>
                </div>

            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-4">
                        <button type="submit" class="btn green btn-circle">Submit</button>
                        <a class="btn default btn-circle" href="<?php echo admin_url() . 'client'; ?>">Cancel</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>