<style>
    .marginTop0 {
        margin-top: 0;
    }

    .label-black {
        background: #000;
    }
</style>


<?php
$subScriptionType = json_decode(SUBSCRIPTION_STATUS);

$teamArray = array();
foreach ($teamData as $value) {
    array_push($teamArray, array('value' => $value['id'], 'text' => $value['var_team_name']));
}

$accountArray = array();
foreach ($accountManager as $value) {
    array_push($accountArray, array('value' => $value['id'], 'text' => $value['var_name']));
}
?>
<script type="text/javascript">
    var clientId = "<?php echo $clientId; ?>";
    var team = '<?= json_encode($teamArray);?>';
    var account_manager = '<?= json_encode($accountArray);?>';
</script>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="tabbable-line boxless tabbable-reversed">
            <?php $this->load->helper('common_tab_helper'); ?>
            <?= clientMenu('overview', $clientId) ?>

            <div class="tab-content">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="control-label col-md-4"><b>Client Name:</b></label>
                            <div class="col-md-8">
                                <label class="control-label">
                                    <a href="javascript:;" class="inline-editing-trigger"
                                       data-title="Enter First Name"
                                       data-placeholder="Enter Client Name"
                                       data-table="client"
                                       data-required="true"
                                       data-field="var_company_name"
                                       data-url="<?= admin_url('commaneditable/editable') ?>"
                                       data-id="<?= $clientData['id']; ?>"
                                    ><?= $clientData['var_company_name']; ?>
                                    </a>
                                </label>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4"><b>Key Account Manager:</b></label>
                            <div class="col-md-8">
                                <label class="control-label">
                                    <a href="javascript:;" class="inline-editing-trigger"
                                       data-title="Enter Account Manager"
                                       data-table="client"
                                       data-required="true"
                                       data-field="fk_assign_account_manager"
                                       data-type="select"
                                       data-value="<?= $clientData['fk_assign_account_manager']; ?>"
                                       data-value-source="account_manager"
                                       data-id="<?= $clientData['id']; ?>"><?= $clientData['account_manager']; ?>
                                    </a>
                                </label>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4"><b>Team:</b></label>
                            <div class="col-md-8">
                                <label class="control-label">
                                    <a href="javascript:;" class="inline-editing-trigger"
                                       data-title="Enter Account Manager"
                                       data-table="client"
                                       data-required="true"
                                       data-field1="fk_assign_team"
                                       data-field2="fk_team"
                                       data-type="select"
                                       data-value-source="team"
                                       data-value="<?= $clientData['fk_assign_team']; ?>"
                                       data-url="<?= admin_url('commaneditable/editableSelectbox') ?>"
                                       data-id="<?= $clientData['id']; ?>"
                                    ><?= $clientData['team_name']; ?>
                                    </a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <?php if($subscriptionData['enum_subscription_type'] !='PAY-AS-YOU-GO') {?>
                        <div class="form-group">
                            <label class="control-label col-md-4"><b>Calls Remaining:</b></label>
                            <div class="col-md-8">
                                <label class="control-label"> 40 </label>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                        <?php } ?>
                        <div class="form-group">
                            <label class="control-label col-md-4"><b>Quarterly Run Rate:</b></label>
                            <div class="col-md-8">
                                <label class="control-label"> $10k (+10% QoQ) </label>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>


                    <div class="col-md-6">
                        <?php
                        //                        echo "<pre>";
                        //                        print_r($subscriptionData);
                        //                        exit;
                        if (!empty($subscriptionData)) {
                            ?>
                            <div class="form-group">
                                <label class="control-label col-md-5"><b>Subscription type:</b></label>
                                <div class="col-md-7">
                                    <label class="control-label label <?= ($subscriptionData['enum_subscription_type'] == 'UNIT-BLOCK') ? 'label-danger' : 'label-black' ?>  label-sm "> <?=  $subScriptionType->{$subscriptionData['enum_subscription_type']}  ?> </label>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-5"><b>Subscriptions Deadline:</b></label>
                                <div class="col-md-7">
                                    <label class="control-label"> <?= date('d/m/Y', strtotime($subscriptionData['dt_end_date'])) ?></label>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group <?= ($subscriptionData['enum_subscription_type'] == 'PAY-AS-YOU-GO') ? 'hide' : ''; ?>">
                                <label class="control-label col-md-5"><b>Unit Number:</b></label>
                                <div class="col-md-7">
                                    <label class="control-label"> <?= $subscriptionData['int_no_of_unit'] ?> </label>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5"><b>Current Subscription:</b></label>
                                <div class="col-md-7">
                                    <label class="control-label"> <?= ($subscriptionData['int_calls_completed']) ? $subscriptionData['int_calls_completed'] : '0' ?>
                                        calls </label>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5"><b>Price per unit:</b></label>
                                <div class="col-md-7">
                                    <label class="control-label"> <?= '$' . $subscriptionData['int_price_per_unit'] ?> </label>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5"><b>Total Subscription Price:</b></label>
                                <div class="col-md-7">
                                    <label class="control-label"> <?= '$' . $subscriptionData['int_total_price'] ?> </label>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-5"><b>Upsell Put in place by:</b></label>
                                <div class="col-md-7">
                                    <label class="control-label"><?= ($subscriptionData['account_manager']) ? $subscriptionData['account_manager'] : $subscriptionData['admin'] ?></label>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <?php

                            } else {
                            ?>
                            <div class="form-group">
                                <label class="control-label col-md-2"></label>
                                <div class="col-md-4">
                                    <a class="btn btn-circle btn-primary btn-sm new-subscription" data-toggle="modal"
                                       data-client-id="1"
                                       data-assign-account-id="1" href="#add_subscription_model">
                                        <i class="fa fa-edit"></i> Add Subscription
                                    </a>
                                </div>
                                <div class="clearfix"></div>
                            </div>

                            <?php
                        }

                        ?>


                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="portlet light portlet-fit portlet-datatable bordered">
                            <div class="portlet-title">
                                <div class="col-md-3" style="padding-left: 0">
                                    <div class="caption">
                                        <i class="icon-equalizer font-blue-hoki"></i>
                                        <span class="caption-subject font-green-haze sbold uppercase"> User</span>
                                    </div>
                                </div>

                                <div class="actions">
                                    <a class="btn btn-circle btn-default btn-sm"
                                       href="<?= admin_url() ?>client/user-add/<?= $clientId ?>">
                                        <i class="fa fa-plus"></i> Add User
                                    </a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover" id="overview_lists">
                                        <thead>
                                        <tr role="row" class="heading">
                                            <th> User</th>
                                            <th>Position</th>
                                            <th>Number</th>
                                            <th>Email</th>
                                            <th>Last Contact(days)</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="add_subscription_model" tabindex="-1" role="basic" aria-hidden="true"
     style="z-index: 999999999 !important;">
    <div class="modal-dialog">
        <form class="form-horizontal" id="add_subscription_frm" name="add_subscription_frm" method="POST"
              enctype="multipart/form-data" action="<?= admin_url('client/addSubscription') ?>">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Add Subscription</h4>
                </div>
                <div class="modal-body">
                    <div class="form-body">

                        <div class="eduction">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Subscription Type <span class="required"
                                                                                              aria-required="true">*</span>:</label>
                                <div class="col-md-7">
                                    <div class="input-icon right">
                                        <input type="hidden" id="client_id" name="client_id" value="<?= $clientId ?>"/>
                                        <select class="form-control select2me subscription_type"
                                                name="subscription_type" id="subscription_type"
                                                data-placeholder="Select Subscription">
                                            <option value=""></option>
                                            <?php

                                            foreach ($subScriptionType as $key => $value){
                                                ?>
                                                <option value="<?= $key ?>" selected><?= $value ?></option>
                                            <?php
                                            }
                                            ?>

                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Start Date<span class="required"
                                                                                      aria-required="true">*</span>
                                    :</label>
                                <div class="col-md-7">
                                    <div class="input-icon right">
                                        <div class="input-icon right">
                                            <input id="dt_start_date" name="dt_start_date"
                                                   placeholder="Select Start Date"
                                                   class="form-control form-control-inline  date-picker" size="20"
                                                   type="text" value="<?= date('d/m/Y') ?>" data-date-format="dd/mm/yyyy">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="unit-block-div">

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Subscription Time <span class="required"
                                                                                                  aria-required="true">*</span>:</label>
                                    <div class="col-md-7">
                                        <div class="input-icon right">
                                            <select class="form-control select2me" name="subcription_time"
                                                    id="subcription_time"
                                                    data-placeholder="Select Time">
                                                <option value=""></option>
                                                <?php
                                                for ($i = 1; $i <= 12; $i++) {
                                                    ?>
                                                    <option value="<?= $i ?>"><?= $i . ' Month' ?></option>
                                                    <?php
                                                }
                                                ?>
                                                ?>

                                            </select>
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-md-4 control-label">Number of Unit <span class="required"
                                                                                               aria-required="true">*</span>:</label>
                                    <div class="col-md-7">
                                        <div class="input-icon right">
                                            <input type="text" id="no_of_unit" name="no_of_unit"
                                                   placeholder="Enter Number of Unit" class="form-control">

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="pay-as-you-div hide">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">End Date<span class="required"
                                                                                        aria-required="true">*</span>
                                        :</label>
                                    <div class="col-md-7">
                                        <div class="input-icon right">
                                            <div class="input-icon right">
                                                <input id="dt_end_date" name="dt_end_date"
                                                       placeholder="Select End Date"
                                                       class="form-control form-control-inline  date-picker" size="20"
                                                       type="text" value="" data-date-format="dd/mm/yyyy">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-4 control-label">Price Per Unit<span class="required"
                                                                                          aria-required="true">*</span>:</label>
                                <div class="col-md-7">
                                    <div class="input-icon right">

                                        <input type="text" id="price_per_unit" name="price_per_unit"
                                               placeholder="Enter Price Per Unit" class="form-control">

                                    </div>
                                </div>
                            </div>


                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn green btn-circle submitcls"> Submit</button>
                </div>
            </div>
        </form>
    </div>
    <!-- /.modal-content -->
</div>