<style>
    .modal-open .colorpicker, .modal-open .datepicker, .modal-open .daterangepicker {
        z-index: 1005599999 !important;
    }
</style>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="tabbable-line boxless tabbable-reversed">
            <?php $this->load->helper('common_tab_helper'); ?>
            <?= clientMenu('history', $clientId) ?>
            <div class="tab-content">
                <?php $this->load->helper('comman_data_helper'); ?>
                <?= clientData($clientData) ?>

                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-title">
                        <div class="col-md-3" style="padding-left: 0">
                            <div class="caption">
                                <i class="icon-equalizer font-blue-hoki"></i>
                                <span class="caption-subject font-green-haze sbold uppercase"> Subscription History</span>
                            </div>
                        </div>

                        <div class="actions">
                            <a class="btn btn-circle btn-default btn-sm" href="#subscibe" id="" data-toggle="modal">
                                <i class="fa fa-plus"></i> Add Subscription 
                            </a>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <h3>Current Subscription</h3></div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-5"><b>Subscription type:</b></b></label>
                                    <div class="col-md-7">
                                        <label class="control-label label label-danger label-sm "> Unit Block </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-5"><b>Start Date:</b></label>
                                    <div class="col-md-7">
                                        <label class="control-label"> 28 March 2017  </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-5"><b>End Date:</b></label>
                                    <div class="col-md-7">
                                        <label class="control-label"> 28 March 2018  </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-5"><b>Subscription Time:</b></label>
                                    <div class="col-md-7">
                                        <label class="control-label"> 12 Month  </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-5"><b>Unit Number:</b></label>
                                    <div class="col-md-7">
                                        <label class="control-label"> 50  </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-5"><b>Calls Completed:</b></label>
                                    <div class="col-md-7">
                                        <label class="control-label"> 1  </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-5"><b>Calls Remaining :</b></label>
                                    <div class="col-md-7">
                                        <label class="control-label"> 49 </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-md-6">

                                <div class="form-group">
                                    <label class="control-label col-md-5"><b>Price per unit:</b></label>
                                    <div class="col-md-7">
                                        <label class="control-label"> $1100  </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-5"><b>Total Subscription Price:</b></label>
                                    <div class="col-md-7">
                                        <label class="control-label"> $55,000  </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-5"><b>Average sub speed per month:</b></label>
                                    <div class="col-md-7">
                                        <label class="control-label"> 4 units/month  </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-5"><b>Average sub speed per week:</b></label>
                                    <div class="col-md-7">
                                        <label class="control-label">  1 unit/week  </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-5"><b>Upsell Put in place by:</b></label>
                                    <div class="col-md-7">
                                        <label class="control-label"> Margarita Polishchuk </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                            </div>
                        </div>

                    </div> 


                    <hr>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-5"><b>Subscription type:</b></label>
                                    <div class="col-md-7">
                                        <label class="control-label label label-danger label-sm "> Unit Block </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-5"><b>Start Date:</b></label>
                                    <div class="col-md-7">
                                        <label class="control-label"> 27 March 2017  </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-5"><b>End Date:</b></label>
                                    <div class="col-md-7">
                                        <label class="control-label"> 26 September 2017  </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-5"><b>Subscription Time:</b></label>
                                    <div class="col-md-7">
                                        <label class="control-label"> 6 Month  </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-5"><b>Unit Number:</b></label>
                                    <div class="col-md-7">
                                        <label class="control-label"> 30  </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-5"><b>Calls Completed:</b></label>
                                    <div class="col-md-7">
                                        <label class="control-label"> 30  </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-5"><b>Calls Remaining :</b></label>
                                    <div class="col-md-7">
                                        <label class="control-label"> 30 </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-md-6">



                                <div class="form-group">
                                    <label class="control-label col-md-5"><b>Price per unit:</b></label>
                                    <div class="col-md-7">
                                        <label class="control-label"> $1200  </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-5"><b>Total Subscription Price:</b></label>
                                    <div class="col-md-7">
                                        <label class="control-label"> $36,000  </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-5"><b>Average sub speed per month:</b></label>
                                    <div class="col-md-7">
                                        <label class="control-label"> 5 units </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-5"><b>Average sub speed per week:</b></label>
                                    <div class="col-md-7">
                                        <label class="control-label">  1.6  </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-5"><b>Upsell Put in place by:</b></label>
                                    <div class="col-md-7">
                                        <label class="control-label"> Jordan Shlosberg</label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>


                        <hr>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-5"><b>Subscription type:</b></label>
                                    <div class="col-md-7">
                                        <label class="control-label label label-success label-sm"> Pay as you GO</label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>


                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-5"><b>Price per unit:</b></label>
                                    <div class="col-md-7">
                                        <label class="control-label"> $1200  </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

                        <div class="row ">

                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-5"><b>Start Date:</b></label>
                                    <div class="col-md-7">
                                        <label class="control-label"> 28 March 2017  </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="control-label col-md-5"><b>End Date:</b></label>
                                    <div class="col-md-7">
                                        <label class="control-label"> 27 March 2018  </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>

                        </div>

                        <div class="clearfix"></div>
                    </div> 
                </div>

            </div>

        </div>

    </div>
    <div class="clearfix"></div>
</div>

<!-- ADD Subscription MODEL -->
<div class="modal fade" id="subscibe" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <form class="form-horizontal" id="add_subscibe" name="add_subscibe_frm" method="POST" enctype="multipart/form-data" action="<?= admin_url() . 'client/overview/'; ?>">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Add Subscription</h4>
                </div>
                <div class="modal-body">
                    <div class="form-body">
                        <div class="row">

                            <div class="form-group">
                                <label class="col-md-4 control-label">Subscription type:</label>
                                <div class="col-md-6">
                                    <select class="form-control select2me" id="sub_type" name="sub_type" data-placeholder="Select Subscription Type">
                                        <option value=""></option>
                                        <option value="1">Unit Block</option>
                                        <option value="2">Pay as you GO</option>
                                    </select>
                                </div>
                            </div>
                            <div id="unit_block" style="display: none;">

                                <div class="form-group">
                                    <label class="control-label col-md-4">Start Date : <span class="required">  </span></label>
                                    <div class="col-md-6">
                                        <input type='text' class="form-control date-picker" name="start_date" placeholder="Enter Start Date"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">End Date : <span class="required">  </span></label>
                                    <div class="col-md-6">
                                        <input type='text' class="form-control date-picker" name="end_date" placeholder="Enter End Date"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Unit Number: <span class="required">  </span></label>
                                    <div class="col-md-6">
                                        <input type='text' class="form-control" name="unit_number" placeholder="Unit number"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Price per Unit: <span class="required">  </span></label>
                                    <div class="col-md-6">
                                        <input type='text' class="form-control" name="price_per_unit" placeholder="Price Per Unit"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Total Subscription Price: <span class="required">  </span></label>
                                    <div class="col-md-6">
                                        <input type='text' class="form-control" name="total_sub_price" placeholder="Total Subsction Price"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">Upsell Put in place by: <span class="required">  </span></label>
                                    <div class="col-md-6">
                                        <input type='text' class="form-control" name="upsell" placeholder="Upsell Put in place by"/>
                                    </div>
                                </div>
                            </div>

                            <div id="pay" style="display: none;">

                                <div class="form-group">
                                    <label class="control-label col-md-4">Start Date : <span class="required">  </span></label>
                                    <div class="col-md-6">
                                        <input type='text' class="form-control date-picker" name="start_date" placeholder="Enter Start Date"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-4">End Date : <span class="required">  </span></label>
                                    <div class="col-md-6">
                                        <input type='text' class="form-control date-picker" name="end_date" placeholder="Enter End Date"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-4">Price per Unit: <span class="required">  </span></label>
                                    <div class="col-md-6">
                                        <input type='text' class="form-control" name="price_per_unit" placeholder="Price Per Unit"/>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-4">Upsell Put in place by: <span class="required">  </span></label>
                                    <div class="col-md-6">
                                        <input type='text' class="form-control" name="upsell" placeholder="Upsell Put in place by"/>
                                    </div>
                                </div>
                            </div>

                            <div class="modal-footer">
                                <button type="submit" class="btn green btn-circle submitcls"> Subscribe </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </form>
    </div>
    <!-- /.modal-content -->
</div>
<!-- End Subscription MODEL -->

