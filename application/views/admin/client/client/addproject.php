<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-user"></i>Add Project </div>
    </div>
    <div class="portlet-body form">
        <form role="form" method="post" enctype="multipart/form-data" id="addClientProjectFrm" action="<?php echo admin_url() . 'client/project_add'; ?>" class="form-horizontal">
            <div class="form-body">
                <div class="form-group">
                    <label class="col-md-3 control-label">Select User :</label>
                    <div class="col-md-4">
                        <div class="input-icon right">
                            <select class="form-control select2me" name="user_id" data-placeholder="Select User">
                                <option value=""></option>
                                <?php for($i=0; $i<count($userdata); $i++){ ?>
                                <option value="<?= $userdata[$i]['id']?>"><?= $userdata[$i]['var_fname'].' '.$userdata[$i]['var_lname']?></option> 
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Project Name :</label>
                    <div class="col-md-4">
                        <div class="input-icon right">
                            <input type="hidden" name="client_id" value="<?= $clientid;?>">
                            <input type="hidden" name="account_manager" value="<?= $clientData['fk_assign_account_manager'];?>">
                            <input type="text" id="var_fname" name="var_project" placeholder="Enter Project Name" class="form-control"> 
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Project Description :</label>
                    <div class="col-md-4">
                        <div class="input-icon right">
                            <textarea class="form-control" id="var_address" name="project_description" placeholder="Enter Project Description" rows="4"></textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Initiate Date :</label>
                    <div class="col-md-4">
                        <div class="input-icon right">
                            <input  id="dt_initiate" name="dt_initiate"  placeholder="Select Initiate Date"class="form-control form-control-inline  date-picker" size="20" type="text" value="" >
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Deadline Date :</label>
                    <div class="col-md-4">
                        <div class="input-icon right">
                            <input  id="dt_deadline" name="dt_deadline"  placeholder="Select Deadline Date"class="form-control form-control-inline  date-picker" size="20" type="text" value="" >
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Select Priority :</label>
                    <div class="col-md-4">
                        <div class="input-icon right">
                            <select class="form-control select2me" name="priority" data-placeholder="Select Priority">
                                <option value="ACTIVE">Active</option>
                                <option value="PRIORITY">Priority</option>
                                <option value="WAITING">Waiting</option>
                                <option value="SCHEDULING">Scheduling</option>
                                <option value="CLOSED">Closed</option>
                                
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Target Company :</label>
                    <div class="col-md-4">
                        <div class="input-icon right">
                            <input type="text" id="var_target_company" name="var_target_company"
                                   placeholder="Enter Target Company" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Client Anonymous?</label>
                    <div class="col-md-4">
                        <div class="md-radio-inline">
                            <div class="md-radio">
                                <input type="radio" id="checkbox1_8" name="client_anonymous" value="YES" class="md-radiobtn" checked>
                                <label for="checkbox1_8">
                                    <span class="inc"></span>
                                    <span class="check"></span>
                                    <span class="box"></span> Yes</label>
                            </div>
                            <div class="md-radio">
                                <input type="radio" id="checkbox1_9" name="client_anonymous" value="NO" class="md-radiobtn">
                                <label for="checkbox1_9">
                                    <span class="inc"></span>
                                    <span class="check"></span>
                                    <span class="box"></span> No </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Target Anonymous?</label>
                    <div class="col-md-4">
                        <div class="md-radio-inline">
                            <div class="md-radio">
                                <input type="radio" id="checkbox1_1" name="target_anonymous" value="YES" class="md-radiobtn" checked>
                                <label for="checkbox1_1">
                                    <span class="inc"></span>
                                    <span class="check"></span>
                                    <span class="box"></span> Yes</label>
                            </div>
                            <div class="md-radio">
                                <input type="radio" id="checkbox1_2" name="target_anonymous" value="NO" class="md-radiobtn">
                                <label for="checkbox1_2">
                                    <span class="inc"></span>
                                    <span class="check"></span>
                                    <span class="box"></span> No </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Country of Interest :</label>
                    <div class="col-md-4">
                        <div class="input-icon right">
                            <input type="text" id="var_country_interest" name="var_country_interest"
                                   placeholder="Enter Rate per Survey" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Companies of Interest:</label>
                    <div class="col-md-4">
                        <div class="input-icon right">
                            <input type="text" id="var_companies_interest" name="var_companies_interest" placeholder="Enter Companies of Interest"
                                   class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Geography of Interest:</label>
                    <div class="col-md-4">
                        <div class="input-icon right">
                            <input type="text" id="var_geography_interest" name="var_geography_interest" placeholder="Enter Geography of Interest" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Industry:</label>
                    <div class="col-md-4">
                        <div class="input-icon right">
                            <input type="text" id="var_industry" name="var_industry" placeholder="Enter Industry" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-4">
                        <button type="submit" class="btn green btn-circle">Submit</button>
                        <a class="btn default btn-circle" href="<?php echo admin_url('client/project/'.$clientid); ?>">Cancel</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>