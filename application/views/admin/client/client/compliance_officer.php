<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="tabbable-line boxless tabbable-reversed">
            <?php $this->load->helper('common_tab_helper'); ?>
            <?= clientMenu('officer', $clientId) ?>
            <div class="tab-content">
                <?php $this->load->helper('comman_data_helper'); ?>
                <?= clientData($clientData) ?>
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-title">
                        <div class="col-md-3" style="padding-left: 0">
                            <div class="caption">
                                <i class="icon-equalizer font-blue-hoki"></i>
                                <span class="caption-subject font-green-haze sbold uppercase"> Compliance Officer </span>
                            </div>
                        </div>

                    </div>
                    <div class="portlet-body">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Name:</label>
                                    <div class="col-md-8">
                                        <label class="control-label CompalianceOfficer">
                                            <a href="javascript:;" class="inline-editing-trigger"
                                               data-id="<?= $complianceData['id']; ?>"
                                               data-title="Enter Name"
                                               data-placeholder="Enter Name"
                                               data-input-class="form-control cuswidth"
                                               data-table="client_has_compliance_officer"
                                               data-required="true"
                                               data-field="var_name"
                                               data-field1="fk_client"
                                               data-client="<?= $clientData['id'];?>"
                                               data-account="<?= $clientData['fk_assign_account_manager'];?>"
                                               data-url = "<?= admin_url('commaneditable/handleCompliance')?>"
                                               ><?= $complianceData['var_name']; ?>
                                            </a>
                                        </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Email :</label>
                                    <div class="col-md-8">
                                        <label class="control-label CompalianceOfficer">
                                            <a href="javascript:;" class="inline-editing-trigger"
                                               data-id="<?= $complianceData['id']; ?>"
                                               data-title="Enter Email"
                                               data-placeholder="Enter Email"
                                               data-input-class="form-control cuswidth"
                                               data-table="client_has_compliance_officer"
                                               data-required="true"
                                               data-validate-email = "true"
                                               data-field="var_email"
                                               data-field1="fk_client"
                                               data-client="<?= $clientData['id'];?>"
                                               data-account="<?= $clientData['fk_assign_account_manager'];?>"
                                               data-url = "<?= admin_url('commaneditable/handleCompliance')?>"
                                               ><?= $complianceData['var_email']; ?>
                                            </a>
                                        </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Phone :</label>
                                    <div class="col-md-8">
                                        <label class="control-label CompalianceOfficer">
                                            <a href="javascript:;" class="inline-editing-trigger"
                                               data-id="<?= $complianceData['id']; ?>"
                                               data-input-class="form-control cuswidth"
                                               data-title="Enter Phone"
                                               data-placeholder="Enter Phone"
                                               data-table="client_has_compliance_officer"
                                               data-required="true"
                                               data-field="bint_phone"
                                               data-field1="fk_client"
                                               data-client="<?= $clientData['id'];?>"
                                               data-account="<?= $clientData['fk_assign_account_manager'];?>"
                                               data-url = "<?= admin_url('commaneditable/handleCompliance')?>"
                                               ><?= $complianceData['bint_phone']; ?>
                                            </a>
                                        </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                    </div> 
                </div>
                <div class="clearfix"></div>

                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-title">
                        <div class="col-md-3" style="padding-left: 0">
                            <div class="caption">
                                <i class="icon-equalizer font-blue-hoki"></i>
                                <span class="caption-subject font-green-haze sbold uppercase">Client Contact Details </span>
                            </div>
                        </div>

                    </div>
                    <div class="portlet-body">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Address :</label>
                                    <div class="col-md-8">
                                        <label class="control-label CompalianceOfficer">
                                            <a href="javascript:;" class="inline-editing-trigger"
                                               data-id="<?= $clientData['id']; ?>"
                                               data-title="Enter Address"
                                               data-placeholder="Enter Address"
                                               data-input-class="form-control cuswidth"
                                               data-table="client"
                                               data-type="textarea"
                                               data-required="true"
                                               data-field="txt_address"
                                            ><?= $clientData['txt_address']; ?>
                                            </a>
                                        </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
</div>


<!-- Start COMPLIANCE OFFICER FORM MODEL end -->
<div class="modal fade" id="edit_compliance_modal" tabindex="-1" role="basic" aria-hidden="true" style="z-index: 999999999 !important;">
    <div class="modal-dialog">
        <form class="form-horizontal" id="edit_compliance" name="edit_compliance_frm" method="POST" enctype="multipart/form-data" action="<?= admin_url() . 'client/updateOfficer'; ?>">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Update Compliance Officer</h4>
                </div>
                <div class="modal-body">
                    <div class="form-body">
                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-md-4">Name : <span class="required">  </span></label>
                                <div class="input-group col-md-6">
                                    <input type="hidden" id="fk_client" class="form-control" name="fk_client"/>
                                    <input type="hidden" id="fk_assign_account" class="form-control" name="fk_assign_account"/>
                                    <input type="hidden" id="officer_id" class="form-control" name="officer_id"/>
                                    <input type="text" id="name" class="form-control" name="name" placeholder="Enter Name"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-md-4">Email : <span class="required">  </span></label>
                                <div class="input-group col-md-6">
                                    <input type="text" class="form-control" id="email" name="email" placeholder="Enter Email"/>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-md-4">Phone : <span class="required">  </span></label>
                                <div class="input-group col-md-6">
                                    <input type="text" class="form-control" id="phone" name="phone" placeholder="Enter Phone"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn green btn-circle submitcls"> Update </button>
                </div>
            </div>
        </form>
    </div>
    <!-- /.modal-content -->
</div>
<!-- End Rules FORM MODEL end -->