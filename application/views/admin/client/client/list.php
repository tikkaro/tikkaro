<style>
    .label-black{
        background: #000;
    }
</style>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="col-md-3" style="padding-left: 0">
                    <div class="caption">
                        <i class="icon-settings font-green-haze"></i>
                        <span class="caption-subject font-green-haze sbold uppercase"> Clients List </span>
                    </div>
                </div>

                <div class="actions">
                    <a class="btn btn-circle btn-default btn-sm" href="<?php echo admin_url() . 'client/add'; ?>">
                        <i class="fa fa-plus"></i> Add Client 
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="client_list">
                        <thead>
                            <tr role="row" class="heading">
                                <th> Client List</th>
                                <th>Live Projects</th>
                                <th>Client Value</th>
                                <th>Last call taken Date</th>
                                <!--<th>How many calls are in scheduling</th>-->
                                <th>Scheduling</th>
                                <th>Scheduled</th>
                                <!--<th>Calls Completed Last 30 Days</th>-->
                                <th>Completed</th>
                                <th>Subscription Pace</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>  
            </div>
        </div>
    </div>
</div>
