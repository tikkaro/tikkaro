<script type="text/javascript">
    var clientId = "<?php echo $clientId; ?>";
</script>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="tabbable-line boxless tabbable-reversed">
            <?php $this->load->helper('common_tab_helper'); ?>
            <?= clientMenu('project', $clientId) ?>

            <div class="tab-content">
                <?php $this->load->helper('comman_data_helper'); ?>
                <?= clientData($clientData) ?>

                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <div class="portlet light portlet-fit portlet-datatable bordered">
                            <div class="portlet-title">
                                <div class="col-md-3" style="padding-left: 0">
                                    <div class="caption">
                                        <i class="icon-equalizer font-blue-hoki"></i>
                                        <span class="caption-subject font-green-haze sbold uppercase"> Project</span>
                                    </div>
                                </div>
                                <div class="actions">
                                    <a class="btn btn-circle btn-default btn-sm"
                                       href="<?= admin_url('project/add?clientId=' . $clientId) ?>">
                                        <i class="fa fa-plus"></i> Add Project
                                    </a>
                                </div>
                            </div>
                            <div class="portlet-body">
                                <div class="portlet-body">
                                    <table class="table table-striped table-bordered table-hover" id="project_list">
                                        <thead>
                                        <tr role="row" class="heading">
                                            <th> Last Call Date</th>
                                            <th>Project Name</th>
                                            <th>User</th>
                                            <th>Calls Pending</th>
                                            <th>Calls Completed</th>
                                            <th>Created Date</th>
                                            <th>Project Status</th>
                                            <th>Calls Requested</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>


        </div>
    </div>
</div>
</div>