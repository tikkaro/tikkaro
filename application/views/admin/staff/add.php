<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-user"></i>Add Staff
        </div>
    </div>
    <div class="portlet-body form">
        <form role="form" method="post" enctype="multipart/form-data" id="addAccountManagerFrm"
              action="<?php echo admin_url() . 'staff/add'; ?>" class="form-horizontal">
            <div class="form-body">

                <div class="form-group">
                    <label class="col-md-3 control-label">Name<span class="required" aria-required="true">*</span>
                        :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="text" id="var_username" name="var_usrname" placeholder="Enter Name"
                                   class="form-control">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Email<span class="required" aria-required="true">*</span>
                        :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="text" id="var_email" name="var_email" placeholder="Enter Email"
                                   class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Password<span class="required" aria-required="true">*</span> :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="password" id="var_password" name="var_password" placeholder="Enter Password"
                                   class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Team
                        :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <select class="form-control select2me" id="team" name="team" data-placeholder="Select Team">
                                <option value=""></option>
                                <?php
                                for ($i = 0; $i < count($teamData); $i++) {
                                    ?>

                                    <option value="<?= $teamData[$i]['id'] ?>"><?= $teamData[$i]['var_team_name'] ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Phone Number :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">
                            <input type="text" id="var_phone" name="var_phone" placeholder="Enter Phone Number"
                                   class="form-control">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Address :</label>
                    <div class="col-md-8">
                        <div class="input-icon right">

                            <textarea class="form-control" id="var_address" name="var_address"
                                      placeholder="Enter Address" rows="3"></textarea>
                        </div>
                    </div>
                </div>

                <!--                <div class="form-group">
                                    <label class="col-md-3 control-label">Manage DOB<span class="required" aria-required="true">*</span> :</label>
                                    <div class="col-md-4">
                                        <div class="input-icon right">
                                            <input  id="var_Manage_Dob" name="var_manage_dob"  placeholder="Enter Date Of Birth" for class="form-control form-control-inline  date-picker" size="20" type="text" value="" >
                                        </div>
                                    </div>
                                </div>-->

                <div class="form-group">
                    <label class="control-label col-md-3">Profile Pic:</label>
                    <div class="col-md-9">
                        <div class="fileinput fileinput-new" data-provides="fileinput">
                            <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;">
                                <img src="http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image" alt=""/>
                            </div>
                            <div class="fileinput-preview fileinput-exists thumbnail"
                                 style="max-width: 200px; max-height: 150px;"></div>
                            <div>
                                <span class="btn default btn-file">
                                    <span class="fileinput-new"> Select image </span>
                                    <span class="fileinput-exists"> Change </span>
                                    <input type="file" name="profile_pic"> </span>
                                <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput">
                                    Remove </a>
                            </div>
                        </div>
                    </div>
                </div>
                <!--                <div class="form-group">
                                    <label class="col-md-3 control-label">Select Team</label>
                                    <div class="col-md-4">
                                        <select class="form-control select2me" id="var_selectteam" name="var_selectteam">
                                            <option> Select Team 1 </option>
                                            <option> Select Team2</option>

                                        </select>
                                    </div>
                                </div>-->


            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-4">
                        <button type="submit" class="btn green btn-circle">Submit</button>
                        <a class="btn default btn-circle" href="<?php echo admin_url() . 'staff'; ?>">Cancel</a>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>