<script type="text/javascript">
    var clientId = '<?= $clientId; ?>';
    var projectId = '<?= $projectId; ?>';
    var expertsName = '<?= ($getExperts['expertsName'] != '') ? str_replace('-', ' ', $getExperts['expertsName']) : ''; ?>';
    var position = '<?= ($getExperts['position'] != '') ? str_replace('-', ' ', $getExperts['position']) : ''; ?>';
    var geoGraphy = '<?= ($getExperts['geoGraphy'] != '') ? str_replace('-', ' ', $getExperts['geoGraphy']) : ''; ?>';
    var expretise = '<?= ($getExperts['expretise'] != '') ? str_replace('-', ' ', $getExperts['expretise']) : ''; ?>';
    var currentCompany = '<?= ($getExperts['currentCompany'] != '') ? str_replace('-', ' ', $getExperts['currentCompany']) : ''; ?>';
    var formerCompany = '<?= ($getExperts['formerCompany'] != '') ? str_replace('-', ' ', $getExperts['formerCompany']) : ''; ?>';
</script>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="col-md-3" style="padding-left: 0">
                    <div class="caption">
                        <i class="icon-settings font-green-haze"></i>
                        <span class="caption-subject font-green-haze sbold uppercase"> Find Experts List </span>
                    </div>
                </div>
            </div>
            <div class="portlet-body">
                <div class="col-md-12">
                    <div class="col-md-4">
                        <div class="form-group">

                            <select class="form-control select2me client" name="client"
                                    data-placeholder="Select Client">
                                <option value=""></option>
                                <?php for ($i = 0; $i < count($allClient); $i++) { ?>
                                    <option value="<?= $allClient[$i]['id'] ?>" <?php if ($clientId == $allClient[$i]['id']) {
                                        echo "selected='selected'";
                                    } ?>
                                            data-account-id="<?= $allClient[$i]['fk_assign_account_manager'] ?>"><?= $allClient[$i]['var_company_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <!--<label class="control-label">Industry</label>-->

                            <select class="form-control select2me project" name="project"
                                    data-placeholder="Select Project" id="clientProject">
                                <option value=""></option>
                                <?php for ($i = 0; $i < count($clientProject); $i++) { ?>
                                    <option value="<?= $clientProject[$i]['id'] ?>" <?php if ($projectId == $clientProject[$i]['id']) {
                                        echo "selected='selected'";
                                    } ?>
                                            data-user-id="<?= $clientProject[$i]['fk_user']; ?>"><?= $clientProject[$i]['var_project_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="col-md-4">
                        <div class="col-md-4">
                            <div class="form-group">
                                <a href="javascript:;" class="btn btn-circle btn-primary experts_search">
                                    <i class="fa fa-search"></i> Search </a>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <a href="javascript:;" class="btn btn-circle btn-primary goToProject">
                                    <i class="fa fa-search"></i> Go to Project </a>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <hr>

                <div class="experts_fliter" style="display: none;">

                    <div class="row">
                        <?php /*
                        <div class="col-md-3">
                            <div class="form-group">
                                <!--<label class="control-label">Industry</label>-->
                                <div class="">
                                    <select class="form-control select2me" name="industry" data-placeholder="Select Industry">
                                        <option value=""></option>
                                        <option value="Industry 1">Industry 1</option>
                                        <option value="Industry 2">Industry 2</option>
                                        <option value="Industry 3">Industry 3</option>
                                        <option value="Industry 4">Industry 4</option>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
 */ ?>
                        <div class="col-md-3">
                            <div class="form-group">
                                <!--<label class="control-label">Job Title</label>-->
                                <div class="">
                                    <input type="text" class="form-control" id="experts_position" name="job_title"
                                           placeholder="Job Title"
                                           value="<?= ($getExperts['position'] != '') ? str_replace('-', ' ', $getExperts['position']) : ''; ?>"/>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <!--<label class="control-label">Current Company</label>-->
                                <div class="">
                                    <input type="text" class="form-control" id="current_company"
                                           placeholder="Current Company" name="contry_company"
                                           value="<?= ($getExperts['currentCompany'] != '') ? str_replace('-', ' ', $getExperts['currentCompany']) : ''; ?>"/>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <!--<label class="control-label">Former Company</label>-->
                                <div class="">
                                    <input type="text" class="form-control" id="formar_company"
                                           placeholder="Former Company" name="former_company"
                                           value="<?= ($getExperts['formerCompany'] != '') ? str_replace('-', ' ', $getExperts['formerCompany']) : ''; ?>"/>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <!--<label class="control-label">Industry</label>-->
                                <div class="">
                                    <input type="text" class="form-control" id="experts_geography"
                                           placeholder="Geography" name="experts_geography"
                                           value="<?= ($getExperts['geoGraphy'] != '') ? str_replace('-', ' ', $getExperts['geoGraphy']) : ''; ?>"/>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">

                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="">
                                    <input type="text" class="form-control" id="expertise" placeholder="Expertise"
                                           name="expertise"
                                           value="<?= ($getExperts['expretise'] != '') ? str_replace('-', ' ', $getExperts['expretise']) : ''; ?>"/>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <div class="">
                                    <input type="text" class="form-control" id="experts_name" name="experts_name"
                                           placeholder="Experts Name"
                                           value="<?= ($getExperts['expertsName'] != '') ? str_replace('-', ' ', $getExperts['expertsName']) : ''; ?>"/>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <!--<label class="control-label">Industry</label>-->
                                <div class="">
                                    <select class="form-control select2me" name="rules"
                                            data-placeholder="Select Private/Public">
                                        <option value=""></option>
                                        <option value="Private">Private</option>
                                        <option value="Public">Public</option>
                                    </select>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <a href="javascript:;"
                                   class="btn btn-circle btn-primary pull-right expertsCustomeSearch">
                                    <i class="fa fa-search"></i> Search </a>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <hr>
                    <div class="findexperts"></div>
                </div>
            </div>
        </div>
    </div>
</div>