<?php
if(!empty($experts)){
$expertsName ='';
if($clientProject['expertName'] !=''){
    $expertsName = str_replace(' ','-',$clientProject['expertName']);
}
$geoGraphy = '';
if ($clientProject['geography'] != '') {
    $geoGraphy = str_replace(' ','-',$clientProject['geography']);
}
$expretise = '';
if ($clientProject['expretise'] != '') {
    $expretise = str_replace(' ','-',$clientProject['expretise']);
}
$position = '';
if ($clientProject['position'] != '') {
    $position = str_replace(' ','-',$clientProject['position']);
}
$currentCompany = '';
if ($clientProject['currentCompany'] != '') {
    $currentCompany = str_replace(' ','-',$clientProject['currentCompany']);
}
$formerCompany = '';
if ($clientProject['formerCompany'] != '') {
    $formerCompany = str_replace(' ','-',$clientProject['formerCompany']);
}
for($i=0; $i<count($experts); $i++) { ?>

<div class="portlet-body">
    <div class="panel-group col-md-12">
        <div class="panel panel-default">
            <div class="panel-body no-padding">
                <div class="row no-margin panel-head">
                    <div class="col-md-2 no-padding">
                        <div class="form-group no-margin text-center">
                            <label class="control-lable"><b>Experts Name</b></label>
                            <div class="">
                                <label class="control-lable"><?= $experts[$i]['var_fname'].' '.$experts[$i]['var_lname']; ?></label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 no-padding">
                        <div class="form-group no-margin text-center">
                            <label class="control-lable"><b>Job title</b></label>
                            <div class="">
                                <label class="control-lable"><?= $experts[$i]['currentPosition']; ?></label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 no-padding">
                        <div class="form-group no-margin text-center">
                            <label class="control-lable"><b>Current Company</b></label>
                            <div class="">
                                <label class="control-lable"><?= $experts[$i]['currentCompanyName']; ?></label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 no-padding">
                        <div class="form-group no-margin text-center">
                            <label class="control-lable"><b>Seniority</b></label>
                            <div class="">
                                <label class="control-lable">Senior</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 no-padding">
                        <div class="form-group no-margin text-center">
                            <label class="control-lable"><b>Rating</b></label>
                            <div class="">
                                <label class="control-lable">4.7/5 (8 reviews)</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-2 no-padding">
                        <div class="form-group no-margin text-center">
                            <label class="control-lable"><b>Geography of expert</b></label>
                            <div class="">
                                <label class="control-lable"><?= $experts[$i]['currentGeography'];?></label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="espandDiv" style="margin-bottom: 20px">
                    <div class="col-md-12">
                        <p><?= $experts[$i]['txt_bio']; ?></p>
                    </div>
                    <div class="col-md-12 text-center">
                        <a class=" btn btn-circle btn-primary btn-sm" href="<?= admin_url() ?>experts/bio/<?=$experts[$i]['id']?>?clientId=<?= $clientProject['client_id']; ?>&projectId=<?= $clientProject['project_id']?>&expertsName=<?= $expertsName ?>&position=<?= $position ?>&geoGraphy=<?= $geoGraphy ?>&expretise=<?= $expretise ?>&currentCompany=<?= $currentCompany ?>&formerCompany=<?= $formerCompany ?>">
                            <i class="fa fa-plus"></i> Full Bio
                        </a>
                        <a class=" btn btn-circle btn-primary btn-sm addtoShortlist" href="javascript:;"
                           data-client-id="<?= $clientProject['client_id']?>" data-assign-account-id="<?= $clientProject['assign_account']?>" data-project-id="<?= $clientProject['project_id']?>"
                           data-user-id="<?= $clientProject['user_id']?>" data-experts-id="<?= $experts[$i]['id']?>">
                            <i class="fa fa-plus"></i> Add to Shortlist
                        </a>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>
<?php } } else{
    echo "No Experts Found";
} ?>