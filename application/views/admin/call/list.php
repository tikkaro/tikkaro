<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="col-md-3" style="padding-left: 0">
                    <div class="caption">
                        <i class="icon-settings font-green-haze"></i>
                        <span class="caption-subject font-green-haze sbold uppercase"> Call List </span>
                    </div>
                </div>

                
            </div>
            <div class="portlet-body">
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="call_list">
                        <thead>
                            <tr role="row" class="heading">
                                <th> Date </th>
                                <th> Time</th>
                                <th> Client </th>
                                <th> User Name</th>
                                <th> Expert</th>
                                <th> Project</th>
                                <th> Length</th>
                                <th> Status</th>
                                <th> Billing Details</th>
                                <th> Action</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>  
            </div>
        </div>
    </div>
</div>


