<div class="content">
    <!-- BEGIN LOGIN FORM -->

    <form id="setPassword_frm" action="<?= base_url('account/setPassword') ?>" method="post">
        <input type="hidden" value="<?= $token; ?>" name="token"/>

        <h3 class="form-title font-green">Set Password</h3>
        <div class="alert alert-danger display-hide">
            <button class="close" data-close="alert"></button>
            <span> Enter a valid Password. </span>
        </div>
        <div class="alert alert-success display-hide">
            <button class="close" data-close="alert"></button>
            <span> Password Changed Successfully.. </span>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off"
                   placeholder="Enter Password" id="password" name="password"/></div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Re-Password</label>
            <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off"
                   id="re_password" placeholder="Re Enter Password" name="re_password"/></div>
        <div class="form-actions">
            <input class="btn green uppercase" type="submit" value="Set Password">
        </div>
    </form>
    <!-- END LOGIN FORM -->
</div>