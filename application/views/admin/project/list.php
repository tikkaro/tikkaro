<?php
$clientData = json_decode(CLIENT_STATUS);
?>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="col-md-3" style="padding-left: 0">
                    <div class="caption">
                        <i class="icon-settings font-green-haze"></i>
                        <span class="caption-subject font-green-haze sbold uppercase"> Projects List </span>
                    </div>
                </div>

                <div class="actions">
                    <a class="btn btn-circle btn-default btn-sm" href="<?php echo admin_url() . 'project/add'; ?>">
                        <i class="fa fa-plus"></i> Add Project 
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="col-md-4">
                    <div class="form-group">

                        <div class="">
                            <input type="text" class="form-control project_custome_search" data-column="0" name="project_name" placeholder="Project Name"/>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">

                        <div class="">

                            <input type="text" id="client_name" class="form-control project_custome_search" data-column="2" name="client_name" placeholder="Client Name"/>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">

                        <div class="">
                            <select class="form-control select2me project_custome_search" data-column="4" name="client_value"
                                    data-placeholder="Client Value">
                                <option value=""></option>
                                <?php
                                foreach ($clientData as $key=>$val) {?>
                                    <option value="<?= $key ?>"><?= $val; ?></option>
                                <?php } ?>
                            </select>

                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <hr>
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="projectList">
                        <thead>
                            <tr role="row" class="heading">
                                <th>Project Name</th>
                                <th>Client</th>
                                <th>User Name</th>
                                <th>Client Value</th>
                                <th>Initiated</th>
                                <th>Deadline</th>
                                <th>Scheduling</th>
                                <th>Scheduled</th>
                                <th>Completed</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>  
            </div>
        </div>
    </div>
</div>
