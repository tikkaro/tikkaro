<script>

    $(document).ready(function () {
        var country = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            prefetch: {
                url: adminurl+'project/getCountry',
                filter: function (list) {
                    return $.map(list, function (country) {
                        return {name: country};
                    });
                }
            }
        });
        country.initialize();

        $('#var_country_interest').tagsinput({
            typeaheadjs: {
                name: 'countryname',
                displayKey: 'name',
                valueKey: 'name',
                source: country.ttAdapter()
            }
        });

//        var regionsUrl = adminurl + 'project/getRegions';
        var regions = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            prefetch: {
                url: adminurl+'project/getRegions',
                filter: function (list) {
                    return $.map(list, function (regions) {
                        return {name: regions};
                    });
                }
            }
        });
        regions.initialize();

        $('#var_regions_interest').tagsinput({
            typeaheadjs: {
                name: 'regionsname',
                displayKey: 'name',
                valueKey: 'name',
                source: regions.ttAdapter()
            }
        });

        var company = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            prefetch: {
                url: adminurl+'project/getCompany',
                filter: function (list) {
                    return $.map(list, function (company) {
                        return {name: company};
                    });
                }
            }
        });
        company.initialize();

        $('#var_companies_interest').tagsinput({
            typeaheadjs: {
                name: 'companyname',
                displayKey: 'name',
                valueKey: 'name',
                source: company.ttAdapter()
            },
        });
        $("#var_companies_interest").tagsinput('items');
    });

</script>

<div class="portlet box green">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-user"></i>Add Project
        </div>
    </div>
    <div class="portlet-body form client-project-frm">
        <form role="form" method="post" enctype="multipart/form-data" id="addClientProjectFrm"
              action="<?php echo admin_url('project/addProject'); ?>" class="form-horizontal">
            <div class="form-body">

                <div class="form-group">
                    <label class="col-md-3 control-label">Project Name<span class="required"
                                                                            aria-required="true">*</span> :</label>

                    <div class="col-md-8">

                        <div class="input-icon right">
                            <input type="hidden" class="clientAssignAcManager" name="account_manager"
                                   id="account_manager" value="<?= $assignAccountManager ?>">
                            <input type="hidden" name="page_from"
                                   id="page_from" value="<?= ($pageFrom) ? $pageFrom : 'project' ?>">
                            <input type="text" id="var_fname" name="var_project" placeholder="Enter Project Name"
                                   class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Project Description<span class="required"
                                                                                   aria-required="true">*</span>
                        :</label>

                    <div class="col-md-8">

                        <div class="input-icon right">
                            <textarea class="form-control" id="var_address" name="project_description"
                                      placeholder="Enter Project Description" rows="4"></textarea>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Select Client<span class="required"
                                                                             aria-required="true">*</span> :</label>

                    <div class="col-md-8">

                        <div class="input-icon right">
                            <select class="form-control select2me client_select" id="client_id" name="client_id"
                                    data-placeholder="Select Client">
                                <option value=""></option>
                                <?php
                                for ($i = 0; $i < count($clientData); $i++) {
                                    $selected = "";
                                    if ($clientData[$i]['id'] == $clientId) {
                                        $selected = "selected = 'selected'";
                                    }
                                    ?>
                                    <option value="<?= $clientData[$i]['id'] ?>" <?= $selected ?> assign-ac-manager="<?= $clientData[$i]['fk_assign_account_manager'] ?>"><?= $clientData[$i]['var_company_name']; ?></option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Select User<span class="required"
                                                                           aria-required="true">*</span> :</label>

                    <div class="col-md-8">

                        <div class="input-icon right">
                            <select class="form-control select2me" id="user_id" name="user_id"
                                    data-placeholder="Select User">
                                <option value=""></option>
                                <?php for ($i = 0; $i < count($userData); $i++) { ?>
                                    <option value="<?= $userData[$i]['id'] ?>">
                                        <?= $userData[$i]['var_fname'] . ' ' . $userData[$i]['var_lname'] ?>
                                    </option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Select Priority :</label>

                    <div class="col-md-8">

                        <div class="input-icon right">
                            <select class="form-control select2me" name="priority" data-placeholder="Select Priority">
                                <option value="ACTIVE">Active</option>
                                <option value="PRIORITY">Priority</option>
                                <option value="WAITING">Waiting</option>
                                <option value="SCHEDULING">Scheduling</option>
                                <option value="CLOSED">Closed</option>

                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Received Date :</label>

                    <div class="col-md-8">

                        <div class="input-icon right">
                            <input id="dt_initiate" name="dt_initiate" placeholder="Select Received Date"
                                   class="form-control form-control-inline  date-picker" data-date-format="dd/mm/yyyy"
                                   size="20" value="<?= date('d/m/Y'); ?>" type="text">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Deadline Date :</label>

                    <div class="col-md-8">

                        <div class="input-icon right">
                            <input id="dt_deadline" name="dt_deadline" placeholder="Select Deadline Date"
                                   class="form-control form-control-inline date-picker" data-date-format="dd/mm/yyyy"
                                   size="20" type="text">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Number of Calls Required:</label>

                    <div class="col-md-8">

                        <div class="input-icon right">
                            <input type="text" id="var_number_call" name="var_number_call"
                                   placeholder="Enter Number of Calls Required" class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-3 control-label">Target Company :</label>

                    <div class="col-md-8">

                        <div class="input-icon right">
                            <input type="text" id="var_target_company" name="var_target_company"
                                   placeholder="Enter Target Company" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Client Anonymous?</label>

                    <div class="col-md-8">

                        <div class="md-radio-inline">
                            <div class="md-radio">
                                <input type="radio" id="checkbox1_8" name="client_anonymous" value="1"
                                       class="md-radiobtn" checked>
                                <label for="checkbox1_8">
                                    <span class="inc"></span>
                                    <span class="check"></span>
                                    <span class="box"></span> Yes</label>
                            </div>
                            <div class="md-radio">
                                <input type="radio" id="checkbox1_9" name="client_anonymous" value="2"
                                       class="md-radiobtn">
                                <label for="checkbox1_9">
                                    <span class="inc"></span>
                                    <span class="check"></span>
                                    <span class="box"></span> No </label>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Target Anonymous?</label>

                    <div class="col-md-8">

                        <div class="md-radio-inline">
                            <div class="md-radio">
                                <input type="radio" id="checkbox1_1" name="target_anonymous" value="1"
                                       class="md-radiobtn" checked>
                                <label for="checkbox1_1">
                                    <span class="inc"></span>
                                    <span class="check"></span>
                                    <span class="box"></span> Yes</label>
                            </div>
                            <div class="md-radio">
                                <input type="radio" id="checkbox1_2" name="target_anonymous" value="2"
                                       class="md-radiobtn">
                                <label for="checkbox1_2">
                                    <span class="inc"></span>
                                    <span class="check"></span>
                                    <span class="box"></span> No </label>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="form-group custom-tagsinput">
                    <label class="col-md-3 control-label">Country of Interest
                        :</label>

                    <div class="col-md-8">

                        <div class="input-icon right">
                            <input type="text" id="var_country_interest" name="var_country_interest"
                                   placeholder="Enter Country of Interest" class="form-control" data-role="tagsinput">
                        </div>
                    </div>
                </div>

                <div class="form-group custom-tagsinput">
                    <label class="col-md-3 control-label">Companies of Interest
                        :</label>

                    <div class="col-md-8">

                        <div class="input-icon right">
                            <input type="text" id="var_companies_interest" name="var_companies_interest"
                                   placeholder="Enter Companies of Interest" data-role="tagsinput"
                                   class="form-control">
                        </div>
                    </div>
                </div>
                <div class="form-group custom-tagsinput">
                    <label class="col-md-3 control-label">Regions of interest:</label>
                    <div class="col-md-8">

                        <div class="input-icon right">
                            <input type="text" id="var_regions_interest" name="var_regions_interest"
                                   placeholder="Enter Regions of Interest" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Industry<span class="required" aria-required="true">*</span> :</label>

                    <div class="col-md-8">

                        <div class="input-icon right">
                            <input type="text" id="var_industry" name="var_industry"
                                   placeholder="Enter Industry" class="form-control">
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-actions">
                <div class="row">
                    <div class="col-md-offset-3 col-md-4">
                        <button type="submit" class="btn green btn-circle">Submit</button>
                        <?php if ($clientId != "") { ?>
                            <a class="btn default btn-circle"
                               href="<?php echo admin_url('client/project/' . $clientId); ?>">Cancel</a>
                        <?php } else { ?>
                            <a class="btn default btn-circle"
                               href="<?php echo admin_url('project/'); ?>">Cancel</a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
