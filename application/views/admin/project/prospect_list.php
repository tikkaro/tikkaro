<style>
    .inline-block {
        display: inline-block !important;
        width: 88%;
    }
    .select2-container {
        width: 100% !important;
    }
</style>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="tabbable-line boxless tabbable-reversed">
            <?php $this->load->helper('common_tab_helper'); ?>
            <?= projectMenu('prospects', $projectId) ?>
            <div class="tab-content">
                <?php $this->load->helper('comman_data_helper'); ?>
                <?= projectData($projectData) ?>
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-title">
                        <div class="col-md-3" style="padding-left: 0">
                            <div class="caption">
                                <i class="icon-equalizer font-blue-hoki"></i>
                                <span class="caption-subject font-green-haze sbold uppercase">Prospects</span>
                            </div>
                        </div>

                        <div class="actions">
                            <a class="btn btn-circle btn-default btn-sm" data-toggle="modal" href="#add_prospect">
                                <i class="fa fa-plus"></i> Add Prospects
                            </a>
                        </div>
                    </div>
                    <?php
                    for ($i = 0; $i < count($prospectData); $i++) {
                        ?>
                        <div class="portlet-body">
                            <div class="panel-group">
                                <div class="panel panel-default">

                                    <div class="panel-body no-padding prospact_data">
                                        <div class="row no-margin panel-head">
                                            <div class="col-md-2 no-padding small-col">
                                                <div class="form-group no-margin text-center">
                                                    <label class="control-lable"><b>Name</b></label>
                                                    <div class="">
                                                        <label class="control-lable">
                                                            <a href="javascript:;" class="inline-editing-trigger"
                                                               data-title="Enter Name"
                                                               data-table="project_has_prospect"
                                                               data-required="true"
                                                               data-field1="var_fname"
                                                               data-field2="var_lname"
                                                               data-id="<?= $prospectData[$i]['id']; ?>"
                                                               data-url="<?= admin_url('commaneditable/editableFnameLname') ?>"
                                                               ><?= $prospectData[$i]['var_fname'] . ' ' . $prospectData[$i]['var_lname']; ?>
                                                            </a>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2 no-padding small-col">
                                                <div class="form-group no-margin text-center">
                                                    <label class="control-lable"><b>Email</b></label>
                                                    <div class="">
                                                        <label class="control-lable">
                                                            <a href="javascript:;" class="inline-editing-trigger"
                                                               data-title="Enter Email"
                                                               data-table="project_has_prospect"
                                                               data-required="true"
                                                               data-field="var_email"
                                                               data-id="<?= $prospectData[$i]['id']; ?>"
                                                               ><?= $prospectData[$i]['var_email']; ?>
                                                            </a>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2 no-padding small-col">
                                                <div class="form-group no-margin text-center">
                                                    <label class="control-lable"><b>Company name</b></label>
                                                    <div class="">
                                                        <label class="control-lable">
                                                            <a href="javascript:;" class="inline-editing-trigger"
                                                               data-title="Enter Company"
                                                               data-table="project_has_prospect"
                                                               data-required="true"
                                                               data-field="var_company"
                                                               data-id="<?= $prospectData[$i]['id']; ?>"
                                                               ><?= $prospectData[$i]['var_company'] ?>
                                                            </a>

                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2 no-padding small-col">
                                                <div class="form-group no-margin text-center">
                                                    <label class="control-lable"><b>Position </b></label>
                                                    <div class="">
                                                        <label class="control-lable">
                                                            <a href="javascript:;" class="inline-editing-trigger"
                                                               data-title="Enter Role/Position"
                                                               data-table="project_has_prospect"
                                                               data-required="true"
                                                               data-field="var_position"
                                                               data-id="<?= $prospectData[$i]['id']; ?>"
                                                            ><?= $prospectData[$i]['var_position'] ?>
                                                            </a>

                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2 no-padding small-col">
                                                <div class="form-group no-margin text-center">
                                                    <label class="control-lable"><b>Phone no</b></label>
                                                    <div class="">
                                                        <label class="control-lable">
                                                            <a href="javascript:;" class="inline-editing-trigger"
                                                               data-title="Enter Phone no"
                                                               data-table="project_has_prospect"
                                                               data-required="true"
                                                               data-field="bint_phone"
                                                               data-id="<?= $prospectData[$i]['id']; ?>"
                                                               ><?= $prospectData[$i]['bint_phone'] ?>
                                                            </a>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2 no-padding small-col">
                                                <div class="form-group no-margin text-center">
                                                    <label class="control-lable"><b>Linkedin Page</b></label>
                                                    <div class="">
                                                        <label class="control-lable">
                                                            <a href="javascript:;" class="inline-editing-trigger"
                                                               data-title="Enter Phone no"
                                                               data-table="project_has_prospect"
                                                               data-required="true"
                                                               data-field="var_linkedin_page"
                                                               data-id="<?= $prospectData[$i]['id']; ?>"
                                                               ><?= $prospectData[$i]['var_linkedin_page'] ?>
                                                            </a>
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-md-2 no-padding small-col">
                                                <div class="form-group no-margin text-center">
                                                    <label class="control-lable"><b>Angle</b></label>
                                                    <div class="">
                                                        <label class="control-lable">
                                                            <a href="javascript:;"
                                                               class="angleOfIntrest"
                                                               data-id="<?= $prospectData[$i]['id']; ?>"><?= ($prospectData[$i]['angleName']) ? $prospectData[$i]['angleName'] : 'Empty'; ?>
                                                            </a>
                                                        </label>
                                                        <div class="input-icon Angletoshow"
                                                             style="display: none;">
                                                            <input type="text"
                                                                   placeholder="Add Angle"
                                                                   value="<?= $prospectData[$i]['angleName'] ?>"
                                                                   name="angle_of_intrest"
                                                                   class="form-control angle_of_intrest"/>
                                                            <div class="editable-buttons cusAngleTags">
                                                                <button type="submit"
                                                                        class="btn blue editable-submit updateTags"
                                                                        data-table="project_has_prospect"
                                                                        data-id="<?= $prospectData[$i]['id']; ?>">
                                                                    <i class="fa fa-check"></i>
                                                                </button>
                                                                <button type="button"
                                                                        class="btn default editable-cancel cancleTags">
                                                                    <i class="fa fa-times"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div>
                                                <a href="<?= admin_url('experts/add?prospectId=' . $prospectData[$i]['id'].'&projectId='.$projectId); ?>"
                                                   class="btn red pull-right btn-sm createBtn">Create Expert</a>
                                            </div>
                                        </div>

                                        <div class="table-responsive col-md-12">
                                            <table class="table table-striped table-bordered table-hover" style="table-layout: fixed;">
                                                <thead>
                                                    <tr>
                                                        <th> Date</th>
                                                        <th> Note left by</th>
                                                        <th> Info</th>
                                                        <th> Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <?php
                                                    $notesData = $prospectData[$i]['notes'];
                                                    for ($j = 0; $j < count($notesData); $j++) {
                                                        ?>
                                                        <tr>
                                                            <td> <?= date('d/m/Y', strtotime($notesData[$j]['created_at'])) ?></td>
                                                            <td><?= $notesData[$j]['noteBy'] ?></td>
                                                            <td>
                                                                <?php if(strlen($notesData[$j]['txt_note']) > 100){ ?>

                                                                <span class="read_less">
                                                                    <?= shortenText($notesData[$j]['txt_note'], 100) ?>
                                                                    <a href="javascript:;" class="readmore">Read more</a>
                                                                </span>

                                                                <span class="read_more" style="display:none">
                                                                    <?= $notesData[$j]['txt_note'] ?>
                                                                    <a href="javascript:;" class="readless">Read Less</a>
                                                                </span>
                                                                <?php } else {
                                                                    echo $notesData[$j]['txt_note'];
                                                                } ?>
                                                            </td>
                                                            <td><a href="javascript:;" data-toggle="modal"
                                                                   note-id="<?= $notesData[$j]['id'] ?>"
                                                                   prospect-id="<?= $prospectData[$i]['id'] ?>"
                                                                   class="btn btn-primary btn-xs edit add_prospect"><i
                                                                        class="fa fa-eye"></i>
                                                                    Edit</a>
                                                            </tr>
                                                        <?php
                                                    }
                                                    ?>
                                                </tbody>
                                            </table>
                                            <div class="form-group col-md-12">
                                                <a class="pull-right btn btn-circle btn-primary btn-sm add_prospect"
                                                   prospect-id="<?= $prospectData[$i]['id'] ?>"
                                                   href="#add_notes_model" data-toggle="modal">
                                                    <i class="fa fa-plus"></i> Add Notes
                                                </a>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ADD Prospect MODEL -->
<div class="modal fade" id="add_prospect" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <form class="form-horizontal" id="add_prospect_frm" name="add_prospect_frm" method="POST"
              enctype="multipart/form-data" action="<?= admin_url('project/addProspect') ?>">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Add Prospect</h4>
                </div>
                <div class="modal-body">
                    <div class="form-body">
                        <div class="row">
                            <div class="form-group">
                                <label class="control-label col-md-4">First Name :</label>
                                <div class="input-group col-md-6">
                                    <input type="hidden" name="project_id" value="<?= $projectId ?>"/>
                                    <input type='text' class="form-control" name="prospect_fname"
                                           placeholder="Enter First Name"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Last Name :</label>
                                <div class="input-group col-md-6">
                                    <input type='text' class="form-control" name="prospect_lname"
                                           placeholder="Enter Last Name"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Email :</label>
                                <div class="input-group col-md-6">
                                    <input type='text' class="form-control date-picker" name="prospect_email"
                                           placeholder="Enter Email"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Company :</label>
                                <div class="input-group col-md-6">
                                    <input type='text' class="form-control" name="prospect_company"
                                           placeholder="Enter Company Name"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Phone Number : <span
                                        class="required">  </span></label>
                                <div class="input-group col-md-6">
                                    <input type='text' class="form-control" name="prospect_phno" placeholder="Enter Phone Number"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Linkedin Page : <span
                                        class="required">  </span></label>
                                <div class="input-group col-md-6">
                                    <input type='text' class="form-control" name="linkedin_page"
                                           placeholder="Enter Linkedin Page"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Company Website : <span
                                        class="required">  </span></label>
                                <div class="input-group col-md-6">
                                    <input type='text' class="form-control" name="compnay_website"
                                           placeholder="Enter Company Website"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-4">Role/Position : <span
                                        class="required">  </span></label>
                                <div class="input-group col-md-6">
                                    <input type='text' class="form-control" name="prospect_position"
                                           placeholder="Enter Role/Position"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-4 control-label">Angle :</label>
                                <div class="input-group col-md-6">
                                    <input type="text" id="angle_of_intrest" placeholder="Add Angle of Intrest" name="angle_of_intrest" class="form-control"/>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn green btn-circle submitcls"> Submit</button>
                </div>
            </div>
        </form>
    </div>
    <!-- /.modal-content -->
</div>
<!-- End Prospect MODEL -->
<div class="modal fade" id="add_prospect_notes" tabindex="-1" role="basic" aria-hidden="true"
     style="z-index: 999999999 !important;">
    <div class="modal-dialog">
        <form class="form-horizontal" id="add_prospect_notes_frm" name="add_prospect_notes_frm" method="POST"
              enctype="multipart/form-data" action="<?= admin_url('project/addProspectNote'); ?>">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title prospect-note-title">Add Notes</h4>
                </div>
                <div class="modal-body">
                    <div class="form-body">

                        <div class="form-group">

                            <label class="control-label col-md-2">Notes : <span class="required">*</span></label>
                            <div class="col-md-10">
                                <input type="hidden" id="note_id" class="form-control" name="note_id"/>
                                <input type="hidden" id="prospect_id" class="form-control" name="prospect_id"/>
                                <textarea class="form-control prospect_notes" rows="6" name="notes"
                                          placeholder="Enter Notes"></textarea>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn green btn-circle submitcls"> Submit</button>
                </div>
            </div>
        </form>
    </div>
    <!-- /.modal-content -->
</div>
<!-- End ADD NOTES MODEL -->