<script type="text/javascript">
    var projectId = '<?php echo $projectId ?>';
</script>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="tabbable-line boxless tabbable-reversed">
            <?php $this->load->helper('common_tab_helper'); ?>
            <?= projectMenu('notes', $projectId) ?>

            <div class="tab-content">
                <?php $this->load->helper('comman_data_helper'); ?>
                <?= projectData($projectData) ?>

                <div class="portlet light portlet-fit portlet-datatable bordered">

                    <div class="portlet-title">
                        <div class="col-md-3" style="padding-left: 0">
                            <div class="caption">
                                <i class="icon-equalizer font-blue-hoki"></i>
                                <span class="caption-subject font-green-haze sbold uppercase"> Notes</span>
                            </div>
                        </div>

                        <div class="actions">
                            <!--<a class="btn btn-circle btn-default btn-sm" href="#add_notes_model" data-toggle="modal">-->
                            <a class="btn btn-circle btn-default btn-sm btn-edit-note" data-note-id=""  data-action="<?= admin_url('project/addProjectNote'); ?>" data-project-id="<?= $projectData['id'] ?>" data-client-id="<?= $projectData['fk_client'] ?>"  href="javascript:;" >
                                <i class="fa fa-plus"></i> Add Notes 
                            </a>
                        </div>
                    </div>


                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover" id="noteList">
                            <thead>
                                <tr role="row" class="heading">
                                    <th> Date </th>
                                    <th> Author </th>
                                    <th> Info </th>
                                    <th> Action </th>
                                </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div> 

                </div>
            </div>
        </div>
    </div>
</div>

<!-- ADD PROJECT NOTES MODEL -->
<div class="modal fade" id="add_project_notes_model" tabindex="-1" role="basic" aria-hidden="true"
     style="z-index: 999999999 !important;">
    <div class="modal-dialog">
        <form class="form-horizontal" id="add_project_note_frm" name="add_project_note_frm" method="POST" action="<?= admin_url('project/addProjectNote'); ?>">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Add Notes</h4>
                </div>
                <div class="modal-body">
                    <div class="form-body">

                            <div class="form-group">
                                <label class="control-label col-md-2">Notes : <span class="required">  </span></label>
                                <div class="col-md-10">
                                    <input type="hidden" name="noteId" class="note_id" value=""/>
                                    <input type="hidden" name="clientId" class="client_id" value=""/>
                                    <input type="hidden" name="projectId" class="project_id" value=""/>
                                    <textarea class="form-control project_note" rows="6" name="notes"
                                              placeholder="Enter Notes"></textarea>
                                </div>
                            </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn green btn-circle submitcls submit-btn"> Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>
<!-- /.modal-content -->
