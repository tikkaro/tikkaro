<style>
    .dataTables_filter{
        display: none;
    }
</style>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="tabbable-line boxless tabbable-reversed">
            <?php $this->load->helper('common_tab_helper'); ?>
            <?= projectMenu('calls', $projectId) ?>
            <div class="tab-content">
                <?php $this->load->helper('comman_data_helper'); ?>
                <?= projectData($projectData) ?>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="portlet light portlet-fit portlet-datatable bordered">
            <div class="portlet-title">
                <div class="col-md-3" style="padding-left: 0">
                    <div class="caption">
                        <i class="icon-settings font-green-haze"></i>
                        <span class="caption-subject font-green-haze sbold uppercase">Call List </span>
                    </div>
                </div>
                <div class="actions">
                    <a class="btn btn-circle btn-default btn-sm" href="javascript:;">
                        <i class="fa fa-plus"></i> Create New Call
                    </a>
                </div>
            </div>
            <div class="portlet-body">
                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover" id="callList">
                        <thead>
                            <tr role="row" class="heading">
                                <th>Date</th>
                                <th>Time</th>
                                <th>Expert</th>
                                <th>User</th>
                                <th>Length</th>
                                <th>Status</th>
                                <th>Billing Details</th>
                            </tr>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>  
            </div>
        </div>
    </div>
</div>


