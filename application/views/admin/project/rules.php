<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="tabbable-line boxless tabbable-reversed">
            <?php $this->load->helper('common_tab_helper'); ?>
            <?= projectMenu('rules', $projectId) ?>
            <div class="tab-content">
                <?php $this->load->helper('comman_data_helper'); ?>
                <?= projectData($projectData) ?>

                <div class="portlet light portlet-fit portlet-datatable bordered">

                    <div class="portlet-title">
                        <div class="col-md-3" style="padding-left: 0">
                            <div class="caption">
                                <i class="icon-equalizer font-blue-hoki"></i>
                                <span class="caption-subject font-green-haze sbold uppercase"> Compliance Officer </span>
                            </div>
                        </div>

                    </div>
                    <div class="portlet-body">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Name:</label>
                                    <div class="col-md-10">
                                        <label class="control-label"> <?= $complianceData['var_name']; ?> </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Email :</label>
                                    <div class="col-md-10">
                                        <label class="control-label"> <?= $complianceData['var_email']; ?> </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Phone :</label>
                                    <div class="col-md-10">
                                        <label class="control-label"> <?= $complianceData['bint_phone']; ?> </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                    </div> 
                </div>
                <div class="clearfix"></div>

                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-title">
                        <div class="col-md-3" style="padding-left: 0">
                            <div class="caption">
                                <i class="icon-equalizer font-blue-hoki"></i>
                                <span class="caption-subject font-green-haze sbold uppercase"> Client Rules </span>
                            </div>
                        </div>

                    </div>
                    <div class="portlet-body">

                        <div class="companyRules col-md-12">
                            <?php
                            if (!empty($clientProjectRules)) {
                                for ($i = 0; $i < count($clientProjectRules); $i++) {
                                    ?>
                                    <div class="row">
                                        <div class="form-group">
                                            <label class="control-label col-md-3">
                                                <?= $clientProjectRules[$i]['var_name']; ?>
                                            </label>
                                            <div class="clearfix"></div>
                                        </div>
                                    </div>
                                    <?php
                                }
                            }else{
                                echo "No Client Rule Found";
                            }
                            ?>
                        </div>

                        <div class="clearfix"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>