<script type="text/javascript">
    var projectId = '<?= $projectId; ?>';
</script>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="tabbable-line boxless tabbable-reversed">
            <?php $this->load->helper('common_tab_helper'); ?>
            <?= projectMenu('shortlist', $projectId) ?>
            <div class="tab-content">
                <?php $this->load->helper('comman_data_helper'); ?>
                <?= projectData($projectData) ?>
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-title">
                        <div class="col-md-3" style="padding-left: 0">
                            <div class="caption">
                                <i class="icon-equalizer font-blue-hoki"></i>
                                <span class="caption-subject font-green-haze sbold uppercase">  Expert Shortlist </span>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <!--<label class="control-label">Industry</label>-->
                                    <div class="">
                                        <select class="form-control select2me projectStatus" name="status" data-placeholder="Select Status">
                                            <option value=""></option>
                                            <?php
                                            $shortListedStatus = json_decode(SHORTLISTED_STATUS);

                                            foreach ($shortListedStatus as $key => $value) { ?>
                                            <option value="<?= $key ;?>"><?= $value; ?></option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                    <div class="clearfix"></div>

                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <!--<label class="control-label">Current Company</label>-->
                                    <div class="">
                                        <input type="text" class="form-control" id="geography" placeholder="Geography" name="Geography"/>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <!--<label class="control-label">Current Company</label>-->
                                    <div class="">
                                        <a href="javascript:;" class="btn btn-circle btn-primary expertshortlist_search">
                                            <i class="fa fa-search"></i> Search </a>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <hr>
                        <div class="clearfix"></div>
                    </div>
                    <div class="expertShortlisted"></div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- START EDIT NOTES MODEL -->
<div class="modal fade" id="project_experts_notes_model" tabindex="-1" role="basic" aria-hidden="true"
     style="z-index: 999999999 !important;">
    <div class="modal-dialog">
        <form class="form-horizontal" id="project_expert_note_frm" name="project_expert_note_frm" method="POST"
              action="<?= admin_url('project/addProjectExpertNote') ?>">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title">Add Notes</h4>
                </div>
                <div class="modal-body">
                    <div class="form-body">

                        <div class="form-group">
                            <label class="control-label col-md-2">Notes : <span class="required">*</span></label>
                            <div class="col-md-10">
                                <input type="hidden" name="expertId" class="expertId" value=""/>
                                <input type="hidden" name="projectId" class="projectId" value=""/>
                                <input type="hidden" name="clientId" class="clientId" value=""/>
                                <input type="hidden" name="userId" class="userId" value=""/>
                                <input type="hidden" name="accountId" class="accountId" value=""/>
                                <input type="hidden" name="projecthasExpertsId" class="projecthasExpertsId" value=""/>
                                <input type="hidden" name="projecthasExpertsNoteId" class="projecthasExpertsNoteId" value=""/>
                                <textarea class="form-control project_exp_note" rows="6" name="notes"
                                          placeholder="Enter Notes"></textarea>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn green btn-circle submitcls submit-btn"> Submit</button>
                </div>
            </div>
        </form>
    </div>
    <!-- /.modal-content -->
</div>
<!-- END EDIT NOTE MODEL -->