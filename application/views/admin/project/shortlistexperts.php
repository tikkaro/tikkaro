<?php if(!empty($experts_shortlisted)){
                        for($i=0; $i<count($experts_shortlisted); $i++){ ?>

                    <div class="portlet-body">
                        <div class="panel-group col-md-12">
                            <div class="panel panel-default">

                                <div class="panel-body no-padding">
                                    <div class="row no-margin panel-head">
                                        <div class="col-md-2 no-padding">
                                            <div class="form-group no-margin text-center">
                                                <label class="control-lable"><b>Eperts Name</b></label>
                                                <div class="">
                                                    <label class="control-lable"><?= $experts_shortlisted[$i]['var_fname'].' '.$experts_shortlisted[$i]['var_lname'];?></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 no-padding">
                                            <div class="form-group no-margin text-center">
                                                <label class="control-lable"><b>Job title</b></label>
                                                <div class="">
                                                    <label class="control-lable"><?= $experts_shortlisted[$i]['currentPosition'];?></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 no-padding">
                                            <div class="form-group no-margin text-center">
                                                <label class="control-lable"><b>Employment</b></label>
                                                <div class="">
                                                    <label class="control-lable"><?= $experts_shortlisted[$i]['currentCompanyName'];?></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 no-padding">
                                            <div class="form-group no-margin text-center">
                                                <label class="control-lable"><b>Status</b></label>
                                                <div class="">
                                                    <label class="control-lable "><?= getShortlistedStatus($experts_shortlisted[$i]['enum_status']);?></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-1 no-padding">
                                            <div class="form-group no-margin text-center">
                                                <label class="control-lable"><b>Geography</b></label>
                                                <div class="">
                                                    <label class="control-lable"><?= $experts_shortlisted[$i]['currentGeography'];?></label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-2 no-padding">
                                            <div class="form-group no-margin text-center">
                                                <label class="control-lable"><b>Angle</b></label>
                                                <div class="">
                                                    <label class="control-lable">
                                                        <a href="javascript:;"
                                                           class="angleOfIntrest"
                                                           data-id="<?= $experts_shortlisted[$i]['id']; ?>"><?= ($experts_shortlisted[$i]['angleName']) ? $experts_shortlisted[$i]['angleName'] : 'Empty'; ?>
                                                        </a>
                                                    </label>
                                                    <div class="input-icon Angletoshow"
                                                         style="display: none;">
                                                        <input type="text"
                                                               placeholder="Add Angle"
                                                               value="<?= $experts_shortlisted[$i]['angleName'] ?>"
                                                               name="angle_of_intrest"
                                                               class="form-control angle_of_intrest"/>
                                                        <div class="editable-buttons cusAngleTags">
                                                            <button type="submit"
                                                                    class="btn blue editable-submit updateTags"
                                                                    data-table="project_has_experts"
                                                                    data-id="<?= $experts_shortlisted[$i]['id']; ?>">
                                                                <i class="fa fa-check"></i>
                                                            </button>
                                                            <button type="button"
                                                                    class="btn default editable-cancel cancleTags">
                                                                <i class="fa fa-times"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php if($experts_shortlisted[$i]['enum_status'] == 'EXPERTS_ATTACHED') { ?>
                                        <div class="customCollapse col-md-1" style="padding:11px;">
                                            <div class="checkbox-list pull-right">
                                                <label>
                                                    <input type="checkbox" class="checkSelect" value="<?= $experts_shortlisted[$i]['id'];?>">
                                                </label>
                                            </div>
                                        </div>
                                        <?php } ?>
                                    </div>

                                    <div class="row espandDiv no-margin" style="margin-bottom: 10px !important;">
                                        <div class="col-md-12">
                                            <p><?= $experts_shortlisted[$i]['txt_bio'];?></p>
                                        </div>

                                        <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#demo_<?= $i;?>" style="width: 98%;margin-bottom: 10px;margin-left: 10px;margin-right: 10px;">Project Experts Notes</button>
                                        <div id="demo_<?= $i;?>" class="collapse">
                                            <div class="table-responsive col-md-12">
                                                <table class="table table-striped table-bordered table-hover" style="table-layout: fixed;">
                                                    <thead>
                                                    <tr>
                                                        <th> Date</th>
                                                        <th> Note left by</th>
                                                        <th> Info</th>
                                                        <th> Action</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    <?php for($j=0; $j<count($experts_shortlisted[$i]['notes']); $j++){ ?>
                                                        <tr>
                                                            <td><?=date('d/m/Y',strtotime($experts_shortlisted[$i]['notes'][$j]['created_at']))?></td>
                                                            <td><?= $experts_shortlisted[$i]['notes'][$j]['noteBy']?></td>
                                                            <td>
                                                                <?php if(strlen($experts_shortlisted[$i]['notes'][$j]['txt_note']) > 100){ ?>

                                                                    <span class="read_less">
                                                                <?= shortenText($experts_shortlisted[$i]['notes'][$j]['txt_note'], 100) ?>
                                                                        <a href="javascript:;" class="readmore">Read more</a>
                                                            </span>

                                                                    <span class="read_more" style="display:none">
                                                                <?= $experts_shortlisted[$i]['notes'][$j]['txt_note'] ?>
                                                                        <a href="javascript:;" class="readless">Read Less</a>
                                                            </span>
                                                                <?php } else {
                                                                    echo $experts_shortlisted[$i]['notes'][$j]['txt_note'];
                                                                } ?>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:;" data-toggle="modal"
                                                                   class="btn btn-primary btn-xs edit project_has_experts_notes"
                                                                   data-project-has-experts-id="<?= $experts_shortlisted[$i]['id'] ?>"
                                                                   data-experts-id="<?= $experts_shortlisted[$i]['fk_experts'] ?>"
                                                                   data-project-id="<?= $experts_shortlisted[$i]['fk_project'] ?>"
                                                                   data-client-id="<?= $experts_shortlisted[$i]['fk_client'] ?>"
                                                                   data-user-id="<?= $experts_shortlisted[$i]['fk_user'] ?>"
                                                                   data-account-manager-id="<?= $experts_shortlisted[$i]['fk_assign_account_manager'] ?>"
                                                                   data-project-has-experts-notes ="<?= $experts_shortlisted[$i]['notes'][$j]['id']?>"
                                                                ><i class="fa fa-edit"></i> Edit</a>
                                                            </td>
                                                        </tr>
                                                    <?php } ?>

                                                    </tbody>
                                                </table>

                                                <div class="clearfix"></div>
                                            </div>
                                        </div>

                                        <div class="col-md-12 panel-btnarea text-center">
                                            <a style="margin-right: 10px;" class=" btn btn-circle btn-primary btn-sm" href="<?= admin_url() ?>experts/bio/<?=$experts_shortlisted[$i]['fk_experts']?>">
                                                <i class="fa fa-plus"></i> Full Bio 
                                            </a>
                                            <a class="btn btn-circle btn-primary btn-sm project_has_experts_notes"
                                               data-project-has-experts-id="<?= $experts_shortlisted[$i]['id'] ?>"
                                               data-experts-id="<?= $experts_shortlisted[$i]['fk_experts'] ?>"
                                               data-project-id="<?= $experts_shortlisted[$i]['fk_project'] ?>"
                                               data-client-id="<?= $experts_shortlisted[$i]['fk_client'] ?>"
                                               data-user-id="<?= $experts_shortlisted[$i]['fk_user'] ?>"
                                               data-account-manager-id="<?= $experts_shortlisted[$i]['fk_assign_account_manager'] ?>"
                                               data-project-has-experts-notes =""
                                               href="javascript:;">
                                                <i class="fa fa-plus"></i> Add Notes
                                            </a>
                                        </div>

                                        <div class="clearfix"></div>

                                    </div>

                                </div>

                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                        <?php } ?>
                        <div class="clearfix"></div>
                        <div class="whole-btnarea text-center" style="padding:15px; margin: 0 34px; border-top: 1px solid #ddd; ">
                            <button style="width:25%;" type="button" class="btn green btn-lg sendBio">Send Bio</button>
                        </div>
                    <?php } else { ?>
                       <b style="display: block;text-align: center;margin-bottom: 30px;">No Experts Shortlisted</b>
                     <?php } ?>

<!-- ADD PROJECT NOTES MODEL -->
<div class="modal fade" id="compliance_list" tabindex="-1" role="basic" aria-hidden="true"
     style="z-index: 999999999 !important;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Warning</h4>
            </div>
            <div class="modal-body">
                <div class="form-body appendHrml">
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn red submit-btn" data-dismiss="modal"> No</button>
                <button type="button" class="btn green sendBio submit-btn" data-status='1'> Yes</button>
            </div>
        </div>
        </form>
    </div>
</div>
<!-- /.modal-content -->