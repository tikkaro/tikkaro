<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="tabbable-line boxless tabbable-reversed">
            <?php $this->load->helper('common_tab_helper'); ?>
            <?= projectMenu('interest', $projectId) ?>
            <div class="tab-content">
                <?php $this->load->helper('comman_data_helper'); ?>
                <?= projectData($projectData) ?>



                <div class="portlet light portlet-fit portlet-datatable bordered">

                    <div class="portlet-title">
                        <div class="col-md-3" style="padding-left: 0">
                            <div class="caption">
                                <i class="icon-equalizer font-blue-hoki"></i>
                                <span class="caption-subject font-green-haze sbold uppercase"> Angles of interest </span>
                            </div>
                        </div>

                    </div>
                    <div class="portlet-body">

                        <div class="angleInterest col-md-12">

                        </div>

                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-12">
                                    <div class="col-md-4">
                                        <select class="form-control select2me Interest" id="Interest" name="interest" data-placeholder="Select Angles">
                                            <option value=""></option>
                                            <option value="Former"> Former </option>
                                            <option value="Competitor">  Competitor </option>
                                            <option value="Customer"> Customer </option>
                                            <option value="Distributor"> Distributor </option>
                                            <option value="Industry expert"> Industry expert </option>
                                        </select>
                                    </div>

                                    <div class="col-md-3">
                                        <a class="btn btn-circle btn-primary  btn-sm addInterest" href="javascript:;">
                                            <i class="fa fa-edit"></i> Add Angles 
                                        </a>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>

                        <div class="clearfix"></div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
