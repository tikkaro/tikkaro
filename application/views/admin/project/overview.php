<style>
    .modal-open .colorpicker, .modal-open .datepicker, .modal-open .daterangepicker {
        z-index: 1005599999 !important;
    }

    .select2-container--bootstrap {
        width: 100% !important;
    }
</style>
<script>
    $(document).ready(function () {
        var country = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            prefetch: {
                url: adminurl + 'project/getCountry',
                filter: function (list) {
                    return $.map(list, function (country) {
                        return {name: country};
                    });
                }
            }
        });
        country.initialize();

        $('#var_country_interest').tagsinput({
            typeaheadjs: {
                name: 'countryname',
                displayKey: 'name',
                valueKey: 'name',
                source: country.ttAdapter()
            }
        });

//        var regionsUrl = adminurl + 'project/getRegions';
        var regions = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            prefetch: {
                url: adminurl + 'project/getRegions',
                filter: function (list) {
                    return $.map(list, function (regions) {
                        return {name: regions};
                    });
                }
            }
        });
        regions.initialize();

        $('#var_regions_interest').tagsinput({
            typeaheadjs: {
                name: 'regionsname',
                displayKey: 'name',
                valueKey: 'name',
                source: regions.ttAdapter()
            }
        });

        var company = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            prefetch: {
                url: adminurl + 'project/getCompany',
                filter: function (list) {
                    return $.map(list, function (company) {
                        return {name: company};
                    });
                }
            }
        });
        company.initialize();

        $('#var_companies_interest').tagsinput({
            typeaheadjs: {
                name: 'companyname',
                displayKey: 'name',
                valueKey: 'name',
                source: company.ttAdapter()
            }
        });
    });
</script>

<?php
$projectStatus = json_decode(PROJECT_STATUS);
    foreach ($projectStatus as $key => $value){
        $ProjectStatus[] = array(
                'value' => $key,'text'=> $value,
        );
    }

$userArray = array();
foreach ($userdata as $value) {
    array_push($userArray, array('value' => $value['id'], 'text' => $value['var_fname'].' '.$value['var_lname']));
}
?>
<script type="text/javascript">
    var project_status = '<?php echo json_encode($ProjectStatus)?>';
    var client_user = '<?php echo json_encode($userArray)?>';

    var client_anonymous_status = [];
    client_anonymous_status.push({value: 'YES', text: 'Yes'}, {value: 'NO', text: 'No'});

    var target_anonymous_status = [];
    target_anonymous_status.push({value: 'YES', text: 'Yes'}, {value: 'NO', text: 'No'});

</script>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="tabbable-line boxless tabbable-reversed">
            <?php $this->load->helper('common_tab_helper'); ?>
            <?= projectMenu('overview', $projectId) ?>
            <div class="tab-content">
                <?php $this->load->helper('comman_data_helper'); ?>
                <?= projectData($projectData) ?>
                <div class="portlet light portlet-fit portlet-datatable bordered">

                    <div class="portlet-title">
                        <div class="col-md-3" style="padding-left: 0">
                            <div class="caption">
                                <i class="icon-equalizer font-blue-hoki"></i>
                                <span class="caption-subject font-green-haze sbold uppercase"> Overview</span>
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Client:</label>
                                    <div class="col-md-8">
                                        <label class="control-label">
                                            <a href="<?php echo admin_url().'client/overview/'.$projectData['fk_client']?>"><?= $projectData['var_company_name']; ?></a>
                                        </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-2">User:</label>
                                    <div class="col-md-8">
                                        <label class="control-label projectOverview">
                                            <a href="javascript:;" class="inline-editing-trigger"
                                               data-title="Select User"
                                               data-table="client_has_project"
                                               data-required="true"
                                               data-field="fk_user"
                                               data-type="select"
                                               data-input-class="form-control cuswidth"
                                               data-value="<?= $projectData['fk_user']; ?>"
                                               data-value-source="client_user"
                                               data-id="<?= $projectData['id']; ?>"><?= $projectData['user_fname'] . ' ' . $projectData['user_lname']; ?>
                                            </a>
                                        </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
<!--                                <div class="form-group">-->
<!--                                    <label class="control-label col-md-2">User:</label>-->
<!--                                    <div class="col-md-8">-->
<!--                                        <label class="control-label">-->
<!--                                            <a href="--><?php //echo admin_url().'client/user-bio/'.$projectData['fk_user']?><!--">-->
<!--                                            --><?//= $projectData['user_fname'] . ' ' . $projectData['user_lname']; ?>
<!--                                            </a>-->
<!--                                        </label>-->
<!--                                    </div>-->
<!--                                    <div class="clearfix"></div>-->
<!--                                </div>-->
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Project Name:</label>
                                    <div class="col-md-8">
                                        <label class="control-label projectOverview">
                                            <a href="javascript:;" class="inline-editing-trigger"
                                               data-table="client_has_project"
                                               data-field="var_project_name"
                                               data-input-class="form-control cuswidth"
                                               data-id="<?= $projectData['id'] ?>"
                                               data-title="Enter Projectname" data-placeholder="Enter Project Name" >

                                                <?= $projectData['var_project_name'] ?> </a>
                                        </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Project Description:</label>
                                    <div class="col-md-8">
                                        <label class="control-label projectOverview">
                                            <a href="javascript:;" class="inline-editing-trigger"
                                               data-table="client_has_project"
                                               data-field="txt_description"
                                               data-type="textarea"
                                               data-input-class="form-control cuswidth"
                                               data-id="<?= $projectData['id'] ?>"
                                               data-title="Enter Description" data-placeholder="Enter Description"><?= $projectData['txt_description'] ?></a>
                                        </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Project Status:</label>
                                    <div class="col-md-8">
                                        <label class="control-label projectOverview">
                                            <a href="javascript:;" class="inline-editing-trigger"
                                               data-table="client_has_project"
                                               data-field="enum_status"
                                               data-type="select"
                                               data-id="<?= $projectData['id']; ?>"
                                               data-value="<?= $projectData['enum_status']; ?>"
                                               data-value-source="project_status"
                                               data-input-class="form-control cuswidth"
                                               data-title="Enter Status"><?= getProjectStatus($projectData['enum_status']); ?></a>
                                        </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Number of Calls Required:</label>
                                    <div class="col-md-8">
                                        <label class="control-label projectOverview">
                                            <a href="javascript:;" class="inline-editing-trigger"
                                               data-table="client_has_project"
                                               data-field="int_no_of_call_required"
                                               data-id="<?= $projectData['id'] ?>"
                                               data-input-class="form-control cuswidth"
                                               data-title="Enter No of Calls Required" data-placeholder="Enter Number of Calls Required"><?= $projectData['int_no_of_call_required'] ?></a>
                                        </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Target Company:</label>
                                    <div class="col-md-8">

                                        <label class="control-label projectOverview">
                                            <a href="javascript:;" class="inline-editing-trigger"
                                               data-table="client_has_project"
                                               data-field="var_target_company"
                                               data-id="<?= $projectData['id'] ?>"
                                               data-input-class="form-control cuswidth"
                                               data-title="Enter Target Company"  data-placeholder="Enter Target Company"><?= $projectData['var_target_company'] ?></a>
                                        </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Client Anonymous?</label>
                                    <div class="col-md-8">
                                        <label class="control-label projectOverview">
                                            <a href="javascript:;" class="inline-editing-trigger"
                                               data-table="client_has_project"
                                               data-field="enum_client_anonymous"
                                               data-type="select"
                                               data-value="<?= $projectData['enum_client_anonymous']; ?>"
                                               data-value-source="client_anonymous_status"
                                               data-id="<?= $projectData['id'] ?>"
                                               data-input-class="form-control cuswidth"
                                               data-title="Select Client Anonymous"><?= $projectData['enum_client_anonymous'] ?></a>
                                        </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Target Anonymous?</label>
                                    <div class="col-md-8">
                                        <label class="control-label projectOverview">
                                            <a href="javascript:;" class="inline-editing-trigger"
                                               data-table="client_has_project"
                                               data-field="enum_target_anonymous"
                                               data-type="select"
                                               data-value-source="target_anonymous_status"
                                               data-input-class="form-control cuswidth"
                                               data-value="<?= $projectData['enum_target_anonymous']; ?>"
                                               data-id="<?= $projectData['id'] ?>"
                                               data-title="Select Target Anonymous"><?= $projectData['enum_target_anonymous'] ?></a>
                                        </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Country of Interest:</label>
                                    <div class="col-md-8">
                                        <label class="control-label">
                                            <?php
                                                $projectCountryName = explode(',', $projectCountry[0]['countryname']);
                                                $projectCountryName = implode(' , ', $projectCountryName);
                                            ?>
                                            <a href="javascript:;"
                                               class="cusComapnine editable editable-click"> <?= (!empty($projectCountryName)) ? $projectCountryName: '<span style="color: #DD1144"><i>Empty</i></span>' ?></a>
                                        </label>
                                        <div class="input-icon Tagstoshow projectOverview" style="display: none;">
                                            <input type="text" id="var_country_interest" name="var_country_interest"
                                                   placeholder="Enter Country of Interest" data-role="tagsinput"
                                                   class="form-control" value="<?=  $projectCountry[0]['countryname']; ?>">
                                            <div class="editable-buttons cusCompanyTags">
                                                <button type="submit" class="btn blue editable-submit updateTags"
                                                        data-table="project_has_country"
                                                        data-id="<?= $projectData['id']; ?>"><i class="fa fa-check"></i>
                                                </button>
                                                <button type="button" class="btn default editable-cancel cancleTags"><i
                                                            class="fa fa-times"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Regions of Interest:</label>
                                    <div class="col-md-8">

                                        <label class="control-label">
                                            <?php
                                            $projectRegionsName = explode(',', $projectRegions[0]['regionsname']);
                                            $projectRegionsName = implode(' , ', $projectRegionsName);
                                            ?>
                                            <a href="javascript:;"
                                               class="cusComapnine editable editable-click"> <?= (!empty($projectRegionsName)) ? $projectRegionsName : '<span style="color: #DD1144"><i>Empty</i></span>' ?></a>
                                        </label>
                                        <div class="input-icon Tagstoshow projectOverview" style="display: none">
                                            <input type="text" id="var_regions_interest" name="var_regions_interest"
                                                   placeholder="Enter Regions of interest" data-role="tagsinput"
                                                   class="form-control"
                                                   value="<?= $projectRegions[0]['regionsname'] ?>">
                                            <div class="editable-buttons cusCompanyTags">
                                                <button type="submit" class="btn blue editable-submit updateTags"
                                                        data-table="project_has_regions"
                                                        data-id="<?= $projectData['id']; ?>"><i class="fa fa-check"></i>
                                                </button>
                                                <button type="button" class="btn default editable-cancel cancleTags"><i
                                                            class="fa fa-times"></i></button>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Companies of Interest:</label>
                                    <div class="col-md-8">
                                        <label class="control-label">
                                            <?php
                                            $projectCompanyName = explode(',', $projectCompany[0]['companyname']);
                                            $projectCompanyName = implode(' , ', $projectCompanyName);
                                            ?>
                                            <a href="javascript:;"
                                               class="cusComapnine editable editable-click"> <?= (!empty($projectCompanyName)) ? $projectCompanyName : '<span style="color: #DD1144"><i>Empty</i></span>' ?></a>
                                        </label>
                                            <div class="input-icon Tagstoshow projectOverview" style="display: none;">
                                            <input type="text" id="var_companies_interest" name="var_companies_interest"
                                                   placeholder="Enter Companies of Interest" data-role="tagsinput"
                                                   class="form-control"
                                                   value="<?= $projectCompany[0]['companyname'] ?>">
                                            <div class="editable-buttons cusCompanyTags">
                                                <button type="submit" class="btn blue editable-submit updateTags"
                                                        data-table="project_has_company"
                                                        data-id="<?= $projectData['id']; ?>"><i class="fa fa-check"></i>
                                                </button>
                                                <button type="button" class="btn default editable-cancel cancleTags"><i
                                                            class="fa fa-times"></i></button>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Industry:</label>
                                    <div class="col-md-8">
                                        <label class="control-label projectOverview">
                                            <a href="javascript:;" class="inline-editing-trigger"
                                               data-table="client_has_project"
                                               data-field="var_industry"
                                               data-input-class="form-control cuswidth"
                                               data-id="<?= $projectData['id'] ?>"
                                               data-title="Enter Industry" data-placeholder="Enter Industry"><?= $projectData['var_industry'] ?> </a>
                                        </label>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
<!--                        <div class="row">-->
<!--                            <div class="col-md-12">-->
<!--                                <div class="form-group">-->
<!--                                    <label class="control-label col-md-2">Deadline Date:</label>-->
<!--                                    <div class="col-md-8">-->
<!--                                        <label class="control-label projectOverview">-->
<!--                                            <a href="javascript:;" class="inline-editing-trigger"-->
<!--                                               data-table="client_has_project"-->
<!--                                               data-field="dt_deadline"-->
<!--                                               data-type="date"-->
<!--                                               data-input-class="cusDate"-->
<!--                                               data-id="--><?//= $projectData['id'] ?><!--"-->
<!--                                               data-title="Enter Deadline date">--><?//= (!empty($projectData['dt_deadline']))? date('d/m/Y', strtotime($projectData['dt_deadline'])):''; ?><!-- </a>-->
<!--                                        </label>-->
<!--                                    </div>-->
<!--                                    <div class="clearfix"></div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label col-md-2">Deadline Date:</label>
                                    <div class="col-md-8">
                                        <label class="control-label">
                                            <a href="javascript:;"
                                               class="expertsComplianceDate editable editable-click"> <?= (!empty($projectData['dt_deadline']))? date('d/m/Y', strtotime($projectData['dt_deadline'])) : '<span style="color: #DD1144"><i>Empty</i></span>'?></a>
                                        </label>
                                        <div class="input-icon dateToshow expertsBio" style="display: none; top:-15px;">
                                            <input type="text" id="compliance_date" name="compliance_date"
                                                   placeholder="Enter Deadline Date"
                                                   class="form-control date-picker" data-date-format="dd/mm/yyyy"
                                                   value="<?= (!empty($projectData['dt_deadline']))? date('d/m/Y', strtotime($projectData['dt_deadline'])):''; ?>">
                                            <div class="editable-buttons cusDateValue">
                                                <button type="submit"
                                                        class="btn blue editable-submit updateDates"
                                                        data-table="client_has_project" data-id="<?= $projectData['id']; ?>">
                                                    <i class="fa fa-check"></i></button>
                                                <button type="button"
                                                        class="btn default editable-cancel cancelDates"><i
                                                            class="fa fa-times"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
</div>