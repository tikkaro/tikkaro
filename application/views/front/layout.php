<!DOCTYPE html>
<html>
    <?php
    $this->load->view('front/includes/header');
    $bodyclass = '';
    if ($pageflag == 'Home') {
        $bodyclass = 'home-bg';
    }else if($pageflag == 'Stores' || $pageflag == 'Summary'){
        $bodyclass = 'select-stores-container';
    }
    ?>

    <body class="<?= $bodyclass; ?>">

        <?php $this->load->view('front/includes/body_header'); ?>

        <div class="page-container">
            <?php $this->load->view($page); ?>
        </div>

        <?php $this->load->view('front/includes/body_modals'); ?>

        <?php $this->load->view('front/includes/footer'); ?>

    </body>
</html>