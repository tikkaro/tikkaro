<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <?php
    $title = empty($var_meta_title) ? get_project_name() : $var_meta_title . ' | ' . get_project_name();
    $description = empty($var_meta_description) ? get_project_name() : $var_meta_description . ' | ' . get_project_name();
    $keywords = empty($var_meta_keyword) ? get_project_name() : $var_meta_keyword . ' | ' . get_project_name();
    ?>
    <title><?= $title; ?></title>
    <meta content="<?= $description; ?>" name="description"/>
    <meta content="<?= $keywords; ?>" name="author"/>

    <link href='https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css' rel='stylesheet'
          type='text/css'/>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic'
          rel='stylesheet' type='text/css'/>
    <link href="https://fonts.googleapis.com/css?family=Fira+Sans" rel="stylesheet">

    <link rel="stylesheet" type="text/css" href="<?= base_url(); ?>public/front/cmspages/css/style.css">

    <?php
    if (!empty($css)) {
        foreach ($css as $value) {
            ?>
            <link rel="stylesheet"
                  href="<?= base_url(); ?>public/front/cmspages/css/<?php echo $value; ?>?v=<?= FRONT_REVISED_VERSION; ?>"/>
            <?php
        }
    }
    ?>
    <?php
    if (!empty($css_plugin)) {
        foreach ($css_plugin as $value_plugin) {
            ?>
            <link rel="stylesheet"
                  href="<?= base_url(); ?>public/front/cmspages/plugins/<?php echo $value_plugin; ?>?v=<?= FRONT_REVISED_VERSION; ?>"/>
            <?php
        }
    }
    ?>

    <script type="text/javascript">
        var baseurl = "<?php echo base_url(); ?>";
        var adminurl = "<?php echo admin_url(); ?>";
        var experturl = "<?php echo expert_url(); ?>";
        var customerurl = "<?php echo customer_url(); ?>";
        var getbaseurl = "<?php getbaseURL(); ?>";
    </script>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.0/jquery.validate.min.js"></script>
    <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.0/additional-methods.js"></script>

    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-97558040-1', 'auto');
        ga('send', 'pageview');

    </script>

    <style>
        .hed-btn > ul {
            float: left;
            width: 77%;
        }

        .hed-btn li {
            padding: 5px 15px;
        }

        .contact-form {
            float: none;
            width: 18%;
            margin: 0 auto;
        }

        .contact-form .form-group input, .contact-form .form-group textarea {
            width: 100%;
        }

        a.register_links {
            color: #fff;
            text-decoration: none;
            font-size: 14px;
        }

        .hed-btn .logo-part img {
            width: 145px;
        }

        .banner-wrap {
            bottom: 0;
            height: 200px;
            left: 0;
            margin: auto;
            position: absolute;
            right: 0;
            top: 0;
            vertical-align: middle;
            width: 220px;
        }

        .banner {
            height: 100vh;
        }

        .form-group input, .form-group textarea {
            width: 100%;
            margin: 0;
        }
    </style>
</head>

<body class="loginpage">
<div class="container">

    <!--.............banner.............-->
    <div class="banner">

        <div class="banner-bg"></div>
        <div class="navbar">
            <div class="hed-btn">
                <div class="logo-part">
                    <a href="<?= base_url(); ?>"><img
                                src="<?= base_url(); ?>public/front/cmspages/images/pro-sapient/proSapientLogo.svg"/></a>
                </div>
                <ul>
                    <li class="menu-item">
                        <a href="<?= base_url(); ?>">
                            <img src="<?= base_url(); ?>public/front/cmspages/images/pro-sapient/home.png"
                                 style="width:27px;"/>
                            Home
                        </a>
                    </li>
                    <li class="menu-item">
                        <a href="<?= base_url(); ?>#services">
                            <img src="<?= base_url(); ?>public/front/cmspages/images/pro-sapient/services-icon.png"
                                 style="width:34px;"/>
                            Services
                        </a>
                    </li>
                    <li class="menu-item">
                        <a href="<?= base_url(); ?>contact">
                            <img src="<?= base_url(); ?>public/front/cmspages/images/pro-sapient/contact.png"/>
                            Contact
                        </a>
                    </li>
                    <li class="menu-item">
                        <a href="<?= base_url(); ?>customer/login">
                            <img src="<?= base_url(); ?>public/front/cmspages/images/pro-sapient/login-icon.png"
                                 style="width:18px;"/>
                            Login
                        </a>
                    </li>
                </ul>
                <div class="clear"></div>
            </div>
        </div>

        <!-- BEGIN PAGE -->
        <?php $this->load->view($page); ?>
        <!-- END PAGE -->

    </div>
</div>

<?php
if (!empty($js_plugin)) {
    foreach ($js_plugin as $value) {
        ?>
        <script src="<?= base_url(); ?>public/front/cmspages/plugins/<?php echo $value ?>?v=<?= FRONT_REVISED_VERSION; ?>"
                type="text/javascript"></script>
        <?php
    }
}
?>

<!-- BEGIN THEME GLOBAL SCRIPTS -->

<script src="<?= base_url(); ?>public/front/js/app.js?v=<?= FRONT_REVISED_VERSION; ?>" type="text/javascript"></script>
<script src="<?= base_url(); ?>public/front/js/common_function.js?v=<?= FRONT_REVISED_VERSION; ?>"
        type="text/javascript"></script>

<!-- END THEME GLOBAL SCRIPTS -->

<?php
if (!empty($js)) {
    foreach ($js as $value) {
        ?>
        <script src="<?php echo base_url(); ?>public/front/js/customer/<?php echo $value ?>?v=<?= FRONT_REVISED_VERSION; ?>"
                type="text/javascript"></script>
        <?php
    }
}
?>

<script>
    jQuery(document).ready(function () {
        App.init();
        <?php
        if (!empty($init)) {
            foreach ($init as $value) {
                echo $value . ';';
            }
        }
        ?>
    });
</script>

</body>
</html>
