<style>
    .child-accordian-panel-body {
        margin-bottom: 0px;
        padding: 15px 0 !important;
        border-bottom: 1px solid #ccc;
    }

    .child-accordian-panel-body:last-child {
        /*margin-bottom: 0 !important;*/
        /*border-bottom: 0 !important;*/
        /*padding-bottom: 0 !important;*/
    }

    .contain-main .panel .panel-body.child-accordian-panel-body .link-box {
        margin-top: 0;
    }

    .editableTextarea {
        background: transparent;
        border: none;
        width: 100%;
        display: block;
        overflow: hidden;
    }
</style>
<div class="row">
    <div class="col-md-12">
        <div class="panel-group" id="">

            <?php if (!empty($allProjects)) {
                for ($i = 0; $i < count($allProjects); $i++) {
                    ?>

                    <div class="panel panel-default" style="margin-bottom: 15px;">
                        <div class="panel-heading">
                            <h3>
                                <a data-project-id="#project_detail_<?= $i + 1; ?>" data-toggle="collapse" data-parent="#accordion"
                                   href="#collapse_<?= $i + 1; ?>"
                                   style="text-transform: capitalize;" class="my-projects-panel"><?= $allProjects[$i]['var_project_name']; ?></a>
                                <label class="date">
                                    <span>Date:</span>
                                    <span><?= date('d/m/Y', strtotime($allProjects[$i]['created_at'])); ?></span>
                                </label>
                            </h3>
                        </div>
                        <div id="collapse_<?= $i + 1; ?>"
                             class=" panel-collapse collapse <?= ($i == 0) ? 'in' : ''; ?>">
                            <div class="panel-body">
                                <div class="panel-body-inner enrolls">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="des-box">
                                                <p style="min-height: 21px;">
                                                    <textarea style="line-height: 20px;" name="ProjectDesc" id="ProjectDesc"
                                                              class="editableTextarea ProjectDetails autoExpand"
                                                              value="<?= $allProjects[$i]['txt_description']; ?>"
                                                              data-edit-field="ProjectDesc"
                                                              data-value="<?= $allProjects[$i]['txt_description']; ?>"
                                                              data-id="<?= $allProjects[$i]['id']; ?>"><?= $allProjects[$i]['txt_description']; ?></textarea>
                                                </p>
                                                <!--                                                <a href="#" class="edit-btn"><i class="fa fa-pencil"-->
                                                <!--                                                                                aria-hidden="true"></i></a>-->
                                            </div>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="projectDetailsArea" style="margin: 0 0 15px; <?= ($i == 0) ? 'display:block' : 'display:none'; ?>" id="project_detail_<?= $i + 1; ?>">
                        <div class="panel-group" id="accordion<?= $i + 1; ?>"
                             style="margin-top:15px; margin-bottom: 0;">


                            <div class="panel panel-default">
                                <div class="panel-heading" style="padding: 4px 15px;">
                                    <h3 style="font-size: 13px">
                                        <!--                                                    <a data-toggle="collapse" data-parent="#accordion-->
                                        <?//= $i + 1; ?><!--"-->
                                        <!--                                                       href="#shortlisted-->
                                        <?//= $i + 1; ?><!--">Shortlisted-->
                                        <!--                                                        Experts:</a>-->
                                        <a data-toggle="collapse" data-parent="#accordion<?= $i + 1; ?>"
                                           href="javascript:;">Shortlisted
                                            Experts:</a>
                                    </h3>
                                </div>
                                <div id="shortlisted<?= $i + 1; ?>" class="panel-collapse collapse in"
                                     style="<?= (!empty($allProjects[$i]['shortListedExperts'])) ? 'background: #f5f5f5;' : 'background: #ffffff;' ; ?>">

                                    <div class="panel-body-inner enroll">
                                        <?php if (!empty($allProjects[$i]['shortListedExperts'])) {
                                            for ($j = 0; $j < count($allProjects[$i]['shortListedExperts']); $j++) {
                                                ?>
                                                <div class="panel-body child-accordian-panel-body">

                                                    <div class="row" style="margin: 0;">
                                                        <div class="col-md-6">
                                                            <div class="exp-list">
                                                                <div class="img-box">
                                                                    <img src="<?= base_url(); ?>public/front/images/user.png"
                                                                         alt=""/>
                                                                </div>
                                                                <div class="detail-box">
                                                                    <?php
                                                                    if ($allProjects[$i]['shortListedExperts'][$j]['Geography']) {
                                                                        $geography = ' (' . $allProjects[$i]['shortListedExperts'][$j]['Geography'] . ')';
                                                                    } else {
                                                                        $geography = '';
                                                                    }
                                                                    ?>
                                                                    <h3 style="font-weight: normal;"><?= $allProjects[$i]['shortListedExperts'][$j]['ExpertName'] . $geography; ?></h3>
                                                                    <?php
                                                                    if ($allProjects[$i]['shortListedExperts'][$j]['Position'] && $allProjects[$i]['shortListedExperts'][$j]['CompanyName']) {
                                                                        $companyPosition = $allProjects[$i]['shortListedExperts'][$j]['Position'] . ' at ' . $allProjects[$i]['shortListedExperts'][$j]['CompanyName'];
                                                                    } else {
                                                                        $companyPosition = '-';
                                                                    }
                                                                    ?>
                                                                    <h2><?= $companyPosition; ?></h2>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="link-box">
                                                                <?php $urlString = base64_encode(json_encode(['expertsHasProjectId' => $allProjects[$i]['shortListedExperts'][$j]["id"], 'expertsId' => $allProjects[$i]['shortListedExperts'][$j]['ExpertsId']]));
                                                                if ($allProjects[$i]['shortListedExperts'][$j]['enum_status'] == 'BIOS_SENT') {
                                                                    ?>
                                                                    <a href="<?php echo base_url() . 'scheduling/schedule?data=' . $urlString ?>">Schedule</a>
                                                                <?php } else { ?>
                                                                    <span style="color: #662d91;"><?= $shortlisted_status->$allProjects[$i]['shortListedExperts'][$j]['enum_status']; ?></span>
                                                                <?php } ?>
                                                                <a href="<?= expert_url(); ?>bio/<?= $allProjects[$i]['shortListedExperts'][$j]['ExpertsId']; ?>">Bio</a>
                                                            </div>
                                                        </div>
                                                    </div>

                                                </div>

                                            <?php }
                                        } else { ?>
                                            <h3 class="Notfound" style="font-size: 13px; font-weight: bold;"> You have no Shortlisted Experts</h3>
                                        <?php } ?>

                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default hide">
                                <div class="panel-heading">
                                    <h3>
                                        <a data-toggle="collapse" data-parent="#accordion<?= $i + 1; ?>"
                                           href="#conferencecalls_<?= $i + 1; ?>">Conference
                                            Calls:</a>
                                    </h3>
                                </div>
                                <div id="conferencecalls_<?= $i + 1; ?>" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="panel-body-inner">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <div class="exp-list no-img">
                                                        <div class="detail-box">
                                                            <h2>Senior Buyer Macy’s</h2>
                                                            <span>20/06/2016</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-7">
                                                    <div class="link-box">
                                                        <a href="#">Replay</a>
                                                        <a href="#">Transcription</a>
                                                        <a href="#">Reschedule</a>
                                                        <a href="#">Rate</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel-body-inner">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <div class="exp-list no-img">
                                                        <div class="detail-box">
                                                            <h2>Buyer Sports Direct</h2>
                                                            <span>10/06/2016</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-7">
                                                    <div class="link-box">
                                                        <a href="#">Replay</a>
                                                        <a href="#">Transcription</a>
                                                        <a href="#">Reschedule</a>
                                                        <a href="#">Rate</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="panel panel-default hide">
                                <div class="panel-heading">
                                    <h3>
                                        <a data-toggle="collapse" data-parent="#accordion<?= $i + 1; ?>"
                                           href="#expert_surveys<?= $i + 1; ?>">Expert
                                            Surveys:</a>
                                    </h3>
                                </div>
                                <div id="expert_surveys<?= $i + 1; ?>" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <div class="panel-body-inner">
                                            <div class="row">
                                                <div class="col-md-5">
                                                    <div class="exp-list no-img">
                                                        <div class="detail-box">
                                                            <h2>Sneaker Trends</h2>
                                                            <span>20/06/2016</span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-7">
                                                    <div class="link-box">
                                                        <a href="#">Get Data</a>
                                                        <a href="#">Add Participants</a>
                                                        <a href="#">Repeat</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php }
            } else { ?>
                <h3 class="Notfound">You have no Projects</h3>
            <?php } ?>
        </div>
    </div>
</div>