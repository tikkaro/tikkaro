<style>
    .login-form .form-group.has-error input {
        border: 2px solid #FF1717;
    }

    .alert-msg {
        display: block;
        text-align: center;
        margin: 10px auto;
        font-size: 12px;
    }

    .alert-msg.alert-danger {
        color: #FF1717;
    }

    .alert-msg.alert-success {
        color: darkgreen;
    }

    .alert-msg.alert-warning {
        color: #00A0D1;
    }
</style>


<div class="banner-wrap login-form">
    <form class="frmlogin" method="post">

        <div class="alert-danger alert-msg"></div>
        <div class="alert-success alert-msg"></div>
        <div class="alert-warning alert-msg"></div>

        <div class="form-group ">
            <input type="text" name="username" id="username" class="form-control border-rad20" placeholder="Email"/>
        </div>
        <div class="form-group " style="margin-bottom:0;">
            <input type="password" name="password" id="password" class="form-control border-rad20"
                   placeholder="Password"/>
        </div>
        <div class="form-group" style="text-align: center;">
<!--            <a href="javascript:;" class="register_links">Register</a> <span-->
<!--                    style="color:#fff;">|</span>-->
<!--            <a href="javascript:;" class="register_links">Forgot my password</a>-->
        </div>
        <div class="form-group border-rad10 login-button" style="text-align: center;">
            <input type="submit" class="btn border-rad20" value="Login"
                   style="color: #560f7c; cursor: pointer; width: 100%;font-weight: 600;font-size: 20px !important;line-height: 13px;font-family: 'Open Sans', sans-serif "/>
        </div>
        <div class="clear"></div>
    </form>
</div>