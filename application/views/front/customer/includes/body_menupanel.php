<div class="col-md-6 col-sm-6 res-menu">
    <div class="menu clearfix">
        <a href="javascript:;" class="menu-tog"><i class="fa fa-angle-double-right" aria-hidden="true"></i></a>
        <ul class="nav navbar-nav">
            <li><a href="<?= customer_url(); ?>my-projects"
                   class="<?= ($menuPage == 'my_projects') ? "active" : ""; ?>"><i class="fa fa-flask"
                                                                                   aria-hidden="true"></i><span>My Projects</span></a>
            </li>
            <li><a href="<?= customer_url(); ?>request-expert"
                   class="<?= ($menuPage == 'request_expert') ? "active" : ""; ?>"><i class="fa fa-flag"
                                                                                      aria-hidden="true"></i><span>Request Expert</span></a>
            </li>
            <li><a href="#" class="<?= ($menuPage == 'my_survey') ? "active" : ""; ?>"><i class="fa fa-file-text-o"
                                                                                          aria-hidden="true"></i><span>My Survey</span></a>
            </li>
        </ul>
    </div>
</div>
<div class="col-md-3 col-sm-3 col-xs-6">
    <div class="user-link">
        <a href="#"><img src="<?= base_url(); ?>public/front/images/user.png" alt=""/></a>

        <div class="drop-down-links">
            <ul>
                <li>
                    <a href="<?= customer_url('logout'); ?>">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>