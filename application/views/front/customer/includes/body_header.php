<header>
    <div class="container">
        <div class="row">
            <div class="col-md-3 col-sm-3 col-xs-6">
                <div class="logo">
                    <a href="<?= customer_url(); ?>"><img src="<?= base_url(); ?>public/front/images/logo.png" alt=""/></a>
                </div>
            </div>
            <!-- BEGIN BODY HEADER -->
            <?php
            if ($pageFlag == 'BioGlobal' || $pageFlag == 'SelectAvailability') {
                if (isCustomerLogin()) {
                    $this->load->view('front/customer/includes/body_menupanel');
                } else if (isExpertLogin()) {
                    $this->load->view('front/expert/includes/body_menupanel');
                }
            } else {
                $this->load->view('front/customer/includes/body_menupanel');
            }
            ?>
            <!-- END BODY HEADER -->
        </div>
    </div>
</header>