<head>
    <meta charset="utf-8">
    <?php
    $title = empty($var_meta_title) ? get_project_name() : $var_meta_title . ' | ' . get_project_name();
    $description = empty($var_meta_description) ? get_project_name() : $var_meta_description . ' | ' . get_project_name();
    $keywords = empty($var_meta_keyword) ? get_project_name() : $var_meta_keyword . ' | ' . get_project_name();
    ?>
    <title><?= $title; ?></title>
    <meta content="<?= $description; ?>" name="description" />
    <meta content="<?= $keywords; ?>" name="author" />

    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <link rel="icon" href="<?= base_url(); ?>public/front/images/favicon.ico">

    <!-- Bootstrap core CSS -->
    <link href="<?= base_url(); ?>public/front/plugins/bootstrap/css/bootstrap.min.css?v=<?= FRONT_REVISED_VERSION; ?>" rel="stylesheet">
    <link href="<?= base_url(); ?>public/front/css/font-awesome.min.css?v=<?= FRONT_REVISED_VERSION; ?>" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,700" rel="stylesheet"> 
    <link href="<?= base_url(); ?>public/front/css/style.css?v=<?= FRONT_REVISED_VERSION; ?>" rel="stylesheet">
    <link href="<?= base_url(); ?>public/front/css/responsive.css?v=<?= FRONT_REVISED_VERSION; ?>" rel="stylesheet">
    <!-- Custom styles for this template -->

    <?php
    if (!empty($css)) {
        foreach ($css as $value) {
            ?>
            <link rel="stylesheet"
                  href="<?= base_url(); ?>public/front/css/<?php echo $value; ?>?v=<?= FRONT_REVISED_VERSION; ?>"/>
                  <?php
              }
          }
          ?>
          <?php
          if (!empty($css_plugin)) {
              foreach ($css_plugin as $value_plugin) {
                  ?>
            <link rel="stylesheet"
                  href="<?= base_url(); ?>public/front/plugins/<?php echo $value_plugin; ?>?v=<?= FRONT_REVISED_VERSION; ?>"/>
                  <?php
              }
          }
          ?>

    <script type="text/javascript">
        var baseurl = "<?php echo base_url(); ?>";
        var adminurl = "<?php echo admin_url(); ?>";
        var experturl = "<?php echo expert_url(); ?>";
        var customerurl = "<?php echo customer_url(); ?>";
        var getbaseurl = "<?php getbaseURL(); ?>";
    </script>           

    <!-- Just for debugging purposes. Don't actually copy these 2 lines! -->
    <!--[if lt IE 9]><script src="../../assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>