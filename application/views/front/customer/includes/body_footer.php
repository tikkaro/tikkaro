<!-- Modal Area -->
<div id="messagePopup" class="modal fade message-popup" role="dialog">
    <div class="modal-dialog">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-body" id="messagePopupContent">
                <div class="cuspopupcontent">
                    <div class="popupiconarea">
                        <img id="popup-icon-img" src="" height="60">
                    </div>
                    <p id="Popupmessage"></p>
                </div>
            </div>
            <div class="modal-footer">
                <div class="popup-submit-btn">
                    <button type="button" data-dismiss="modal">Got it</button>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal Area -->
