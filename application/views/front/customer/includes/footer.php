<!-- Placed at the end of the document so the pages load faster -->
<script type="text/javascript"
        src="<?= base_url(); ?>public/front/plugins/jquery/jquery.min.js?v=<?= FRONT_REVISED_VERSION; ?>"></script>
<script type="text/javascript"
        src="<?= base_url(); ?>public/front/plugins/jquery/jquery-migrate.min.js?v=<?= FRONT_REVISED_VERSION; ?>"></script>
<script type="text/javascript"
        src="<?= base_url(); ?>public/front/plugins/bootstrap/js/bootstrap.min.js?v=<?= FRONT_REVISED_VERSION; ?>"></script>

<script type="text/javascript"
        src="<?= base_url(); ?>public/front/plugins/jquery-validation/js/jquery.validate.min.js?v=<?= FRONT_REVISED_VERSION; ?>"></script>
<script type="text/javascript"
        src="<?= base_url(); ?>public/front/plugins/jquery.form.min.js?v=<?= FRONT_REVISED_VERSION; ?>"></script>
<script type="text/javascript"
        src="<?= base_url(); ?>public/front/plugins/ajaxfileupload.js?v=<?= FRONT_REVISED_VERSION; ?>"></script>
<script type="text/javascript"
        src="<?= base_url(); ?>public/front/plugins/moment.min.js?v=<?= FRONT_REVISED_VERSION; ?>"></script>
<script type="text/javascript"
        src="<?= base_url(); ?>public/front/plugins/autosize-master/dist/autosize.min.js?v=<?= FRONT_REVISED_VERSION; ?>"></script>

<?php
if (!empty($js_plugin)) {
    foreach ($js_plugin as $value) {
        ?>
        <script src="<?= base_url(); ?>public/front/js/plugins/<?php echo $value ?>?v=<?= FRONT_REVISED_VERSION; ?>"
                type="text/javascript"></script>
        <?php
    }
}
?>

<!-- BEGIN THEME GLOBAL SCRIPTS -->

<script src="<?= base_url(); ?>public/front/js/app.js?v=<?= FRONT_REVISED_VERSION; ?>" type="text/javascript"></script>
<script src="<?= base_url(); ?>public/front/js/common_function.js?v=<?= FRONT_REVISED_VERSION; ?>"
        type="text/javascript"></script>

<!-- END THEME GLOBAL SCRIPTS -->

<?php
if (!empty($js)) {
    foreach ($js as $value) {
        ?>
        <script src="<?php echo base_url(); ?>public/front/js/customer/<?php echo $value ?>?v=<?= FRONT_REVISED_VERSION; ?>"
                type="text/javascript"></script>
        <?php
    }
}
?>

<script>
    jQuery(document).ready(function () {
        App.init();
        <?php
        if (!empty($init)) {
            foreach ($init as $value) {
                echo $value . ';';
            }
        }
        ?>
    });
</script>