<style>
    .messageBox{
        margin:0 auto;
        width: 700px;
        border: 2px solid #000;
        border-radius: 6px;
        padding: 20px;
        text-align: center;
    }
    .messageBox h3{
        font-size: 20px;
        font-family: "Open Sans", sans-serif;
        line-height: 28px;
    }
    .messageBox h3 > b,
    .messageBox h3 > span{
        color: #662d91;
    }
    .boxBtnArea{
        border-top: 1px solid #ccc;
        padding: 20px 0 0;
        margin-top: 20px;
    }
    .boxBtnArea button{
        border: 2px solid #662d91;
        border-radius: 40px;
        color: #662d91;
        display: inline-block;
        font-size: 16px;
        margin: 0 0 0 3px;
        min-width: 130px;
        padding: 5px 15px;
        text-align: center;
        text-decoration: none;
        transition: all 300ms ease 0s;
        background: #fff;
    }
    .boxBtnArea button:hover{
        color: #fff;
        background: #662d91;
    }
</style>
<?php if(empty($checkClientAvailability)) { ?>
<div class="row">
    <div class="col-md-12">
        <div class="messageBox">
            <h3>Are you sure, You want to book appointment with <b><?= $expertsAvailability[0]['var_fname'].' '.$expertsAvailability[0]['var_lname'];?></b> on <span><?php echo date('d/m/Y',strtotime($expertsAvailability[0]['dt_date_availability']))?> <?php echo date("g:i a", strtotime($expertsAvailability[0]['dt_time_availability']))?> </span></h3>

            <div class="boxBtnArea">
                <button type="button" class="bookbtn" data-id="<?= $expertsAvailability[0]['id'];?>">Yes, Book it.</button>
                <button type="button" class="cancelbtn">Cancel</button>
            </div>

        </div>
    </div>
</div>
<?php } else { ?>
    <div class="row">
        <div class="col-md-12">
            <div class="messageBox">
                <h3> You have already select time !.</h3>
            </div>
        </div>
    </div>
<?php } ?>
