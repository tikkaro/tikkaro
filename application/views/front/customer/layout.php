<!DOCTYPE html>
<html lang="en">
    <!-- BEGIN HEADER -->
    <?php $this->load->view('front/customer/includes/header'); ?>
    <!-- END HEADER -->
    <body>
        <!-- BEGIN BODY HEADER -->
        <?php $this->load->view('front/customer/includes/body_header'); ?>
        <!-- END BODY HEADER -->

        <div class="contain-main">
            <div class="container">
                <!-- BEGIN PAGE -->
                <?php $this->load->view($page); ?>
                <!-- END PAGE -->
            </div>
            <!-- BEGIN BODY FOOTER -->
            <?php $this->load->view('front/customer/includes/body_footer'); ?>
            <!-- END BODY FOOTER -->
        </div>

        <!-- BEGIN FOOTER -->
        <?php $this->load->view('front/customer/includes/footer'); ?>
        <!-- END FOOTER -->

    </body>
</html>
