<div class="row">
    <div class="col-md-12">

        <form action="<?= customer_url(); ?>request-expert" name="frm-request-expert" id="frm-request-expert"
              method="post">

            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3><a data-toggle="collapse" data-parent="#accordion" href="#collapse1">Target Company:</a>
                        </h3>
                    </div>
                    <div id="collapse1" class="panel-collapse collapse in">
                        <div class="panel-body">
                            <div class="panel-body-inner">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Project Outline (what are you hoping to learn?)</label>
                                        <div class="input-box">
                                            <textarea name="projectOnline"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body-inner">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Type of people you want to speak to (formers), competitors, supliers,
                                            customers, industry, etc?)</label>
                                        <div class="input-box">
                                            <textarea name="peopleType"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body-inner">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Industry expert preferred role (C-Suit, sales, finanse, procurement,
                                            etc?)</label>
                                        <div class="input-box">
                                            <textarea name="IndustryExpert"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="panel-body-inner">
                                <div class="row">
                                    <div class="col-md-12">
                                        <label>Geographies of interest</label>
                                        <div class="input-box">
                                            <textarea name="Geographies"></textarea>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="btnbox">
                    <button type="submit">Submit</button>
                </div>
            </div>
        </form>
    </div>
</div>