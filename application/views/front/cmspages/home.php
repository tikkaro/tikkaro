<div class="banner-container">
    <section class="homepage-banner">
        <div id="index-banner" class="">
            <div class="section no-pad-bot">
                <div class="container">
                    <div class="row centerF">
                        <div class="col s12">
                            <div class="row clearfix">
                                <div class="col s2"></div>
                                <div class="input-field col s8">
                                    <h2 class="header center white-text margin-bottom-75">Lorem Ipsum text here...</h2>
                                    <div style="" class="location-input"> 
                                        <i class="material-icons prefix blue-text darken-3">search</i>
                                        <input type="text" id="global-search" class="autocomplete white-text medium">
                                        <label style="font-size: 20px;" for="global-search" class="white-text">Find services and stores</label>
                                    </div>
                                    <div class="button-container">
                                        <a href="booking.php" class="btn-medium btn blue darken-3 waves-effect waves-light"><i class="material-icons verticle-middle white-text">done_all</i> Tik your Store </a>
                                        <a href="javascript:;"class="btn-large margin-top-100 width100 btn orange darken-2 waves-effect waves-light">
                                            <i class="material-icons verticle-middle white-text darken-3">content_cut</i> Become Smart Barber
                                        </a>
                                    </div>
                                </div>
                                <div class="col s2"></div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="backoverlay"></div>