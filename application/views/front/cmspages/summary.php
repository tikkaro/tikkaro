<div class="sub-header blue darken-3">
    <div class="input-field ">
        <div class="container">
            <div class="page-title text-center">
                <h3>Booking Summary</h3>
                <h5>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</h5>
            </div>
        </div>
    </div>
</div>
<div class="page-content margin0">

    <div class="row container ">
        <div class="col s12">
            <div class="stores-listing">
                <div class="row margin-bottom-0 store-list">
                    <div class="store-container col s12">
                        <div class="col s12 m5 store-left-area">
                            <div class="card">
                                <div class="card-content padding0">
                                    <span class="card-title">Store Name Barber Shop</span>
                                    <div class="clearfix"></div>
                                    <div class="no-padding">
                                        <h5 class="display-inline-block"><i class="material-icons dp48 orange darken-2 white-text">call</i> 8866886677</h5>
                                        <h5><i class="material-icons dp48 orange darken-2 white-text">location_on</i> Manglam Society, Arbudanagar , Odhav , Ahmedabad - 382415</h5>
                                    </div>



                                </div>
                            </div>
                        </div>
                        <div class="col s7 card store-left-area">
                            <div class="service-list">
                                <b class="text-left sub-title margin-bottom-15">Your selected services :</b>
                                <ul>
                                    <?php for ($j = 0; $j < 2; $j++) { ?>
                                        <li  class="choose-service"><a href="javascript:;">
                                                <span>Service Name 1</span>
                                                <span>90 min / <b>₹</b> 100.00</span>
                                                <div class="clearfix"></div>
                                            </a>
                                        </li>
                                    <?php } ?>
                                </ul>
                            </div>
                            <div class="row margin0">
                                <a class="col s8 margin0 confirm-time padding8" href="javascript:;">
                                    <span>Thursday 06 june 2017, 09:00 AM TO 10:00 AM</span>
                                </a>
                                <span class="col s4 totaltxt">Total : <b>₹ 200.00</b> </span>
                                <div class="clearfix"></div>   
                            </div>
                        </div>
                        <div class="clearfix"></div>   
                    </div>
                </div>
                <div class="row payment-container">
                    <div class="col s2"></div>
                    <div class="col s8">
                        <div class="text-center">
                            <h5 class="payment-title">Select Payment Method</h5>
                        </div>
                        <div class="tabs-vertical margin-top-50">
                            <div class="col s3">
                                <ul class="tabs">
                                    <li class="tab">
                                        <a class="waves-effect waves-cyan" href="#savedetails"><i class="zmdi zmdi-apps"></i>Save Details</a>
                                    </li>
                                    <li class="tab">
                                        <a class="waves-effect waves-cyan" href="#debitcard"><i class="zmdi zmdi-email"></i>Debit Card</a>
                                    </li>
                                    <li class="tab">
                                        <a class="waves-effect waves-cyan" href="#creditcard"><i class="zmdi zmdi-code"></i>Credit Card</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col s7">
                                <div id="savedetails" class="tab-content">

                                </div>
                                <div id="debitcard" class="tab-content paddingTop10">
                                    <div class="row">
                                        <form class="col s12">
                                            <div class="input-field">
                                                <input placeholder="" id="debitcardno" type="text" class="validate">
                                                <label for="debitcardno">Enter your debit card Number</label>
                                            </div>
                                            <div class="row">
                                                <div class="input-field col s4">
                                                    <label>Expiry Date</label>
                                                </div>
                                                <div class="input-field col s4"></div>
                                                <div class="input-field col s4">
                                                    <label>CVV Code</label>
                                                </div>
                                            </div>
                                            <br>
                                            <div class="row margin-bottom-0">
                                                <div class="input-field col s4">
                                                    <select>
                                                        <option value="" disabled selected>MM</option>
                                                        <option value="1">01</option>
                                                        <option value="2">02</option>
                                                        <option value="3">03</option>
                                                    </select>

                                                </div>
                                                <div class="input-field col s4">
                                                    <select>
                                                        <option value="" disabled selected>YY</option>
                                                        <option value="1">2017</option>
                                                        <option value="2">2018</option>
                                                        <option value="3">2019</option>
                                                    </select>

                                                </div>
                                                <div class="input-field col s4">
                                                    <input id="debitcardno" type="text" class="validate">

                                                </div>
                                            </div>
                                            <div class="row margin-bottom-25">
                                                <div class="input-field col s12">
                                                    <input type="checkbox" id="savecard" checked="checked" />
                                                    <label for="savecard">Save card for faster payment</label>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                            <div class="input-field">
                                                <button type="submit" class="waves-effect waves-light btn btn-medium signbtn orange darken-2">
                                                    Pay now
                                                </button>
                                                <div class="clearfix"></div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div id="creditcard" class="tab-content paddingTop10"></div>
                            </div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="col s2"></div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
</div>