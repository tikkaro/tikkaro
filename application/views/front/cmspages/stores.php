<div class="fixed-header sub-header blue darken-3">
    <div class="input-field ">
        <div style="" class="location-input"> 
            <i class="material-icons prefix blue-text darken-3">search</i>
            <input type="text" id="global-search" class="autocomplete white-text medium">
            <label style="font-size: 20px;" for="global-search" class="white-text">Find services and stores</label>
        </div>
        <!--                    <div class="container">
                                <div class="page-title">
                                    <h3>Service : Hair Cut</h3>
                                    <h5>If you want multiple services than you can select more services from your selected store.</h5>
                                </div>
                            </div>-->
    </div>
</div>
<div class="page-content">
    <!--                <div class="fixed-header select-date-container">
                        <div class="container">
                            <ul>
                                <li>
                                    <b>01</b> 
                                    <span>Mon</span>
                                </li>
                                <li>
                                    <b>02</b> 
                                    <span>Tue</span>
                                </li>
                                <li class="active">
                                    <b>03</b> 
                                    <span>Wed</span>
                                </li>
                                <li>
                                    <b>04</b> 
                                    <span>Thu</span>
                                </li>
                                <li>
                                    <b>05</b> 
                                    <span>Fri</span>
                                </li>
                                <li>
                                    <b>06</b> 
                                    <span>SAT</span>
                                </li>
                                <li>
                                    <b>07</b> 
                                    <span>SUN</span>
                                </li>
                            </ul>
                        </div>
                    </div>-->

    <div class="row container ">
        <div class="col s3 google-map">
            <img src="<?= base_url(); ?>public/front/images/google-map.png" width="100%" height="100%;">
        </div>
        <div class="col s9">
            <div class="stores-listing">
                <div class="row store-list">
                    <?php for ($i = 1; $i < 25; $i++) { ?>
                        <div class="store-container col s12">
                            <div class="col s12 m5 store-left-area">
                                <div class="card">
                                    <div class="card-image">
                                        <img src="<?= base_url(); ?>public/front/images/barbershop/0<?= $i; ?>.jpg">
                                        <div class="btnarea">
                                            <a class="btn-floating btn-large waves-effect waves-light blue darken-3"><i class="material-icons">link</i></a>
                                            <a class="btn-floating btn-large waves-effect waves-light blue darken-3"><i class="material-icons">perm_media</i></a>
                                            <a class="btn-floating btn-large waves-effect waves-light blue darken-3"><i class="material-icons">directions</i></a>
                                        </div>
                                    </div>
                                    <div class="card-content">
                                        <span class="card-title">Store Name Barber Shop</span>
                                        <ul class="rating-container">
                                            <li><i class="material-icons">star</i></li>
                                            <li><i class="material-icons">star</i></li>
                                            <li><i class="material-icons">star_half</i></li>
                                            <li><i class="material-icons">star_border</i></li>
                                            <li><i class="material-icons">star_border</i></li>
                                            <li style="
                                                width: auto;
                                                height: auto;
                                                margin: 0 8px 0;
                                                line-height: 31px;
                                                color: #F57C00;
                                                "> / 100 Reviews</li>

                                        </ul>
                                        <div class="clearfix"></div>
                                        <div class="no-padding">
                                            <h5 class="display-inline-block"><i class="material-icons dp48 orange darken-2 white-text">access_time</i> 09:00 AM TO 10:00 PM</h5>
                                            <h5 class="display-inline-block"><i class="material-icons dp48 orange darken-2 white-text">call</i> 8866886677</h5>
                                            <h5><i class="material-icons dp48 orange darken-2 white-text">location_on</i> Manglam Society, Arbudanagar , Odhav , Ahmedabad - 382415</h5>
                                        </div>
                                        <ul class="facility-container">
                                            <li><a class="tooltipped" data-position="top" data-delay="50" data-tooltip="AC"><i class="material-icons">videogame_asset</i></a></li>
                                            <li class="active"><a class="tooltipped" data-position="top" data-delay="50" data-tooltip="Wi-Fi"><i class="material-icons">wifi</i></a></li>
                                            <li><a class="tooltipped" data-position="top" data-delay="50" data-tooltip="TV"><i class="material-icons">desktop_windows</i></a></li>
                                        </ul>

                                    </div>
                                </div>
                            </div>
                            <div class="col s7 card store-left-area">
                                <div class="service-list">
                                    <b class="text-left sub-title margin-bottom-15">You can choose multiple services :</b>

                                    <ul>
                                        <?php for ($j = 0; $j < 8; $j++) { ?>
                                            <li  class="choose-service <?= ($j == 0) ? 'active' : ''; ?>"><a href="javascript:;">
                                                    <span>Service Name 1</span>
                                                    <span>90 min / <b>₹</b> 100.00</span>
                                                    <div class="clearfix"></div>
                                                </a>
                                            </li>
                                        <?php } ?>
                                        <a href="javascript:;">show more services...</a>
                                    </ul>
                                </div>
                                <a href="javascript:;" class="choose-time width100 book-now-btn btn orange darken-2 waves-effect waves-light">
                                    Book now (<b>₹</b> 100.00) 
                                </a>
                            </div>
                            <div class="clearfix"></div>   
                        </div>
                    <?php } ?>

                </div>
            </div>
        </div>
    </div>
</div>