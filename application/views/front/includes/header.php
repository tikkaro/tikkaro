<head>
    <meta charset="utf-8"/>
    <?php
    $title = empty($var_meta_title) ? get_project_name() : $var_meta_title . ' | ' . get_project_name();
    $description = empty($var_meta_description) ? get_project_name() : $var_meta_description . ' | ' . get_project_name();
    $keywords = empty($var_meta_keyword) ? get_project_name() : $var_meta_keyword . ' | ' . get_project_name();
    ?>
    <title><?= $title; ?></title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <meta content="<?= $description; ?>" name="description" />
    <meta content="<?= $keywords; ?>" name="author" />

    <!--Import Google Icon Font-->
    <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <!--Import materialize.css-->
    <link type="text/css" rel="stylesheet" href="<?= base_url(); ?>public/front/plugins/materialize/css/materialize.min.css"  media="screen,projection"/>
    <link type="text/css" rel="stylesheet" href="<?= base_url(); ?>public/front/css/style.css"  media="all"/>
    <!--Let browser know website is optimized for mobile-->
    <?php
    if (!empty($css)) {
        foreach ($css as $value) {
            ?>
            <link rel="stylesheet"
                  href="<?= base_url(); ?>public/front/css/<?php echo $value; ?>?v=<?= REVISED_VERSION; ?>"/>
                  <?php
              }
          }
          ?>
          <?php
          if (!empty($css_plugin)) {
              foreach ($css_plugin as $value_plugin) {
                  ?>
            <link rel="stylesheet"
                  href="<?= base_url(); ?>public/front/plugins/<?php echo $value_plugin; ?>?v=<?= REVISED_VERSION; ?>"/>
                  <?php
              }
          }
          ?>
    <script type="text/javascript">
        var baseurl = "<?php echo base_url(); ?>";
        var adminurl = "<?php echo admin_url(); ?>";
        var userurl = "<?php echo user_url(); ?>";
        var getbaseurl = "<?php getbaseURL(); ?>";
    </script>
    <script type="text/javascript" src="<?= base_url(); ?>public/front/plugins/jquery/jquery-3.2.1.min.js"></script>
</head>