<header class="tik-header">
    <div class="navbar-fixed">
        <nav class="white fixed">
            <div class="nav-wrapper">
                <a href="<?= base_url(); ?>" class="brand-logo blue-text darken-3 center"><i class="orange-text darken-3 material-icons bigfont">done_all</i>Tikkaro</a>
                <ul class="right hide-on-med-and-down">
                    <li class="loginlinks <?= ($this->session->userdata['valid_user']) ? 'loggedin' : ''; ?>">
                        <?php
                        if ($this->session->userdata['valid_user']) {
                            echo $this->session->userdata['valid_user']['var_fname'] . ' ' . $this->session->userdata['valid_user']['var_lname'] . ' / ';
                            ?>
                            <a class="logout-link" href="<?= user_url(); ?>logout">Logout</a>
                        <?php } else { ?>
                            <a href="#login-popup" class="modal-trigger blue-text darken-3 waves-effect waves-light"><i class="material-icons bigfont">account_circle</i></a>
                        <?php } ?>

                    </li>
                </ul>
            </div>
        </nav>
    </div>
</header>