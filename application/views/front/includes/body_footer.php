<div class="footer_section2">
    <div class="footer-wrap">
        <div class="left">
            <div class="fSection-hed">
                <i>“We connect our clients to industry experts around the world”</i>
            </div>
            <span class="copyright">Copyright proSapient 2017</span>
        </div>
        <div class="right">
            <div class="address-con">
                <h4 class="heading">Contact Us</h4>
                <div class="address">
                    <p class="margin0"><span>Address:</span> <label>2 Eastbourne Terrace,
                            Paddington,
                            London, W2 6LG</label></p>
                    <p class="margin0"><span>Email:</span> <label><a href="mailto:hello@prosapient.com">hello@prosapient.com</a></label>
                    </p>
                    <p class="margin0"><span>Telephone:</span> <label>0208 50 50 500</label></p>
                </div>

            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>