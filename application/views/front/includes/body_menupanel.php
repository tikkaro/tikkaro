<div class="navbar">
    <div class="hed-btn">
        <div class="logo-part">
            <a href="<?= base_url(); ?>"><img
                        src="<?= base_url(); ?>public/front/cmspages/images/pro-sapient/proSapientLogo.svg"/></a>
        </div>
        <ul>
            <li class="menu-item">
                <a href="<?= base_url(); ?>">
                    <img src="<?= base_url(); ?>public/front/cmspages/images/pro-sapient/home.png"
                         style="width:27px;"/>
                    Home
                </a>
            </li>
            <li class="menu-item">
                <a href="<?= base_url(); ?>#services">
                    <img src="<?= base_url(); ?>public/front/cmspages/images/pro-sapient/services-icon.png"
                         style="width:34px;"/>
                    Services
                </a>
            </li>
            <li class="menu-item">
                <a href="<?= base_url(); ?>contact">
                    <img src="<?= base_url(); ?>public/front/cmspages/images/pro-sapient/contact.png"/>
                    Contact
                </a>
            </li>
            <li class="menu-item">
                <a href="<?= base_url(); ?>customer/login">
                    <img src="<?= base_url(); ?>public/front/cmspages/images/pro-sapient/login-icon.png"
                         style="width:18px;"/>
                    Login
                </a>
            </li>
        </ul>
        <div class="clear"></div>
    </div>
</div>