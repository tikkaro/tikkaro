<!--Import jQuery before materialize.js-->
<script type="text/javascript" src="<?= base_url(); ?>public/front/plugins/materialize/js/materialize.min.js"></script>

<!--<script src="<?= base_url(); ?>public/front/plugins/bootstrap/js/bootstrap.min.js?v=<?= REVISED_VERSION; ?>"
type="text/javascript"></script>-->

<script src="<?= base_url(); ?>public/front/plugins/jquery.cookie.min.js?v=<?= REVISED_VERSION; ?>"
type="text/javascript"></script>

<script src="<?= base_url(); ?>public/front/plugins/jquery-validation/js/jquery.validate.js?v=<?= REVISED_VERSION; ?>"
type="text/javascript"></script>
<script src="<?= base_url(); ?>public/front/plugins/jquery-validation/js/jquery.validate.min.js?v=<?= REVISED_VERSION; ?>"
type="text/javascript"></script>

<?php
if (!empty($js_plugin)) {
    foreach ($js_plugin as $value) {
        ?>
        <script src="<?= base_url(); ?>public/front/plugins/<?php echo $value ?>?v=<?= REVISED_VERSION; ?>"
        type="text/javascript"></script>
        <?php
    }
}
?>
<script src="<?= base_url(); ?>public/front/js/app.js?v=<?= REVISED_VERSION; ?>"
type="text/javascript"></script>
<script src="<?= base_url(); ?>public/front/plugins/moment.min.js?v=<?= REVISED_VERSION; ?>"
type="text/javascript"></script>
<script src="<?= base_url(); ?>public/front/js/common_function.js?v=<?= REVISED_VERSION; ?>"
type="text/javascript"></script>
<?php
if (!empty($js)) {
    foreach ($js as $value) {
        ?>
        <script src="<?php echo base_url(); ?>public/front/js/front/<?php echo $value ?>?v=<?= REVISED_VERSION; ?>"
        type="text/javascript"></script>
        <?php
    }
}
?>
        <script src="<?php echo base_url(); ?>public/front/js/front/custom.js?v=<?= REVISED_VERSION; ?>"
        type="text/javascript"></script>
        
        <script src="<?= base_url(); ?>public/front/js/user/login.js?v=<?= REVISED_VERSION; ?>" type="text/javascript"></script>
<script>
    jQuery(document).ready(function () {
        App.init();
        Custom.init();
        Login.init();
<?php
if (!empty($init)) {
    foreach ($init as $value) {
        echo $value . ';';
    }
}
?>
    });
</script>
<script type="text/javascript">
    $(document).ready(function () {
        // the "href" attribute of .modal-trigger must specify the modal ID that wants to be triggered
        
        $('body').on('click', '#global-search', function () {
            $('#services-stores-popup').modal('open');
        });

        $('body').on('click', '#closebtn', function () {
            $('#services-stores-popup').modal('close');
        });

        //Login and forgot password form
        $('body').on('click', '#forgotpass', function () {
            $('#frmlogin').hide();
            $('#frmforgotpass').show();
        });
        $('body').on('click', '#cancel-btn', function () {
            $('#frmlogin').show();
            $('#frmforgotpass').hide();
        });
    });
</script>