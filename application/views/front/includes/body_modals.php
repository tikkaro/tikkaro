<!-- Popup Area -->

<!-- Common Modals -->
<div id="login-popup" class="modal custom-modal">
    <div class="modal-content">
        <div class="row blue darken-3 margin-bottom-0">
            <div class="col s5 no-padding">

            </div>
            <div class="col s7 no-padding">
                <ul class="tabs tab-demo z-depth-1">
                    <li class="tab"><a href="#login-tab" class="active">Login</a></li>
                    <li class="tab"><a href="#signup-tab">Sign Up</a></li>
                    <div class="indicator" style="right: 395px; left: 0px;"></div>
                </ul>
                <div id="login-tab" class="col s12 tab-cont">
                    <div class="row margin-top-25 margin-bottom-25">
                        <form class="col s12" id="frmlogin" action="javacript:;" method="post">
                            
                            <div class="alert-danger alert-msg"></div>
                            <div class="alert-success alert-msg"></div>
                            <div class="alert-warning alert-msg"></div>
                            
                            <div class="row margin-bottom-0">
                                <div class="input-field col s12">
                                    <input id="username" name="username" type="text" class="validate">
                                    <label for="username">Enter your Mobile or Email</label>
                                </div>
                            </div>

                            <div class="row margin-bottom-0">
                                <div class="input-field col s12">
                                    <input id="password" type="password" name="password" class="validate">
                                    <label for="password">Enter your password</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col s12 text-right">
                                    <a href="javascript:;" id="forgotpass" class="blue-text darken-3 font-size-14">Forgot password ?</a>
                                </div>
                            </div>
                            <div class="row margin-bottom-0 margin-top-15">
                                <div class="col s12 text-center">
                                    <button type="submit" class="waves-effect waves-light btn btn-medium signbtn orange darken-2">
                                        Login 
<!--                                                <i class="blue-text darken-3 material-icons bigfont">done_all</i>-->
                                    </button>
                                </div>
                            </div>
                        </form>

                        <form class="col s12" id="frmforgotpass" style="display: none;" action="javacript:;" method="post">

                            <div class="row margin-bottom-0">
                                <div class="input-field col s12">
                                    <input id="mobile" type="text" class="validate">
                                    <label for="mobile">Enter your Mobile or Password</label>
                                </div>
                            </div>
                            <div class="row margin-bottom-0 margin-top-15">
                                <div class="col s12">
                                    <div class="col s8 text-center">
                                        <button type="submit" class="waves-effect waves-light btn btn-medium signbtn width100 orange darken-2">
                                            Reset Password 
<!--                                                    <i class="blue-text darken-3 material-icons bigfont">done_all</i>-->
                                        </button>
                                    </div>
                                    <div class="col s4 text-center">
                                        <button type="button" id="cancel-btn" class="waves-effect waves-light btn-flat btn btn-medium signbtn width100">
                                            Cancel
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div id="signup-tab" class="col s12 tab-cont">
                    <div class="row margin-top-25 margin-bottom-25">
                        <form class="col s12" id="frmsignup" action="javacript:;" method="post">
                            <div class="row margin-bottom-0">
                                <div class="input-field col s6">
                                    <input id="first_name" type="text" class="validate">
                                    <label for="first_name">Enter your first name</label>
                                </div>
                                <div class="input-field col s6">
                                    <input id="last_name" type="text" class="validate">
                                    <label for="last_name">Enter your last name</label>
                                </div>
                            </div>

                            <div class="row margin-bottom-0">
                                <div class="input-field col s12">
                                    <input id="mobile" type="text" class="validate">
                                    <label for="password">Enter your mobile number</label>
                                </div>
                            </div>
                            <div class="row margin-bottom-0">
                                <div class="input-field col s12">
                                    <input id="email" type="email" class="validate">
                                    <label for="email">Email Address</label>
                                </div>
                            </div>
                            <div class="row margin-bottom-0">
                                <div class="input-field col s12">
                                    <input id="password" type="password" class="validate">
                                    <label for="password">Enter your password</label>
                                </div>
                            </div>
                            <div class="row margin-bottom-0 margin-top-15">
                                <div class="col s12 text-center">
                                    <button type="submit" class="waves-effect waves-light btn btn-medium signbtn orange darken-2">
                                        Sign Up
<!--                                                <i class="blue-text darken-3 material-icons bigfont">done_all</i>-->
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
<!-- Common Modals -->

<?php if ($pageflag == 'Home') { ?>
    <!-- Home Modals -->
    <div id="services-stores-popup" class="modal booking-modal">
        <div class="modal-content">
            <div class="row margin-bottom-0">
                <div class="col s12 no-padding">
                    <div class="modal-header blue darken-3">
                        <div class="input-field ">
                            <div style="" class="location-input"> 
                                <i class="material-icons prefix blue-text darken-3">search</i>
                                <input type="text" id="global-search" class="autocomplete white-text medium">
                                <label style="font-size: 20px;" for="global-search" class="white-text">Find services and stores</label>
                            </div>
                        </div>
                        <i class="material-icons closebtn" id="closebtn">close</i>
                    </div> 
                    <div class="modal-container tab-container">
                        <ul class="modal-tabs tabs tab-demo z-depth-1">
                            <li class="tab"><a href="#services-tab" class="active">Services</a></li>
                            <li class="tab"><a href="#stores-tab">Stores</a></li>
                            <div class="clearfix"></div>
                        </ul>
                        <div id="services-tab" class="col s12 tab-cont">
                            <?php
                            $services = array('Hair Cutting', 'Saving', 'Hair Spa', 'Hair Colour', 'Massage', 'Facial', 'Hair Hightlight', 'Body Massage', 'Hair Cutting', 'Saving', 'Hair Spa', 'Hair Colour', 'Massage', 'Facial', 'Hair Hightlight', 'Body Massage', 'Saving', 'Hair Spa', 'Hair Colour', 'Massage', 'Facial',);
                            for ($i = 0; $i < count($services); $i++) {
                                ?>
                                <a href="selectstores.php" class="backcolor-<?= $i + 1; ?> services-container animated waves-effect waves-circle waves-light">
                                    <i class="material-icons">spa</i>
                                    <p class="service-title"><?= $services[$i]; ?></p>
                                    <span class="service-price">(₹ 50.00)</span>
                                </a>
                            <?php } ?>
                        </div>

                        <div id="stores-tab" class="col s12 tab-cont">
                            <div class="row store-list">
                                <?php for ($i = 1; $i < 25; $i++) { ?>
                                    <div class="store-container col s6 waves-dark waves-effect ">
                                        <div class="col s3 store-left-area">
                                            <img src="<?= base_url(); ?>public/front/images/barbershop/0<?= $i; ?>.jpg" class="imageSqare responsive-img" width="150" height="150" />
                                            <!--                                                <br>
                                                                                            <h5><i class="material-icons dp48 blue-text darken-3">access_time</i> 09:00 AM</h5>
                                                                                            <h5><i class="material-icons dp48 blue-text darken-3 ">restore</i> 10:00 PM</h5>-->
                                        </div>
                                        <div class="col s9 store-right-area" style="text-align:left;">
                                            <div class="6">
                                                <h3>Store Name Barber Shop</h3>
                                                <div class="col s9 no-padding">
                                                    <h5 class="display-inline-block"><i class="material-icons dp48 orange darken-2 white-text">access_time</i> 09:00 AM TO 10:00 PM</h5>
                                                    <h5 class="display-inline-block"><i class="material-icons dp48 orange darken-2 white-text">call</i> 8866886677</h5>
                                                    <h5><i class="material-icons dp48 orange darken-2 white-text">location_on</i> Manglam Society, Arbudanagar , Odhav , Ahmedabad - 382415</h5>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>    
                                    </div>
                                <?php } ?>
                                <?php for ($i = 1; $i < 25; $i++) { ?>
                                    <div class="store-container col s6 waves-dark waves-effect ">
                                        <div class="col s3 store-left-area">
                                            <img src="<?= base_url(); ?>public/front/images/barbershop/0<?= $i; ?>.jpg" class="imageSqare responsive-img" width="100%" />
                                            <br>
                                            <h5><i class="material-icons dp48 blue-text darken-3">access_time</i> 09:00 AM</h5>
                                            <h5><i class="material-icons dp48 blue-text darken-3 ">restore</i> 10:00 PM</h5>
                                        </div>
                                        <div class="col s9 store-right-area" style="text-align:left;">
                                            <div class="6">
                                                <h3>Store Name Barber Shop</h3>
                                                <div class="col s9 no-padding">
                                                    <h5 class="display-inline-block"><i class="material-icons dp48 orange darken-2 white-text">person</i> Jignesh Dasadiya</h5>
                                                    <h5 class="display-inline-block"><i class="material-icons dp48 orange darken-2 white-text">call</i> 8866886677</h5>
                                                    <h5><i class="material-icons dp48 orange darken-2 white-text">location_on</i> Manglam Society, Arbudanagar , Odhav , Ahmedabad - 382415</h5>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>    
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <!-- Home Modals -->
<?php } ?>

<?php if ($pageflag == 'Stores') { ?>
    <!-- Stores Modals -->
    <div id="choose-time" class="modal blue darken-3" style="">
        <div class="modal-content">
            <h4 class="text-center modal-heading">Tik Date & Time</h4>
            <div class="select-date-container blue darken-3">
                <div class="container">
                    <ul>
                        <li>
                            <b>01</b> 
                            <span>Mon</span>
                        </li>
                        <li>
                            <b>02</b> 
                            <span>Tue</span>
                        </li>
                        <li class="active">
                            <b>03</b> 
                            <span>Wed</span>
                        </li>
                        <li>
                            <b>04</b> 
                            <span>Thu</span>
                        </li>
                        <li>
                            <b>05</b> 
                            <span>Fri</span>
                        </li>
                        <li>
                            <b>06</b> 
                            <span>SAT</span>
                        </li>
                        <li>
                            <b>07</b> 
                            <span>SUN</span>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="choose-time-container">
                <div class="row margin-bottom-0">
                    <div class="col s4">
                        <b class="time-heading">Morning</b>
                        <ul class="time-slot-list">
                            <?php for ($k = 00; $k < 16; $k++) { ?>
                                <li><a href="javascript:;">10:<?= $k; ?> AM</a></li>
                            <?php } ?>
                        </ul>
                    </div>
                    <div class="col s4">
                        <b class="time-heading">Afternoon</b>
                        <ul class="time-slot-list">
                            <?php for ($k = 00; $k < 16; $k++) { ?>
                                <li><a href="javascript:;">10:<?= $k; ?> PM</a></li>
                            <?php } ?>
                        </ul>
                    </div>
                    <div class="col s4">
                        <b class="time-heading">Evening</b>
                        <ul class="time-slot-list">
                            <?php for ($k = 00; $k < 16; $k++) { ?>
                                <li><a href="javascript:;">10:<?= $k; ?> PM</a></li>
                            <?php } ?>
                        </ul>
                    </div>
                    <a href="payment.php" class="choose-time width40 book-now-btn btn orange darken-2 waves-effect waves-light">
                        Continue (09:00 AM TO 10:30 AM) 
                    </a>
                </div>

            </div>
        </div>
    </div>
    <!-- Stores Modals -->
<?php } ?>

<!-- Popup Area -->

