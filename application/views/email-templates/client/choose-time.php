<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>ProSaoient</title>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,700" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <style>
        a {
            text-decoration: none;
            color: #fff;
        }
    </style>
</head>

<body style="background: #f2f2f2; margin: 0; padding: 0;">

<table cellpadding="0" cellspacing="0" cellpadding="0" cellspacing="0"
       style="width: 600px; margin: auto; font-size: 13px; background-color: #FFF; font-family: Arial ,Helvetica, sans-serif;">
    <tr>
        <td>
            <table cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td style="padding: 25px 20px; background-color: #0d011f; box-shadow: 0 4px 3px -4px #000; text-align: center;">
                        <a href="javascript:;">
                            <img style="width: 135px;" src="<?php echo base_url(); ?>public/assets/images/logo-1.png"
                                 alt=""/>
                        </a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="height: 30px"></td>
    </tr>
    <tr>
        <td style="padding: 0 25px;">
            <table cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" style="width: 100%;">
                            <tr>
                                <td>
                                    <label style="margin: 0; font-weight: bold;font-family: 'ArialMT', sans-serif;">Dear <?= $ExpertsAndClientUser[0]['userFname'] . ' ' . $ExpertsAndClientUser[0]['userLname'] ?>
                                        ,</label></td>
                            </tr>
                            <tr>
                                <td style="height: 10px"></td>
                            </tr>
                            <tr>
                                <td style="font-family: 'ArialMT', sans-serif;">
                                    <span><?= $ExpertsAndClientUser[0]['expetsFname'] . ' ' . $ExpertsAndClientUser[0]['expetsLname'] ?></span>
                                    has come back to us with the following availability for your project:
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="height: 30px"></td>
                </tr>
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" style="width: 100%;">
                            <?php if (!empty($WeekData)) {
                                for ($i = 0; $i < count($WeekData); $i++) {
                                    ?>
                                    <tr>
                                        <td colspan="2"><label style="margin:0; font-weight:600;"> Week
                                                (<?= date('d/m/y', strtotime($WeekData[$i]['start'])) . ' - ' . date('d/m/y', strtotime($WeekData[$i]['end'])); ?>
                                                )</label></td>
                                    </tr>
                                    <tr>
                                        <td style="height: 10px"></td>
                                    </tr>
                                    <?php for ($j = 0; $j < count($WeekData[$i]['time']); $j++) { ?>
                                        <tr>
                                            <td style="background-color: #0D011F;border-bottom: solid 1px #fff; width:40%; border-right: solid 2px #E2E2E2;font-family: 'ArialMT', sans-serif; font-weight: bold; color: #fff; padding: 3px 10px; text-align: left;"><?= date('l jS', strtotime($WeekData[$i]['time'][$j]['date'])); ?></td>
                                            <td style="background-color: #0D011F; border-bottom: solid 1px #fff; width:60%; color: #fff; padding: 3px 10px;font-family: 'ArialMT', sans-serif; font-weight: bold; text-align: left;">
                                                <?php for ($k = 0; $k < count($WeekData[$i]['time'][$j]['dayTime']); $k++) {
                                                    $urlString = base64_encode(json_encode(['expertsHasTimeAvailabilityId' => $WeekData[$i]['time'][$j]['dayTimeId'][$k]]));
                                                    ?>
                                                    <a href="<?php echo base_url() . 'scheduling/clientSelectAvailability?data=' . $urlString ?>"
                                                       style="color: #ffffff; text-decoration: none;">
                                                        <?php
                                                        if (($k + 1) == count($WeekData[$i]['time'][$j]['dayTime'])) {
                                                            echo date("g:i a", strtotime($WeekData[$i]['time'][$j]['dayTime'][$k]));
                                                        } else {
                                                            echo date("g:i a", strtotime($WeekData[$i]['time'][$j]['dayTime'][$k])) . ' / ';
                                                        }
                                                        ?>
                                                    </a>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    <tr>
                                        <td style="height: 20px"></td>
                                    </tr>
                                <?php }
                            } ?>
                        </table>
                    </td>
                </tr>

                <tr>
                    <td style="height: 30px"></td>
                </tr>
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" style="width: 100%;">
                            <tr>
                                <td style="font-family: 'ArialMT', sans-serif;">You can click on your preferred time or
                                    just send us an email to schedule the consultation
                                </td>
                            </tr>
                            <tr>
                                <td style="height: 30px"></td>
                            </tr>
                            <tr>
                                <td style="font-family: 'ArialMT', sans-serif;">Best,</td>
                            </tr>
                            <tr>
                                <td style="height: 20px"></td>
                            </tr>
                            <tr>
                                <td style="font-family: 'ArialMT', sans-serif;">
                                    <span><?= $ExpertsAndClientUser[0]['stafName'] ?></span>, Associate<br/>
                                    Direct Line: <span><?= $ExpertsAndClientUser[0]['staffPhone'] ?></span><br/>
                                    Mobile: <span><?= $ExpertsAndClientUser[0]['staffPhone'] ?></span><br/>
                                    <?= $ExpertsAndClientUser[0]['staffEmail'] ?><br/>
                                    <a href="http://proSapient.com"><span>http://proSapient.com</span></a></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="height: 30px"></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="padding: 15px 20px; background-color: #0d011f; box-shadow: 0 -4px 3px -4px #000; text-align: center;">
            <table cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td style="color: #fff; font-size: 12px;font-family: 'ArialMT', sans-serif;">This email was intended
                        for <?= $ExpertsAndClientUser[0]['userFname'] . ' ' . $ExpertsAndClientUser[0]['userLname'] ?>.
                    </td>
                </tr>
                <tr>
                    <td style="height: 12px"></td>
                </tr>
                <tr>
                    <td>
                        <a href="javascript:;">
                            <img style="width: 90px;" src="<?php echo base_url(); ?>public/assets/images/logo-1.png"
                                 alt=""/></a>
                    </td>
                </tr>
                <tr>
                    <td style="height: 8px"></td>
                </tr>
                <tr>
                    <td>
                        <a style="color: #fff;font-family: 'ArialMT', sans-serif; font-size: 12px; text-decoration: none; padding: 0 8px 0 0; border-right: solid 1px #fff;"
                           href="javascript:;">Unsubscribe</a>
                        <a style="color: #fff;font-family: 'ArialMT', sans-serif; font-size: 12px; text-decoration: none; padding: 0 5px;"
                           href="javascript:;">View in browser</a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
<!--  -->
