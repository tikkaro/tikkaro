<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>ProSaoient</title>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,700" rel="stylesheet">
    <style>
        a {
            text-decoration: none;
            color: #fff;
        }
    </style>
</head>

<body style="background: #f2f2f2; margin: 0; padding: 0;">

<table cellpadding="0" cellspacing="0" cellpadding="0" cellspacing="0"
       style="width: 600px; margin: auto; font-size: 13px; background-color: #FFF; font-family: Arial ,Helvetica, sans-serif;">
    <tr>
        <td>
            <table cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td style="padding: 25px 20px; background-color: #0d011f; box-shadow: 0 4px 3px -4px #000; text-align: center;">
                        <a href="#">
                            <img style="width: 135px;" src="<?php echo base_url() ?>public/assets/images/logo-1.png"
                                 alt=""/>
                        </a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="height: 30px"></td>
    </tr>
    <tr>
        <td style="padding: 0 25px;">
            <table cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" style="width: 100%;">
                            <tr>
                                <td>
                                    <label style="margin: 0; font-weight: bold;font-family: 'ArialMT', sans-serif;">Dear <?= $expertsdata[0]['userfname'] . ' ' . $expertsdata[0]['userlname'] ?>
                                        ,</label></td>
                            </tr>
                            <tr>
                                <td style="height: 10px"></td>
                            </tr>
                            <tr>
                                <td style="font-family: 'ArialMT', sans-serif;">We have curated this list of industry
                                    experts for your upcoming project:
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="height: 30px"></td>
                </tr>
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0"
                               style="width: 100%; border: 1px solid #0d011f;border-radius: 3px; background: #F5F5F5;">
                            <tr>
                                <td colspan="2"
                                    style="background-color: #0D011F; font-family: 'Open Sans', sans-serif; font-weight: bold; color: #fff; padding: 3px 10px; text-align: left;">
                                    Shortlisted Experts:
                                </td>
                            </tr>
                            <?php for ($i = 0; $i < count($expertsdata); $i++) { ?>
                                <tr>
                                    <td style="border-bottom: 1px solid #ccc; padding: 10px 0 20px;">
                                        <table cellpadding="0" cellspacing="0" style="width: 100%;">
                                            <tr>
                                                <td colspan="2" style="padding: 0 10px">
                                                    <label style="color: #1a1a1a;font-weight: bold;font-family: 'Open Sans', sans-serif;font-size: 18px;"><?= $expertsdata[$i]['expertsfname'] . ' ' . $expertsdata[$i]['expertslname']; ?></label><br>
                                                    <?php if ($expertsdata[$i]['currentPosition'] != '' && $expertsdata[$i]['currentCompanyName']) { ?>
                                                        <label style="color: #1a1a1a;font-weight: bold;font-family: 'Open Sans', sans-serif;font-size: 13px;"><?= $expertsdata[$i]['currentPosition'] . ' at ' . $expertsdata[$i]['currentCompanyName']; ?></label>
                                                    <?php } ?>
                                                    <p style="margin: 10px 0 0;;font-family: 'Open Sans', sans-serif;"><?= $expertsdata[$i]['txt_bio']; ?></p>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="height: 20px"></td>
                                            </tr>
                                            <tr>
                                                <td style="width:50%; text-align:right; padding:0 5px">
                                                    <?php $urlString = base64_encode(json_encode(['expertsHasProjectId' => $expertsdata[$i]["id"], 'expertsId' => $expertsdata[$i]["fk_experts"]])); ?>
                                                    <a taget="_blank"
                                                       href="<?php echo base_url() . 'scheduling/schedule?data=' . $urlString ?>"
                                                       style="background-color:#fff; font-family: 'helvetica'; border: 2px solid #662d91;border-radius: 20px;color: #662d91;display: inline-block;font-size: 15px;min-width: 130px;padding: 5px;text-align: center;text-decoration: none;">Schedule</a>
                                                </td>
                                                <td style="width:50%; text-align:left;padding:0 5px">
                                                    <a target="_blank"
                                                       href="<?php echo base_url() . 'expert/bio/' . $expertsdata[$i]['fk_experts'] ?>"
                                                       style="background-color:#fff; font-family: 'helvetica';border: 2px solid #662d91;border-radius: 20px;color: #662d91;display: inline-block;font-size: 15px;min-width: 130px;padding: 5px;text-align: center;text-decoration: none;">Bio</a>
                                                </td>
                                            </tr>
                                        </table>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" style="width: 100%;">
                            <tr>
                                <td style="height: 30px"></td>
                            </tr>
                            <tr>
                                <td style="font-family: 'ArialMT', sans-serif;">Best,</td>
                            </tr>
                            <tr>
                                <td style="height: 20px"></td>
                            </tr>
                            <tr>
                                <td style="font-family: 'ArialMT', sans-serif;">
                                    <span><?= $expertsdata[0]['stafname'] ?></span>, Associate<br/>
                                    Mobile: <span><?= $expertsdata[0]['staffphone'] ?></span><br/>
                                    <?= $expertsdata[0]['staffemail'] ?><br/>
                                    <a href="http://proSapient.com"><span>http://proSapient.com</span></a></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="height: 30px"></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="padding: 15px 20px; background-color: #0d011f; box-shadow: 0 -4px 3px -4px #000; text-align: center;">
            <table cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td style="color: #fff; font-size: 12px;font-family: 'ArialMT', sans-serif;">This email was intended
                        for <?= $expertsdata[0]['userfname'] . ' ' . $expertsdata[0]['userlname'] ?>.
                    </td>
                </tr>
                <tr>
                    <td style="height: 12px"></td>
                </tr>
                <tr>
                    <td>
                        <a href="javascript:;">
                            <img style="width: 90px;" src="<?php echo base_url() ?>public/assets/images/logo-1.png"
                                 alt=""/></a>
                    </td>
                </tr>
                <tr>
                    <td style="height: 8px"></td>
                </tr>
                <tr>
                    <td>
                        <a style="color: #fff;font-family: 'ArialMT', sans-serif; font-size: 12px; text-decoration: none; padding: 0 8px 0 0; border-right: solid 1px #fff;"
                           href="javascript:;">Unsubscribe</a>
                        <a style="color: #fff;font-family: 'ArialMT', sans-serif; font-size: 12px; text-decoration: none; padding: 0 5px;"
                           href="javascript:;">View in browser</a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
<!--  -->
