<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">
    <title>ProSaoient</title>
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,700" rel="stylesheet">
    <style>
        a {
            text-decoration: none;
            color: #fff;
        }
    </style>
</head>

<body style="background: #f2f2f2; margin: 0; padding: 0;">
<table cellpadding="0" cellspacing="0" cellpadding="0" cellspacing="0" style="width: 600px; margin: auto; font-size: 13px; background-color: #FFF; font-family: Arial ,Helvetica, sans-serif;">
    <tr>
        <td>
            <table cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td style="padding: 25px 20px; background-color: #0d011f; box-shadow: 0 4px 3px -4px #000; text-align: center;">
                        <a href="javascript:;">
                            <img style="width: 135px;" src="<?php echo base_url() ?>public/assets/images/logo-1.png" alt="" />
                        </a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="height: 30px"></td>
    </tr>
    <tr>
        <td style="padding: 0 25px;">
            <table cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" style="width: 100%;">
                            <tr>
                                <td>
                                    <label style="margin: 0; font-weight: bold;font-family: 'ArialMT', sans-serif;">Dear <?= $expertsData[0]['expertsfname'].' '.$expertsData[0]['expertslname'];?>,</label></td>
                            </tr>
                            <tr>
                                <td style="height: 10px"></td>
                            </tr>
                            <tr>
                                <td style="font-family: 'ArialMT', sans-serif;">One of our clients (<span><?= $expertsData[0]['var_company_name']?></span>) has requested a consultation on the following project:</td>
                            </tr>
                            <tr>
                                <td style="height: 15px"></td>
                            </tr>
                            <tr>
                                <td style="font-family: 'ArialMT', sans-serif;">
<!--                                    Project Title<br />-->
                                    <span><?= $expertsData[0]['var_project_name']?></span></td>
                            </tr>
                            <tr>
                                <td style="height: 15px"></td>
                            </tr>
                            <tr>
                                <td style="font-family: 'ArialMT', sans-serif;">
<!--                                    Project Description<br />-->
                                    <span><?= nl2br($expertsData[0]['txt_description']); ?></span></td>
                            </tr>
                            <tr>
                                <td style="height: 15px"></td>
                            </tr>
                            <tr>
                                <td style="font-family: 'ArialMT', sans-serif;">Please let us know your availability by clicking the button below or responding via email</td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="height: 30px"></td>
                </tr>
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" style="width: 100%;">
                            <tr>
                                <td style="width:100%; text-align:center; padding:0 5px">
                                    <?php $urlString = base64_encode(json_encode(['expertsHasProjectId' => $expertsData[0]["id"]]));?>
                                    <a href="<?php echo base_url() . 'expert/availability/?data=' . $urlString ?>" style="background-color:#fff; font-family: 'LucidaGrande';border: 2px solid #662d91;border-radius: 20px;color: #662d91;display: inline-block;font-size: 15px;min-width: 250px;padding: 5px;text-align: center;text-decoration: none;">Provide Times</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table cellpadding="0" cellspacing="0" style="width: 100%;">

                            <tr>
                                <td style="height: 30px"></td>
                            </tr>
                            <tr>
                                <td style="font-family: 'ArialMT', sans-serif;">Best,</td>
                            </tr>
                            <tr>
                                <td style="height: 20px"></td>
                            </tr>
                            <tr>
                                <td style="font-family: 'ArialMT', sans-serif;"><span><?= $expertsData[0]['stafname'];?></span>, Associate<br />
                                    Mobile: <span><?= $expertsData[0]['staffphone'];?></span><br />
                                    <?= $expertsData[0]['staffemail'];?><br />
                                    <a href="http://proSapient.com"><span>http://proSapient.com</span></a></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td style="height: 30px"></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="padding: 15px 20px; background-color: #0d011f; box-shadow: 0 -4px 3px -4px #000; text-align: center;">
            <table cellpadding="0" cellspacing="0" style="width: 100%;">
                <tr>
                    <td style="color: #fff; font-size: 12px;font-family: 'ArialMT', sans-serif;">This email was intended for <?= $expertsData[0]['expertsfname'].' '.$expertsData[0]['expertslname'];?>.</td>
                </tr>
                <tr>
                    <td style="height: 12px"></td>
                </tr>
                <tr>
                    <td>
                        <a href="#">
                            <img style="width: 90px;" src="<?php echo base_url() ?>public/assets/images/logo-1.png" alt="" /></a>
                    </td>
                </tr>
                <tr>
                    <td style="height: 8px"></td>
                </tr>
                <tr>
                    <td>
                        <a style="color: #fff;font-family: 'ArialMT', sans-serif; font-size: 12px; text-decoration: none; padding: 0 8px 0 0; border-right: solid 1px #fff;" href="javascript:;">Unsubscribe</a>
                        <a style="color: #fff;font-family: 'ArialMT', sans-serif; font-size: 12px; text-decoration: none; padding: 0 5px;" href="javascript:;">View in browser</a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
</body>
</html>
<!--  -->
