<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>ProSaoient</title>

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,700" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    <style>
        a {
            text-decoration: none;
            color: #fff;
        }
    </style>
</head>

<body>
    <table cellpadding="0" cellspacing="0" cellpadding="0" cellspacing="0" style="width: 700px; margin: auto; font-size: 13px; background-color: #E2E2E2">
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tr>
                        <td style="padding: 25px 20px; background-color: #0d011f; box-shadow: 0 4px 3px -4px #000; text-align: center;">
                            <a href="javascript:;">
                                <img style="width: 135px;" src="<?php echo base_url(); ?>public/assets/images/logo-1.png" alt="" />
                            </a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="height: 30px"></td>
        </tr>
        <tr>
            <td style="padding: 0 50px;">
                <table cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" style="width: 100%;">
                                <tr>
                                    <td>
                                        <label style="margin: 0; font-weight: bold;font-family: 'ArialMT', sans-serif;">Dear <?= $timeGiven[0]['expetsFname'].' '.$timeGiven[0]['expetsLname']?>,</label></td>
                                </tr>
                                <tr>
                                    <td style="height: 10px"></td>
                                </tr>
                                <tr>
                                    <td style="font-family: 'ArialMT', sans-serif;">Your consultation (with <span><?= $timeGiven[0]['var_company_name'];?></span>) has been confirmed!</td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 30px"></td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" style="width: 100%;">
                                <tr>
                                    <?php
                                    $Daytime = strtotime($timeGiven[0]['dt_date_availability']);

                                    ?>
                                    <th colspan="3" style="background-color: #787281; font-weight: bold; border-bottom: solid 2px #E2E2E2; font-family: 'Open Sans',sans-serif; text-align: center; color: #fff; padding: 3px;"><?=  date('D', $Daytime);?> <?= date('d/m/y',strtotime($timeGiven[0]['dt_date_availability']))?></th>
                                </tr>
                                <tr>
                                    <?php $cenvertedTime = date('H:i:s',strtotime('+1 hour',strtotime($timeGiven[0]['dt_time_availability'])));?>
                                    <td colspan="2" style="background-color: #0D011F; border-bottom: solid 2px #E2E2E2;font-family: 'ArialMT', sans-serif; font-weight: bold; color: #fff; padding: 3px; text-align: center;"><?= date('g:i a',strtotime($timeGiven[0]['dt_time_availability'])); ?> - <?= date('g:i a',strtotime($cenvertedTime)); ?></td>
                                </tr>
                                <tr>
                                    <td style="background-color: #0D011F; width:50%; border-right: solid 2px #E2E2E2;font-family: 'ArialMT', sans-serif; font-weight: bold; color: #fff; padding: 3px; text-align: center;">Dial number: <span>0208 548 5965</span></td>
                                    <td style="background-color: #0D011F; width:50%; color: #fff; padding: 3px;font-family: 'ArialMT', sans-serif; font-weight: bold; text-align: center;">Passcode: <span>128 946</span></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    
                    <tr>
                        <td style="height: 30px"></td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" style="width: 100%;">
                                <tr>
                                    <td style="width:50%; text-align:center; padding:0 5px">
                                        <a href="javascript:;" style="background-color:#fff; font-family: 'LucidaGrande';border: 2px solid #662d91;border-radius: 20px;color: #662d91;display: inline-block;font-size: 15px;min-width: 250px;padding: 5px;text-align: center;text-decoration: none;">Add to Outlook</a>
                                    </td>                                    
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table cellpadding="0" cellspacing="0" style="width: 100%;">
                                
                                <tr>
                                    <td style="height: 30px"></td>
                                </tr>
                                <tr>
                                    <td style="font-family: 'ArialMT', sans-serif;">Thank you for your help.</td>
                                </tr>
                                <tr>
                                    <td style="height: 30px"></td>
                                </tr>
                                <tr>
                                    <td style="font-family: 'ArialMT', sans-serif;">Best,</td>
                                </tr>
                                <tr>
                                    <td style="height: 20px"></td>
                                </tr>
                                <tr>
                                    <td style="font-family: 'ArialMT', sans-serif;"><span><?= $timeGiven[0]['stafName']?></span>, Associate<br />
                                        Direct Line: <span><?= $timeGiven[0]['staffPhone']?></span><br />
                                        Mobile: <span><?= $timeGiven[0]['staffPhone']?></span><br />
                                        <?= $timeGiven[0]['staffEmail']?><br />
                                        <a href="http://proSapient.com"><span>http://proSapient.com</span></a></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 30px"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="padding: 15px 20px; background-color: #0d011f; box-shadow: 0 -4px 3px -4px #000; text-align: center;">
                <table cellpadding="0" cellspacing="0" style="width: 100%;">
                    <tr>
                        <td style="color: #fff; font-size: 12px;font-family: 'ArialMT', sans-serif;">This email was intended for <?= $timeGiven[0]['expetsFname'].' '.$timeGiven[0]['expetsLname']?>.</td>
                    </tr>
                    <tr>
                        <td style="height: 12px"></td>
                    </tr>
                    <tr>
                        <td>
                            <a href="javascript:;">
                                <img style="width: 90px;" src="<?php echo base_url(); ?>public/assets/images/logo-1.png" alt="" /></a>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 8px"></td>
                    </tr>
                    <tr>
                        <td>
                            <a style="color: #fff;font-family: 'ArialMT', sans-serif; font-size: 12px; text-decoration: none; padding: 0 8px 0 0; border-right: solid 1px #fff;" href="javascript:;">Unsubscribe</a>
                            <a style="color: #fff;font-family: 'ArialMT', sans-serif; font-size: 12px; text-decoration: none; padding: 0 5px;" href="javascript:;">View in browser</a>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</body>
</html>
<!--  -->
