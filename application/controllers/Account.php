<?php

class Account extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->library('AuthLibrary');
        $this->load->helper('cookie');
    }

    function index() {
        if (isset($this->session->userdata['valid_login_user'])) {
            redirect('admin/dashboard');
        } else {
            $this->login();
        }
    }

    function login() {
        $data['page'] = "admin/account/login";
        $data['var_meta_title'] = 'Login';
        $data['var_meta_description'] = 'Login';
        $data['var_meta_keyword'] = 'Login';
        $data['js'] = array(
            'admin/login.js'
        );
        $data['js_plugin'] = array(
        );
        $data['css'] = array(
        );
        $data['css_plugin'] = array(
        );
        $data['init'] = array(
            'Login.init()'
        );
        if ($this->input->post()) {
            $this->email = $this->input->post('username');
            $this->password = $this->input->post('password');
            $this->remember = $this->input->post('remember');
            if ($this->email && $this->password) {

                if ($row = $this->authlibrary->loginAttempt($this->email, $this->password)) {

                    if (!empty($row)) {
                        if ($row['enum_enable'] == 'YES') {
                            $this->json_response['status'] = 'success';
                            $this->json_response['message'] = 'Well Done Login Successfully Done..';
                            $this->json_response['redirect'] = admin_url() . 'dashboard';
                            echo json_encode($this->json_response);
                            exit;
                        } else {
                            $this->json_response['status'] = 'warning';
                            $this->json_response['message'] = 'Account has been disabled please contact to Administrator';
                            echo json_encode($this->json_response);
                            exit;
                        }
                    } else {
                        $this->json_response['status'] = 'error';
                        $this->json_response['message'] = 'Username and password does not match';
                        echo json_encode($this->json_response);
                        exit;
                    }
                } else {
                    $this->json_response['status'] = 'error';
                    $this->json_response['message'] = 'Username and password does not match';
                    echo json_encode($this->json_response);
                    exit;
                }
            }
        } else {
            
            $this->load->view(ADMIN_LAYOUT_LOGIN, $data);
        }
    }

    function logout($type) {

        $this->session->unset_userdata('valid_' . $type);
        redirect(base_url());
    }
}

?>