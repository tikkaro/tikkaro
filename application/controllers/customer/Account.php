<?php

class Account extends Customer_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        if (isset($this->session->userdata['valid_customer'])) {
            redirect('customer/my-projects');
        } else {
            $this->login();
        }
    }

    function login()
    {
        $data['page'] = "front/customer/account/login";
        $data['var_meta_title'] = 'Customer Login';
        $data['var_meta_description'] = 'Customer Login';
        $data['var_meta_keyword'] = 'Customer Login';
        $data['js'] = array(
            'login.js'
        );
        $data['js_plugin'] = array();
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['init'] = array(
            'Login.init()'
        );
        if ($this->input->post()) {
            $this->email = $this->input->post('username');
            $this->password = $this->input->post('password');
            if ($this->email && $this->password) {

                if ($response = $this->customerauthlibrary->loginAttempt($this->email, $this->password)) {

                    echo json_encode($response);
                    exit;

                } else {
                    $this->json_response['status'] = 'error';
                    $this->json_response['message'] = 'Something will be wrong';
                    echo json_encode($this->json_response);
                    exit;
                }
            }
        } else {
            $this->load->view(CUSTOMER_LAYOUT_LOGIN, $data);
        }
    }

    function logout()
    {
        $this->session->unset_userdata('valid_customer');
        redirect(customer_url().'login');
    }
}

?>