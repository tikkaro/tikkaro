<?php

class My_projects extends Customer_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('customer/My_projects_model');
    }

    function index()
    {
        $data['page'] = "front/customer/my_projects/projects";
        $data['menuPage'] = "my_projects";
        $data['breadcrumb'] = 'My Projects';
        $data['breadcrumb_sub'] = 'My Projects';
        $data['breadcrumb_list'] = array(
            array('My Projects', ''),
        );
        $data['var_meta_title'] = 'My Projects';
        $data['var_meta_description'] = 'My Projects';
        $data['var_meta_keyword'] = 'My Projects';
        $data['js'] = array(
            'my_projects.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['js_plugin'] = array();
        $data['init'] = array(
            "My_projects.init()"
        );
        $data['allProjects'] = $this->My_projects_model->getCustomerProjectsById();
        $data['shortlisted_status'] = json_decode(SHORTLISTED_STATUS);
//        echo "<pre>";
//        print_r($data['allProjects']); exit;
        $this->load->view(CUSTOMER_LAYOUT, $data);
    }

    function editProject()
    {
        if ($this->input->post()) {
            if ($this->input->post('editType') == 'ProjectDetails') {
                $response = $this->My_projects_model->editProjectDetails($this->input->post());
                echo json_encode($response);
                exit;
            }
        }
    }

}
