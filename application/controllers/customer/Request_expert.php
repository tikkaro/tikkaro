<?php

class Request_expert extends Customer_Controller
{

    function __construct()
    {
        parent::__construct();
    }

    function index()
    {
        $data['page'] = "front/customer/request_expert/request";
        $data['menuPage'] = "request_expert";
        $data['breadcrumb'] = 'Request Expert';
        $data['breadcrumb_sub'] = 'Request Expert';
        $data['breadcrumb_list'] = array(
            array('Request Expert', ''),
        );
        $data['var_meta_title'] = 'Request Expert';
        $data['var_meta_description'] = 'Request Expert';
        $data['var_meta_keyword'] = 'Request Expert';
        $data['js'] = array(
            'request_expert.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['js_plugin'] = array();
        $data['init'] = array(
            "Request_expert.init()"
        );

        if ($this->input->post()) {
            $config = array(
                array('field' => 'projectOnline', 'label' => 'Project Online', 'rules' => 'required'),
                array('field' => 'peopleType', 'label' => 'People Type', 'rules' => 'required'),
                array('field' => 'IndustryExpert', 'label' => 'Industry Expert', 'rules' => 'required'),
                array('field' => 'Geographies', 'label' => 'Geographies', 'rules' => 'required'),
            );

            $this->form_validation->set_rules($config);

            if ($this->form_validation->run() !== FALSE) {

                $mailBody = '';
                $mailBody .= "<label style='display: inline-block; width: 150px;font-size: 15px; margin-bottom: 10px;'>Project Online:</label> <b style='font-size: 15px;'>" . $this->input->post('projectOnline') . "</b><br/>";
                $mailBody .= "<label style='display: inline-block; width: 150px;font-size: 15px; margin-bottom: 10px;'>People Type:</label> <b style='font-size: 15px;'>" . $this->input->post('peopleType') . "</b><br/>";
                $mailBody .= "<label style='display: inline-block; width: 150px;font-size: 15px; margin-bottom: 10px;'>Industry Expert:</label> <b style='font-size: 15px;'>" . $this->input->post('IndustryExpert') . "</b><br/>";
                $mailBody .= "<label style='display: inline-block; width: 150px;font-size: 15px; margin-bottom: 10px;'>Geographies:</label> <b style='font-size: 15px;'>" . $this->input->post('Geographies') . "</b></p>";

                $this->load->library('Mylibrary');
                $configs['to'] = 'jordan@prosapient.com';
                $configs['subject'] = 'Request Expert';
                $configs['mail_body'] = $mailBody;
                if (APP_ENVIRONMENT == 'dev' || APP_ENVIRONMENT == 'local') {
                    $sendMail = true;
                } else {
                    $sendMail = $this->mylibrary->sendMail($configs);
                }

                if ($sendMail) {
                    $json_response['message'] = 'Mail successfully sent!';
                    $json_response['status'] = 'success';
                    $json_response['redirect'] = customer_url('request-expert');
                } else {
                    $json_response['message'] = 'Mail not sent';
                    $json_response['status'] = 'error';
                }
            } else {
                $json_response['message'] = validation_errors();
                $json_response['status'] = 'warning';
            }

            echo json_encode($json_response);
            exit;
        }

        $this->load->view(CUSTOMER_LAYOUT, $data);
    }

}
