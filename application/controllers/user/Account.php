<?php

class Account extends User_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        if (isset($this->session->userdata['valid_user'])) {
            redirect(base_url());
        } else {
            $this->login();
        }
    }

    function login() {

        if ($this->input->post()) {
            $this->email = $this->input->post('username');
            $this->password = $this->input->post('password');
            if ($this->email && $this->password) {

                if ($response = $this->userauthlibrary->loginAttempt($this->email, $this->password)) {
                    echo json_encode($response);
                    exit;
                } else {
                    $this->json_response['status'] = 'error';
                    $this->json_response['message'] = 'Something will be wrong';
                    echo json_encode($this->json_response);
                    exit;
                }
            }
        }
    }

    function logout() {
        $this->session->unset_userdata('valid_user');
        redirect(base_url());
    }

}

?>