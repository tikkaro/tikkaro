<?php

class User_reset_password extends CI_Controller {

    function __construct() {
        parent::__construct();
    }

    function reset_password($id) {
        $data['page'] = "admin/account/user_reset_password";
        $data['var_meta_title'] = 'User Reset Password';
        $data['var_meta_description'] = 'User Reset Password';
        $data['var_meta_keyword'] = 'User Reset Password';
        $data['js'] = array(
            'admin/resetpassword.js'
        );
        $data['js_plugin'] = array(
        );
        $data['css'] = array(
        );
        $data['css_plugin'] = array(
        );
        $data['init'] = array(
            'Resetpassword.reset_user_pass()'
        );

        $data['id'] = base64_decode($id);

        $this->load->view(ADMIN_LAYOUT_LOGIN, $data);
    }

    function resetPassword(){
        if ($this->input->post()) {

            $updatepass = array(
                'var_password' => md5($this->input->post('password')),
            );

            $this->db->where('id',$this->input->post('userid'));
            $res = $this->db->update('user',$updatepass);
            if($res){
                $this->json_response['status'] = 'success';
                $this->json_response['message'] = 'Well Done Password Reset Sucessfully..';
                $this->json_response['redirect'] = base_url();
                echo json_encode($this->json_response); exit;
            }else{
                $this->json_response['status'] = 'error';
                $this->json_response['message'] = 'Something will be worng';
                echo json_encode($this->json_response); exit;
            }
        }
    }
}

?>