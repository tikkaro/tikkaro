<?php

class Find_experts extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/Find_experts_model');
    }

    function index() {
        $data['page'] = "admin/find_experts/experts";
        $data['breadcrumb'] = 'Find Experts';
        $data['breadcrumb_sub'] = 'Find Experts';
        $data['breadcrumb_list'] = array(
            array('Find Experts', 'find-experts'),
        );
        $data['findexperts'] = 'active open';
        $data['findexperts'] = 'active';
        $data['var_meta_title'] = 'Find Experts List';
        $data['var_meta_description'] = 'Find Experts List';
        $data['var_meta_keyword'] = 'Find Experts List';
        $data['js'] = array(
            'admin/experts.js'
        );
        $data['css'] = array(
            'findexperts.css',
        );
        $data['css_plugin'] = array(
            
        );
        $data['js_plugin'] = array(
            
        );
        $data['init'] = array(
            'Experts.findexperts()'
        );

        if($this->input->get()){
            $data['getExperts'] = $this->input->get();
            $data['clientId'] = $this->input->get('clientId');
            $data['projectId'] = $this->input->get('projectId');
            $data['clientProject'] = $this->Find_experts_model->clientProject($data['clientId']);
        }

        $data['allClient'] = $this->Find_experts_model->getClient();
        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function clientProject(){
        if($this->input->post()){
            $result = $this->Find_experts_model->clientProject($this->input->post('client_id'));
            echo json_encode($result);
        }
    }

    function expertFind(){
        if($this->input->post()){
            $result = $this->Find_experts_model->expertShortlist($this->input->post());
            $data['clientProject'] = $this->input->post();
            $data['experts'] = $result;
            echo $this->load->view('admin/find_experts/expertsfind',$data);
        }
    }

    function addShortlist(){
        if($this->input->post()){
            $this->json_response  = $this->Find_experts_model->addtoShortlist($this->input->post());
            echo json_encode($this->json_response);
        }
    }

}
