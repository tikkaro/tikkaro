<?php

class Project extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();

        $this->load->model('admin/Project_model');
        $this->load->model('admin/Client_model');
    }

    function index()
    {
        $data['page'] = "admin/project/list";
        $data['breadcrumb'] = 'Project';
        $data['breadcrumb_sub'] = 'Project';
        $data['breadcrumb_list'] = array(
            array('Project', ''),
            array('list', ''),
        );
        $data['project'] = 'active open';
        $data['project'] = 'active';
        $data['var_meta_title'] = 'Project List';
        $data['var_meta_description'] = 'Project List';
        $data['var_meta_keyword'] = 'Project List';
        $data['js'] = array(
            'admin/project.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['js_plugin'] = array();
        $data['init'] = array(
            'Project.init()',
        );

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function getProjectList()
    {
        $this->load->library('Datatables');
        $res = $this->Project_model->getProjectDatatables();
        echo json_encode($res);
        exit;
    }

    function add()
    {
        if ($this->input->get()) {

            if ($this->input->get('clientId')) {
                $clientData = $this->toval->idtorowarr('id', $this->input->get('clientId'), 'client');
                if (empty($clientData)) {
                    $this->json_response['status'] = 'error';
                    $this->json_response['message'] = 'Client Not Found';
                    $this->session->set_flashdata($this->json_response);
                    return redirect(admin_url('project/add'));
                }
            }

            $data['clientId'] = $this->input->get('clientId');
            $data['assignAccountManager'] = $clientData['fk_assign_account_manager'];
            $data['userData'] = $this->db->get_where('user', array('fk_client' => $this->input->get('clientId')))->result_array();
            $data['pageFrom'] = 'client';
        }
        $data['page'] = "admin/project/add";
        $data['breadcrumb'] = 'Project';
        $data['breadcrumb_sub'] = 'Project';
        $data['breadcrumb_list'] = array(
            array('Project', 'project'),
            array('Add', ''),
        );
        $data['project'] = 'active open';
        $data['list'] = 'active';
        $data['var_meta_title'] = 'Project Add';
        $data['var_meta_description'] = 'Project Add';
        $data['var_meta_keyword'] = 'Project Add';
        $data['js'] = array(
            'admin/project.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array(
            'bootstrap-tagsinput/bootstrap-tagsinput.css',
            'typeahead/typeahead.css',
        );
        $data['js_plugin'] = array(
            'bootstrap-tagsinput/bootstrap-tagsinput.min.js',
            'typeahead/handlebars.min.js',
            'typeahead/typeahead.bundle.js',
            'typeahead/typeahead.bundle.min.js',
        );
        $data['any_js'] = array(
            'components-bootstrap-tagsinput.min.js',
        );
        $data['clientData'] = $this->db->get('client')->result_array();


        $data['init'] = array(
            'Project.add_project()',
            'ComponentsDateTimePickers.init()',
        );
        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function addProject()
    {
        if ($this->input->post() && $this->input->is_ajax_request()) {
            $this->json_response = $this->Client_model->addProject($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }
    }

    function getUserByClientId()
    {
        if ($this->input->post() && $this->input->is_ajax_request()) {
            $clientId = $this->input->post('clientId');
            $result = $this->db->get_where('user', array('fk_client' => $clientId))->result_array();
            echo json_encode($result);
            exit;
        }
    }

//
    function overview($projectId)
    {

        $projectData = $this->toval->idtorowarr('id', $projectId, 'client_has_project');
        if (empty($projectData)) {
            $this->json_response['status'] = 'error';
            $this->json_response['message'] = 'Project Not Found';
            $this->session->set_flashdata($this->json_response);
            return redirect(admin_url('project'));
        }

        $data['page'] = "admin/project/overview";
        $data['breadcrumb'] = 'Client';
        $data['breadcrumb_sub'] = 'Client';
        $data['breadcrumb_list'] = array(
            array('Project', 'project'),
            array('Overview', ''),
        );
        $data['project'] = 'active open';
        $data['project'] = 'active';
        $data['var_meta_title'] = 'Project Overview';
        $data['var_meta_description'] = 'Project Overview';
        $data['var_meta_keyword'] = 'Project Overview';
        $data['js'] = array(
            'admin/project.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array(
            'bootstrap-tagsinput/bootstrap-tagsinput.css',
            'typeahead/typeahead.css',
        );
        $data['js_plugin'] = array(
            'bootstrap-tagsinput/bootstrap-tagsinput.min.js',
            'typeahead/handlebars.min.js',
            'typeahead/typeahead.bundle.min.js',
        );
        $data['init'] = array(
            'Project.overview()',
            'ComponentsDateTimePickers.init()',
        );

        $data['projectId'] = $projectId;
        $data['projectData'] = $this->Project_model->getProjectCommanHeader($projectId);
        $data['projectCountry'] = $this->Project_model->getContryData($projectId);
        $data['projectCompany'] = $this->Project_model->getCompanyData($projectId);
        $data['projectRegions'] = $this->Project_model->getRegionsData($projectId);
        $data['userdata'] = $this->toval->idtorowarray('fk_client', $data['projectData']['fk_client'], 'user');

        $this->load->view(ADMIN_LAYOUT, $data);
    }

//    function getProject(){
//        if($this->input->post()){
//            $projectData = $this->Project_model->getProjectCommanHeader($this->input->post('project_id'));
//            echo json_encode($projectData);
//        }
//    }

    function expert_shortlist($projectId)
    {
        $projectData = $this->toval->idtorowarr('id', $projectId, 'client_has_project');
        if (empty($projectData)) {
            $this->json_response['status'] = 'error';
            $this->json_response['message'] = 'Project Not Found';
            $this->session->set_flashdata($this->json_response);
            return redirect(admin_url('project'));
        }

        $data['page'] = "admin/project/shortlist";
        $data['breadcrumb'] = 'Project';
        $data['breadcrumb_sub'] = 'Project';
        $data['breadcrumb_list'] = array(
            array('Project', 'project'),
            array('Expert Shortlist', ''),
        );
        $data['project'] = 'active open';
        $data['project'] = 'active';
        $data['var_meta_title'] = 'Project Expert Shortlist';
        $data['var_meta_description'] = 'Project Expert Shortlist';
        $data['var_meta_keyword'] = 'Project Expert Shortlist';
        $data['js'] = array(
            'admin/project.js',
        );
        $data['css'] = array(
            'findexperts.css',
        );
        $data['css_plugin'] = array(
            'typeahead/typeahead.css',
        );
        $data['js_plugin'] = array(
            'typeahead/handlebars.min.js',
            'typeahead/typeahead.bundle.min.js',
        );
        $data['init'] = array(
            'Project.experts_init()',
        );
        $data['projectId'] = $projectId;
        $data['projectData'] = $this->Project_model->getProjectCommanHeader($projectId);

//        $data['experts_shortlisted'] = $this->Project_model->getShortlistedExperts($projectId);
        $this->load->view(ADMIN_LAYOUT, $data);
    }


    function shortListedExperts()
    {
        if ($this->input->post()) {
            $data['experts_shortlisted'] = $this->Project_model->getShortlistedExperts($this->input->post());
            echo $this->load->view('admin/project/shortlistexperts', $data);
        }
    }

    function expert_prospects($projectId)
    {

        $projectData = $this->toval->idtorowarr('id', $projectId, 'client_has_project');
        if (empty($projectData)) {
            $this->json_response['status'] = 'error';
            $this->json_response['message'] = 'Project Not Found';
            $this->session->set_flashdata($this->json_response);
            return redirect(admin_url('project'));
        }


        $data['page'] = "admin/project/prospect_list";
        $data['breadcrumb'] = 'Project';
        $data['breadcrumb_sub'] = 'Project';
        $data['breadcrumb_list'] = array(
            array('Project', 'project'),
            array('Prospects', ''),
        );
        $data['project'] = 'active open';
        $data['project'] = 'active';
        $data['var_meta_title'] = 'Project Expert Shortlist';
        $data['var_meta_description'] = 'Project Expert Shortlist';
        $data['var_meta_keyword'] = 'Project Expert Shortlist';
        $data['js'] = array(
            'admin/project.js',
        );
        $data['css'] = array(
            'findexperts.css',
        );
        $data['css_plugin'] = array(
            'typeahead/typeahead.css',
        );
        $data['js_plugin'] = array(
            'typeahead/handlebars.min.js',
            'typeahead/typeahead.bundle.min.js',
        );
        $data['init'] = array(
            'Project.prospect_init()'
        );

        $data['projectId'] = $projectId;
        $data['projectData'] = $this->Project_model->getProjectCommanHeader($projectId);
        $data['prospectData'] = $this->Project_model->getProspects($projectId);

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function addProspect()
    {
        if ($this->input->post() && $this->input->is_ajax_request()) {
            $this->json_response = $this->Project_model->addProspect($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }
    }

    function addProspectNote()
    {
        if ($this->input->post() && $this->input->is_ajax_request()) {
            $this->json_response = $this->Project_model->addProspectNote($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }
    }

    function getProspectNoteById()
    {
        if ($this->input->post() && $this->input->is_ajax_request()) {
            $result = $this->toval->idtorowarr('id', $this->input->post('noteId'), 'prospect_has_notes');
            echo json_encode($result);
            exit;
        }
    }

    function compliance_rules($projectId)
    {

        $projectData = $this->toval->idtorowarr('id', $projectId, 'client_has_project');

        if (empty($projectData)) {
            $this->json_response['status'] = 'error';
            $this->json_response['message'] = 'Project Not Found';
            $this->session->set_flashdata($this->json_response);
            return redirect(admin_url('project'));
        }


        $data['page'] = "admin/project/rules";
        $data['breadcrumb'] = 'Project';
        $data['breadcrumb_sub'] = 'Project';
        $data['breadcrumb_list'] = array(
            array('Project', 'project'),
            array('Compliance Rules', ''),
        );
        $data['project'] = 'active open';
        $data['project'] = 'active';
        $data['var_meta_title'] = 'Project Rules';
        $data['var_meta_description'] = 'Project Rules';
        $data['var_meta_keyword'] = 'Project Rules';
        $data['js'] = array(
            'admin/project.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['js_plugin'] = array();
        $data['init'] = array(
            'Project.rules_init()',
        );
        $data['projectId'] = $projectId;
        $data['projectData'] = $this->Project_model->getProjectCommanHeader($projectId);
        $data['complianceData'] = $this->toval->idtorowarr('fk_client', $data['projectData']['fk_client'], 'client_has_compliance_officer');
        $data['clientProjectRules'] = $this->Project_model->getClientProjectRules($projectData['fk_client']);
        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function notes($projectId)
    {

        $projectData = $this->toval->idtorowarr('id', $projectId, 'client_has_project');
        if (empty($projectData)) {
            $this->json_response['status'] = 'error';
            $this->json_response['message'] = 'Project Not Found';
            $this->session->set_flashdata($this->json_response);
            return redirect(admin_url('project'));
        }

        $data['page'] = "admin/project/notes";
        $data['breadcrumb'] = 'Client';
        $data['breadcrumb_sub'] = 'Client';
        $data['breadcrumb_list'] = array(
            array('Project', 'project'),
            array('Notes', ''),
        );
        $data['project'] = 'active open';
        $data['project'] = 'active';
        $data['var_meta_title'] = 'Project Notes';
        $data['var_meta_description'] = 'Project Notes';
        $data['var_meta_keyword'] = 'Project Notes';
        $data['js'] = array(
            'admin/project.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['js_plugin'] = array();
        $data['init'] = array(
            'Project.notes_init()',
        );
        $data['projectId'] = $projectId;
        $data['projectData'] = $this->Project_model->getProjectCommanHeader($projectId);

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function getNotesList($projectId)
    {
        $this->load->library('Datatables');
        $res = $this->Project_model->getNotesDatatables($projectId);
        echo $res;
        exit;
    }

    function getProjectNoteById()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $projectId = $this->input->post('id');
            $result = $this->db->get_where('project_has_notes', array('id' => $projectId))->row_array();
            echo json_encode($result);
            exit;
        }
    }

    function addProjectNote()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $this->json_response = $this->Project_model->addProjectNote($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }
    }

    function deleteProjectNote()
    {
        if ($this->input->post()) {

            $this->json_response = $this->Project_model->deleteProjectNote($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }
    }

    function calls($projectId)
    {

        $projectData = $this->toval->idtorowarr('id', $projectId, 'client_has_project');
        if (empty($projectData)) {
            $this->json_response['status'] = 'error';
            $this->json_response['message'] = 'Project Not Found';
            $this->session->set_flashdata($this->json_response);
            return redirect(admin_url('project'));
        }

        $data['page'] = "admin/project/calls";
        $data['breadcrumb'] = 'Project';
        $data['breadcrumb_sub'] = 'Project';
        $data['breadcrumb_list'] = array(
            array('Project', 'project'),
            array('Calls', ''),
        );
        $data['project'] = 'active open';
        $data['project'] = 'active';
        $data['var_meta_title'] = 'Project Calls';
        $data['var_meta_description'] = 'Project Calls';
        $data['var_meta_keyword'] = 'Project Calls';
        $data['js'] = array(
            'admin/project.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['js_plugin'] = array();
        $data['init'] = array(
            'Project.call_init()',
        );
        $data['projectId'] = $projectId;
        $data['projectData'] = $this->Project_model->getProjectCommanHeader($projectId);
        $this->load->view(ADMIN_LAYOUT, $data);
    }



    function getProjectCallList()
    {
        $this->load->library('Datatables');
        $res = $this->Project_model->getProjectCallDatatables();
        echo json_encode($res);
        exit;
    }

    function sendBio()
    {
        if ($this->input->post()) {
            $this->json_response = $this->Project_model->bioSend($this->input->post());
            echo json_encode($this->json_response);
            exit;
        }
    }

    function checkValidExperts()
    {
        if ($this->input->post()) {
            $this->json_response = $this->Project_model->checkValidExperts($this->input->post());
            echo json_encode($this->json_response);
            exit;
        }
    }

    function updateTags()
    {
        if ($this->input->post()) {
            $this->json_response = $this->Project_model->updateTags($this->input->post());
            echo json_encode($this->json_response);
            exit;
        }
    }

    function getCountry()
    {
        $result = $this->db->get('master_country')->result_array();
        for ($i = 0; $i < count($result); $i++) {
            $res[] = $result[$i]['var_country_name'];
        }
        echo json_encode($res);
        exit;
    }

    function getCompany()
    {
        $result = $this->db->get('master_company')->result_array();
        for ($i = 0; $i < count($result); $i++) {
            $res[] = $result[$i]['var_company_name'];
        }
        echo json_encode($res);
        exit;
    }

    function getRegions()
    {
        $result = $this->db->get('master_regions')->result_array();
        for ($i = 0; $i < count($result); $i++) {
            $res[] = $result[$i]['var_regions_name'];
        }
        echo json_encode($res);
        exit;
    }

    function addProjectExpertNote()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $this->json_response = $this->Project_model->addProjectExpertNote($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }
    }

    function getProjecthasExpertsNotes()
    {
        if ($this->input->post()) {
            $ProjecthasExpertsNotesId = $this->input->post('noteId');
            $result = $this->db->get_where('project_has_experts_notes', array('id' => $ProjecthasExpertsNotesId))->row_array();
            echo json_encode($result);
            exit;
        }
    }

    function updateDeadlineDate()
    {
        if ($this->input->post()) {
            $this->json_response = $this->Project_model->updateDeadlineDate($this->input->post());
            echo json_encode($this->json_response);
            exit;
        }
    }

    function getAngles(){

        $angles = $this->db->get('master_angle')->result_array();

        if(!empty($angles)){
            for($i=0; $i<count($angles); $i++){
                $res[] = $angles[$i]['var_name'];
            }

            echo json_encode($res); exit;
        }
    }

    function updateAngle(){
        if($this->input->post()){
            $this->json_response = $this->Project_model->updateAngle($this->input->post());
        }
    }


}

?>