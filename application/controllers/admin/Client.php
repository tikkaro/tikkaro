<?php

class Client extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('admin/Client_model');
    }

    function index()
    {
        $data['page'] = "admin/client/client/list";
        $data['breadcrumb'] = 'Client';
        $data['breadcrumb_sub'] = 'Client';
        $data['breadcrumb_list'] = array(
            array('Clients', ''),
            array('list', ''),
        );
        $data['clients'] = 'active open';
        $data['clients'] = 'active';
        $data['var_meta_title'] = 'Client List';
        $data['var_meta_description'] = 'Client List';
        $data['var_meta_keyword'] = 'Client List';
        $data['js'] = array(
            'admin/client.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['js_plugin'] = array();
        $data['init'] = array(
            'Client.init()',
        );

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function getClientList()
    {
        $this->load->library('Datatables');
        $res = $this->Client_model->getClientDatatables();
        echo json_encode($res);
        exit;
    }

    function add()
    {
        $data['page'] = "admin/client/client/add";
        $data['breadcrumb'] = 'Client';
        $data['breadcrumb_sub'] = 'Client';
        $data['breadcrumb_list'] = array(
            array('Clients', 'client'),
            array('Add', ''),
        );
        $data['clients'] = 'active open';
        $data['list'] = 'active';
        $data['var_meta_title'] = 'Client Add';
        $data['var_meta_description'] = 'Client Add';
        $data['var_meta_keyword'] = 'Client Add';
        $data['js'] = array(
            'admin/client.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array(
            'bootstrap-fileinput/bootstrap-fileinput.css'
        );
        $data['js_plugin'] = array(
            'jquery.form.min.js',
            'ajaxfileupload.js',
            'bootstrap-fileinput/bootstrap-fileinput.js'
        );
        $data['init'] = array(
            'ComponentsDateTimePickers.init()',
            'Client.addClient()',
        );
        $data['teamData'] = $this->db->get('team')->result_array();
        $data['accountManager'] = $this->db->get('account_manager')->result_array();

        if ($this->input->post() && $this->input->is_ajax_request()) {
            $this->json_response = $this->Client_model->addClient($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }
        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function overview($clientId)
    {
        $clientData = $this->toval->idtorowarr('id', $clientId, 'client');

        if (empty($clientData)) {
            $this->json_response['status'] = 'error';
            $this->json_response['message'] = 'Client Not Found';
            $this->session->set_flashdata($this->json_response);
            return redirect(admin_url('client'));
        }

        $data['page'] = "admin/client/client/overview";
        $data['breadcrumb'] = 'Client';
        $data['breadcrumb_sub'] = 'Client';
        $data['breadcrumb_list'] = array(
            array('Clients', 'client'),
            array('Overview', ''),
        );
        $data['clients'] = 'active open';
        $data['clients'] = 'active';
        $data['var_meta_title'] = 'Client Overview';
        $data['var_meta_description'] = 'Client Overview';
        $data['var_meta_keyword'] = 'Client Overview';
        $data['js'] = array(
            'admin/client.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['js_plugin'] = array();
        $data['clientId'] = $clientId;
        $data['clientData'] = $this->Client_model->getClientCommonHeader($clientId);
        $data['subscriptionData'] = $this->Client_model->getClientSubscription($clientId);

        $data['teamData'] = $this->db->get('team')->result_array();
        $data['accountManager'] = $this->db->get('account_manager')->result_array();

        $data['init'] = array(
            'Client.overview_init()',
            'ComponentsDateTimePickers.init()',
        );

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function getOverviewList($id)
    {

        $this->load->library('Datatables');
        echo $this->Client_model->getOverviewDatatables($id);
        exit;
    }

    function rules($clientId)
    {

        $clientData = $this->toval->idtorowarr('id', $clientId, 'client');

        if (empty($clientData)) {
            $this->json_response['status'] = 'error';
            $this->json_response['message'] = 'Client Not Found';
            $this->session->set_flashdata($this->json_response);
            return redirect(admin_url('client'));
        }

        $data['page'] = "admin/client/client/rules";
        $data['breadcrumb'] = 'Client';
        $data['breadcrumb_sub'] = 'Client';
        $data['breadcrumb_list'] = array(
            array('Client', 'client'),
            array('Rules', ''),
        );
        $data['clients'] = 'active open';
        $data['clients'] = 'active';
        $data['var_meta_title'] = 'Client Rules';
        $data['var_meta_description'] = 'Client Rules';
        $data['var_meta_keyword'] = 'Client Rules';
        $data['js'] = array(
            'admin/client.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['js_plugin'] = array();
        $data['init'] = array(
            'Client.rules_init()',
        );
        $data['clientid'] = $clientId;

        $data['clientData'] = $this->Client_model->getClientCommonHeader($clientId);
        $data['masterRules'] = $this->Client_model->getRules();
        $data['clientRules'] = $this->Client_model->getClientRules($clientId);

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function addRules()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $this->json_response = $this->Client_model->addRules($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }
    }

    function deleteRules()
    {
        if ($this->input->post()) {
            $this->json_response = $this->Client_model->deleteClientRules($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }
    }

    function notes($clientId)
    {
        $clientData = $this->toval->idtorowarr('id', $clientId, 'client');

        if (empty($clientData)) {
            $this->json_response['status'] = 'error';
            $this->json_response['message'] = 'Client Not Found';
            $this->session->set_flashdata($this->json_response);
            return redirect(admin_url('client'));
        }

        $data['page'] = "admin/client/client/notes";
        $data['breadcrumb'] = 'Client';
        $data['breadcrumb_sub'] = 'Client';
        $data['breadcrumb_list'] = array(
            array('Client', 'client'),
            array('Notes', ''),
        );
        $data['clients'] = 'active open';
        $data['clients'] = 'active';
        $data['var_meta_title'] = 'Client Notes';
        $data['var_meta_description'] = 'Client Notes';
        $data['var_meta_keyword'] = 'Client Notes';
        $data['js'] = array(
            'admin/client.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['js_plugin'] = array();
        $data['init'] = array(
            'Client.notes_init()',
        );
        $data['clientId'] = $clientId;
        $data['clientData'] = $this->Client_model->getClientCommonHeader($clientId);
        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function getNotesList($clientId)
    {
        $this->load->library('Datatables');
        $res = $this->Client_model->getNotesDatatables($clientId);
        echo $res;
        exit;
    }

    function project($clientId)
    {

        $clientData = $this->toval->idtorowarr('id', $clientId, 'client');

        if (empty($clientData)) {
            $this->json_response['status'] = 'error';
            $this->json_response['message'] = 'Client Not Found';
            $this->session->set_flashdata($this->json_response);
            return redirect(admin_url('client'));
        }
        $data['page'] = "admin/client/client/project";
        $data['breadcrumb'] = 'Client';
        $data['breadcrumb_sub'] = 'Client';
        $data['breadcrumb_list'] = array(
            array('Client', 'client'),
            array('Project', ''),
        );
        $data['clients'] = 'active open';
        $data['clients'] = 'active';
        $data['var_meta_title'] = 'Client Project';
        $data['var_meta_description'] = 'Client Project';
        $data['var_meta_keyword'] = 'Client Project';
        $data['js'] = array(
            'admin/client.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['js_plugin'] = array();
        $data['init'] = array(
            'Client.project_init()',
        );

        $data['clientId'] = $clientId;
        $data['clientData'] = $this->Client_model->getClientCommonHeader($clientId);

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function getProjectList($clientid)
    {
        $this->load->library('Datatables');
        $res = $this->Client_model->getProjectDatatables($clientid);
        echo json_encode($res);
        exit;
    }

    function compliance_officer($clientId)
    {
        $clientData = $this->toval->idtorowarr('id', $clientId, 'client');

        if (empty($clientData)) {
            $this->json_response['status'] = 'error';
            $this->json_response['message'] = 'Client Not Found';
            $this->session->set_flashdata($this->json_response);
            return redirect(admin_url('client'));
        }

        $data['page'] = "admin/client/client/compliance_officer";
        $data['breadcrumb'] = 'Client';
        $data['breadcrumb_sub'] = 'Client';
        $data['breadcrumb_list'] = array(
            array('Client', 'client'),
            array('Compliance Officer', ''),
        );
        $data['clients'] = 'active open';
        $data['clients'] = 'active';
        $data['var_meta_title'] = 'Client Compliance Officer';
        $data['var_meta_description'] = 'Client Compliance Officer';
        $data['var_meta_keyword'] = 'Client Compliance Officer';
        $data['js'] = array(
            'admin/client.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['js_plugin'] = array(
            'jquery.form.min.js',
            'ajaxfileupload.js',
        );
        $data['init'] = array(
            'Client.officer()',
        );
        $data['clientId'] = $clientId;
        $data['clientData'] = $this->Client_model->getClientCommonHeader($clientId);
        $data['complianceData'] = $this->toval->idtorowarr('fk_client', $clientId, 'client_has_compliance_officer');

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function client_call_history($clientId)
    {

        $data['page'] = "admin/client/client/call_history";
        $data['breadcrumb'] = 'Client';
        $data['breadcrumb_sub'] = 'Client';
        $data['breadcrumb_list'] = array(
            array('Client', 'client'),
            array('Call History', ''),
        );
        $data['clients'] = 'active open';
        $data['clients'] = 'active';
        $data['var_meta_title'] = 'Client Call History';
        $data['var_meta_description'] = 'Client Call History';
        $data['var_meta_keyword'] = 'Client Call History';
        $data['js'] = array(
            'admin/client.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['js_plugin'] = array();
        $data['init'] = array(
            'Client.call_history()',
        );

        $data['clientId'] = $clientId;
        $data['clientData'] = $this->Client_model->getClientCommonHeader($clientId);

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function getClientCallList()
    {

        $this->load->library('Datatables');
        $res = $this->Client_model->getClientCallList();
        echo json_encode($res);
        exit;
    }

    function userbio($userid)
    {
        $data['page'] = "admin/client/user/bio";
        $data['breadcrumb'] = 'Client';
        $data['breadcrumb_sub'] = 'Client';
        $data['breadcrumb_list'] = array(
            array('Client', 'client'),
            array('User', ''),
        );
        $data['clients'] = 'active open';
        $data['clients'] = 'active';
        $data['var_meta_title'] = 'Client User';
        $data['var_meta_description'] = 'Client User';
        $data['var_meta_keyword'] = 'Client User';
        $data['js'] = array(
            'admin/client.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['js_plugin'] = array();
        $data['init'] = array(
            'Client.user_reset_pass()',
        );
        $data['userid'] = $userid;

        $data['userData'] = $this->Client_model->getUsersData($userid);

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function usernotes($userid)
    {
        $data['page'] = "admin/client/user/notes";
        $data['breadcrumb'] = 'Client';
        $data['breadcrumb_sub'] = 'Client';
        $data['breadcrumb_list'] = array(
            array('Client', 'client'),
            array('User', ''),
            array('Notes', ''),
        );
        $data['clients'] = 'active open';
        $data['clients'] = 'active';
        $data['var_meta_title'] = 'Client User Notes';
        $data['var_meta_description'] = 'Client User Notes';
        $data['var_meta_keyword'] = 'Client User Notes';
        $data['js'] = array(
            'admin/client.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['js_plugin'] = array();
        $data['init'] = array(
            'Client.user_notes_init()',
        );
        $data['userId'] = $userid;
        $data['noteData'] = $this->Client_model->getUsersData($userid);

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function addUserNote()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $this->json_response = $this->Client_model->addUserNote($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }
    }

    function addClientNote()
    {

        if ($this->input->is_ajax_request() && $this->input->post()) {
            $this->json_response = $this->Client_model->addClientNote($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }
    }

    function deleteUserNote()
    {
        if ($this->input->post()) {
            $this->json_response = $this->Client_model->deleteUserNote($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }
    }

    function deleteClientNote()
    {
        if ($this->input->post()) {
            $this->json_response = $this->Client_model->deleteClientNote($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }
    }

    function getNoteById()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $result = $this->db->get_where('user_has_notes', array('id' => $this->input->post('noteId')))->row_array();
            echo json_encode($result);
            exit;
        }
    }

    function getClientNoteById()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $result = $this->db->get_where('client_has_notes', array('id' => $this->input->post('noteId')))->row_array();
            echo json_encode($result);
            exit;
        }
    }

    function user_call_history($userid)
    {

        $data['page'] = "admin/client/user/call_history";
        $data['breadcrumb'] = 'Client';
        $data['breadcrumb_sub'] = 'Client';
        $data['breadcrumb_list'] = array(
            array('Client', 'client'),
            array('User', ''),
            array('Call History', ''),
        );
        $data['clients'] = 'active open';
        $data['clients'] = 'active';
        $data['var_meta_title'] = 'Client User Call History';
        $data['var_meta_description'] = 'Client User Call History';
        $data['var_meta_keyword'] = 'Client User Call History';
        $data['js'] = array(
            'admin/client.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['js_plugin'] = array();
        $data['init'] = array(
            'Client.user_call_history()',
        );

        $data['userid'] = $userid;

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function userproject($userid)
    {
        $data['page'] = "admin/client/user/project";
        $data['breadcrumb'] = 'Client';
        $data['breadcrumb_sub'] = 'Client';
        $data['breadcrumb_list'] = array(
            array('Client', 'client'),
            array('User', ''),
            array('Project', ''),
        );
        $data['clients'] = 'active open';
        $data['clients'] = 'active';
        $data['var_meta_title'] = 'Client User Project';
        $data['var_meta_description'] = 'Client User Project';
        $data['var_meta_keyword'] = 'Client User Project';
        $data['js'] = array(
            'admin/client.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['js_plugin'] = array();
        $data['init'] = array(
            'Client.user_project_list()',
        );

        $data['userid'] = $userid;
        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function getUserProjectList()
    {

        $this->load->library('Datatables');
        $res = $this->Client_model->getUserProjectList();
        echo json_encode($res);
        exit;
    }

    function getUserCallList()
    {

        $this->load->library('Datatables');
        $res = $this->Client_model->getUserCallList();
        echo json_encode($res);
        exit;
    }

    function subscription_history($clientId)
    {
        $data['page'] = "admin/client/client/subscription_history";
        $data['breadcrumb'] = 'Client';
        $data['breadcrumb_sub'] = 'Client';
        $data['breadcrumb_list'] = array(
            array('Client', 'client'),
            array('Subscription History', ''),
        );
        $data['clients'] = 'active open';
        $data['clients'] = 'active';
        $data['var_meta_title'] = 'Client Subscription History';
        $data['var_meta_description'] = 'Client Subscription History';
        $data['var_meta_keyword'] = 'Client Subscription History';
        $data['js'] = array(
            'admin/client.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['js_plugin'] = array();
        $data['init'] = array(
            'Client.history_init()',
            'ComponentsDateTimePickers.init()',
        );

        $data['clientId'] = $clientId;

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function useradd($clientId)
    {

        $clientData = $this->toval->idtorowarr('id', $clientId, 'client');

        if (empty($clientData)) {
            $this->json_response['status'] = 'error';
            $this->json_response['message'] = 'Client Not Found';
            $this->session->set_flashdata($this->json_response);
            return redirect(admin_url('client'));
        }

        $data['page'] = "admin/client/user/add";
        $data['breadcrumb'] = 'Client';
        $data['breadcrumb_sub'] = 'Client';
        $data['breadcrumb_list'] = array(
            array('Client', 'client'),
            array('User', ''),
            array('Add', ''),
        );
        $data['clients'] = 'active open';
        $data['clients'] = 'active';
        $data['var_meta_title'] = 'Client User Add';
        $data['var_meta_description'] = 'Client User Add';
        $data['var_meta_keyword'] = 'Client User Add';
        $data['js'] = array(
            'admin/client.js'
        );
        $data['css'] = array();
        $data['css_plugin'] = array(
            'bootstrap-fileinput/bootstrap-fileinput.css'
        );
        $data['js_plugin'] = array(
            'jquery.form.min.js',
            'ajaxfileupload.js',
            'bootstrap-fileinput/bootstrap-fileinput.js'
        );
        $data['init'] = array(
            'ComponentsDateTimePickers.init()',
            'Client.addUser()',
        );

        $data['clientId'] = $clientId;
        $data['clientData'] = $clientData;

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function addUser()
    {
        if ($this->input->post() && $this->input->is_ajax_request()) {
            $post = $this->input->post();
            $this->json_response = $this->Client_model->Useradd($post, $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }
    }

    function getUserNotesList($id)
    {
        $this->load->library('Datatables');
        $res = $this->Client_model->getUserNotesDatatables($id);
        echo $res;
        exit;
    }

    function projectadd($clientId)
    {

        $clientData = $this->toval->idtorowarr('id', $clientId, 'client');

        if (empty($clientData)) {
            $this->json_response['status'] = 'error';
            $this->json_response['message'] = 'Client Not Found';
            $this->session->set_flashdata($this->json_response);
            return redirect(admin_url('client'));
        }

        $data['page'] = "admin/client/client/addproject";
        $data['breadcrumb'] = 'Client';
        $data['breadcrumb_sub'] = 'Client';
        $data['breadcrumb_list'] = array(
            array('Client', 'client'),
            array('User', ''),
            array('Add', ''),
        );
        $data['clients'] = 'active open';
        $data['clients'] = 'active';
        $data['var_meta_title'] = 'Client User Add';
        $data['var_meta_description'] = 'Client User Add';
        $data['var_meta_keyword'] = 'Client User Add';
        $data['js'] = array(
            'admin/client.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['js_plugin'] = array(
            'jquery.form.min.js',
            'ajaxfileupload.js',
        );
        $data['init'] = array(
            'Client.add_project()',
            'ComponentsDateTimePickers.init()',
        );

        $data['clientData'] = $clientData;
        $data['clientid'] = $clientId;
        $data['userdata'] = $this->toval->idtorowarray('fk_client', $clientId, 'user');

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function project_add()
    {

        if ($this->input->post() && $this->input->is_ajax_request()) {
            $this->json_response = $this->Client_model->addProject($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }
    }

    function getOfficer()
    {
        if ($this->input->post()) {
            $complianceData = $this->toval->idtorowarr('fk_client', $this->input->post('client_id'), 'client_has_compliance_officer');
            echo json_encode($complianceData);
        }
    }

    function updateOfficer()
    {

        if ($this->input->post() && $this->input->is_ajax_request()) {
            $this->json_response = $this->Client_model->updateOfficer($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }
    }

    function addSubscription()
    {
        if ($this->input->post() && $this->input->is_ajax_request()) {
            $this->json_response = $this->Client_model->addSubscription($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }
    }

    function userResetpassword()
    {
        if ($this->input->post() && $this->input->is_ajax_request()) {
            $this->json_response = $this->Client_model->userResetpassword($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }
    }

    function deleteClient()
    {
        if ($this->input->post()) {
            $this->json_response = $this->Client_model->deleteClient($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }
    }

}
