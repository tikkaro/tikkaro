<?php

class Team extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('admin/Team_model');
    }

    function index()
    {
        $data['page'] = "admin/team/list";
        $data['breadcrumb'] = 'Team';
        $data['breadcrumb_sub'] = 'Team';
        $data['breadcrumb_list'] = array(
            array('Team', ''),
            array('List', ''),
        );
        $data['team'] = 'active open';
        $data['team'] = 'active';
        $data['var_meta_title'] = 'Team List';
        $data['var_meta_description'] = 'Team List';
        $data['var_meta_keyword'] = 'Team List';
        $data['js'] = array(
            'admin/team.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['js_plugin'] = array();
        $data['init'] = array(
            'Team.init()',
        );

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function getTeamList()
    {
        $this->load->library('Datatables');
        echo $this->Team_model->getTeamDatatables();
        exit;
    }

    function add()
    {
        $data['page'] = "admin/team/add";
        $data['breadcrumb'] = 'Team';
        $data['breadcrumb_sub'] = 'Team';
        $data['breadcrumb_list'] = array(
            array('Team', 'team'),
            array('Add', ''),
        );
        $data['team'] = 'active open';
        $data['team'] = 'active';
        $data['var_meta_title'] = 'Team Add';
        $data['var_meta_description'] = 'Team Add';
        $data['var_meta_keyword'] = 'Team Add';
        $data['js'] = array(
            'admin/team.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['js_plugin'] = array(
            'jquery.form.min.js',
        );
        if ($this->input->post() && $this->input->is_ajax_request()) {
            $this->json_response = $this->Team_model->addTeam($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }

        $data['init'] = array(
            'Team.add_team()',
        );

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function edit($id)
    {

        $data['teamData'] = $this->toval->idtorowarr('id', $id, 'team');

        if (empty($data['teamData'])) {
            $this->json_response['status'] = 'error';
            $this->json_response['message'] = 'Team Not Found';
            $this->session->set_flashdata($this->json_response);
            return redirect(admin_url('team'));
        }


        $data['page'] = "admin/team/edit";
        $data['breadcrumb'] = 'Team';
        $data['breadcrumb_sub'] = 'Team';
        $data['breadcrumb_list'] = array(
            array('Team', 'team'),
            array('Edit', ''),
        );
        $data['team'] = 'active open';
        $data['team'] = 'active';
        $data['var_meta_title'] = 'Team Edit';
        $data['var_meta_description'] = 'Team Edit';
        $data['var_meta_keyword'] = 'Team Edit';
        $data['js'] = array(
            'admin/team.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['js_plugin'] = array(
            'jquery.form.min.js',
        );
        $data['init'] = array(
            'Team.edit_team()',
        );

        $data['staffData'] = $this->db->get_where('account_manager', array('fk_team' => $id))->result_array();
        
        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function teamEdit()
    {
        if ($this->input->post() && $this->input->is_ajax_request()) {
            $this->json_response = $this->Team_model->addTeam($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }
    }

    function deleteTeam()
    {
        if ($this->input->post()) {
            $this->json_response = $this->Team_model->deleteTeam($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }
    }

}

?>