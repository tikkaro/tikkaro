<?php

class Experts extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('admin/Experts_model');
    }

    function index()
    {

        $data['page'] = "admin/experts/list";
        $data['breadcrumb'] = 'Experts';
        $data['breadcrumb_sub'] = 'Experts';
        $data['breadcrumb_list'] = array(
            array('Experts', ''),
            array('List', ''),
        );
        $data['experts'] = 'active open';
        $data['experts'] = 'active';
        $data['var_meta_title'] = 'Experts List';
        $data['var_meta_description'] = 'Experts List';
        $data['var_meta_keyword'] = 'Experts List';
        $data['js'] = array(
            'admin/experts.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['js_plugin'] = array();
        $data['init'] = array(
            'Experts.init()',
            'ComponentsDateTimePickers.init()',
        );

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function blockExperts()
    {
        if ($this->input->post() && $this->input->is_ajax_request()) {
            $this->json_response = $this->Experts_model->espertBlock($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }
    }

    function unblockExperts()
    {
        if ($this->input->post() && $this->input->is_ajax_request()) {
            $this->json_response = $this->Experts_model->expertUnblock($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }
    }

    function getExpertsList()
    {

        $this->load->library('Datatables');
        $res = $this->Experts_model->getExpertsList();
        echo json_encode($res);
        exit;
    }

    function add()
    {
        if ($this->input->get()) {
            $prospectId = $this->input->get('prospectId');
            $projectId = $this->input->get('projectId');
            if (!empty($prospectId) && $prospectId != '' && is_numeric($prospectId)) {

                $prospect = $this->toval->idtorowarr('id', $prospectId, 'project_has_prospect');

                if (empty($prospect)) {
                    $this->json_response['status'] = 'error';
                    $this->json_response['message'] = 'Prospect Not Found';
                    $this->session->set_flashdata($this->json_response);
                    return redirect(admin_url('project'));
                }
            }
            $data['prospectData'] = $prospect;
            $data['projectId'] = $projectId;
        }

        $data['page'] = "admin/experts/add";
        $data['breadcrumb'] = 'Experts';
        $data['breadcrumb_sub'] = 'Experts';
        $data['breadcrumb_list'] = array(
            array('Experts', 'experts'),
            array('Add', ''),
        );
        $data['experts'] = 'active open';
        $data['experts'] = 'active';
        $data['var_meta_title'] = 'Experts Add';
        $data['var_meta_description'] = 'Experts Add';
        $data['var_meta_keyword'] = 'Experts Add';
        $data['js'] = array(
            'admin/experts.js'
        );
        $data['css'] = array();
        $data['css_plugin'] = array(
            'bootstrap-tagsinput/bootstrap-tagsinput.css',
            'typeahead/typeahead.css',
        );
        $data['js_plugin'] = array(
            'bootstrap-tagsinput/bootstrap-tagsinput.min.js',
            'typeahead/handlebars.min.js',
            'typeahead/typeahead.bundle.min.js',
            'jquery.form.min.js',
        );

        $data['init'] = array(
            'Experts.expertsadd()',
            'ComponentsDateTimePickers.init()',
        );
        if ($this->input->post() && $this->input->is_ajax_request()) {
            $this->json_response = $this->Experts_model->addExpert($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }
        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function getArea()
    {
        $result = $this->db->get('master_area')->result_array();
        for ($i = 0; $i < count($result); $i++) {
            $res[] = $result[$i]['var_area_name'];
        }
        echo json_encode($res);
        exit;
    }

    function bio($id)
    {

        $expertData = $this->toval->idtorowarr('id', $id, 'experts');

        if (empty($expertData)) {
            $this->json_response['status'] = 'error';
            $this->json_response['message'] = 'Experts Not Found';
            $this->session->set_flashdata($this->json_response);
            return redirect(admin_url('experts'));
        }

        $data['page'] = "admin/experts/bio";
        $data['breadcrumb'] = 'Experts';
        $data['breadcrumb_sub'] = 'Experts';
        $data['breadcrumb_list'] = array(
            array('Experts', 'experts'),
            array('Profile', ''),
        );
        $data['experts'] = 'active open';
        $data['experts'] = 'active';
        $data['var_meta_title'] = 'Experts Profile';
        $data['var_meta_description'] = 'Experts Profile';
        $data['var_meta_keyword'] = 'Experts Profile';
        $data['js'] = array(
            'admin/experts.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array(
            'bootstrap-tagsinput/bootstrap-tagsinput.css',
            'typeahead/typeahead.css',
        );
        $data['js_plugin'] = array(
            'bootstrap-tagsinput/bootstrap-tagsinput.min.js',
            'typeahead/handlebars.min.js',
            'typeahead/typeahead.bundle.min.js',
            'jquery.form.min.js',

        );
        $data['init'] = array(
            'Experts.init()',
            'ComponentsDateTimePickers.init()',
        );

        if ($this->input->get()) {
            $data['getExperts'] = $this->input->get();
        }

        $data['expertsid'] = $id;
        $data['expertBio'] = $this->Experts_model->getExpertbio($id);
        $data['expertEducation'] = $this->Experts_model->getExpertEducation($id);
        $data['expertsEmployement'] = $this->Experts_model->getExpertEmployment($id);
        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function updateTags()
    {
        if ($this->input->post()) {
            $this->json_response = $this->Experts_model->updateTags($this->input->post());
            echo json_encode($this->json_response);
            exit;
        }
    }

    function addEmployHistory()
    {

        if ($this->input->is_ajax_request() && $this->input->post()) {
            $this->json_response = $this->Experts_model->addEmployHistory($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }
    }

    function getEducationHistoryById()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $data['edu_data'] = $this->db->get_where('experts_has_education', array('id' => $this->input->post('id')))->row_array();
            echo $this->load->view('admin/experts/edit_education_modal', $data, TRUE);
            exit;
        }
    }

    function addEducationHistory()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $this->json_response = $this->Experts_model->addEducationHistory($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }
    }

    function getEmployHistoryById()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $result = $this->db->get_where('experts_has_employment_history', array('id' => $this->input->post('histId')))->row_array();

            $result['dt_from_respective_date'] = (!empty($result['dt_from_respective_date'])) ? date('d/m/Y', strtotime($result['dt_from_respective_date'])) : '';
            $result['dt_to_respective_date'] = (!empty($result['dt_to_respective_date'])) ? date('d/m/Y', strtotime($result['dt_to_respective_date'])) : '';

            echo json_encode($result);
            exit;
        }
    }

    function notes($id)
    {

        $expertData = $this->toval->idtorowarr('id', $id, 'experts');

        if (empty($expertData)) {
            $this->json_response['status'] = 'error';
            $this->json_response['message'] = 'Experts Not Found';
            $this->session->set_flashdata($this->json_response);
            return redirect(admin_url('experts'));
        }


        $data['page'] = "admin/experts/notes";
        $data['breadcrumb'] = 'Experts';
        $data['breadcrumb_sub'] = 'Experts';
        $data['breadcrumb_list'] = array(
            array('Experts', 'experts'),
            array('notes', ''),
        );
        $data['experts'] = 'active open';
        $data['experts'] = 'active';
        $data['var_meta_title'] = 'Experts Notes';
        $data['var_meta_description'] = 'Experts Notes';
        $data['var_meta_keyword'] = 'Experts Notes';
        $data['js'] = array(
            'admin/experts.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['js_plugin'] = array();
        $data['init'] = array(
            'Experts.notes_init()',
        );

        if ($this->input->get()) {
            $data['getExperts'] = $this->input->get();
        }

        $data['expertsid'] = $id;
        $data['expertDataArray'] = $this->Experts_model->getExpertById($id);
        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function getNotesList($id)
    {
        $this->load->library('Datatables');
        $res = $this->Experts_model->getNotesDatatables($id);
        echo $res;
        exit;
    }

    function getExpertNoteById()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $result = $this->db->get_where('experts_has_notes', array(id => $this->input->post('noteId')))->row_array();
            echo json_encode($result);
            exit;
        }
    }

    function addExpertNote()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $this->json_response = $this->Experts_model->addExpertNotes($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }
    }

    function deleteExpertNote()
    {
        if ($this->input->is_ajax_request() && $this->input->post()) {
            $this->json_response = $this->Experts_model->deleteExpertNote($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }
    }

    function projects($id)
    {

        $expertData = $this->toval->idtorowarr('id', $id, 'experts');

        if (empty($expertData)) {
            $this->json_response['status'] = 'error';
            $this->json_response['message'] = 'Experts Not Found';
            $this->session->set_flashdata($this->json_response);
            return redirect(admin_url('experts'));
        }

        $data['page'] = "admin/experts/projects";
        $data['breadcrumb'] = 'Experts';
        $data['breadcrumb_sub'] = 'Experts';
        $data['breadcrumb_list'] = array(
            array('Experts', 'experts'),
            array('projects', ''),
        );
        $data['experts'] = 'active open';
        $data['experts'] = 'active';
        $data['var_meta_title'] = 'Experts Projects';
        $data['var_meta_description'] = 'Experts Projects';
        $data['var_meta_keyword'] = 'Experts Projects';
        $data['js'] = array(
            'admin/experts.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['js_plugin'] = array();
        $data['init'] = array(
            'Experts.projects_init()',
        );

        if ($this->input->get()) {
            $data['getExperts'] = $this->input->get();
        }

        $data['expertsid'] = $id;
        $data['expertDataArray'] = $this->Experts_model->getExpertById($id);

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function call_history($id)
    {
        $expertData = $this->toval->idtorowarr('id', $id, 'experts');
        if (empty($expertData)) {
            $this->json_response['status'] = 'error';
            $this->json_response['message'] = 'Experts Not Found';
            $this->session->set_flashdata($this->json_response);
            return redirect(admin_url('experts'));
        }


        $data['page'] = "admin/experts/call_history";
        $data['breadcrumb'] = 'Experts';
        $data['breadcrumb_sub'] = 'Experts';
        $data['breadcrumb_list'] = array(
            array('Experts', 'experts'),
            array('Call History', ''),
        );
        $data['experts'] = 'active open';
        $data['experts'] = 'active';
        $data['var_meta_title'] = 'Experts Call History';
        $data['var_meta_description'] = 'Experts Call History';
        $data['var_meta_keyword'] = 'Experts Call History';
        $data['js'] = array(
            'admin/experts.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['js_plugin'] = array();
        $data['init'] = array(
            'Experts.call_history()',
        );

        if ($this->input->get()) {
            $data['getExperts'] = $this->input->get();
        }

        $data['expertsid'] = $id;
        $data['expertDataArray'] = $this->Experts_model->getExpertById($id);

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function getCallHistoryList()
    {
        $this->load->library('Datatables');
        $res = $this->Experts_model->getCallHistoryList();
        echo json_encode($res);
        exit;
    }

    function getProjectList($expertsId)
    {
        $this->load->library('Datatables');
        $res = $this->Experts_model->getProjectList($expertsId);
        echo json_encode($res);
        exit;
    }

    function updateComplianceDate(){
        if ($this->input->post()) {
            $this->json_response = $this->Experts_model->updateDates($this->input->post());
            echo json_encode($this->json_response);
            exit;
        }
    }

    function resetPassword(){
        if ($this->input->post()) {
            $this->json_response = $this->Experts_model->resetPassword($this->input->post());
            echo json_encode($this->json_response);
            exit;
        }
    }
}
