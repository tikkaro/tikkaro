<?php

class Staff extends Admin_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('admin/Staff_model');
    }

    function index()
    {
        $data['page'] = "admin/staff/list";
        $data['breadcrumb'] = 'Staff';
        $data['breadcrumb_sub'] = 'Staff';
        $data['breadcrumb_list'] = array(
            array('Staff', ''),
            array('List', ''),
        );
        $data['accountmanager'] = 'active open';
        $data['accountmanager'] = 'active';
        $data['var_meta_title'] = 'Staff List';
        $data['var_meta_description'] = 'Staff List';
        $data['var_meta_keyword'] = 'Staff List';
        $data['js'] = array(
            'admin/staff.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['js_plugin'] = array();
        $data['init'] = array(
            'Staff.init()',
        );

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function getAccountManagerList()
    {
        $this->load->library('Datatables');
        echo $this->Staff_model->getAccountManagerList();
        exit;
    }

    function add()
    {
        $data['page'] = "admin/staff/add";
        $data['breadcrumb'] = 'Staff';
        $data['breadcrumb_sub'] = 'Staff';
        $data['breadcrumb_list'] = array(
            array('Staff', 'staff'),
            array('List', ''),
        );
        $data['accountmanager'] = 'active open';
        $data['accountmanager'] = 'active';
        $data['var_meta_title'] = 'Staff Add';
        $data['var_meta_description'] = 'Staff Add';
        $data['var_meta_keyword'] = 'Staff Add';

        $data['js'] = array(
            'admin/staff.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array(
            'bootstrap-fileinput/bootstrap-fileinput.css'
        );
        $data['js_plugin'] = array(
            'jquery.form.min.js',
            'ajaxfileupload.js',
            'bootstrap-fileinput/bootstrap-fileinput.js'
        );
        $data['init'] = array(
            'Staff.Add_init()',
            'ComponentsDateTimePickers.init()',
        );

        $data['teamData'] = $this->db->get('team')->result_array();
        if ($this->input->post() && $this->input->is_ajax_request()) {
            $this->json_response = $this->Staff_model->add($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }


        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function edit($id)
    {

        $data['accountData'] = $this->toval->idtorowarr('id', $id, 'account_manager');

        if (empty($data['accountData'])) {
            $this->json_response['status'] = 'error';
            $this->json_response['message'] = 'Staff Not Found';
            $this->session->set_flashdata($this->json_response);
            return redirect(admin_url('staff'));
        }


        $data['page'] = "admin/staff/edit";
        $data['breadcrumb'] = 'Staff';
        $data['breadcrumb_sub'] = 'Staff';
        $data['breadcrumb_list'] = array(
            array('Staff', 'staff'),
            array('Edit', ''),
        );
        $data['accountmanager'] = 'active open';
        $data['accountmanager'] = 'active';
        $data['var_meta_title'] = 'Staff Edit';
        $data['var_meta_description'] = 'Staff Edit';
        $data['var_meta_keyword'] = 'Staff Edit';
        $data['js'] = array(
            'admin/staff.js',
        );
        $data['css'] = array();
        $data['css_plugin'] = array(
            'bootstrap-fileinput/bootstrap-fileinput.css'
        );
        $data['js_plugin'] = array(
            'jquery.form.min.js',
            'ajaxfileupload.js',
            'bootstrap-fileinput/bootstrap-fileinput.js'
        );
        $data['init'] = array(
            'Staff.Edit_init()',
            'ComponentsDateTimePickers.init()',
        );
        $data['teamData'] = $this->db->get('team')->result_array();
        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function editKeyaccountmanager()
    {
        if ($this->input->post() && $this->input->is_ajax_request()) {
            $this->json_response = $this->Staff_model->add($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }
    }

    function delete()
    {
        if ($this->input->post()) {
            $this->json_response = $this->Staff_model->delete($this->input->post(), $this->json_response);
            echo json_encode($this->json_response);
            exit;
        }
    }

}
