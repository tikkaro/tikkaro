<?php

class Cron extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('admin/Cron_model');
    }

    function expertsUnblock() {
        $expertsUnblock = $this->Cron_model->manageExpertsUnblock();
    }

    function manageSubscription() {
        $manageSubscription = $this->Cron_model->manageSubscription();
    }

    function doCompliance() {
        $manageSubscription = $this->Cron_model->managedoCompliance();
    }

}
