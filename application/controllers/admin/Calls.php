<?php

class Calls extends Admin_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('admin/Call_model');
    }

    function index() {
        $data['page'] = "admin/call/list";
        $data['breadcrumb'] = 'Experts';
        $data['breadcrumb_sub'] = 'Experts';
        $data['breadcrumb_list'] = array(
            array('Call', ''),
            array('List', ''),
        );
        $data['calls'] = 'active open';
        $data['calls'] = 'active';
        $data['var_meta_title'] = 'Call List';
        $data['var_meta_description'] = 'Call List';
        $data['var_meta_keyword'] = 'Call List';
        $data['js'] = array(
            'admin/call.js',
        );
        $data['css'] = array(
        );
        $data['css_plugin'] = array(
        );
        $data['js_plugin'] = array(
        );
        $data['init'] = array(
            'Call.init()',
        );

        $this->load->view(ADMIN_LAYOUT, $data);
    }

    function getCallList() {

        $this->load->library('Datatables');
        $res = $this->Call_model->getCallList();
        echo json_encode($res);
        exit;
    }

}
