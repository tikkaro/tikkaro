<?php

class Scheduling extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('ExpertAuthLibrary');
        $this->load->library('CustomerAuthLibrary');
        $this->load->helper('expert_auth_helper');
        $this->load->helper('customer_auth_helper');
        $this->load->model('expert/Call_scheduling_model');
    }

    function schedule()
    {
        $UrlString = json_decode(base64_decode($this->input->get('data')));

        $expertsHasProjectId = $UrlString->expertsHasProjectId;
        $expertsId = $UrlString->expertsId;
        $callScheduling = $this->Call_scheduling_model->callScheduling($expertsHasProjectId,$expertsId);
    }

    function clientSelectAvailability() {
        $data['page'] = "front/customer/selectAvailability/view";
        $data['pageFlag'] = "SelectAvailability";
        $data['breadcrumb'] = 'Select Availability';
        $data['breadcrumb_sub'] = 'Select Availability';
        $data['breadcrumb_list'] = array(
            array('Select Availability', ''),
        );
        $data['var_meta_title'] = 'Select Availability';
        $data['var_meta_description'] = 'Select Availability';
        $data['var_meta_keyword'] = 'Select Availability';
        $data['js'] = array(
            'call_schedule.js'
        );

        $data['css'] = array();

        $data['css_plugin'] = array();

        $data['js_plugin'] = array(

        );

        $data['init'] = array(
            'Call_schedule.init()',
        );

        $UrlString = json_decode(base64_decode($this->input->get('data')));
        $expertsAvailabilityId = $UrlString->expertsHasTimeAvailabilityId;

        $data['expertsAvailability'] = $this->Call_scheduling_model->getExpertsTimeSelection($expertsAvailabilityId);
        $data['checkClientAvailability'] = $this->db->get_where('client_has_time_availability',array('fk_project_has_experts_has_availability' => $expertsAvailabilityId))->result_array();
        $this->load->view(CUSTOMER_LAYOUT, $data);
    }

    function clientSelectTimeAvailability(){
        $response = $this->Call_scheduling_model->clientTimeSelection($this->input->post('bookId'));
        echo json_encode($response);
        exit;
    }
}

?>