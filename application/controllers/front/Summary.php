<?php

class Summary extends Front_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $data['pageflag'] = 'Summary';
        $data['page'] = "front/cmspages/summary";
        $data['var_meta_title'] = 'Booking Summary';
        $data['var_meta_description'] = 'Booking Summary';
        $data['var_meta_keyword'] = 'Booking Summary';
        $data['js'] = array(
            'summary.js'
        );
        $data['js_plugin'] = array();
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['init'] = array(
            'Summary.init()'
        );
        $this->load->view(FRONT_LAYOUT, $data);
    }

}

?>