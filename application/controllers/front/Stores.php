<?php

class Stores extends Front_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $data['pageflag'] = 'Stores';
        $data['page'] = "front/cmspages/stores";
        $data['var_meta_title'] = 'Stores';
        $data['var_meta_description'] = 'Stores';
        $data['var_meta_keyword'] = 'Stores';
        $data['js'] = array(
            'stores.js'
        );
        $data['js_plugin'] = array();
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['init'] = array(
            'Stores.init()'
        );
        $this->load->view(FRONT_LAYOUT, $data);
    }

}

?>