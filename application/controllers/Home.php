<?php

class Home extends Front_Controller {

    function __construct() {
        parent::__construct();
    }

    function index() {
        $data['pageflag'] = 'Home';
        $data['page'] = "front/cmspages/home";
        $data['var_meta_title'] = 'Home';
        $data['var_meta_description'] = 'Home';
        $data['var_meta_keyword'] = 'Home';
        $data['js'] = array(
            'home.js'
        );
        $data['js_plugin'] = array();
        $data['css'] = array();
        $data['css_plugin'] = array();
        $data['init'] = array(
            'Home.init()'
        );
        $this->load->view(FRONT_LAYOUT, $data);
    }

}

?>