<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_client_has_subscription_table extends CI_Migration
{

    public $table = 'client_has_subscription';

    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'auto_increment' => TRUE
            ),
            'fk_client' => array(
                'type' => 'INT',
                'constraint' => '11',
            ),
            'fk_admin' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => TRUE
            ),
            'fk_account_manager' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => TRUE
            ),
            'dt_start_date' => array(
                'type' => 'datetime',
            ),
            'dt_end_date' => array(
                'type' => 'datetime',
            ),
            'int_subscription_time' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => false
            ),
            'int_no_of_unit' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => true
            ),
            'int_price_per_unit' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => false
            ),
            'int_total_price' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => false
            ),
            'int_calls_completed' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => true
            ),
            'int_calls_remains' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => true
            ),
            'int_avg_subscription_per_month' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => true
            ),
            'int_avg_subscription_per_week' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => true
            ),
            'var_subscription_type' => array(
                'type' => 'VARCHAR',
                'constraint' => '100'
            ),
            'enum_pace_status' => array(
                'type' => 'enum("AHEAD", "BEHIND","NO-PACE")',
                'default' => 'NO-PACE',
                'null' => FALSE
            ),
            'enum_enable' => array(
                'type' => 'enum("YES", "NO")',
                'default' => 'YES',
                'null' => FALSE
            ),
            'created_at' => array(
                'type' => 'datetime',
            ),
            'updated_at' => array(
                'type' => 'timestamp'
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB');
        $this->dbforge->create_table($this->table, TRUE, $attributes);

        dropForeignKey($this->table, 'fk_admin', 'admin', 'id');
        dropForeignKey($this->table, 'fk_account_manager', 'account_manager', 'id');
        dropForeignKey($this->table, 'fk_client', 'client', 'id');

        addForeignKey($this->table, 'fk_admin', 'admin', 'id', 'SET NULL', 'SET NULL');
        addForeignKey($this->table, 'fk_account_manager', 'account_manager', 'id', 'SET NULL', 'SET NULL');
        addForeignKey($this->table, 'fk_client', 'client', 'id', 'CASCADE', 'CASCADE');

    }

    public function down()
    {
        dropForeignKey($this->table, 'fk_admin', 'admin', 'id');
        dropForeignKey($this->table, 'fk_account_manager', 'account_manager', 'id');
        dropForeignKey($this->table, 'fk_client', 'client', 'id');
        $this->dbforge->drop_table($this->table, TRUE);
    }

}
