<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_angle_field_in_project_has_experts_table extends CI_Migration {

    public $table = 'project_has_experts';

    public function up() {

        $fields = array(
            'fk_angle' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => TRUE
            ),
        );
        $this->dbforge->add_column($this->table, $fields, 'fk_experts');

        dropForeignKey($this->table, 'fk_angle', 'master_angle', 'id');
        addForeignKey($this->table, 'fk_angle', 'master_angle', 'id', 'CASCADE', 'CASCADE');

    }

    public function down() {
        dropForeignKey($this->table, 'fk_angle', 'master_angle', 'id');
        $this->dbforge->drop_table($this->table, TRUE);
    }

}
