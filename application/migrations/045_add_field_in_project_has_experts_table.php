<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_field_in_project_has_experts_table extends CI_Migration {

    public $table = 'project_has_experts';

    public function up() {


        $fields = array(
            'enum_scheduling_status' => array(
                'type' => 'enum("CLIENT_REQUEST","TIME_GIVEN")',
                'null' => TRUE
            ),
        );

        $this->dbforge->add_column($this->table, $fields, 'enum_status');
    }

    public function down() {

        $this->dbforge->drop_column($this->table,'enum_scheduling_status');

    }

}
