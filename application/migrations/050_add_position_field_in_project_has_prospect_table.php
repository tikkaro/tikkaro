<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_position_field_in_project_has_prospect_table extends CI_Migration {

    public $table = 'project_has_prospect';

    public function up() {
        $fields = array(
            'enum_enable' => array(
                'type' => 'enum("YES", "NO")',
                'default' => 'YES',
                'null' => FALSE
            ),
            'var_position' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => TRUE
            ),
        );
        $this->dbforge->add_column($this->table, $fields, 'var_linkedin_page');
    }

    public function down() {

        $this->dbforge->drop_column($this->table,'enum_enable');
        $this->dbforge->drop_column($this->table,'var_position');

    }
}
