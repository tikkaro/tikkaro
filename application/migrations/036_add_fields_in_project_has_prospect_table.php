<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_fields_in_project_has_prospect_table extends CI_Migration
{

    public $table = 'project_has_prospect';

    public function up()
    {
        $fields = array(
            'var_linkedin_page' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => TRUE
            ),
            'var_compnay_website' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => TRUE
            ),
        );

        $this->dbforge->add_column($this->table, $fields, 'bint_phone');
    }

    public function down()
    {
        $this->dbforge->drop_column($this->table, 'var_linkedin_page');
        $this->dbforge->drop_column($this->table, 'var_compnay_website');
    }

}
