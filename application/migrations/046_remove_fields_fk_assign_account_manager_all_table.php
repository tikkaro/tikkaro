<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Remove_fields_fk_assign_account_manager_all_table extends CI_Migration
{

    public function up()
    {
        $fields = array(
            'project_has_experts' => 'fk_assign_account_manager',
            'client_has_project' => 'fk_assign_account_manager',
            'project_has_experts_notes' => 'fk_assign_account_manager',
            'project_has_prospect' => 'fk_assign_account_manager',
            'user_has_notes' => 'fk_assign_account_manager',
            'user' => 'fk_assign_account_manager',
            'client_has_notes' => 'fk_assign_account_manager',
            'client_has_compliance_officer' => 'fk_assign_account_manager',
            'client_has_rules' => 'fk_assign_account_manager',
        );

        foreach ($fields as $key => $value){
            $this->dbforge->drop_column($key,$value);
        }
    }


}
