<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Change_field_phone_in_client_has_compliance_officer_table extends CI_Migration
{

    public $table = 'client_has_compliance_officer';

    public function up()
    {
        $fields = array(
            'bint_phone' => array(
                'name' => 'bint_phone',
                'type' => 'VARCHAR',
                'constraint' => '20',
                'null' => TRUE
            ),
        );
        $this->dbforge->modify_column($this->table, $fields);

    }

    public function down()
    {

        $fields = array(
            'bint_phone' => array(
                'name' => 'bint_phone',
                'type' => 'bigint',
                'constraint' => '20',
                'null' => TRUE
            ),
        );
        $this->dbforge->modify_column($this->table, $fields);

    }

}
