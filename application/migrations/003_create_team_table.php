<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_team_table extends CI_Migration
{

    public $table = 'team';

    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'auto_increment' => TRUE
            ),
            'fk_admin' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => TRUE
            ),
            'var_team_name' => array(
                'type' => 'VARCHAR',
                'constraint' => '100'
            ),
            'txt_description' => array(
                'type' => 'TEXT',
                'null' => TRUE
            ),
            'enum_enable' => array(
                'type' => 'enum("YES", "NO")',
                'default' => 'YES',
                'null' => FALSE
            ),
            'created_at' => array(
                'type' => 'datetime',
            ),
            'updated_at' => array(
                'type' => 'timestamp'
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB');
        $this->dbforge->create_table($this->table, TRUE, $attributes);

        dropForeignKey($this->table, 'fk_admin', 'admin', 'id');

        addForeignKey($this->table, 'fk_admin', 'admin', 'id', 'SET NULL', 'SET NULL');
    }

    public function down()
    {
        dropForeignKey($this->table, 'fk_admin', 'admin', 'id');
        $this->dbforge->drop_table($this->table, TRUE);
    }

}
