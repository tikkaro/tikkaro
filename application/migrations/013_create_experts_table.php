<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Create_experts_table extends CI_Migration
{

    public $table = 'experts';

    public function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => '11',
                'auto_increment' => TRUE
            ),
            'fk_admin' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => TRUE,
            ),
            'fk_account_manager' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => TRUE,
            ),
            'fk_prospect' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => TRUE,
            ),
            'var_fname' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => TRUE
            ),
            'var_lname' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => TRUE
            ),
            'var_email' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => TRUE
            ),
            'var_company' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => TRUE
            ),
            'var_rate_per_hour' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => FALSE
            ),
            'var_rate_per_survey' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => TRUE
            ),
            'var_country' => array(
                'type' => 'VARCHAR',
                'constraint' => '255',
                'null' => TRUE
            ),
            'dec_review_score' => array(
                'type' => 'DECIMAL',
                'constraint' => '10,2',
                'null' => TRUE
            ),
            'txt_bio' => array(
                'type' => 'TEXT',
                'null' => TRUE
            ),
            'enum_block' => array(
                'type' => 'enum("YES", "NO")',
                'default' => 'NO',
                'null' => FALSE
            ),
            'enum_enable' => array(
                'type' => 'enum("YES", "NO")',
                'default' => 'YES',
                'null' => FALSE
            ),
            'created_at' => array(
                'type' => 'datetime',
            ),
            'updated_at' => array(
                'type' => 'timestamp'
            ),
        ));
        $this->dbforge->add_key('id', TRUE);
        $attributes = array('ENGINE' => 'InnoDB');
        $this->dbforge->create_table($this->table, TRUE, $attributes);

        dropForeignKey($this->table, 'fk_admin', 'admin', 'id');
        dropForeignKey($this->table, 'fk_account_manager', 'account_manager', 'id');

        addForeignKey($this->table, 'fk_admin', 'admin', 'id', 'SET NULL', 'SET NULL');
        addForeignKey($this->table, 'fk_account_manager', 'account_manager', 'id', 'SET NULL', 'SET NULL');

    }

    public function down()
    {
        dropForeignKey($this->table, 'fk_admin', 'admin', 'id');
        dropForeignKey($this->table, 'fk_account_manager', 'account_manager', 'id');

        $this->dbforge->drop_table($this->table, TRUE);
    }

}
