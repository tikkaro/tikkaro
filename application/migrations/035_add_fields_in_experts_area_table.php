<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_fields_in_experts_area_table extends CI_Migration
{

    public $table = 'experts_area';

    public function up()
    {
        $fields = array(
            'fk_area' => array(
                'type' => 'INT',
                'constraint' => '11',
                'null' => FALSE
            ),
        );
        $this->dbforge->add_column($this->table, $fields, 'fk_experts');

        $this->dbforge->drop_column($this->table, 'var_name');
        $this->dbforge->drop_column($this->table, 'txt_description');

        dropForeignKey($this->table, 'fk_area', 'master_area', 'id');

        addForeignKey($this->table, 'fk_area', 'master_area', 'id', 'CASCADE', 'CASCADE');

    }

    public function down()
    {
        dropForeignKey($this->table, 'fk_area', 'master_area', 'id');
        $this->dbforge->drop_column($this->table, 'fk_area');

        $fields = array(
            'var_name' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
                'null' => FALSE
            ),
            'txt_description' => array(
                'type' => 'TEXT',
                'null' => TRUE
            ),
        );
        $this->dbforge->add_column($this->table, $fields, 'var_area_name');
    }

}
