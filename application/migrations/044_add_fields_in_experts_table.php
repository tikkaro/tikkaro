<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_fields_in_experts_table extends CI_Migration
{

    public $table = 'experts';

    public function up()
    {
        $fields = array(
            'dt_compliance_date' => array(
                'type' => 'DATE',
                'null' => TRUE
            ),
        );

        $this->dbforge->add_column($this->table, $fields, 'txt_bio');
    }

    public function down()
    {
        $this->dbforge->drop_column($this->table, 'dt_compliance_date');
    }

}
