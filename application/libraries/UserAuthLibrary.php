<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class UserAuthLibrary {

    var $ci;
    var $userData = null;
    var $loginUserId = null;

    public function __construct() {

        $this->ci = &get_instance();
        $this->userData = $this->ci->session->userdata;
        $this->loginUserId = ($this->loginUserId == null) ? $this->getLoginUserId() : $this->loginUserId;
        $this->currentIp = $this->ci->input->ip_address();
    }

    public function refreshData() {
        $this->userData = $this->ci->session->userdata;
    }

    public function isLogin() {
        if (!empty($this->userData['valid_user']['id'])) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    public function setLoginUserId($id) {
        $this->loginUserId = $id;
    }

    public function resetLoginUserId() {
        $this->loginUserId = $this->getLoginUserId();
    }

    public function getLoginUserData() {
        return $this->userData['valid_user'];
    }

    public function getLoginUserId() {
        return $this->getLoginUserData()['id'];
    }

    public function loginAttempt($email, $password, $response) {
        $this->ci->db->select('user.*');
        $this->ci->db->from('user');
        $this->ci->db->group_start();
        $this->ci->db->where("user.bint_phone", $email);
        $this->ci->db->or_where("user.var_email", $email);
        $this->ci->db->group_end();
        $userData = $this->ci->db->get()->row_array();

        if ((!empty($userData) && $userData['var_password'] == md5($password))) {
            return $this->doLogin($userData);
        } else {
            $response['status'] = 'error';
            $response['message'] = 'Email and password does not match';
        }
        return $response;
    }

    function doLogin($userData) {

        if ($userData['enum_enable'] == 'YES') {

            $this->ci->session->set_userdata(['valid_user' => $userData]);
            $this->refreshData();
            $response['status'] = 'success';
            $response['message'] = 'Well Done Login Successfully Done..';
            $response['reload'] = '1';
        } else {
            //User has been baned
            $response['status'] = 'warning';
            $response['message'] = 'You Has been Blocked, Contact system Administrator!!';
        }

        return $response;
    }

    function checkLoggedinUser() {
        if ($this->isLogin()) {
            $userData = $this->ci->db->where('id', $this->getLoginUserId())->get('user')->row();
            if ($userData->enum_block == 'YES' || $userData->enum_enable == 'NO' || empty($userData)) {
                $json_response['status'] = 'error';
                $json_response['message'] = 'You Has been Blocked, Contact system Administrator!!';
                $this->ci->session->sess_destroy();
                $this->ci->session->set_flashdata($json_response);
                redirect(customer_url() . 'login');
            }
        }
    }

}

/* End of file AuthLibrary.php */