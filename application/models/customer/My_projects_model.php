<?php

class My_projects_model extends CI_Model
{

    public function getCustomerProjectsById($id)
    {

        $id = (empty($id) && !isset($id)) ? getLoginCustomerUserId() : $id;
        if ($id) {

            $this->db->where('fk_user', $id);
            $projects = $this->db->order_by('id','DESC')->get('client_has_project')->result_array();

            foreach ($projects as $key => $project) {

                $this->db->select('project_has_experts.*,
                                   experts.id as ExpertsId,
                                   CONCAT(' . TABLE_PREFIX . 'experts.var_fname," ",' . TABLE_PREFIX . 'experts.var_lname) as ExpertName,
                                   experts_has_employment_history.var_company as CompanyName,
                                   experts_has_employment_history.var_position as Position,
                                   experts_has_employment_history.var_geography as Geography
                                   ');
                $this->db->from('project_has_experts');
                $this->db->join('experts', 'experts.id = project_has_experts.fk_experts', 'left');
                $this->db->join('experts_has_employment_history', 'experts_has_employment_history.fk_experts = experts.id AND experts_has_employment_history.enum_present = "YES" ', 'left');
                $this->db->where('project_has_experts.fk_project', $project['id']);
                $this->db->where('project_has_experts.enum_status != ', 'EXPERTS_ATTACHED');
                $shortListedExperts = $this->db->get()->result_array();
                $projects[$key]['shortListedExperts'] = $shortListedExperts;
            }
        }
        return $projects;
    }

    public function editProjectDetails($data, $json_response)
    {
        if ($data['editField'] == 'ProjectDesc') {
            $projectDetails = array(
                'txt_description' => $data['value'],
            );
            if ($data['id']) {
                $this->db->where('id', $data['id']);
                $this->db->update("client_has_project", $projectDetails);
                $json_response['status'] = 'success';
                $json_response['message'] = 'Project Details Updated!..';
            } else {
                $json_response['status'] = 'error';
                $json_response['message'] = 'Something will be wrong';
            }
        }
        return $json_response;
    }
}
