<?php

class My_projects_model extends CI_Model
{

    public function getExpertOfferedProjects($id)
    {
        $id = (empty($id) && !isset($id)) ? getLoginExpertUserId() : $id;
        if ($id) {

            $this->db->select('project_has_experts.*,
                               client.id as clientId,
                               client.var_company_name as clientName,
                               client_has_project.id as clientHasProjectId,
                               client_has_project.var_project_name as projectName,
                               client_has_project.txt_description as projectDescription,
                               ');
            $this->db->from('project_has_experts');
            $this->db->join('client', 'client.id = project_has_experts.fk_client');
            $this->db->join('client_has_project', 'client_has_project.fk_client = client.id');
            $this->db->where('project_has_experts.fk_experts', $id);
            $this->db->where('project_has_experts.enum_status', 'CALLS_IN_SCHEDULING');
            $this->db->group_by('project_has_experts.id');
            $data = $this->db->get()->result_array();
        }
        return $data;
    }

    public function getExpertUpcomingProjects($id)
    {
        $id = (empty($id) && !isset($id)) ? getLoginExpertUserId() : $id;
        if ($id) {

            $this->db->select('project_has_experts.*,
                               client.id as clientId,
                               client.var_company_name as clientName,
                               client_has_project.id as clientHasProjectId,
                               client_has_project.var_project_name as projectName,
                               client_has_project.txt_description as projectDescription,
                               ');
            $this->db->from('project_has_experts');
            $this->db->join('client', 'client.id = project_has_experts.fk_client');
            $this->db->join('client_has_project', 'client_has_project.fk_client = client.id');
            $this->db->where('project_has_experts.fk_experts', $id);
            $this->db->where('project_has_experts.enum_status', 'CALLS_SCHEDULED');
            $this->db->group_by('project_has_experts.id');
            $data = $this->db->get()->result_array();
        }
        return $data;
    }

    public function addRejectedProjectNotes($data, $json_response)
    {
        $config = array(
            array('field' => 'client-id', 'label' => 'Client Id', 'rules' => 'required'),
            array('field' => 'user-id', 'label' => 'User Id', 'rules' => 'required'),
            array('field' => 'project-id', 'label' => 'Project Id', 'rules' => 'required'),
            array('field' => 'expert-id', 'label' => 'Expert Id', 'rules' => 'required'),
            array('field' => 'project-has-expert-id', 'label' => 'Project Expert Id', 'rules' => 'required'),
            array('field' => 'txt_notes', 'label' => 'Project Notes', 'rules' => 'required')
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() !== FALSE) {
            // Expert Notes
            $expertNotes = array(
                'fk_experts' => $data['expert-id'],
                'txt_note' => $data['txt_notes'],
                'created_at' => date('Y-m-d H:i:s'),
            );
            $this->db->insert('experts_has_notes', $expertNotes);
            $expertNoteLastId = $this->db->insert_id();
            // Expert Notes
            // Project has experts Notes
            $projectHasExpertNotes = array(
                'fk_client' => $data['client-id'],
                'fk_user' => $data['user-id'],
                'fk_project' => $data['project-id'],
                'fk_experts' => $data['expert-id'],
                'fk_project_has_experts' => $data['project-has-expert-id'],
                'txt_note' => $data['txt_notes'],
                'created_at' => date('Y-m-d H:i:s'),
            );
            $this->db->insert('project_has_experts_notes', $projectHasExpertNotes);
            $projectHasExpertNotesLastId = $this->db->insert_id();
            // Project has experts Notes

            if ($expertNoteLastId > 0 && $projectHasExpertNotesLastId > 0) {
                $this->db->where('id', $data['project-has-expert-id']);
                $this->db->update('project_has_experts', array('enum_status' => 'CALLS_CANCELLED'));
                if ($this->db->affected_rows()) {
                    $json_response['message'] = 'Notes added';
                    $json_response['status'] = 'success';
                    $json_response['jscode'] = 'location.reload(); $("#reject-project").modal("hide")';
                }else{
                    $json_response['message'] = 'Something will be wrong';
                    $json_response['status'] = 'error';
                }
            }

        }else{

            $json_response['message'] = validation_errors();
            $json_response['status'] = 'warning';
        }
        return $json_response;
    }
}
