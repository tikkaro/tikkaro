<?php

class Call_scheduling_model extends CI_Model {

    function callScheduling($expertsHasProjectId,$expertsId){

        $checkProjectHasExperts = $this->db->get_where('project_has_experts', array('id' => $expertsHasProjectId, 'fk_experts' => $expertsId))->result_array();
        $checkExperts = $this->db->get_where('experts', array('id' => $expertsId))->result_array();

        if (!empty($checkExperts)) {
            if (!empty($checkProjectHasExperts)) {

                if ($checkProjectHasExperts[0]['enum_status'] == 'BIOS_SENT') {
                    $scheduling_status = array(
                        'enum_status' => 'CALLS_IN_SCHEDULING',
                        'enum_scheduling_status' => 'CLIENT_REQUEST',
                    );
                    $this->db->where('id', $expertsHasProjectId);
                    $this->db->update('project_has_experts', $scheduling_status);

                    $this->db->select('project_has_experts.id,project_has_experts.fk_experts,client.var_company_name,client_has_project.var_project_name,client_has_project.var_project_name,txt_description,
                           experts.var_fname as expertsfname,experts.var_lname as expertslname,experts.var_email as expertsemail,
                           account_manager.var_name as stafname,account_manager.var_email as staffemail,account_manager.bint_phone staffphone,
                           ');
                    $this->db->join('experts', 'experts.id = project_has_experts.fk_experts');
                    $this->db->join('client', 'client.id = project_has_experts.fk_client');
                    $this->db->join('client_has_project', 'client_has_project.id = project_has_experts.fk_project');
                    $this->db->join('account_manager', 'account_manager.id = client.fk_assign_account_manager', 'left');
                    $this->db->where('project_has_experts.id', $expertsHasProjectId);
                    $data['expertsData'] = $this->db->get('project_has_experts')->result_array();
                    $mail_templet = $this->load->view('email-templates/expert/time-request', $data, TRUE);

                    $this->load->library('Mylibrary');
                    $configs['to'] = $data['expertsData'][0]['expertsemail'];
                    $configs['cc'] = $data['expertsData'][0]['staffemail'];
                    $configs['subject'] = 'Time Request';
                    $configs['mail_body'] = $mail_templet;
                    $sendMail = $this->mylibrary->sendMail($configs);

                    if ($sendMail) {
                        echo "Mail Send Successfully";
                    }

                } else {
                    echo "Already Call Scheduling for this Project";
                }
            } else {
                echo "Sorry Experts not found for this project";
            }
        } else {
            echo "Sorry Experts not found in system";
        }
    }

    function getExpertsTimeSelection($expertsAvailabilityId){
        $this->db->select('experts_has_time_availability.*,experts.var_fname,experts.var_lname');
        $this->db->join('experts','experts.id = experts_has_time_availability.fk_expert');
        $this->db->where('experts_has_time_availability.id',$expertsAvailabilityId);
        $expertsAvailability = $this->db->get('experts_has_time_availability')->result_array();
        return $expertsAvailability;
    }

    function clientTimeSelection($timeAvaibilityId,$json_response){
        $checkTimeSelection = $this->db->get_where('client_has_time_availability',array('fk_project_has_experts_has_availability' => $timeAvaibilityId))->result_array();

        if(empty($checkTimeSelection)){
            $this->db->select('experts_has_time_availability.*,user.var_fname as userFname,user.var_lname as userLname,user.var_email as userEmail,
                           experts.var_fname as expetsFname,experts.var_lname as expetsLname,experts.var_email as expetsEmail, 
                           client.var_company_name,account_manager.var_name as stafName,account_manager.var_email as staffEmail,account_manager.bint_phone staffPhone');
            $this->db->join('client','client.id = experts_has_time_availability.fk_client');
            $this->db->join('account_manager', 'account_manager.id = client.fk_assign_account_manager', 'left');
            $this->db->join('user','user.id = experts_has_time_availability.fk_user');
            $this->db->join('experts','experts.id = experts_has_time_availability.fk_expert');
            $this->db->where('experts_has_time_availability.id',$timeAvaibilityId);
            $getExpertsAvailability = $this->db->get('experts_has_time_availability')->result_array();


            if(!empty($getExpertsAvailability)){
                $client_time = array(
                    'fk_client' => $getExpertsAvailability[0]['fk_client'],
                    'fk_user' => $getExpertsAvailability[0]['fk_user'],
                    'fk_project' => $getExpertsAvailability[0]['fk_project'],
                    'fk_expert' => $getExpertsAvailability[0]['fk_expert'],
                    'fk_project_has_experts' => $getExpertsAvailability[0]['fk_project_has_experts'],
                    'fk_project_has_experts_has_availability' => $getExpertsAvailability[0]['id'],
                    'dt_date_availability' => date('Y-m-d',strtotime($getExpertsAvailability[0]['dt_date_availability'])),
                    'dt_time_availability' => $getExpertsAvailability[0]['dt_time_availability'],
                    'created_at' => date('Y-m-d H:i:s'),
                );

                $this->db->insert('client_has_time_availability',$client_time);

                $projectHasStatus = array(
                    'enum_status' => 'CALLS_SCHEDULED',
                    'enum_scheduling_status' => 'TIME_GIVEN',
                );

                $this->db->where('id',$getExpertsAvailability[0]['fk_project_has_experts']);
                $this->db->update('project_has_experts',$projectHasStatus);

                $data['timeGiven'] = $getExpertsAvailability;
                $mail_templet =  $this->load->view('email-templates/expert/expert-scheduled-time', $data, TRUE);

                $this->load->library('Mylibrary');
                $configs['to'] = $getExpertsAvailability[0]['expetsEmail'];
                $configs['cc'] = $getExpertsAvailability[0]['staffEmail'].','.$getExpertsAvailability[0]['userEmail'];
                $configs['subject'] = 'Confirmation Time Availability';
                $configs['mail_body'] = $mail_templet;
                $sendMail = $this->mylibrary->sendMail($configs);
                if($sendMail){
                    $json_response['status'] = 'success';
                    $json_response['message'] = 'Mail Send Successfully!..';
                }else{
                    $json_response['status'] = 'error';
                    $json_response['message'] = 'Something will be wrong!..';
                }

            }else{
                $json_response['status'] = 'warning';
                $json_response['message'] = 'experts time availability not found in system!..';
            }
        }else{
            $json_response['status'] = 'warning';
            $json_response['message'] = 'You have already select time!..';
        }

        return $json_response;
    }
}
