<?php

class Expert_time_availability_model extends CI_Model
{

    function addExpertsTimeAvailability($data)
    {

        /* Check Availability */

        $checkProjectHasExperts = $this->db->get_where('project_has_experts', array('id' => $data['projectHasExpertId'], 'enum_scheduling_status' => 'CLIENT_REQUEST'))->result_array();
        $checkAllReadyTimeGiven = $this->db->get_where('experts_has_time_availability', array('fk_project_has_experts' => $data['projectHasExpertId']))->result_array();

        if (!empty($checkProjectHasExperts)) {
            if (empty($checkAllReadyTimeGiven)) {

                /* Insert data in table */

                for ($i = 0; $i < count($data['selectTime']); $i++) {
                    $time_availability = array(
                        'fk_client' => $checkProjectHasExperts[0]['fk_client'],
                        'fk_user' => $checkProjectHasExperts[0]['fk_user'],
                        'fk_project' => $checkProjectHasExperts[0]['fk_project'],
                        'fk_expert' => $checkProjectHasExperts[0]['fk_experts'],
                        'fk_project_has_experts' => $checkProjectHasExperts[0]['id'],
                        'dt_date_availability' => date('Y-m-d', strtotime($data['selectTime'][$i]['startDate'])),
                        'dt_time_availability' => $data['selectTime'][$i]['startTime'],
                        'created_at' => date('Y-m-d H:i:s'),
                    );
                    $this->db->insert('experts_has_time_availability', $time_availability);
                }

                /* get data from experts availability */

                $this->db->select('experts_has_time_availability.dt_date_availability,
                                           GROUP_CONCAT(' . TABLE_PREFIX . 'experts_has_time_availability.id) AS dayTimeId,
                                           GROUP_CONCAT(' . TABLE_PREFIX . 'experts_has_time_availability.dt_time_availability) AS dayTime');
                $this->db->where('experts_has_time_availability.fk_project_has_experts', $data['projectHasExpertId']);
                $this->db->order_by('experts_has_time_availability.dt_date_availability', 'ASC');
                $this->db->group_by('experts_has_time_availability.dt_date_availability');
                $expertsTimes = $this->db->get('experts_has_time_availability')->result_array();

                /* get data from experts availability */

                /* date saprate with current week and set data*/

                $Week = array();
                $numWeek = array();

                for ($i = 0; $i < count($expertsTimes); $i++) {
                    array_push($Week, $expertsTimes[$i]['dt_date_availability']);
                }


                $weekStartDate = $Week[0];
                $StartWeek = date("W", strtotime($weekStartDate));
                $year = date("Y", strtotime($weekStartDate));

                $weekEndDate = date("W", strtotime(end($Week)));

                for ($i = $StartWeek; $i <= $weekEndDate; $i++) {
                    $result = $this->getWeek($i, $year);

                    $NumWeek = array(
                        'start' => $result['start'],
                        'end' => $result['end'],
                        'time' => array(),
                    );
                    array_push($numWeek, $NumWeek);
                }

                /* date saprate with current week and set data*/

                for ($i = 0; $i < count($numWeek); $i++) {
                    for ($j = 0; $j < count($expertsTimes); $j++) {
                        $expertsDate = date('Y-m-d', strtotime($expertsTimes[$j]['dt_date_availability']));
                        $startWeekDate = date('Y-m-d', strtotime($numWeek[$i]['start']));
                        $endWeekDate = date('Y-m-d', strtotime($numWeek[$i]['end']));

                        if (($expertsDate >= $startWeekDate) && ($expertsDate <= $endWeekDate)) {
                            $timeData = array(
                                'dayTimeId' => explode(',', $expertsTimes[$j]['dayTimeId']),
                                'dayTime' => explode(',', $expertsTimes[$j]['dayTime']),
                                'date' => $expertsDate,
                            );
                            array_push($numWeek[$i]['time'], $timeData);
                        }
                    }
                }


                /* get Data of user account manager and experts */

                $this->db->select('user.var_fname as userFname,user.var_lname as userLname,user.var_email as userEmail,
                           experts.var_fname as expetsFname,experts.var_lname as expetsLname,
                           client.var_company_name,account_manager.var_name as stafName,account_manager.var_email as staffEmail,account_manager.bint_phone staffPhone');
                $this->db->join('client', 'client.id = project_has_experts.fk_client');
                $this->db->join('account_manager', 'account_manager.id = client.fk_assign_account_manager', 'left');
                $this->db->join('user', 'user.id = project_has_experts.fk_user');
                $this->db->join('experts', 'experts.id = project_has_experts.fk_experts');
                $this->db->where('project_has_experts.id', $data['projectHasExpertId']);
                $getExpertsAndClientUser = $this->db->get('project_has_experts')->result_array();

                /* get Data of user account manager and experts */

                /* Set data for mail template*/

                $data['WeekData'] = $numWeek;
                $data['ExpertsAndClientUser'] = $getExpertsAndClientUser;

                $mail_templet = $this->load->view('email-templates/client/choose-time', $data, TRUE);

                $this->load->library('Mylibrary');
                $configs['to'] = $getExpertsAndClientUser[0]['userEmail'];
                $configs['cc'] = $getExpertsAndClientUser[0]['staffEmail'];
                $configs['subject'] = 'Experts Time Availability';
                $configs['mail_body'] = $mail_templet;
                $sendMail = $this->mylibrary->sendMail($configs);
                if ($sendMail) {
                    $json_response['status'] = 'success';
                    $json_response['popup'] = 1;
                    $json_response['message'] = 'Your request has been sent to the expert';
                } else {
                    $json_response['status'] = 'error';
                    $json_response['message'] = 'Something will be wrong!..';
                }
            } else {
                $json_response['status'] = 'warning';
                $json_response['message'] = 'You have already select time!..';
            }
        } else {
            $json_response['status'] = 'warning';
            $json_response['message'] = 'Sorry experts not found for this project!..';
        }

        return $json_response;
    }

    function getWeek($week, $year)
    {
        $dto = new DateTime();
        $result['start'] = $dto->setISODate($year, $week, 1)->format('Y-m-d');
        $result['end'] = $dto->setISODate($year, $week, 7)->format('Y-m-d');
        return $result;
    }
}
