<?php

class Expert_profile_model extends CI_Model
{

    public function editExpertDetails($data, $json_response)
    {


        if ($data['editField'] == 'ExpertName') {

            $config = array(
                array('field' => 'value', 'label' => 'Expert Name', 'rules' => 'required'),
            );

            $this->form_validation->set_rules($config);

            if ($this->form_validation->run() !== FALSE) {

                $valueArr = explode(' ', $data['value']);
                $firstName = '';
                $lastName = '';
                $lastNameArr = array();

                for ($i = 0; $i < count($valueArr); $i++) {
                    if ($i == 0) {
                        $firstName = $valueArr[$i];
                    } else {
                        $lastNameArr[] = $valueArr[$i];
                    }
                }
                $lastName = implode(' ', $lastNameArr);
                $expertDetails = array(
                    'var_fname' => $firstName,
                    'var_lname' => $lastName,
                );
            }
        } else {
            $expertDetails = array(
                'txt_bio' => $data['value'],
            );
        }

        if ($data['id']) {
            $this->db->where('id', $data['id']);
            $this->db->update("experts", $expertDetails);
            $json_response['status'] = 'success';
            $json_response['message'] = 'Expert Details Updated!..';
        } else {
            $json_response['status'] = 'error';
            $json_response['message'] = 'Something will be wrong';
        }
        return $json_response;
    }

    public function editExpertExperience($data, $json_response)
    {

        $config = array(
            array('field' => 'txt_company', 'label' => 'Company Name', 'rules' => 'required'),
            array('field' => 'txt_position', 'label' => 'Position', 'rules' => 'required'),
            array('field' => 'txt_geography', 'label' => 'Geography', 'rules' => 'required'),
            array('field' => 'txt_desc', 'label' => 'Description', 'rules' => 'required'),
            array('field' => 'fromMonth', 'label' => 'From Month', 'rules' => 'required'),
            array('field' => 'fromYear', 'label' => 'From Year', 'rules' => 'required'),
        );
        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() !== FALSE) {
            if (!isset($data['is_present']) && empty($data['toMonth']) && empty($data['toYear'])) {
                $json_response['message'] = "Please select To date !...";
                $json_response['status'] = 'warning';
                return $json_response;
            }

            $fromYearMonthDate = ($data['fromMonth'] && $data['fromYear']) ? $data['fromYear'] . '-' . $data['fromMonth'] . '-01' : NULL;
            if ($data['is_present']) {
                $toYearMonthDate = NULL;
            } else {
                $toYearMonthDate = ($data['toMonth'] && $data['toYear']) ? $data['toYear'] . '-' . $data['toMonth'] . '-01' : NULL;
            }

            $expertHistory = array(
                'fk_experts' => ($data['fk_experts']) ? $data['fk_experts'] : getLoginExpertUserId(),
                'var_company' => $data['txt_company'],
                'var_position' => $data['txt_position'],
                'var_geography' => $data['txt_geography'],
                'txt_responsibilities' => $data['txt_desc'],
                'dt_from_respective_date' => $fromYearMonthDate,
                'dt_to_respective_date' => $toYearMonthDate,
                'enum_present' => ($data['is_present'] != '') ? 'YES' : 'NO',
            );

            if ($data['experienceId']) {
                $this->db->where('id', $data['experienceId']);
                $this->db->update("experts_has_employment_history", $expertHistory);
                $json_response['status'] = 'success';
                $json_response['message'] = 'Employment History Updated!..';
                $json_response['jscode'] = 'Bio.handleRefresh("experience");';
            } else {
                $expertHistory['created_at'] = date('Y-m-d H:i:s');
                $result = $this->db->insert('experts_has_employment_history', $expertHistory);
                if ($result) {
                    $json_response['status'] = 'success';
                    $json_response['message'] = 'Employment History Added!..';
                    $json_response['jscode'] = 'Bio.handleRefresh("experience");';
                } else {
                    $json_response['status'] = 'error';
                    $json_response['message'] = 'Something will be wrong';
                }
            }
        } else {
            $json_response['message'] = validation_errors();
            $json_response['status'] = 'warning';
        }
        return $json_response;
    }

    public function editExpertEducation($data, $json_response)
    {

        $config = array(
            array('field' => 'txtschool', 'label' => 'School Name', 'rules' => 'required'),
            array('field' => 'txtdegree', 'label' => 'Degree', 'rules' => 'required'),
            array('field' => 'fromMonth', 'label' => 'From Month', 'rules' => 'required'),
            array('field' => 'fromYear', 'label' => 'From Year', 'rules' => 'required'),
            array('field' => 'toMonth', 'label' => 'To Month', 'rules' => 'required'),
            array('field' => 'toYear', 'label' => 'To Year', 'rules' => 'required'),
        );
        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() !== FALSE) {

            $fromYearMonthDate = ($data['fromMonth'] && $data['fromYear']) ? $data['fromYear'] . '-' . $data['fromMonth'] . '-01' : NULL;
            $toYearMonthDate = ($data['toMonth'] && $data['toYear']) ? $data['toYear'] . '-' . $data['toMonth'] . '-01' : NULL;

            $educationHistory = array(
                'fk_experts' => ($data['fk_experts']) ? $data['fk_experts'] : getLoginExpertUserId(),
                'var_colleges' => $data['txtschool'],
                'var_certificates' => $data['txtdegree'],
                'dt_from_respective_date' => $fromYearMonthDate,
                'dt_to_respective_date' => $toYearMonthDate,
            );

            if ($data['educationId']) {
                $this->db->where('id', $data['educationId']);
                $this->db->update("experts_has_education", $educationHistory);
                $json_response['status'] = 'success';
                $json_response['message'] = 'Education History Updated!..';
                $json_response['jscode'] = 'Bio.handleRefresh("education");';
            } else {
                $educationHistory['created_at'] = date('Y-m-d H:i:s');

                $result = $this->db->insert('experts_has_education', $educationHistory);
                if ($result) {
                    $json_response['status'] = 'success';
                    $json_response['message'] = 'Education History Added!..';
                    $json_response['jscode'] = 'Bio.handleRefresh("education");';
                } else {
                    $json_response['status'] = 'error';
                    $json_response['message'] = 'Something will be wrong';
                }
            }
        } else {
            $json_response['message'] = validation_errors();
            $json_response['status'] = 'warning';
        }
        return $json_response;
    }

    public function addExpertSkills($data, $json_response)
    {

        $config = array(
            array('field' => 'txtareaname', 'label' => 'Area Name', 'rules' => 'required'),
        );
        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() !== FALSE) {

            $checkMasterArea = $this->db->where('var_area_name', $data['txtareaname'])->get('master_area')->row_array();
            $checkExpertsArea = $this->db->where('fk_area', $checkMasterArea['id'])->where('fk_experts', getLoginExpertUserId())->get('experts_area')->num_rows();

            if (count($checkMasterArea) > 0 && $checkExpertsArea == 0) {

                if ($checkMasterArea['id']) {
                    $expertskills = array(
                        'fk_experts' => getLoginExpertUserId(),
                        'fk_area' => $checkMasterArea['id'],
                        'created_at' => date('Y-m-d H:i:s'),
                    );
                    $this->db->insert('experts_area', $expertskills);
                    if ($this->db->insert_id()) {
                        $json_response['status'] = 'success';
                        $json_response['message'] = 'Expert Skills Added!..';
                        $json_response['jscode'] = 'Bio.handleRefresh("skills");';
                    } else {
                        $json_response['status'] = 'error';
                        $json_response['message'] = 'Something will be wrong';
                    }
                }
            }
            if (count($checkMasterArea) == 0 && $checkExpertsArea == 0) {

                $skills = array(
                    'var_area_name' => $data['txtareaname'],
                    'created_at' => date('Y-m-d H:i:s'),
                );

                $this->db->insert('master_area', $skills);
                $lastID = $this->db->insert_id();
                if ($lastID > 0) {
                    $expertskills = array(
                        'fk_experts' => getLoginExpertUserId(),
                        'fk_area' => $lastID,
                        'created_at' => date('Y-m-d H:i:s'),
                    );
                    $this->db->insert('experts_area', $expertskills);
                    if ($this->db->insert_id()) {
                        $json_response['status'] = 'success';
                        $json_response['message'] = 'Expert Skills Added!..';
                        $json_response['jscode'] = 'Bio.handleRefresh("skills");';
                    } else {
                        $json_response['status'] = 'error';
                        $json_response['message'] = 'Something will be wrong';
                    }
                }
            }
            if (count($checkMasterArea) > 0 && $checkExpertsArea > 0) {
                $json_response['status'] = 'error';
                $json_response['message'] = 'Expert Skills Already Exists!..';
                $json_response['jscode'] = 'Bio.handleRefresh("skills");';
            }
        } else {
            $json_response['message'] = validation_errors();
            $json_response['status'] = 'warning';
        }
        return $json_response;
    }

    public function editExpertSkills($data, $json_response)
    {

        $config = array(
            array('field' => 'txtareaname', 'label' => 'Area Name', 'rules' => 'required'),
        );
        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() !== FALSE) {

            $checkMasterArea = $this->db->where('var_area_name', $data['txtareaname'])->get('master_area')->row_array();
            $checkMasterDetails = $this->db->where('id', $data['masterAreaId'])->get('master_area')->row_array();
            $checkExpertsArea = $this->db->where('fk_area', $checkMasterArea['id'])->where('fk_experts', getLoginExpertUserId())->get('experts_area')->num_rows();

            if ($checkMasterDetails['var_area_name'] != $data['txtareaname']) {

                if (count($checkMasterArea) > 0 && $checkExpertsArea == 0) {

                    if ($checkMasterArea['id']) {

                        $this->db->where('fk_area', $data['masterAreaId']);
                        $this->db->where('fk_experts', getLoginExpertUserId());
                        $this->db->delete('experts_area');

                        $expertskills = array(
                            'fk_experts' => getLoginExpertUserId(),
                            'fk_area' => $checkMasterArea['id'],
                            'created_at' => date('Y-m-d H:i:s'),
                        );
                        $this->db->insert('experts_area', $expertskills);
                        if ($this->db->insert_id()) {
                            $json_response['status'] = 'success';
                            $json_response['message'] = 'Expert Skills Updated!..';
                            $json_response['jscode'] = 'Bio.handleRefresh("skills");';
                        } else {
                            $json_response['status'] = 'error';
                            $json_response['message'] = 'Something will be wrong';
                        }
                    }
                }
                if (count($checkMasterArea) == 0 && $checkExpertsArea == 0) {

                    $this->db->where('fk_area', $data['masterAreaId']);
                    $this->db->where('fk_experts', getLoginExpertUserId());
                    $this->db->delete('experts_area');
                    $skills = array(
                        'var_area_name' => $data['txtareaname'],
                        'created_at' => date('Y-m-d H:i:s'),
                    );

                    $this->db->insert('master_area', $skills);
                    $lastID = $this->db->insert_id();
                    if ($lastID > 0) {
                        $expertskills = array(
                            'fk_experts' => getLoginExpertUserId(),
                            'fk_area' => $lastID,
                            'created_at' => date('Y-m-d H:i:s'),
                        );
                        $this->db->insert('experts_area', $expertskills);
                        if ($this->db->insert_id()) {
                            $json_response['status'] = 'success';
                            $json_response['message'] = 'Expert Skills Updated!..';
                            $json_response['jscode'] = 'Bio.handleRefresh("skills");';
                        } else {
                            $json_response['status'] = 'error';
                            $json_response['message'] = 'Something will be wrong';
                        }
                    }
                }
                if (count($checkMasterArea) > 0 && $checkExpertsArea > 0) {
                    $json_response['status'] = 'warning';
                    $json_response['message'] = 'Expert Skills Already Exists!..';
                    $json_response['jscode'] = 'Bio.handleRefresh("skills");';
                }
            } else {
                $json_response['jscode'] = 'Bio.handleRefresh("skills");';
            }
        } else {
            $json_response['message'] = validation_errors();
            $json_response['status'] = 'warning';
        }
        return $json_response;
    }

    public function getExpertDetails($id)
    {
        $id = (empty($id) && !isset($id)) ? getLoginExpertUserId() : $id;
        if ($id) {
            $this->db->select('experts.*,'
                . 'CONCAT(' . TABLE_PREFIX . 'experts.var_fname," ",' . TABLE_PREFIX . 'experts.var_lname) as ExpertName,'
                . 'CONCAT("$","",' . TABLE_PREFIX . 'experts.var_rate_per_hour) as ExpertRatePerHour,'
                . 'CONCAT("$","",' . TABLE_PREFIX . 'experts.var_rate_per_survey) as ExpertRatePerSurvey,'
                . 'GROUP_CONCAT(' . TABLE_PREFIX . 'master_area.var_area_name) as AreaOfExpertise,'
                . 'experts_has_employment_history.var_company as CompanyName,'
                . 'experts_has_employment_history.var_position as Position,'
                . 'experts_has_employment_history.var_geography as Geography,'
                . 'experts_has_employment_history.txt_responsibilities as Responsibilities,'
                . 'experts_has_employment_history.dt_from_respective_date as from_respective_date,'
                . 'experts_has_employment_history.dt_to_respective_date as to_respective_date,'
                . 'experts_has_employment_history.enum_present as Present');
            $this->db->from('experts');
            $this->db->join('experts_area', 'experts_area.fk_experts = experts.id', 'left');
            $this->db->join('master_area', 'master_area.id = experts_area.fk_area', 'left');
            $this->db->join('experts_has_employment_history', 'experts_has_employment_history.fk_experts = experts.id AND experts_has_employment_history.enum_present = "YES"', 'left');
            $this->db->where('experts.id', $id);
            $this->db->group_by('experts.id');
            $data = $this->db->get()->row_array();
        }
        return $data;
    }

    public function getExpertCurrentEmployment($id)
    {
        $id = (empty($id) && !isset($id)) ? getLoginExpertUserId() : $id;
        if ($id) {
            $this->db->where('fk_experts', $id);
            $this->db->where('enum_present', 'YES');
            $data = $this->db->get('experts_has_employment_history')->row_array();
        }
        return $data;
    }

    public function getExpertEmploymentHistory($id)
    {
        $id = (empty($id) && !isset($id)) ? getLoginExpertUserId() : $id;
        if ($id) {
            $this->db->where('fk_experts', $id);
            $data = $this->db->get('experts_has_employment_history')->result_array();
        }
        return $data;
    }

    public function getExpertEmploymentHistoryById($id)
    {
        if ($id) {
            $this->db->where('id', $id);
            $data = $this->db->get('experts_has_employment_history')->row_array();
        }
        return $data;
    }

    public function getExpertArea($id)
    {
        $id = (empty($id) && !isset($id)) ? getLoginExpertUserId() : $id;
        if ($id) {
            $this->db->select('experts_area.*,'
                . 'master_area.var_area_name,'
                . 'master_area.txt_description,'
                . 'master_area.enum_enable');
            $this->db->from('experts_area');
            $this->db->join('master_area', 'master_area.id = experts_area.fk_area');
            $this->db->where('experts_area.fk_experts', $id);
            $this->db->order_by('master_area.var_area_name', 'ASC');
            $data = $this->db->get()->result_array();
        }
        return $data;
    }

    public function getExpertAreaById($id)
    {
        if ($id) {
            $this->db->select('experts_area.*,'
                . 'master_area.id as masterAreaId,'
                . 'master_area.var_area_name,'
                . 'master_area.txt_description,'
                . 'master_area.enum_enable');
            $this->db->from('experts_area');
            $this->db->join('master_area', 'master_area.id = experts_area.fk_area');
            $this->db->where('experts_area.id', $id);
            $data = $this->db->get()->row_array();
        }
        return $data;
    }

    public function getExpertEducation($id)
    {
        $id = (empty($id) && !isset($id)) ? getLoginExpertUserId() : $id;
        if ($id) {
            $this->db->where('fk_experts', $id);
            $data = $this->db->get('experts_has_education')->result_array();
        }
        return $data;
    }

    public function getExpertEducationById($id)
    {
        if ($id) {
            $this->db->where('id', $id);
            $data = $this->db->get('experts_has_education')->row_array();
        }
        return $data;
    }

}
