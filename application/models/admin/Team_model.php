<?php

class Team_model extends CI_Model
{

    function getTeamDatatables()
    {
        $this->datatables->select('var_team_name,id,fk_admin');
        $this->datatables->from('team');
        $this->datatables->edit_column('id', '<a title="Edit" class="btn btn-primary btn-xs edit" href="' . admin_url('team/edit/') . '$1"><i class="fa fa-eye"></i> Edit</a>  <a title="Delete" class="btn blue btn-xs common_delete" data-href="' . admin_url('team/deleteTeam') . '" data-id="$1" data-toggle="modal" href="#delete_model" class="deleteTeam"><i class="fa fa-trash"></i> Delete</a>', 'id');
        return $this->datatables->generate();

    }

    function addTeam($data, $json_response)
    {

        $config = array(
            array('field' => 'var_team', 'label' => 'Team Name', 'rules' => 'required'),
        );
        $this->form_validation->set_rules($config);
        $adminId = $this->authlibrary->getLoginUserId();
        if ($this->form_validation->run() !== FALSE) {
            $TeamData = array(
                'fk_admin' => $adminId,
                'var_team_name' => $data['var_team'],
            );
            if (empty($data['teamId'])) {
                $TeamData['created_at'] = date('Y-m-d H:i:s');
                $result = $this->db->insert("team", $TeamData);
                if ($result) {
                    $json_response['status'] = 'success';
                    $json_response['message'] = 'Team added Successfully!..';
                    $json_response['redirect'] = admin_url('team');
                } else {
                    $json_response['status'] = 'error';
                    $json_response['message'] = 'Something will be wrong';
                }
            } else {
                $this->db->where('id', $data['teamId']);
                $this->db->Update("team", $TeamData);
                $json_response['status'] = 'success';
                $json_response['message'] = 'Updated';
                $json_response['redirect'] = admin_url('team');
            }
        } else {
            $json_response['message'] = validation_errors();
            $json_response['status'] = 'warning';
        }
        return $json_response;
    }

    function deleteTeam($data, $json_response)
    {
        $result = $this->toval->id_delete($data['id'], 'team', 'id');
        if ($result > 0) {
            $json_response['status'] = 'success';
            $json_response['message'] = 'Team Deleted Successfully!..';
            $json_response['jscode'] = " setTimeout(function () { $('#delete_model').modal('hide');teamDatatables.refresh(); }, 100);";
        } else {
            $json_response['status'] = 'error';
            $json_response['message'] = 'Something will be wrong';
        }
        return $json_response;
    }

}
