<?php

class Call_model extends CI_Model {

    function getCallList() {
        $iTotalRecords = 5;
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;
        $j = 1;
        $bill = array('Yes', 'No');
        $billLable = array('label-danger', 'label-success');
        for ($i = $iDisplayStart; $i < $end; $i++) {
            $index = array_rand($bill);
            $records["data"][$i][0] = date('d/m/Y');
            $records["data"][$i][1] = date('H:i');
//            $records["data"][$i][2] = 'Client' . $j;
            $records["data"][$i][2] = "<a href='" . admin_url() . 'client/overview/' . $j . "'>Client $j</a>";;
            $records["data"][$i][3] = 'User' . $j;
            $records["data"][$i][4] = "<a href='" . admin_url() . 'experts/bio/' . $j . "'>Expert" . $j . "</a>";
            $records["data"][$i][5] = "<a href='" . admin_url() . 'project/overview/' . $j . "'>Project" . $j . "</a>";
            $records["data"][$i][6] = '1hr';
            $records["data"][$i][7] = '<span class="label label-success label-sm "> Completed </span>';
            $records["data"][$i][8] = '<span class="label ' . $billLable[$index] . ' label-sm "> ' . $bill[$index] . ' </span>';
            $records["data"][$i][9] = '<a href="javascript:;" data-toggle ="modal" class="btn btn-primary btn-xs edit">Create Call Bridge</a>';
            $j++;
        }

        $records["sEcho"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return $records;
    }

}
