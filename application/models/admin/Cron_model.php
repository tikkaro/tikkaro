<?php

class Cron_model extends CI_Model
{

    function manageExpertsUnblock() {

        $this->db->select('experts_block.*');
        $this->db->where('enum_permanent', 'NO');
        $this->db->where('dt_to_block_date <= CURDATE()');
        $expertUnblock = $this->db->get('experts_block')->result_array();

        if (!empty($expertUnblock)) {
            for ($i = 0; $i < count($expertUnblock); $i++) {

                /*Update unblock status*/

                $unblockExperts = array(
                    'enum_block' => 'NO',
                );
                $this->db->where('id', $expertUnblock[$i]['fk_experts']);
                $this->db->update('experts', $unblockExperts);
            }
        }

        $this->db->where('enum_permanent', 'NO');
        $this->db->where('dt_to_block_date <= CURDATE()');
        $this->db->from('experts_block');
        $this->db->delete();
    }


    function manageSubscription() {
        /* Pay as you go*/
        $this->db->where('enum_subscription_type', 'PAY-AS-YOU-GO');
        $this->db->where('dt_end_date <', date('Y-m-d'));
        $this->db->update('client_has_subscription', array('enum_enable' => 'NO'));

        /* Unit block */
        $this->db->where('enum_subscription_type', 'UNIT-BLOCK');
        $this->db->group_start();
        $this->db->where('dt_end_date <', date('Y-m-d'));
        $this->db->or_where('int_calls_remains', '0');
        $this->db->group_end();
        $this->db->update('client_has_subscription', array('enum_enable' => 'NO'));
    }

    function managedoCompliance() {
        $expertCompaliance = $this->db->get('experts')->result_array();
        for($i=0; $i<count($expertCompaliance); $i++){
            if($expertCompaliance[$i]['dt_compliance_date'] != ''){
                if (strtotime($expertCompaliance[$i]['dt_compliance_date']) < strtotime('-1 Year')) {
                    $expertsEmail = $expertCompaliance[$i]['var_email'];
                    $this->load->library('Mylibrary');
                    $configs['to'] = $expertsEmail;
                    $configs['subject'] = 'Do Compliance';
                    $configs['mail_body'] = 'Hello '.$expertCompaliance[$i]['var_fname'].' '.$expertCompaliance[$i]['var_lname'].',<br/>Please do Compliance.<br/>Thank You.';
                    $sendMail = $this->mylibrary->sendMail($configs);
                }
            }else{
                $expertsEmail = $expertCompaliance[$i]['var_email'];
                $this->load->library('Mylibrary');
                $configs['to'] = $expertsEmail;
                $configs['subject'] = 'Do Compliance';
                $configs['mail_body'] = 'Hello '.$expertCompaliance[$i]['var_fname'].' '.$expertCompaliance[$i]['var_lname'].',<br/>Please do Compliance.<br/>Thank You.';
                $sendMail = $this->mylibrary->sendMail($configs);
            }
        }
    }
}
