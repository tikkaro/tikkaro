<?php

class Client_model extends CI_Model
{

    function getClientDatatables()
    {
        $this->datatables->select('client.var_company_name as companyname,'
            . '"2" as live_project,'
            . 'client.enum_value,'
            . '"13/03/2017" as last_call_taken,'
            . '"5" as scheduling,'
            . 'client.id as clientid,'
            . '"1" as action');

        $this->datatables->from('client');
        $this->datatables->edit_column('companyname', '<a href="' . admin_url('client/overview') . '/$1">$2</a>', 'clientid,companyname');
        $result = $this->datatables->generate();
        $records = (array)json_decode($result);
        $sub_pace = array('Ahead', 'Behind', 'Pay as you Go');
        $subLabel = array('label-success', 'label-danger', 'label-black');

        for ($i = 0; $i < count($records['data']); $i++) {
            $sub_index = array_rand($sub_pace);
            $id = $records["data"][$i][5];
            $records["data"][$i][2] = clientLable($records['data'][$i][2]);
            $records["data"][$i][7] = '<span class="label ' . $subLabel[$sub_index] . ' label-sm "> ' . $sub_pace[$sub_index] . ' </span>';
            $records["data"][$i][8] = '<a title="Delete" class="btn blue btn-xs common_delete" data-href="' . admin_url('client/deleteClient') . '" data-id="' . $id . '" data-toggle="modal" href="#delete_model" class="deleteClient"><i class="fa fa-trash"></i> Delete</a>';
        }

        return $records;
    }

    function deleteClient($data, $json_response)
    {
        $result = $this->toval->id_delete($data['id'], 'client', 'id');
        if ($result > 0) {
            $json_response['status'] = 'success';
            $json_response['message'] = 'Client Deleted Successfully!..';
            $json_response['jscode'] = "setTimeout(function () { $('#delete_model').modal('hide');clientDatatables.refresh(); }, 100);";
        } else {
            $json_response['status'] = 'error';
            $json_response['message'] = 'Something will be wrong';
        }
        return $json_response;
    }

    function addClient($data, $json_response)
    {

        $config = array(
            array('field' => 'company_name', 'label' => 'Company Name', 'rules' => 'required'),
            array('field' => 'team', 'label' => 'Team', 'rules' => 'required'),
            array('field' => 'ac_manager', 'label' => 'Account Manager', 'rules' => 'required'),
            array('field' => 'set_priority', 'label' => 'Select Priority', 'rules' => 'required'),
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() !== FALSE) {
            $checkClient = $this->db->get_where('client', array('var_company_name' => $data['company_name']))->num_rows();
            if ($checkClient == 0) {

                $clinetData = array(
                    'fk_admin' => ($this->user_type == 'ADMIN') ? $this->user_id : NULL,
                    'fk_team' => $data['team'],
                    'fk_assign_team' => $data['team'],
                    'fk_account_manager' => ($this->user_type == 'KEY_ACCOUNT_MANAGER') ? $this->user_id : NULL,
                    'fk_assign_account_manager' => ($data['ac_manager']) ? $data['ac_manager'] : NULL,
                    'var_company_name' => $data['company_name'],
                    'bint_phone' => ($data['phone']) ? $data['phone'] : NULL,
                    'txt_address' => ($data['address']) ? $data['address'] : NULL,
                    'enum_value' => $data['set_priority'],
                    'created_at' => date('Y-m-d H:i:s'),
                );
                $result = $this->db->insert('client', $clinetData);
                if ($result) {
                    $json_response['status'] = 'success';
                    $json_response['message'] = 'Client added Successfully!..';
                    $json_response['redirect'] = admin_url('client');
                } else {
                    $json_response['status'] = 'error';
                    $json_response['message'] = 'Something will be wrong';
                }
            } else {
                $json_response['message'] = 'Company alredy exist!..';
                $json_response['status'] = 'warning';
            }
        } else {
            $json_response['message'] = validation_errors();
            $json_response['status'] = 'warning';
        }

        return $json_response;
    }

    function getOverviewDatatables($clientId)
    {
        $this->datatables->select('CONCAT(' . TABLE_PREFIX . 'user.var_fname," ",' . TABLE_PREFIX . 'user.var_lname) as username,
                                   user.var_position,
                                   user.bint_phone,
                                   user.var_email,"22" as last_contact,user.id as userid');
        $this->datatables->where('user.fk_client', $clientId);
        $this->datatables->edit_column('username', '<a href="' . admin_url('client/user-bio') . '/$1">$2</a>', 'userid,username');
        $this->datatables->from('user');
        return $this->datatables->generate();
    }

    function addClientNote($data, $json_response)
    {
        $config = array(
            array('field' => 'notes', 'label' => 'Note', 'rules' => 'required'),
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE) {
            if (!empty($data['notes'])) {
                $noteData = array(
                    'txt_note' => $data['notes'],
                    'fk_client' => $data['fk_client'],
                    'fk_admin' => ($this->user_type == 'ADMIN') ? $this->user_id : NULL,
                    'fk_account_manager' => ($this->user_type == 'KEY_ACCOUNT_MANAGER') ? $this->user_id : NULL,
                );
                if (empty($data['note_id'])) {
                    $noteData['created_at'] = date('Y-m-d H:i:s');
                    $result = $this->db->insert("client_has_notes", $noteData);
                    $json_response['message'] = 'Note added Successfully!..';
                } else {
                    $this->db->where('id', $data['note_id']);
                    $result = $this->db->update("client_has_notes", $noteData);
                    $json_response['message'] = 'Updated';
                }
                if ($result) {
                    $json_response['status'] = 'success';
                    $json_response['jscode'] = "setTimeout(function () { $('#add_client_notes').modal('hide');notesDatatables.refresh(); }, 100);";
                } else {
                    $json_response['status'] = 'error';
                    $json_response['message'] = 'Something will be wrong';
                }
            } else {
                $json_response['message'] = validation_errors();
                $json_response['status'] = 'warning';
            }
            return $json_response;
        }
    }

    function getNotesDatatables($clientId = NULL)
    {
        $this->datatables->select('DATE_FORMAT(' . TABLE_PREFIX . 'client_has_notes.created_at, \'%d/%m/%Y\') AS created_at,
                                  IF((' . TABLE_PREFIX . 'admin.var_name !=""),' . TABLE_PREFIX . 'admin.var_name,IF((' . TABLE_PREFIX . 'account_manager.var_name !=""),' . TABLE_PREFIX . 'account_manager.var_name,"")) as noteBy,
                                   client_has_notes.txt_note,
                                   "1" as action,
                                   client_has_notes.id');
        $this->datatables->from('client_has_notes');
        $this->datatables->join('client', 'client_has_notes.fk_client = client.id');
        $this->datatables->join('admin', 'client_has_notes.fk_admin= admin.id', 'left');
        $this->datatables->join('account_manager', 'client_has_notes.fk_account_manager= account_manager.id', 'left');
        if (!empty($clientId) && $clientId != '') {
            $this->datatables->where('client_has_notes.fk_client', $clientId);
        }
        $this->db->order_by('client_has_notes.id', 'DESC');
        $this->datatables->edit_column('action', '<a href="javascript:;" class="btn btn-primary btn-xs edit_client_notes" data-note-id="$1"><i class="fa fa-eye"></i> Edit</a>  <a href="#delete_model" data-toggle="modal" class="btn blue btn-xs common_delete" data-href="' . admin_url('client/deleteClientNote') . '" data-id="$1"><i class="fa fa-trash"></i> Delete</a>', 'id');
        $result = $this->datatables->generate();
        return $result;
    }

    function getUserNotesDatatables($userId)
    {
        $this->datatables->select('DATE_FORMAT(' . TABLE_PREFIX . 'user_has_notes.updated_at, \'%d/%m/%Y\') AS initiate,
                                   IF((' . TABLE_PREFIX . 'admin.var_name !=""),' . TABLE_PREFIX . 'admin.var_name,IF((' . TABLE_PREFIX . 'account_manager.var_name !=""),' . TABLE_PREFIX . 'account_manager.var_name,"")) as noteBy,
                                   user_has_notes.txt_note,
                                   "1" as action,
                                   user_has_notes.id');
        $this->datatables->from('user_has_notes');
        $this->datatables->join('account_manager', 'account_manager.id = user_has_notes.fk_account_manager', 'left');
        $this->datatables->join('admin', 'admin.id = user_has_notes.fk_admin', 'left');
        $this->datatables->join('user', 'user.id = user_has_notes.fk_user');
        $this->datatables->where('user_has_notes.fk_user', $userId);
        $this->db->order_by('user_has_notes.id', 'DESC');
        $this->datatables->edit_column('action', '<a href="javascript:;" data-toggle ="modal" class="btn btn-primary btn-xs btn-edit-note" data-note-id="$1"><i class="fa fa-eye"></i> Edit</a>  <a title="Delete" class="btn blue btn-xs common_delete" data-href="' . admin_url('client/deleteUserNote') . '" data-id="$1" data-toggle="modal" href="#delete_model"><i class="fa fa-trash"></i> Delete</a>', 'id');
        $result = $this->datatables->generate();

        return $result;
    }

    function getProjectDatatables($clientId)
    {
        $this->datatables->select('"23/07/2015" as staticdate,
                                   client_has_project.var_project_name as projectname,
                                   CONCAT(' . TABLE_PREFIX . 'user.var_fname," ",' . TABLE_PREFIX . 'user.var_lname) as username,
                                   "2" as callpending,    
                                   "2" as callcompleted,    
                                   DATE_FORMAT(' . TABLE_PREFIX . 'client_has_project.dt_initiate, \'%d/%m/%Y\') AS initiate, 
                                   client_has_project.enum_status,
                                   client_has_project.id as project_id,
                                   client_has_project.fk_user as user_id');
        $this->datatables->join('user', 'client_has_project.fk_user = user.id');
        $this->datatables->where('client_has_project.fk_client', $clientId);
        $this->datatables->from('client_has_project');
        $this->datatables->edit_column('projectname', '<a href="' . admin_url('project/overview') . '/$1">$2</a>', 'project_id,projectname');
        $this->datatables->edit_column('username', '<a href="' . admin_url('client/user-bio') . '/$1">$2</a>', 'user_id,username');
        $result = $this->datatables->generate();
        $records = (array)json_decode($result);
        for ($i = 0; $i < count($records['data']); $i++) {
            $records["data"][$i][6] = projectLable($records['data'][$i][6]);
        }

        return $records;
    }

    function getUserProjectList()
    {

        $iTotalRecords = 5;
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $status = array('priority', 'active', 'scheduling', 'waiting', 'closed');
        $lable = array('label-success', 'label-info', 'label-warning', 'label-info', 'label-danger');

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;
        $j = 0;
        for ($i = $iDisplayStart; $i < $end; $i++) {
            $j++;
            $index = array_rand($status);
            $records["data"][$i][0] = "<a href='" . admin_url() . 'project/overview/' . $j . "'>Project" . $j . "</a>";
            $records["data"][$i][1] = "Project Description" . $j;
            $records["data"][$i][2] = '01/04/2017 ';
            $records["data"][$i][3] = $j + 2;
            $records["data"][$i][4] = '<span class="label ' . $lable[$index] . ' label-sm "> ' . $status[$index] . ' </span>';
        }

        $records["sEcho"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return $records;
    }

    function Useradd($data, $json_response)
    {

        $config = array(
            array('field' => 'var_fname', 'label' => 'Firt Name', 'rules' => 'required'),
            array('field' => 'var_lname', 'label' => 'Last Name', 'rules' => 'required'),
//            array('field' => 'var_email', 'label' => 'Email', 'rules' => 'required'),
//            array('field' => 'bint_phone', 'label' => 'Phone', 'rules' => 'required'),
//            array('field' => 'var_position', 'label' => 'Position', 'rules' => 'required'),
//            array('field' => 'var_password', 'label' => 'Password', 'rules' => 'required'),
//            array('field' => 'var_dob', 'label' => 'Date of birth', 'rules' => 'required'),
//            array('field' => 'user_address', 'label' => 'Address', 'rules' => 'required'),
        );
        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() !== FALSE) {

            $checkUser = $this->db->get_where('user', array('var_email' => $data['var_email']))->num_rows();

            if ($checkUser == 0) {
//                $var_image = ($data['var_profile_image']) ? $data['var_profile_image'] : '';
//
//                if ($_FILES['profile_pic']['name'] != "") {
//                    $image = upload_single_image($_FILES, 'user_profile_pic', USER_PROFILE_IMG, FALSE);
//                    $var_image = ($image['error'] == '') ? $image['data']['orig_name'] : '';
//                }
                $UsertData = array(
                    'var_fname' => $data['var_fname'],
                    'var_lname' => $data['var_lname'],
                    'var_email' => ($data['var_email']) ? $data['var_email'] : NULL,
                    'bint_phone' => ($data['bint_phone']) ? $data['bint_phone'] : NULL,
                    'var_position' => $data['var_position'],
                    'var_password' => NULL,
                    'fk_admin' => ($this->user_type == 'ADMIN') ? $this->user_id : NULL,
                    'fk_client' => $data['client_id'],
                    'fk_account_manager' => ($this->user_type == 'KEY_ACCOUNT_MANAGER') ? $this->user_id : NULL,
                    'fk_team' => $data['fk_team'],
                    'fk_assign_team' => $data['fk_team'],
//                    'var_profile_image' => $var_image,
//                    'txt_address' => $data['user_address'],
//                    'dt_dob' => date('Y-m-d', strtotime($data['var_dob'])),
                    'enum_enable' => 'YES',
                    'created_at' => date('Y-m-d H:i:s'),
                );

                $result = $this->db->insert("user", $UsertData);


                if ($result) {
                    $json_response['status'] = 'success';
                    $json_response['message'] = 'User added Successfully!..';
//                $json_response['reload'] = 'true';
                    $json_response['redirect'] = admin_url('client/overview/' . $data['client_id']);
                } else {
                    $json_response['status'] = 'error';
                    $json_response['message'] = 'Something will be wrong';
                }
            } else {
                $json_response['message'] = 'Email alredy exist!..';
                $json_response['status'] = 'warning';
            }
        } else {
            $json_response['message'] = validation_errors();
            $json_response['status'] = 'warning';
        }

        return $json_response;
    }

    function getClientCommonHeader($clientId)
    {
        $data = $this->db->select('client.*,team.var_team_name as team_name,account_manager.var_name as account_manager')
            ->join('team', 'team.id = client.fk_team', 'left')
            ->join('account_manager', 'account_manager.id = client.fk_assign_account_manager', 'left')
            ->where('client.id', $clientId)
            ->get('client')->row_array();

        return $data;
    }

    function getClientSubscription($clientId)
    {
        $data = $this->db->select('client_has_subscription.*,
                                   account_manager.var_name as account_manager,
                                   admin.var_name as admin')
            ->join('account_manager', 'account_manager.id = client_has_subscription.fk_account_manager', 'left')
            ->join('admin', 'admin.id = client_has_subscription.fk_admin', 'left')
            ->where('client_has_subscription.fk_client', $clientId)
            ->where('client_has_subscription.enum_enable', 'YES')
            ->get('client_has_subscription')->row_array();

        return $data;
    }

    function getClientCallList()
    {
        $iTotalRecords = 5;
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;
        $j = 1;
        $bill = array('Yes', 'No');
        $billLable = array('label-danger', 'label-success');
        for ($i = $iDisplayStart; $i < $end; $i++) {
            $index = array_rand($bill);
            $records["data"][$i][0] = date('m/d/Y');
            $records["data"][$i][1] = date('H:i');
            $records["data"][$i][2] = 'User' . $j;
            $records["data"][$i][3] = "<a href='" . admin_url() . 'experts/bio/' . $j . "'>Expert" . $j . "</a>";
            $records["data"][$i][4] = "<a href='" . admin_url() . 'project/overview/' . $j . "'>Project" . $j . "</a>";
            $records["data"][$i][5] = '1hr';
            $records["data"][$i][6] = '<span class="label label-success label-sm "> Completed </span>';
            $records["data"][$i][7] = '<span class="label ' . $billLable[$index] . ' label-sm "> ' . $bill[$index] . ' </span>';
            $records["data"][$i][8] = '<a href="javascript:;" data-toggle ="modal" class="btn btn-primary btn-xs edit">Create Call Bridge</a>';
            $j++;
        }

        $records["sEcho"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return $records;
    }

    function getUserCallList()
    {
        $iTotalRecords = 5;
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;
        $j = 1;
        $bill = array('Yes', 'No');
        $billLable = array('label-danger', 'label-success');
        for ($i = $iDisplayStart; $i < $end; $i++) {
            $index = array_rand($bill);
            $records["data"][$i][0] = date('m/d/Y');
            $records["data"][$i][1] = date('H:i');
            $records["data"][$i][2] = "<a href='" . admin_url() . 'experts/bio/' . $j . "'>Expert" . $j . "</a>";
            $records["data"][$i][3] = "<a href='" . admin_url() . 'project/overview/' . $j . "'>Project" . $j . "</a>";
            $records["data"][$i][4] = '1hr';
            $records["data"][$i][5] = '<span class="label label-success label-sm "> Completed </span>';
            $records["data"][$i][6] = '<span class="label ' . $billLable[$index] . ' label-sm "> ' . $bill[$index] . ' </span>';
            $records["data"][$i][7] = '<a href="javascript:;" data-toggle ="modal" class="btn btn-primary btn-xs edit">Create Call Bridge</a>';
            $j++;
        }

        $records["sEcho"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return $records;
    }

    function addProject($data, $json_response)
    {
        $config = array(
            array('field' => 'var_project', 'label' => 'Project Name', 'rules' => 'required'),
            array('field' => 'project_description', 'label' => 'Project Description', 'rules' => 'required'),
            array('field' => 'var_industry', 'label' => 'Industry', 'rules' => 'required'),
            array('field' => 'user_id', 'label' => 'Select User', 'rules' => 'required'),
            array('field' => 'client_id', 'label' => 'Select Client', 'rules' => 'required'),
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() !== FALSE) {

            $projetc_array = array(
                'fk_user' => $data['user_id'],
                'fk_client' => $data['client_id'],
                'fk_admin' => ($this->user_type == 'ADMIN') ? $this->user_id : NULL,
                'fk_account_manager' => ($this->user_type === 'KEY_ACCOUNT_MANAGER') ? $this->user_id : NULL,
                'var_project_name' => $data['var_project'],
                'txt_description' => $data['project_description'],
                'var_industry' => $data['var_industry'],
                'int_no_of_call_required' => ($data['var_number_call']) ? $data['var_number_call'] : NULL,
                'created_at' => date('Y-m-d H:i:s'),
                'dt_initiate' => ($data['dt_initiate']) ? DateTime::createFromFormat('d/m/Y', $data['dt_initiate'])->format('Y-m-d') : NULL,
                'dt_deadline' => ($data['dt_deadline']) ? DateTime::createFromFormat('d/m/Y', $data['dt_deadline'])->format('Y-m-d') : NULL,
                'var_target_company' => ($data['var_target_company']) ? $data['var_target_company'] : NULL,
                'enum_client_anonymous' => ($data['client_anonymous']) ? $data['client_anonymous'] : NULL,
                'enum_target_anonymous' => ($data['target_anonymous']) ? $data['target_anonymous'] : NULL,
                'enum_status' => ($data['priority']) ? $data['priority'] : NULL,
            );

            $result = $this->db->insert('client_has_project', $projetc_array);
            $project_id = $this->db->insert_id();

            if (!empty($data['var_country_interest'])) {
                $country = explode(',', $data['var_country_interest']);

                for ($i = 0; $i < count($country); $i++) {
                    $countryData = $this->toval->idtorowarr('var_country_name', $country[$i], 'master_country');
                    if (!empty($countryData)) {
                        $countryId = $countryData['id'];
                    } else {
                        $countrydata = array(
                            'var_country_code' => $country[$i],
                            'var_country_name' => $country[$i],
                            'created_at' => date('Y-m-d H:i:s')
                        );
                        $this->db->insert('master_country', $countrydata);
                        $countryId = $this->db->insert_id();
                    }

                    $project_has_country = array(
                        'fk_project' => $project_id,
                        'fk_country' => $countryId,
                        'created_at' => date('Y-m-d H:i:s')
                    );

                    $this->db->insert('project_has_country', $project_has_country);
                }
            }

            if (!empty($data['var_companies_interest'])) {
                $company = explode(',', $data['var_companies_interest']);
                for ($i = 0; $i < count($company); $i++) {
                    $companyData = $this->toval->idtorowarr('var_company_name', $company[$i], 'master_company');
                    if (!empty($companyData)) {
                        $companyId = $companyData['id'];
                    } else {
                        $companydata = array(
                            'var_company_name' => $company[$i],
                            'created_at' => date('Y-m-d H:i:s')
                        );
                        $this->db->insert('master_company', $companydata);
                        $companyId = $this->db->insert_id();
                    }

                    $project_has_company = array(
                        'fk_project' => $project_id,
                        'fk_company' => $companyId,
                        'created_at' => date('Y-m-d H:i:s'),
                    );

                    $this->db->insert('project_has_company', $project_has_company);
                }
            }

            if (!empty($data['var_regions_interest'])) {
                $regions = explode(',', $data['var_regions_interest']);
                for ($i = 0; $i < count($regions); $i++) {
                    $regionsData = $this->toval->idtorowarr('var_regions_name', $regions[$i], 'master_regions');
                    if (!empty($regionsData)) {
                        $regionsId = $regionsData['id'];
                    } else {
                        $regionsdata = array(
                            'var_regions_name' => $regions[$i],
                            'created_at' => date('Y-m-d H:i:s')
                        );

                        $this->db->insert('master_regions', $regionsdata);
                        $regionsId = $this->db->insert_id();
                    }

                    $project_has_region = array(
                        'fk_project' => $project_id,
                        'fk_regions' => $regionsId,
                        'created_at' => date('Y-m-d H:i:s'),
                    );
                    $this->db->insert('project_has_regions', $project_has_region);
                }
            }

            if ($result) {
                $json_response['status'] = 'success';
                $json_response['message'] = 'Project added Successfully!..';
                if ($data['page_from'] == 'client') {
                    $json_response['redirect'] = admin_url('client/project/' . $data['client_id']);
                } else {
                    $json_response['redirect'] = admin_url('project');
                }
            } else {
                $json_response['status'] = 'error';
                $json_response['message'] = 'Something will be wrong';
            }
        } else {
            $json_response['message'] = validation_errors();
            $json_response['status'] = 'warning';
        }
        return $json_response;
    }

    function getUsersData($userId = NULL)
    {
        $this->db->select('user.*,
                           client.var_company_name,
                           account_manager.var_name as account_manager,
                           team.var_team_name');
        $this->db->join('client', 'client.id = user.fk_client');
        $this->db->join('account_manager', 'account_manager.id = client.fk_assign_account_manager', 'left');
        $this->db->join('team', 'team.id = client.fk_team', 'left');
        if ($userId !== NULL && !empty($userId)) {
            $this->db->where('user.id', $userId);
        }
        $result = $this->db->get('user')->result_array();
        return $result;
    }

    function addUserNote($data, $json_response)
    {
        $config = array(
            array('field' => 'notes', 'label' => 'Notes', 'rules' => 'required'),
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE) {
            if (!empty($data['notes']) && !empty($data['userId'])) {
                $noteData = array(
                    'txt_note' => $data['notes'],
                    'fk_user' => $data['userId'],
                    'fk_client' => $data['clientId'],
                    'fk_admin' => ($this->user_type == 'ADMIN') ? $this->user_id : NULL,
                    'fk_account_manager' => ($this->user_type == 'KEY_ACCOUNT_MANAGER') ? $this->user_id : NULL,
                );
                if (empty($data['noteId'])) {
                    $noteData['created_at']=date('Y-m-d H:i:s');
                    $result = $this->db->insert("user_has_notes", $noteData);
                    $json_response['message'] = 'Note added Successfully!..';
                } else {
                    $this->db->where('id', $data['noteId']);
                    $result = $this->db->update("user_has_notes", $noteData);
                    $json_response['message'] = 'Updated';
                }
                if ($result) {
                    $json_response['status'] = 'success';
                    $json_response['jscode'] = " setTimeout(function () { $('#add_notes_model').modal('hide');usernotesDatatables.refresh(); }, 100);";
                } else {
                    $json_response['status'] = 'error';
                    $json_response['message'] = 'Something will be wrong';
                }
            }
        } else {
            $json_response['message'] = validation_errors();
            $json_response['status'] = 'warning';
        }

        return $json_response;
    }

    function deleteUserNote($data, $json_response)
    {
        $result = $this->toval->id_delete($data['id'], 'user_has_notes', 'id');
        if ($result > 0) {
            $json_response['status'] = 'success';
            $json_response['message'] = 'User Note Deleted Successfully!..';
            $json_response['reload'] = 'true';
        } else {
            $json_response['status'] = 'error';
            $json_response['message'] = 'Something will be wrong';
        }
        return $json_response;
    }

    function deleteClientNote($data, $json_response)
    {

        $result = $this->toval->id_delete($data['id'], 'client_has_notes', 'id');
        if ($result > 0) {
            $json_response['status'] = 'success';
            $json_response['message'] = 'Client Note Deleted Successfully!..';
            $json_response['jscode'] = " setTimeout(function () { $('#delete_model').modal('hide');notesDatatables.refresh(); }, 100);";
        } else {
            $json_response['status'] = 'error';
            $json_response['message'] = 'Something will be wrong';
        }
        return $json_response;
    }

    function updateOfficer($data, $json_response)
    {
        $config = array(
            array('field' => 'name', 'label' => 'Name', 'rules' => 'required'),
            array('field' => 'email', 'label' => 'Email', 'rules' => 'required'),
            array('field' => 'phone', 'label' => 'Phone', 'rules' => 'required'),
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE) {
            $adminId = NULL;
            $accountId = NULL;

            if ($this->user_type === 'ADMIN') {
                $adminId = $this->user_id;
            } else if ($this->user_type === 'KEY_ACCOUNT_MANAGER') {
                $accountId = $this->user_id;
            }

            $officer_array = array(
                'fk_client' => $data['fk_client'],
                'fk_admin' => $adminId,
                'var_email' => $data['email'],
                'bint_phone' => $data['phone'],
                'var_name' => $data['name'],
                'created_at' => date('Y-m-d H:i:s'),
            );

            if ($data['officer_id'] == '') {
                $result = $this->db->insert('client_has_compliance_officer', $officer_array);
                if ($result) {
                    $json_response['status'] = 'success';
                    $json_response['message'] = 'Officer added Successfully!..';
                    $json_response['reload'] = 'true';
                } else {
                    $json_response['status'] = 'error';
                    $json_response['message'] = 'Something will be wrong';
                }
            } else {
                $this->db->where('id', $data['officer_id']);
                $result = $this->db->update('client_has_compliance_officer', $officer_array);
                if ($result) {
                    $json_response['status'] = 'success';
                    $json_response['message'] = 'Updated';
                    $json_response['reload'] = 'true';
                } else {
                    $json_response['status'] = 'error';
                    $json_response['message'] = 'Something will be wrong';
                }
            }
        } else {
            $json_response['message'] = validation_errors();
            $json_response['status'] = 'warning';
        }
        return $json_response;
    }

    function getRules()
    {
        $query = $this->db->where('enum_enable', 'NO')->get('rules')->result_array();
        return $query;
    }

    function getClientRules($clientId)
    {
        $this->db->select('client_has_rules.id as rule_id,client_has_rules.fk_client as clientid,rules.var_name');
        $this->db->join('rules', 'rules.id = client_has_rules.fk_rules');
        $this->db->where('client_has_rules.fk_client', $clientId);
        $query = $this->db->get('client_has_rules')->result_array();
        return $query;
    }

    function addRules($data, $json_response)
    {

        if ($data['optionvlaue'] != '' || $data['textvalue'] != '') {
            if ($data['textvalue'] != '') {

                $chekRules = $this->db->get_where('rules', array('var_name' => $data['textvalue']))->row_array();
                if (empty($chekRules)) {
                    $rules_array = array(
                        'var_name' => $data['textvalue'],
                        'txt_description' => $data['textvalue'],
                        'enum_added_by_client' => 'YES',
                        'enum_enable' => 'YES',
                        'created_at' => date('Y-m-d'),
                    );

                    $this->db->insert('rules', $rules_array);
                    $last_id = $this->db->insert_id();
                } else {
                    $last_id = $chekRules['id'];
                }
            }

            $rule_id = ($data['optionvlaue']) ? $data['optionvlaue'] : $last_id;


            $client_rules = array(
                'fk_client' => $data['clientid'],
                'fk_rules' => $rule_id,
                'fk_admin' => ($this->user_type === 'ADMIN') ? $this->user_id : NULL,
                'fk_account_manager' => ($this->user_type === 'KEY_ACCOUNT_MANAGER') ? $this->user_id : NULL,
                'created_at' => date('Y-m-d'),
            );
            $result = $this->db->insert('client_has_rules', $client_rules);
            if ($result) {
                $json_response['status'] = 'success';
                $json_response['message'] = 'Rules added Successfully!..';
                $json_response['reload'] = 'true';
            } else {
                $json_response['status'] = 'error';
                $json_response['message'] = 'Something will be wrong';
            }

            return $json_response;
        }
    }

    function deleteClientRules($data, $json_response)
    {

        $result = $this->toval->id_delete($data['id'], 'client_has_rules', 'id');
        if ($result > 0) {
            $json_response['status'] = 'success';
            $json_response['message'] = 'Client Rule Deleted Successfully!..';
            $json_response['reload'] = 'true';
        } else {
            $json_response['status'] = 'error';
            $json_response['message'] = 'Something will be wrong';
        }
        return $json_response;
    }

    function addSubscription($data, $json_response)
    {
        if ($data['subscription_type'] == 'PAY-AS-YOU-GO') {
            $config = array(
                array('field' => 'subscription_type', 'label' => 'Subscription Type', 'rules' => 'required'),
                array('field' => 'dt_start_date', 'label' => 'Subscription Start Date', 'rules' => 'required'),
                array('field' => 'dt_end_date', 'label' => 'Subscription End', 'rules' => 'required'),
                array('field' => 'price_per_unit', 'label' => 'Price Per Unit', 'rules' => 'required')
            );

        } else {
            $config = array(
                array('field' => 'subscription_type', 'label' => 'Subscription Type', 'rules' => 'required'),
                array('field' => 'dt_start_date', 'label' => 'Subscription Start Date', 'rules' => 'required'),
                array('field' => 'subcription_time', 'label' => 'Subscription Time', 'rules' => 'required'),
                array('field' => 'no_of_unit', 'label' => 'Number Of Unit', 'rules' => 'required'),
                array('field' => 'price_per_unit', 'label' => 'Price Per Unit', 'rules' => 'required')
            );
        }

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE) {
            $startDate = DateTime::createFromFormat('d/m/Y', $data['dt_start_date'])->format('Y-m-d');
            $endDate = (!empty($data['dt_end_date']) ? DateTime::createFromFormat('d/m/Y', $data['dt_end_date'])->format('Y-m-d') : (date('Y-m-d', strtotime("+" . $data['subcription_time'] . " months", strtotime($startDate)))));
            $query = $this->db->get_where('client_has_subscription', array('fk_client' => $data['client_id'], 'enum_enable' => 'YES'));
            $checkSubscription = $query->num_rows();

            if ($checkSubscription == '0') {
                $this->toval->id_delete($data['client_id'], 'client_has_subscription', 'fk_client');
                $subscriptionArray = array(
                    'fk_client' => $data['client_id'],
                    'fk_admin' => ($this->user_type == 'ADMIN') ? $this->user_id : NULL,
                    'fk_account_manager' => ($this->user_type == 'KEY_ACCOUNT_MANAGER') ? $this->user_id : NULL,
                    'dt_start_date' => $startDate,
                    'dt_end_date' => $endDate,
                    'int_subscription_time' => ($data['subcription_time']) ? $data['subcription_time'] : 0,
                    'int_no_of_unit' => ($data['no_of_unit']) ? $data['no_of_unit'] : 0,
                    'int_price_per_unit' => $data['price_per_unit'],
                    'int_total_price' => ($data['no_of_unit']) ? ($data['no_of_unit'] * $data['price_per_unit']) : $data['price_per_unit'],
                    'int_calls_remains' => ($data['no_of_unit']) ? $data['no_of_unit'] : 0,
                    'int_calls_completed' => 0,
                    'enum_subscription_type' => $data['subscription_type'],
                    'enum_pace_status' => ($data['subscription_type'] == "Unit Block") ? 'AHEAD' : 'NO-PACE',
                    'created_at' => date('Y-m-d H:i:s'),
                );
                $result = $this->db->insert('client_has_subscription', $subscriptionArray);
                /*
                 * $clientData = $this->toval->idtorowarr('id', $data['client_id'], 'client');
                 * Send email on successfull subscription
                 *
                 * $mail_body = '<p style="margin:8px 0"> Thanks for subscription!</p>';
                 * $this->load->library('Mylibrary');
                 * $configs['to'] = $clientData['var_email'];
                 * $configs['subject'] = 'Client Subscription';
                 * $configs['mail_body'] = $mail_body;
                 * $sendMail = $this->mylibrary->sendMail($configs);
                 */

                if ($result) {
                    $json_response['status'] = 'success';
                    $json_response['message'] = 'Subscription added Successfully!..';
                    $json_response['reload'] = 'true';
                } else {
                    $json_response['status'] = 'error';
                    $json_response['message'] = 'Something will be wrong';
                }

            } else {
                $json_response['status'] = 'error';
                $json_response['message'] = 'Subscription already Exist!..';
                $json_response['reload'] = 'true';
            }


        } else {
            $json_response['message'] = validation_errors();
            $json_response['status'] = 'warning';
        }
        return $json_response;
    }


    function userResetpassword($data, $json_response)
    {
        $user_email = $this->db->get_where('user',array('id'=>$data['id']))->result_array();

        $id = $data['id'];
        $url = base_url() . 'user_reset_password/reset_password/' . base64_encode($id);
        $mail_body = '<p style="margin:8px 0"> Please click on below link to reset password</p>';
        $mail_body .= 'Link :-' . $url;
        $this->load->library('Mylibrary');
        $configs['to'] = $user_email[0]['var_email'];
        $configs['subject'] = 'User reset password';
        $configs['mail_body'] = $mail_body;
        $sendMail = $this->mylibrary->sendMail($configs);

        if ($sendMail) {
            $json_response['status'] = 'success';
            $json_response['message'] = 'Mail Sent Successfully User!..';
//            $json_response['reload'] = 'true';
        } else {
            $json_response['status'] = 'error';
            $json_response['message'] = 'Something will be wrong';
        }

        return $json_response;
    }
}
