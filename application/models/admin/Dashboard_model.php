<?php

class Dashboard_model extends CI_Model
{
    function getProjectDatatables()
    {
        $start_date = date('Y-m-d');
        $end_date = date('Y-m-d', strtotime("+10 day", strtotime($start_date)));
        $this->datatables->select('client_has_project.var_project_name as projectname,
                                   client.var_company_name as companyname,
                                   CONCAT(' . TABLE_PREFIX . 'user.var_fname," ",' . TABLE_PREFIX . 'user.var_lname) as username,
                                   client.enum_value as client_status,
                                   DATE_FORMAT(' . TABLE_PREFIX . 'client_has_project.dt_initiate, \'%d/%m/%Y\') AS initiate, 
                                   DATE_FORMAT(' . TABLE_PREFIX . 'client_has_project.dt_deadline, \'%d/%m/%Y\') AS deadline,
                                   2 as scheduling,
                                   3 as scheduled,
                                   4 as completed,
                                   client_has_project.id as project_id,
                                   client_has_project.fk_client as client_id,
                                   client_has_project.enum_status,
                                   ');
        $this->datatables->from('client_has_project');
        $this->datatables->join('user', 'client_has_project.fk_user = user.id');
        $this->datatables->join('client', 'client_has_project.fk_client = client.id');
        $this->datatables->where('client_has_project.enum_status !=', 'CLOSED');
        $this->datatables->where('client_has_project.dt_deadline BETWEEN "' . date('Y-m-d', strtotime($start_date)) . '" and "' . date('Y-m-d', strtotime($end_date)) . '"', NULL);
        $this->datatables->where('client_has_project.enum_status !=', 'CLOSED');
        $this->datatables->edit_column('companyname', '<a href="' . admin_url('client/overview') . '/$1">$2</a>', 'client_id,companyname');
        $this->datatables->edit_column('projectname', '<a href="' . admin_url('project/overview') . '/$1">$2</a>', 'project_id,projectname');
        $result = $this->datatables->generate();
        $records = (array)json_decode($result);
        for ($i = 0; $i < count($records['data']); $i++) {
            $records["data"][$i][3] = clientLable($records['data'][$i][3]);
            $records["data"][$i][9] = projectLable($records['data'][$i][11]);
        }
        return $records;
    }

}
