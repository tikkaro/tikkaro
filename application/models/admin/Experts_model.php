<?php

class Experts_model extends CI_Model
{

    function getExpertsList()
    {
        $this->datatables->select('experts.var_fname as fname,'
            . 'experts.var_rate_per_hour as rateperhour,'
            . 'experts.var_rate_per_survey as ratepersurvey,'
            . 'experts.var_country,'
            . 'experts.id as experts_id,experts.enum_block,
             experts.var_lname as lname');
        $this->datatables->from('experts');

        $this->datatables->edit_column('fname', '<a href="' . admin_url('experts/bio') . '/$1">$2 $3</a>', 'experts_id,fname,lname');
        //$this->datatables->edit_column('rateperhour', '$1 USD', 'rateperhour');
        // $this->datatables->edit_column('ratepersurvey', '$1 USD', 'ratepersurvey');

        $result = $this->datatables->generate();
        $records = (array)json_decode($result);
//        print_r($records); exit();
        for ($i = 0; $i < count($records['data']); $i++) {

            if ($records['data'][$i][1] != '') {
                $records['data'][$i][1] = $records['data'][$i][1] . ' USD';
            } else {
                $records['data'][$i][1] = '';
            }

            if ($records['data'][$i][2] != '') {
                $records['data'][$i][2] = $records['data'][$i][2] . ' USD';
            } else {
                $records['data'][$i][2] = '';
            }
            $id = $records['data'][$i][4];

            $blockStatus = $records['data'][$i][5];

            if ($blockStatus == 'YES') {
                $str = '<a href="javascript:;" data-id=' . $id . '  class="btn btn-primary btn-xs unblock"> Unblock </a>';
            } else {
                $str = '<a href="javascript:;" data-id=' . $id . '  class="btn btn-danger btn-xs block"> Block </a>';
            }
            $records["data"][$i][4] = $str;
        }
        return $records;
    }

    function getExpertbio($expertId)
    {
        $this->db->select('experts.*,GROUP_CONCAT(' . TABLE_PREFIX . 'master_area.var_area_name) AS  area_of_expert,
        IF((' . TABLE_PREFIX . 'admin.var_name !=""),' . TABLE_PREFIX . 'admin.var_name,IF((' . TABLE_PREFIX . 'account_manager.var_name !=""),' . TABLE_PREFIX . 'account_manager.var_name,"")) as RecruitedBy', TRUE);
        $this->db->join('experts_area', 'experts_area.fk_experts = experts.id', 'left');
        $this->db->join('master_area', 'master_area.id = experts_area.fk_area', 'left');
        $this->db->join('account_manager', 'account_manager.id = experts.fk_account_manager', 'left');
        $this->db->join('admin', 'admin.id = experts.fk_admin', 'left');
        $this->db->group_by('experts_area.fk_experts');
        $this->db->where('experts.id', $expertId);
        $query = $this->db->get('experts')->result_array();
        return $query;
    }

    function getExpertById($expertId)
    {
        $this->db->select('experts.*,
                           experts_has_employment_history.dt_to_respective_date,
                           experts_has_employment_history.enum_present,
                           GROUP_CONCAT(DISTINCT ' . TABLE_PREFIX . 'experts_has_employment_history.var_company) AS  company,
                           experts_has_employment_history.var_position,
                           GROUP_CONCAT(DISTINCT ' . TABLE_PREFIX . 'master_area.var_area_name) AS  area_of_expert', TRUE);
        $this->db->join('experts_has_employment_history', 'experts_has_employment_history.fk_experts = experts.id', 'left');
        $this->db->join('experts_area', 'experts_area.fk_experts = experts.id', 'left');
        $this->db->join('master_area', 'master_area.id = experts_area.fk_area', 'left');
        $this->db->where('experts.id', $expertId);
        $this->db->order_by('experts_has_employment_history.enum_present asc,experts_has_employment_history.dt_to_respective_date desc');
        $this->db->group_by('experts_area.fk_experts');
        $query = $this->db->get('experts')->row_array();
        return $query;
    }

    function getExpertArea($expertId)
    {
        $this->db->select('experts_area.*');
        $this->db->where('experts_area.fk_experts', $expertId);
        $query = $this->db->get('experts_area')->result_array();

        return $query;
    }

    function getExpertEducation($expertId)
    {
        $this->db->select('experts_has_education.var_colleges,'
            . 'experts_has_education.var_certificates,'
            . 'experts_has_education.dt_from_respective_date,'
            . 'experts_has_education.dt_to_respective_date,'
            . 'experts_has_education.id as id');
        $this->db->from('experts_has_education');
        $this->db->where('experts_has_education.fk_experts', $expertId);
        $result = $this->db->get()->result_array();

        return $result;
    }


    function getExpertEmployment($expertId)
    {

        $this->db->select('experts_has_employment_history.var_company,'
            . 'experts_has_employment_history.var_position,'
            . 'experts_has_employment_history.var_geography,'
            . 'experts_has_employment_history.txt_responsibilities,'
            . 'experts_has_employment_history.dt_from_respective_date,'
            . 'experts_has_employment_history.dt_to_respective_date,'
            . 'experts_has_employment_history.fk_experts as exp,'
            . 'experts_has_employment_history.enum_present as present,'
            . 'experts_has_employment_history.id as id');
        $this->db->from('experts_has_employment_history');
        $this->db->where('experts_has_employment_history.fk_experts', $expertId);
        $this->db->order_by("ps_experts_has_employment_history.dt_to_respective_date <> 'NULL'", "ps_experts_has_employment_history.dt_to_respective_date DESC");
        $result = $this->db->get()->result_array();
        return $result;
    }

    function addExpert($data, $json_response)
    {
        $config = array(
            array('field' => 'var_fname', 'label' => 'First Name', 'rules' => 'required'),
        );

        $this->form_validation->set_rules($config);

        if ($this->form_validation->run() !== FALSE) {
            $checkExpert = $this->toval->idtorowarr('var_email', $data['var_email'], 'experts');

            if (empty($checkExpert)) {
                $ExpertData = array(
                    'fk_admin' => ($this->user_type == 'ADMIN') ? $this->user_id : NULL,
                    'fk_account_manager' => ($this->user_type == 'KEY_ACCOUNT_MANAGER') ? $this->user_id : NULL,
                    'fk_prospect' => ($data['prospectId']) ? $data['prospectId'] : NULL,
                    'var_rate_per_hour' => $data['var_rateperhours'],
                    'txt_bio' => $data['txt_bio'],
                    'dt_compliance_date' => ($data['compliance_date']) ? DateTime::createFromFormat('d/m/Y', $data['compliance_date'])->format('Y-m-d') : NULL,
                    'created_at' => date('Y-m-d H:i:s'),
                    'var_fname' => ($data['var_fname']) ? $data['var_fname'] : NULL,
                    'var_lname' => ($data['var_lname']) ? $data['var_lname'] : NULL,
                    'var_email' => ($data['var_email']) ? $data['var_email'] : NULL,
                    'var_password' => ($data['var_password']) ? md5($data['var_password']) : NULL,
                    'var_country' => ($data['var_country']) ? $data['var_country'] : NULL,
                    'var_rate_per_survey' => ($data['var_ratepersurvey']) ? $data['var_ratepersurvey'] : NULL,
                );

                $result = $this->db->insert("experts", $ExpertData);
                $expertid = $this->db->insert_id();

                if (!empty($data['var_areaofexpert'])) {
                    $area = explode(',', $data['var_areaofexpert']);
                    for ($i = 0; $i < count($area); $i++) {
                        $areaData = $this->toval->idtorowarr('var_area_name', $area[$i], 'master_area');
                        if (!empty($areaData)) {
                            $areaId = $areaData['id'];
                        } else {

                            $areadata = array(
                                'var_area_name' => $area[$i],
                                'created_at' => date('Y-m-d H:i:s'),
                            );
                            $this->db->insert('master_area', $areadata);
                            $areaId = $this->db->insert_id();
                        }
                        $areaexpert = array(
                            'fk_experts' => $expertid,
                            'fk_area' => $areaId,
                            'created_at' => date('Y-m-d H:i:s'),

                        );
                        $this->db->insert('experts_area', $areaexpert);
                    }
                }

                if (!empty($data['var_college']) && !empty($data['eduction_from']) && !empty($data['eduction_to'])) {

                    for ($i = 0; $i < count($data['var_college']); $i++) {
                        if (!empty($data['var_college'][$i])) {
                            $expertsEducation = array(
                                'fk_experts' => $expertid,
                                'var_colleges' => $data['var_college'][$i],
                                'var_certificates' => $data['var_certificates'][$i],
                                'dt_from_respective_date' => (!empty($data['eduction_from'][$i])) ? DateTime::createFromFormat('d/m/Y', $data['eduction_from'][$i])->format('Y-m-d') : NULL,
                                'dt_to_respective_date' => (!empty($data['eduction_to'][$i])) ? DateTime::createFromFormat('d/m/Y', $data['eduction_to'][$i])->format('Y-m-d') : NULL,
                                'created_at' => date('Y-m-d H:i:s'),
                            );
                            $this->db->insert("experts_has_education", $expertsEducation);
                        }
                    }
                }

                if (!empty($data['var_company'])) {
                    for ($i = 0; $i < count($data['var_company']); $i++) {
                        if (!empty($data['var_company'][$i])) {
                            $expertEmployeement = array(
                                'fk_experts' => $expertid,
                                'var_company' => $data['var_company'][$i],
                                'var_position' => $data['var_position'][$i],
                                'var_geography' => $data['var_geography'][$i],
                                'txt_responsibilities' => $data['txt_resposibility'][$i],
                                'dt_from_respective_date' => (!empty($data['history_from'][$i])) ? DateTime::createFromFormat('d/m/Y', $data['history_from'][$i])->format('Y-m-d') : NULL,
                                'dt_to_respective_date' => ($data['history_to'][$i]) ? DateTime::createFromFormat('d/m/Y', $data['history_to'][$i])->format('Y-m-d') : NULL,
                                'enum_present' => ($data['checkVal'][$i] == '1') ? 'YES' : 'NO',
//                                'enum_present' => ($data['work'][$i]) ? 'YES' : 'NO',
                                'created_at' => date('Y-m-d H:i:s'),
                            );
                            if (!empty($data['history_from'][$i])) {
                                if ($data['checkVal'][$i] == '1') {
                                    $this->db->insert("experts_has_employment_history", $expertEmployeement);
                                } else {
                                    if (!empty($data['history_to'][$i])) {
                                        $this->db->insert("experts_has_employment_history", $expertEmployeement);
                                    }
                                }

                            }

                        }
                    }
                }

                if (!empty($data['projectId'])) {
                    $projectData = $this->db->get_where('ps_client_has_project', array('id' => $data['projectId']))->row_array();
                    $ProspectData = $this->db->get_where('project_has_prospect',array('id' => $data['prospectId']))->result_array();
                    $projectHasExpers = array(
                        'fk_admin' => ($this->user_type == 'ADMIN') ? $this->user_id : NULL,
                        'fk_account_manager' => ($this->user_type == 'KEY_ACCOUNT_MANAGER') ? $this->user_id : NULL,
                        'fk_client' => $projectData['fk_client'],
                        'fk_user' => $projectData['fk_user'],
                        'fk_project' => $data['projectId'],
                        'fk_experts' => $expertid,
                        'fk_angle' => $ProspectData[0]['fk_angle'],
                        'created_at' => date('Y-m-d H:i:s')
                    );
                    $this->db->insert('ps_project_has_experts', $projectHasExpers);
                    $projectHasexpertsNotes = $this->db->insert_id();

                    $Prospect = $this->db->get_where('prospect_has_notes',array('fk_prospect' => $data['prospectId']))->result_array();
                    if(!empty($Prospect)){
                        for($i=0; $i<count($Prospect); $i++){
                            $prospect_has_expert = array(
                                'fk_experts' => $expertid,
                                'fk_client' => $projectData['fk_client'],
                                'fk_user' => $projectData['fk_user'],
                                'fk_project' => $data['projectId'],
                                'fk_project_has_experts' => $projectHasexpertsNotes,
                                'txt_note' => $Prospect[$i]['txt_note'],
                                'fk_admin' => ($this->user_type == 'ADMIN') ? $this->user_id : NULL,
                                'fk_account_manager' => ($this->user_type == 'KEY_ACCOUNT_MANAGER') ? $this->user_id : NULL,
                                'created_at' => date('Y-m-d H:i:s'),
                            );
                            $this->db->insert('project_has_experts_notes',$prospect_has_expert);
                        }
                    }
                }

                if(!empty($data['prospectId'])){
                    $prospectUpdate = array(
                        'enum_enable' => 'NO',
                    );

                    $this->db->where('id',$data['prospectId']);
                    $this->db->update('project_has_prospect',$prospectUpdate);
                }

                if ($result) {
                    $json_response['status'] = 'success';
                    $json_response['message'] = 'Expert added Successfully!..';
                    if (empty($data['projectId'])) {
                        $json_response['redirect'] = admin_url('experts');
                    } else {
                        $json_response['redirect'] = admin_url('project/expert-shortlist/' . $data['projectId']);
                    }

                } else {
                    $json_response['status'] = 'error';
                    $json_response['message'] = 'Something will be wrong';
                }
            } else {
                $json_response['status'] = 'warning';
                $json_response['message'] = 'Email already exist!';
            }
        } else {
            $json_response['message'] = validation_errors();
            $json_response['status'] = 'warning';
        }
        return $json_response;
    }

    function updateTags($data, $json_response)
    {
        $this->db->where('fk_experts', $data['id']);
        $this->db->delete($data['table']);

        if (!empty($data['value'])) {
            $area = explode(',', $data['value']);
            for ($i = 0; $i < count($area); $i++) {
                $area[$i] = trim($area[$i]);
                $areaData = $this->toval->idtorowarr('var_area_name', $area[$i], 'master_area');
                if (!empty($areaData)) {
                    $areaId = $areaData['id'];
                } else {
                    $areadata = array(
                        'var_area_name' => $area[$i],
                        'created_at' => date('Y-m-d H:i:s'),
                    );
                    $this->db->insert('master_area', $areadata);
                    $areaId = $this->db->insert_id();
                }
                $areaexpert = array(
                    'fk_experts' => $data['id'],
                    'fk_area' => $areaId,
                    'created_at' => date('Y-m-d H:i:s'),
                );
                $this->db->insert('experts_area', $areaexpert);
            }
        }

    }

    function addEmployHistory($data, $json_response)
    {
//        print_r($data); exit();
        $config = array(
            array('field' => 'var_company', 'label' => 'Company', 'rules' => 'required'),
            array('field' => 'var_position', 'label' => 'Position', 'rules' => 'required'),
            array('field' => 'var_geography', 'label' => 'Geography', 'rules' => 'required'),
            array('field' => 'history_from', 'label' => 'Date', 'rules' => 'required'),
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE) {
            if (empty($data['work']) && empty($data['history_to'])) {
                $json_response['message'] = "Please select To date !...";
                $json_response['status'] = 'warning';
                return $json_response;
            }
            $expertHistory = array(
                'fk_experts' => $data['expert_id'],
                'var_company' => $data['var_company'],
                'var_position' => $data['var_position'],
                'var_geography' => $data['var_geography'],
                'txt_responsibilities' => $data['txt_resposibility'],
                'dt_from_respective_date' => ($data['history_from']) ? DateTime::createFromFormat('d/m/Y', $data['history_from'])->format('Y-m-d') : NULL,
                'dt_to_respective_date' => ($data['work'] == 'NO' || empty($data['work'])) ? DateTime::createFromFormat('d/m/Y', $data['history_to'])->format('Y-m-d') : NULL,
                'enum_present' => ($data['work']) ? 'YES' : 'NO',
            );
            if (empty($data['employee_history_id'])) {
                $expertHistory['created_at'] = date('Y-m-d H:i:s');
                $result = $this->db->insert('experts_has_employment_history', $expertHistory);
                $json_response['message'] = 'Employment History Added Successfully!..';
//                $json_response['jscode'] = " setTimeout(function () { $('#add_bio_employeement_model').modal('hide');$('#add_employeement_frm')[0].reset();$('.present').hide();$('.to').show();employeementDatatables.refresh(); }, 100);";
                $json_response['reload'] = "true";
            } else {

                $this->db->where('id', $data['employee_history_id']);
                $result = $this->db->update("experts_has_employment_history", $expertHistory);
                $json_response['message'] = 'Updated';
                $json_response['reload'] = "true";
//                $json_response['jscode'] = " setTimeout(function () { $('#edit_bio_employeement_model').modal('hide');  employeementDatatables.refresh(); }, 100);";
            }
            if ($result) {
                $json_response['status'] = 'success';

            } else {
                $json_response['status'] = 'error';
                $json_response['message'] = 'Something will be wrong !..';
            }
        } else {
            $json_response['message'] = validation_errors();
            $json_response['status'] = 'warning';
        }
        return $json_response;
    }

    function addEducationHistory($data, $json_response)
    {


        $config = array(
            array('field' => 'var_college', 'label' => 'College', 'rules' => 'required'),
            array('field' => 'var_certificates', 'label' => 'Certificate', 'rules' => 'required'),
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE) {

            $expertEduHistory = array(
                'fk_experts' => $data['expert_id'],
                'var_colleges' => $data['var_college'],
                'var_certificates' => $data['var_certificates'],
                'dt_from_respective_date' => (!empty($data['from'])) ? DateTime::createFromFormat('d/m/Y', $data['from'])->format('Y-m-d') : NULL,
                'dt_to_respective_date' => (!empty($data['to'])) ? DateTime::createFromFormat('d/m/Y', $data['to'])->format('Y-m-d') : NULL,

            );

            if (empty($data['edu_id'])) {
                $expertEduHistory['created_at'] = date('Y-m-d H:i:s');
                $result = $this->db->insert('experts_has_education', $expertEduHistory);
                $json_response['message'] = 'Expert Education Added Successfully!..';
            } else {
                $this->db->where('id', $data['edu_id']);
                $result = $this->db->update("experts_has_education", $expertEduHistory);
                $json_response['message'] = 'Updated';
            }
            if ($result) {
                $json_response['status'] = 'success';
                $json_response['reload'] = "true";
            } else {
                $json_response['status'] = 'error';
                $json_response['message'] = 'Something will be wrong !..';
            }
        } else {
            $json_response['message'] = validation_errors();
            $json_response['status'] = 'warning';
        }
        return $json_response;
    }

    function getNotesDatatables($expertId)
    {
        $this->datatables->select('DATE_FORMAT(' . TABLE_PREFIX . 'experts_has_notes.updated_at, \'%d/%m/%Y\') AS notetDate,
                                   IF((' . TABLE_PREFIX . 'admin.var_name !=""),' . TABLE_PREFIX . 'admin.var_name,IF((' . TABLE_PREFIX . 'account_manager.var_name !=""),' . TABLE_PREFIX . 'account_manager.var_name,"")) as noteBy,       
                                   experts_has_notes.txt_note,
                                   "1" as action,
                                   experts_has_notes.id,
                                   experts_has_notes.fk_experts');
        $this->datatables->from('experts_has_notes');
        $this->datatables->join('account_manager', 'account_manager.id = experts_has_notes.fk_account_manager', 'left');
        $this->datatables->join('admin', 'admin.id = experts_has_notes.fk_admin', 'left');
        if ($expertId !== NULL && !empty($expertId)) {
            $this->datatables->where('experts_has_notes.fk_experts', $expertId);
        }
        $this->db->order_by('experts_has_notes.id', 'DESC');
        $this->datatables->edit_column('action', '<a href="javascript:;" note-id="$1" expert-id="$2" data-toggle ="modal" class="btn btn-primary btn-xs expert-notes"><i class="fa fa-eye"></i> Edit </a>  <a title="Delete" class="btn blue btn-xs common_delete" data-href="' . admin_url('experts/deleteExpertNote') . '" data-id="$1" data-toggle="modal" href="#delete_model"><i class="fa fa-trash"></i> Delete</a>', 'id,fk_experts');
        $result = $this->datatables->generate();
        return $result;
    }

    function addExpertNotes($data, $json_response)
    {
        $config = array(
            array('field' => 'notes', 'label' => 'Note', 'rules' => 'required'),
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE) {
            if (!empty($data['notes'])) {
                $noteData = array(
                    'txt_note' => $data['notes'],
                    'fk_experts' => $data['expertId'],
                    'fk_admin' => ($this->user_type == 'ADMIN') ? $this->user_id : NULL,
                    'fk_account_manager' => ($this->user_type == 'KEY_ACCOUNT_MANAGER') ? $this->user_id : NULL,
                );
                if (empty($data['note_id'])) {
                    $noteData['created_at'] = date('Y-m-d H:i:s');
                    $result = $this->db->insert("experts_has_notes", $noteData);
                    $json_response['message'] = 'Note added Successfully!..';
                } else {
                    $this->db->where('id', $data['note_id']);
                    $result = $this->db->update("experts_has_notes", $noteData);
                    $json_response['message'] = 'Updated';
                }
                if ($result) {
                    $json_response['status'] = 'success';
                    $json_response['jscode'] = " setTimeout(function () { $('#edit_notes_model').modal('hide');notesDatatables.refresh(); }, 100);";
                } else {
                    $json_response['status'] = 'error';
                    $json_response['message'] = 'Something will be wrong';
                }
            } else {
                $json_response['message'] = validation_errors();
                $json_response['status'] = 'warning';
            }
            return $json_response;
        }
    }

    function deleteExpertNote($data, $json_response)
    {
        $result = $this->toval->id_delete($data['id'], 'experts_has_notes', 'id');
        if ($result > 0) {
            $json_response['status'] = 'success';
            $json_response['message'] = 'Note Deleted Successfully!..';
            $json_response['jscode'] = " setTimeout(function () { $('#delete_model').modal('hide');notesDatatables.refresh(); }, 100);";
        } else {
            $json_response['status'] = 'error';
            $json_response['message'] = 'Something will be wrong';
        }
        return $json_response;
    }

    function getProjectList($expertsId)
    {
        $this->datatables->select('client_has_project.var_project_name,
                                   client_has_project.txt_description,
                                   client.var_company_name,
                                   client.enum_value,
                                   DATE_FORMAT(' . TABLE_PREFIX . 'client_has_project.dt_initiate, \'%d/%m/%Y\') AS initiate, 
                                   DATE_FORMAT(' . TABLE_PREFIX . 'client_has_project.dt_deadline, \'%d/%m/%Y\') AS deadline,
                                   4 as completed');
        $this->datatables->from('project_has_experts');
        $this->datatables->join('client', 'client.id = project_has_experts.fk_client');
        $this->datatables->join('client_has_project', 'client_has_project.id = project_has_experts.fk_project');
        $this->datatables->where('project_has_experts.fk_experts', $expertsId);
        $this->datatables->where('project_has_experts.enum_status', 'BIOS_SENT');
        $result = $this->datatables->generate();
        $records = (array)json_decode($result);
        for ($i = 0; $i < count($records['data']); $i++) {
            $records["data"][$i][2] = shortenText($records['data'][$i][2], '100');
            $records["data"][$i][3] = clientLable($records['data'][$i][3]);
        }
        return $records;
    }

    function getCallHistoryList()
    {

        $iTotalRecords = 5;
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;
        $j = 1;
        $bill = array('Yes', 'No');
        $billLable = array('label-danger', 'label-success');
        for ($i = $iDisplayStart; $i < $end; $i++) {
            $index = array_rand($bill);
            $records["data"][$i][0] = date('d/m/Y');
            $records["data"][$i][1] = date('H:i');
//            $records["data"][$i][2] = 'Client' . $j;
            $records["data"][$i][2] = "<a href='" . admin_url() . 'client/overview/' . $j . "'>Client $j</a>";;
            $records["data"][$i][3] = 'User' . $j;
            $records["data"][$i][4] = "<a href='" . admin_url() . 'project/overview/' . $j . "'>Project" . $j . "</a>";
            $records["data"][$i][5] = '1hr';
            $records["data"][$i][6] = '<span class="label label-success label-sm "> Completed </span>';
            $records["data"][$i][7] = '<span class="label ' . $billLable[$index] . ' label-sm "> ' . $bill[$index] . ' </span>';
            $records["data"][$i][8] = '<a href="javascript:;" data-toggle ="modal" class="btn btn-primary btn-xs edit">PAYMENT REQUESTED</a>';
            $j++;
        }

        $records["sEcho"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return $records;
    }

    function espertBlock($data, $json_response)
    {
        $config = array(
            array('field' => 'txt_reason', 'label' => 'Reason', 'rules' => 'required'),
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE) {

            $expertHistory = array(
                'fk_experts' => $data['expertId'],
                'txt_reason' => $data['txt_reason'],
                'dt_from_block_date' => ($data['from']) ? DateTime::createFromFormat('d/m/Y', $data['from'])->format('Y-m-d') : NULL,
                'dt_to_block_date' => ($data['to']) ? DateTime::createFromFormat('d/m/Y', $data['to'])->format('Y-m-d') : NULL,
                'enum_permanent' => ($data['block']) ? 'YES' : 'NO',
                'created_at' => date('Y-m-d H:i:s'),
            );
            $this->db->insert('experts_block', $expertHistory);

            $data_array = array(
                'enum_block' => 'YES'
            );

            $this->db->where('id', $data['expertId']);
            $result = $this->db->update('experts', $data_array);
            if ($result) {
                $json_response['status'] = 'success';
                $json_response['message'] = 'Expert Block Successfully!..';
                $json_response['reload'] = 'true';
            } else {
                $json_response['status'] = 'error';
                $json_response['message'] = 'Something will be wrong !..';
            }
        } else {
            $json_response['message'] = validation_errors();
            $json_response['status'] = 'warning';
        }
        return $json_response;
    }

    function expertUnblock($data, $json_response)
    {
        $data_array = array(
            'enum_block' => 'NO',
        );
        $this->db->where('id', $data['id']);
        $this->db->update('experts', $data_array);

        $result = $this->toval->id_delete($data['id'], 'experts_block', 'fk_experts');
        if ($result > 0) {
            $json_response['status'] = 'success';
            $json_response['message'] = 'Experts Unblock Successfully!..';
            $json_response['reload'] = 'true';
        } else {
            $json_response['status'] = 'error';
            $json_response['message'] = 'Something will be wrong';
        }
        return $json_response;
    }


    function updateDates($data,$json_response){
        if($data['value'] != ''){
            $value = DateTime::createFromFormat('d/m/Y',$data['value'])->format('Y-m-d');
        }else{
            $value = NULL;
        }

        $date_array = array(
            'dt_compliance_date' => $value,
        );

        $this->db->where('id',$data['id']);
        $this->db->update($data['table'],$date_array);
    }

    function resetPassword($data,$json_response){

        $experts_email = $this->db->get_where('experts',array('id'=>$data['id']))->result_array();

        $id = $data['id'];
        $url = base_url() . 'experts_reset_password/reset_password/' . base64_encode($id);
        $mail_body = '<p style="margin:8px 0"> Please click on below link to reset password</p>';
        $mail_body .= 'Link :-' . $url;
        $this->load->library('Mylibrary');
        $configs['to'] = $experts_email[0]['var_email'];
        $configs['subject'] = 'Experts reset password';
        $configs['mail_body'] = $mail_body;
        $sendMail = $this->mylibrary->sendMail($configs);

        if ($sendMail) {
            $json_response['status'] = 'success';
            $json_response['message'] = 'Mail Sent Successfully User!..';
            $json_response['jscode'] = '$("#experts_reset_password").modal("hide")';
        } else {
            $json_response['status'] = 'error';
            $json_response['message'] = 'Something will be wrong';
        }

        return $json_response;
    }
}
