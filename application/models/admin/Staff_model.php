<?php

class Staff_model extends CI_Model
{

    function getAccountManagerList()
    {
        $this->datatables->select('var_name,var_email,id,fk_admin');
        $this->datatables->from('account_manager');
        $this->datatables->edit_column('id', '<a title="Edit" class="btn btn-primary btn-xs edit" href="' . admin_url('staff/edit/') . '$1"><i class="fa fa-eye"></i> Edit </a> <a title="Delete" class="btn blue btn-xs common_delete" data-href="' . admin_url('staff/delete') . '" data-id="$1" data-toggle="modal" href="#delete_model"><i class="fa fa-trash"></i> Delete </a>', 'id');
        return $this->datatables->generate();
    }

    function add($data, $json_response)
    {

        $config = array(
            array('field' => 'var_usrname', 'label' => 'User Name', 'rules' => 'required'),
            array('field' => 'var_email', 'label' => 'Email', 'rules' => 'required'),
//            array('field' => 'var_address', 'label' => 'Address', 'rules' => 'required'),
        );

        if (empty($data['accountId'])) {
            array_push($config, array('field' => 'var_password', 'label' => 'Password', 'rules' => 'required'));
        }

        $this->form_validation->set_rules($config);


        if ($this->form_validation->run() !== FALSE) {


//            if(empty($data['accountId'])){
            $checkAccount = $this->db->get_where('account_manager', array('var_email' => $data['var_email']))->num_rows();
//            }else{
            $checkAccountEdit = $this->db->get_where('account_manager', array('id' => $data['accountId'], 'var_email' => $data['var_email']))->num_rows();
//            }
//            echo $checkAccount; exit();

            if ($checkAccount == 0 || $checkAccountEdit == 1) {

                $var_image = ($data['var_profile_image']) ? $data['var_profile_image'] : '';

                if ($_FILES['profile_pic']['name'] != "") {
                    $image = upload_single_image($_FILES, 'account_manager', KEY_ACCOUNT_PROFILE_IMG, FALSE);
                    $var_image = ($image['error'] == '') ? $image['data']['orig_name'] : '';

                    if ($data['var_profile_image'] != '') {
                        delete_single_image(KEY_ACCOUNT_PROFILE_IMG, $data['var_profile_image']);
                    }
                }

                $AccountData = array(
                    'fk_admin' => $this->user_id,
                    'var_name' => $data['var_usrname'],
                    'var_email' => $data['var_email'],
                    'fk_team' => ($data['team']) ? $data['team'] : NULL,
                    'txt_address' => ($data['var_address']) ? $data['var_address'] : NULL,
                    'bint_phone' => ($data['var_phone']) ? $data['var_phone'] : NULL,
                    'dt_dob' => ($data['var_manage_dob']) ? date('Y-m-d', strtotime($data['var_manage_dob'])) : NULL,
                    'enum_enable' => 'YES',
                    'var_profile_image' => $var_image,
                    'var_register_ip' => $this->input->ip_address(),
                );
                if (trim($data['var_password']) != '') {
                    $password = array(
                        'var_password' => md5($data['var_password']),
                    );
                    $AccountData = array_merge($AccountData, $password);
                }

                if (empty($data['accountId'])) {
                    $AccountData['created_at']= date('Y-m-d H:i:s');
                    $this->db->insert("account_manager", $AccountData);
                    $lastid = $this->db->insert_id();
                    if ($lastid > 0) {
                        $json_response['status'] = 'success';
                        $json_response['message'] = 'Key account manager added Successfully!..';
                        $json_response['redirect'] = admin_url('staff');
                    } else {
                        $json_response['status'] = 'error';
                        $json_response['message'] = 'Something will be wrong';
                    }
                } else {
                    $this->db->where('id', $data['accountId']);
                    $this->db->update("account_manager", $AccountData);

                    $json_response['status'] = 'success';
                    $json_response['message'] = 'Updated';
                    $json_response['redirect'] = admin_url('staff');
                }
            } else {
                $json_response['message'] = 'Email alredy exist!..';
                $json_response['status'] = 'warning';
            }
        } else {
            $json_response['message'] = validation_errors();
            $json_response['status'] = 'warning';
        }
        return $json_response;
    }

    function delete($data, $json_response)
    {
        $result = $this->toval->id_delete($data['id'], 'account_manager', 'id');
        if ($result > 0) {
            $json_response['status'] = 'success';
            $json_response['message'] = 'Key Account manager Deleted Successfully!..';
            $json_response['jscode'] = " setTimeout(function () { $('#delete_model').modal('hide');accountmanagerDatatables.refresh(); }, 100);";
        } else {
            $json_response['status'] = 'error';
            $json_response['message'] = 'Something will be wrong';
        }
        return $json_response;
    }

}
