<?php

class Comman_editable extends CI_Model {

    function inlineEdit($data) {

        $field = $data['field'];
        $table = $data['table'];
        $value = $data['value'];
        $id = $data['id'];

        if ($data['type'] == 'date') {
            $value = DateTime::createFromFormat('d/m/Y',$value)->format('Y-m-d');
        }
        if (!empty($data['id'])) {
            $data_array = array(
                $field => $value,
            );
            $this->db->where('id', $id);
            $this->db->update($table, $data_array);
        }
        return TRUE;
    }

    function inlineEditFnameLname($data) {
        $varfname = $data['field1'];
        $varlname = $data['field2'];
        $table = $data['table'];
        $id = $data['id'];
        $value = explode(' ', $data['value']);
        $fname = $value[0];
        $lname = $value[1];

        if (!empty($data['id'])) {
            $data_array = array(
                $varfname => $fname,
                $varlname => ($lname) ? ($lname) : '',
            );
            $this->db->where('id', $id);
            $this->db->update($table, $data_array);
        }
        return TRUE;
    }

    function inlineEditSelect($data) {
        $table = $data['table'];
        $id = $data['id'];

        if (!empty($data['id'])) {
            $data_array = array(
                $data['field1'] => $data['value'],
                $data['field2'] => $data['value'],
            );
            $this->db->where('id', $id);
            $this->db->update($table, $data_array);
        }
        return TRUE;
    }

    function inlineCompliance($data){
        $checkOfficer = $this->db->get_where($data['table'],array($data['field1'] => $data['client']))->row_array();

        if(empty($checkOfficer)){
            $data_array = array(
               $data['field'] => $data['value'],
               $data['field1'] => $data['client'],
               'created_at' => date('Y-m-d H:i:s'),
               'fk_admin' => ($this->user_type == 'ADMIN')?$this->user_id : NULL,
               'fk_account_manager' => ($this->user_type == 'KEY_ACCOUNT_MANAGER')?$this->user_id : NULL,
            );
            $this->db->insert($data['table'],$data_array);
        }else{
            $data_array = array(
                $data['field'] => $data['value'],
            );
            $this->db->where($data['field1'],$data['client']);
            $this->db->update($data['table'],$data_array);
        }
        return TRUE;
    }

}
