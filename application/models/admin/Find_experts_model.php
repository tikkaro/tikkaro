<?php

class Find_experts_model extends CI_Model
{

    function getClient()
    {
        $result = $this->db->get('client')->result_array();
        return $result;
    }

    function clientProject($data)
    {
        $result = $this->db->get_where('client_has_project', array('fk_client' => $data))->result_array();
        return $result;
    }

    function expertShortlist($data)
    {
        if ($data['client_id'] && $data['project_id']) {
            $checkExperts = $this->db->select('fk_experts')
                ->get_where('project_has_experts', array('fk_client' => $data['client_id'], 'fk_project' => $data['project_id']))
                ->result_array();

            if (!empty($checkExperts)) {
                for ($i = 0; $i < count($checkExperts); $i++) {
                    $checkExpert[] = $checkExperts[$i]['fk_experts'];
                }
                $this->db->where_not_in('experts.id', $checkExpert);
            }
        }
        $this->db->select('experts.id,experts.var_fname,experts.var_lname,experts.txt_bio,
                           currentCompany.var_company as currentCompanyName,
                           currentCompany.var_position as currentPosition,
                           currentCompany.var_geography as currentGeography,
                           GROUP_CONCAT(DISTINCT ' . TABLE_PREFIX . 'master_area.var_area_name) AS  area_of_expert', TRUE);
        $this->db->from('experts');
        $this->db->join('experts_has_employment_history', 'experts_has_employment_history.fk_experts = experts.id');
        $this->db->join('experts_has_employment_history as currentCompany', 'currentCompany.fk_experts = experts.id AND currentCompany.enum_present = "YES"','left');
        $this->db->join('experts_area', 'experts_area.fk_experts = experts.id', 'left');
        $this->db->join('master_area', 'master_area.id = experts_area.fk_area', 'left');

        if ($data['position'] != '') {
            $this->db->like('experts_has_employment_history.var_position', $data['position']);
        }

        if ($data['geography'] != '') {
            $this->db->like('experts_has_employment_history.var_geography', $data['geography']);
        }

        if ($data['expertName'] != '') {
            $this->db->group_start();
            $name = explode(" ", $data['expertName']);
            $this->db->like('experts.var_fname', $name[0]);
            $this->db->or_like('experts.var_lname', $name[0]);
            if ($name[1] != '') {
                $this->db->or_like('experts.var_fname', $name[1]);
                $this->db->or_like('experts.var_lname', $name[1]);
            }
            $this->db->group_end();
        }

        if ($data['expretise'] != '') {
            $this->db->like('master_area.var_area_name', $data['expretise']);
        }

        if ($data['currentCompany'] != '' && $data['formerCompany'] != '') {
            $this->db->like('experts_has_employment_history.var_company', $data['currentCompany']);
            $this->db->where('experts_has_employment_history.enum_present', 'YES');
            $this->db->or_like('experts_has_employment_history.var_company', $data['formerCompany']);
            $this->db->where('experts_has_employment_history.enum_present', 'NO');
        } else {
            if ($data['currentCompany'] != '') {
                $this->db->like('experts_has_employment_history.var_company', $data['currentCompany']);
                $this->db->where('experts_has_employment_history.enum_present', 'YES');
            }
            if ($data['formerCompany'] != '') {
                $this->db->like('experts_has_employment_history.var_company', $data['formerCompany']);
                $this->db->where('experts_has_employment_history.enum_present', 'NO');
            }
        }
        $this->db->group_by('experts_has_employment_history.fk_experts');
        $experts = $this->db->get()->result_array();
        return $experts;
    }

    function addtoShortlist($data, $json_response)
    {

        $expertsShortlist = array(
            'fk_admin' => ($this->user_type == 'ADMIN') ? $this->user_id : NULL,
            'fk_account_manager' => ($this->user_type == 'KEY_ACCOUNT_MANAGER') ? $this->user_id : NULL,
            'fk_client' => $data['client_id'],
            'fk_project' => $data['project_id'],
            'fk_user' => $data['user_id'],
            'fk_experts' => $data['experts_id'],
            'created_at' => date('Y-m-d H:i:s'),
        );
        $result = $this->db->insert('project_has_experts', $expertsShortlist);
        if ($result) {
            $json_response['status'] = 'success';
            $json_response['message'] = 'Experts Short Listed Successfully!..';
//            $json_response['jscode'] = '$(\'.experts_search\').trigger(\'click\');';
        } else {
            $json_response['status'] = 'error';
            $json_response['message'] = 'Something will be wrong';
        }
        return $json_response;
    }

}
