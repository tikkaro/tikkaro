<?php

class Project_model extends CI_Model
{

    function getProjectDatatables()
    {
        $postData = $this->input->post();
        $this->datatables->select('client_has_project.var_project_name as projectname,
                                   client.var_company_name as companyname,
                                   CONCAT(' . TABLE_PREFIX . 'user.var_fname," ",' . TABLE_PREFIX . 'user.var_lname) as username,
                                   client.enum_value as client_status,
                                   DATE_FORMAT(' . TABLE_PREFIX . 'client_has_project.dt_initiate, \'%d/%m/%Y\') AS initiate, 
                                   DATE_FORMAT(' . TABLE_PREFIX . 'client_has_project.dt_deadline, \'%d/%m/%Y\') AS deadline,
                                   2 as scheduling,
                                   3 as scheduled,
                                   4 as completed,
                                   client_has_project.id as project_id,
                                   client_has_project.fk_client as client_id,
                                   client_has_project.enum_status,
                                   ');
        $this->datatables->from('client_has_project');
        $this->datatables->join('user', 'client_has_project.fk_user = user.id');
        $this->datatables->join('client', 'client_has_project.fk_client = client.id');
        if ($postData['projectName'] != '') {
            $this->datatables->like('client_has_project.var_project_name', $postData['projectName']);
        }
        if ($postData['clientName'] != '') {
            $this->datatables->like('client.var_company_name', $postData['clientName']);
        }
        if ($postData['clientValue'] != '') {
            $this->datatables->where('client.enum_value', $postData['clientValue']);

        }
        $this->datatables->where('client_has_project.enum_status !=', 'CLOSED');
        $this->datatables->edit_column('companyname', '<a href="' . admin_url('client/overview') . '/$1">$2</a>', 'client_id,companyname');
        $this->datatables->edit_column('projectname', '<a href="' . admin_url('project/overview') . '/$1">$2</a>', 'project_id,projectname');
//        $this->db->order_by('client_has_project.id','DESC');
        $result = $this->datatables->generate();
        $records = (array)json_decode($result);
        for ($i = 0; $i < count($records['data']); $i++) {
            $records["data"][$i][3] = clientLable($records['data'][$i][3]);
            $records["data"][$i][9] = projectLable($records['data'][$i][11]);
        }
        return $records;
    }

    function getProjectCallDatatables()
    {
        $iTotalRecords = 5;
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);

        $records = array();
        $records["data"] = array();

        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;
        $j = 0;
        for ($i = $iDisplayStart; $i < $end; $i++) {

            $j++;
            $records["data"][$i][0] = date('d/m/Y');
            $records["data"][$i][1] = date('H:i');
            $records["data"][$i][2] = "<a href='" . admin_url() . 'experts/bio/' . $j . "'>Expert$j</a>";
            $records["data"][$i][3] = 'User' . $j;
            $records["data"][$i][4] = '1hr';
            $records["data"][$i][5] = 'Completed';
            $records["data"][$i][6] = 'Yes';
        }

        $records["sEcho"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        return $records;
    }

    function getNotesDatatables($projectId = NULL)
    {
        $this->datatables->select('DATE_FORMAT(' . TABLE_PREFIX . 'project_has_notes.updated_at, \'%d/%m/%Y\') AS projectDate,
                                   IF((' . TABLE_PREFIX . 'admin.var_name !=""),' . TABLE_PREFIX . 'admin.var_name,IF((' . TABLE_PREFIX . 'account_manager.var_name !=""),' . TABLE_PREFIX . 'account_manager.var_name,"")) as noteBy,
                                   project_has_notes.txt_note,
                                   "1" as action,
                                   project_has_notes.id,
                                   project_has_notes.fk_project,
                                   project_has_notes.fk_client');
        $this->datatables->from('project_has_notes');
        $this->datatables->join('account_manager', 'account_manager.id = project_has_notes.fk_account_manager', 'left');
        $this->datatables->join('admin', 'admin.id = project_has_notes.fk_admin', 'left');
        if ($projectId !== NULL && !empty($projectId)) {
            $this->datatables->where('project_has_notes.fk_project', $projectId);
        }
        $this->db->order_by('project_has_notes.id', 'DESC');
        $this->datatables->edit_column('action', '<a href="javascript:;" data-note-id="$1" data-project-id="$2" data-client-id="$3"   data-toggle ="modal" class="btn btn-primary btn-xs btn-edit-note"><i class="fa fa-eye"></i> Edit </a>  <a title="Delete" class="btn blue btn-xs common_delete" data-href="' . admin_url('project/deleteProjectNote') . '" data-id="$1" data-toggle="modal" href="#delete_model"><i class="fa fa-trash"></i> Delete</a>', 'id,fk_project,fk_client');
        $result = $this->datatables->generate();
        return $result;
    }

    function getProjectCommanHeader($projectId)
    {
        $data = $this->db->select('client_has_project.*,'
            . 'user.var_fname as user_fname,'
            . 'user.var_lname as user_lname,'
            . 'client.var_company_name,client.enum_value as client_status,'
            . 'team.var_team_name as team_name,'
            . 'account_manager.var_name as account_manager')
            ->join('client', 'client.id = client_has_project.fk_client')
            ->join('account_manager', 'account_manager.id = client.fk_assign_account_manager', 'left')
            ->join('user', 'user.id = client_has_project.fk_user')
            ->join('team', 'team.id = client.fk_assign_team', 'left')
            ->where('client_has_project.id', $projectId)
            ->get('client_has_project')->row_array();
        return $data;
    }

    function getContryData($projectId)
    {
        $this->db->select('GROUP_CONCAT(' . TABLE_PREFIX . 'master_country.var_country_name) as countryname');
        $this->db->join('' . TABLE_PREFIX . 'master_country', '' . TABLE_PREFIX . 'master_country.id = project_has_country.fk_country', 'left');
        $this->db->group_by('project_has_country.fk_project');
        $this->db->where('project_has_country.fk_project', $projectId);
        $country = $this->db->get('project_has_country')->result_array();
        return $country;
    }

    function getCompanyData($projectId)
    {
        $this->db->select('GROUP_CONCAT(' . TABLE_PREFIX . 'master_company.var_company_name) as companyname');
        $this->db->join('' . TABLE_PREFIX . 'master_company', '' . TABLE_PREFIX . 'master_company.id = project_has_company.fk_company', 'left');
        $this->db->group_by('project_has_company.fk_project');
        $this->db->where('project_has_company.fk_project', $projectId);
        $company = $this->db->get('project_has_company')->result_array();

        return $company;
    }

    function getRegionsData($projectId)
    {
        $this->db->select('GROUP_CONCAT(' . TABLE_PREFIX . 'master_regions.var_regions_name) as regionsname');
        $this->db->join('' . TABLE_PREFIX . 'master_regions', '' . TABLE_PREFIX . 'master_regions.id = project_has_regions.fk_regions', 'left');
        $this->db->group_by('project_has_regions.fk_project');
        $this->db->where('project_has_regions.fk_project', $projectId);
        $company = $this->db->get('project_has_regions')->result_array();

        return $company;
    }

    function addProjectNote($data, $json_response)
    {
        $config = array(
            array('field' => 'notes', 'label' => 'Notes', 'rules' => 'required'),
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE) {
            if (!empty($data['notes']) && !empty($data['projectId'])) {
                $noteData = array(
                    'txt_note' => $data['notes'],
                    'fk_client' => $data['clientId'],
                    'fk_project' => $data['projectId'],
                    'fk_admin' => ($this->user_type == 'ADMIN') ? $this->user_id : NULL,
                    'fk_account_manager' => ($this->user_type == 'KEY_ACCOUNT_MANAGER') ? $this->user_id : NULL,
                );
                if (empty($data['noteId'])) {
                    $noteData['created_at'] = date('Y-m-d H:i:s');
                    $result = $this->db->insert("project_has_notes", $noteData);
                    $json_response['message'] = 'Note added Successfully!..';
                } else {
                    $this->db->where('id', $data['noteId']);
                    $result = $this->db->update("project_has_notes", $noteData);
                    $json_response['message'] = 'Updated';
                }
                if ($result) {
                    $json_response['status'] = 'success';
                    $json_response['jscode'] = " setTimeout(function () { $('#add_project_notes_model').modal('hide');projectNotesDatatables.refresh(); }, 100);";
                } else {
                    $json_response['status'] = 'error';
                    $json_response['message'] = 'Something will be wrong';
                }
            }
        } else {
            $json_response['message'] = validation_errors();
            $json_response['status'] = 'warning';
        }

        return $json_response;
    }

    function deleteProjectNote($data, $json_response)
    {
        $result = $this->toval->id_delete($data['id'], 'project_has_notes', 'id');
        if ($result > 0) {
            $json_response['status'] = 'success';
            $json_response['message'] = 'Note Deleted Successfully!..';
            $json_response['jscode'] = " setTimeout(function () { $('#delete_model').modal('hide');projectNotesDatatables.refresh(); }, 100);";
        } else {
            $json_response['status'] = 'error';
            $json_response['message'] = 'Something will be wrong';
        }
        return $json_response;
    }

    function getClientProjectRules($clientId)
    {
        $this->db->select('client_has_rules.id as rule_id,client_has_rules.fk_client as clientid ,rules.var_name');
        $this->db->join('client_has_project', 'ps_client_has_project.fk_client = client_has_rules.fk_client');
        $this->db->join('rules', 'rules.id = client_has_rules.fk_rules');
        $this->db->where('client_has_rules.fk_client', $clientId);
        $this->db->group_by('rules.id');
        $query = $this->db->get('client_has_rules')->result_array();
        return $query;
    }

    /* get prospect with their notes */

    function getProspects($projectId)
    {

                  $this->db->select('project_has_prospect.*,master_angle.var_name as angleName');
                  $this->db->join('master_angle','master_angle.id = project_has_prospect.fk_angle','left');
                  $this->db->where('project_has_prospect.enum_enable','YES');
                  $this->db->where('project_has_prospect.fk_project', $projectId);
                  $this->db->order_by('project_has_prospect.id', 'DESC');
        $result = $this->db->get('project_has_prospect')->result_array();
        for ($i = 0; $i < count($result); $i++) {
            $result[$i]['notes'] = $this->getProspectNotesByProspectId($result[$i]['id']);
        }
        return $result;
    }

    function getProspectNotesByProspectId($prospectId)
    {
        $this->db->select('prospect_has_notes.*,
                           IF((' . TABLE_PREFIX . 'admin.var_name !=""),' . TABLE_PREFIX . 'admin.var_name,IF((' . TABLE_PREFIX . 'account_manager.var_name !=""),' . TABLE_PREFIX . 'account_manager.var_name,"")) as noteBy,
                           account_manager.var_name as ac_manager,
                           admin.var_name as admin');
        $this->db->join('account_manager', 'account_manager.id = prospect_has_notes.fk_account_manager', 'left');
        $this->db->join('admin', 'admin.id = prospect_has_notes.fk_admin', 'left');
        $this->db->order_by('prospect_has_notes.id', 'DESC');
        $notesArray = $this->db->get_where('prospect_has_notes', array('fk_prospect' => $prospectId))->result_array();
        return $notesArray;
    }

    /* add prospect */

    function addProspect($data, $json_response)
    {

        if (!empty($data['prospect_fname']) || !empty($data['prospect_lname']) || !empty($data['compnay_website']) || !empty($data['linkedin_page']) ||
            !empty($data['prospect_email']) || !empty($data['prospect_company']) || !empty($data['prospect_phno'])
        ) {

//        $config = array(
//            array('field' => 'prospect_fname', 'label' => 'First Name', 'rules' => 'required'),
//            array('field' => 'prospect_lname', 'label' => 'Last Name', 'rules' => 'required'),
//            array('field' => 'prospect_email', 'label' => 'Email', 'rules' => 'required'),
//            array('field' => 'prospect_company', 'label' => 'Company', 'rules' => 'required'),
//        );
//        $this->form_validation->set_rules($config);
//        if ($this->form_validation->run() !== FALSE) {


            if (!empty($data['project_id'])) {
                $checkEmail = $this->db->get_where('project_has_prospect', array('var_email' => $data['prospect_email']))->num_rows();
                $projectData = $this->toval->idtorowarr('id', $data['project_id'], 'client_has_project');
                if ($checkEmail == 0) {
                    $prospectData = array(
                        'var_fname' => ($data['prospect_fname']) ? $data['prospect_fname'] : NULL,
                        'var_lname' => ($data['prospect_lname']) ? $data['prospect_lname'] : NULL,
                        'var_email' => ($data['prospect_email']) ? $data['prospect_email'] : NULL,
                        'var_company' => ($data['prospect_company']) ? $data['prospect_company'] : NULL,
                        'bint_phone' => ($data['prospect_phno']) ? $data['prospect_phno'] : NULL,
                        'var_compnay_website' => ($data['compnay_website']) ? $data['compnay_website'] : NULL,
                        'var_linkedin_page' => ($data['linkedin_page']) ? $data['linkedin_page'] : NULL,
                        'var_position' => ($data['prospect_position']) ? $data['prospect_position'] : NULL,
                        'fk_admin' => ($this->user_type == 'ADMIN') ? $this->user_id : NULL,
                        'fk_account_manager' => ($this->user_type == 'KEY_ACCOUNT_MANAGER') ? $this->user_id : NULL,
                        'fk_client' => ($projectData['fk_client']) ? $projectData['fk_client'] : NULL,
                        'fk_user' => ($projectData['fk_user']) ? $projectData['fk_user'] : NULL,
                        'fk_project' => $data['project_id'],
                        'created_at' => date('Y-m-d H:i:s'),
                    );

                    $result = $this->db->insert("project_has_prospect", $prospectData);
                    $prospectId = $this->db->insert_id();

                    if($data['angle_of_intrest'] !=''){
                        $checkAngle = $this->toval->idtorowarr('var_name', $data['angle_of_intrest'], 'master_angle');
                        if(!empty($checkAngle)){
                            $angleId = $checkAngle['id'];
                        }else{
                            $angleData = array(
                                'var_name' => $data['angle_of_intrest'],
                                'created_at' => date('Y-m-d H:i:s'),
                            );
                            $this->db->insert('master_angle',$angleData);
                            $angleId = $this->db->insert_id();
                        }

                        $updateAngles = array(
                            'fk_angle' => $angleId,
                        );

                        $this->db->where('id',$prospectId);
                        $this->db->update('project_has_prospect',$updateAngles);
                    }

                    if ($result) {
                        $json_response['status'] = 'success';
                        $json_response['message'] = 'Prospect added Successfully!..';
                        $json_response['reload'] = "true";
                    } else {
                        $json_response['status'] = 'error';
                        $json_response['message'] = 'Something will be wrong';
                    }
                } else {
                    $json_response['status'] = 'warning';
                    $json_response['message'] = 'Email already Exist!..';
                }
            }
        } else {
            $json_response['message'] = 'please enter atleast one field';
            $json_response['status'] = 'warning';
        }
        return $json_response;
    }

    /* Add-Update of prospect-notes */

    function addProspectNote($data, $json_response)
    {
        $config = array(
            array('field' => 'notes', 'label' => 'Notes', 'rules' => 'required')
        );

        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE) {
            if (!empty($data['prospect_id'])) {
                $prospectData = $this->toval->idtorowarr('id', $data['prospect_id'], 'project_has_prospect');
                if (empty($data['note_id'])) {
                    $prospectData = array(
                        'txt_note' => $data['notes'],
                        'fk_prospect' => $data['prospect_id'],
                        'fk_client' => $prospectData['fk_client'],
                        'fk_user' => $prospectData['fk_user'],
                        'fk_admin' => ($this->user_type == 'ADMIN') ? $this->user_id : NULL,
                        'fk_account_manager' => ($this->user_type == 'KEY_ACCOUNT_MANAGER') ? $this->user_id : NULL,
                        'created_at' => date('Y-m-d H:i:s'),
                    );
                    $result = $this->db->insert("prospect_has_notes", $prospectData);
                    $json_response['message'] = 'Prospect note added Successfully!..';

                } else {
                    $prospectData = array(
                        'txt_note' => $data['notes'],
                    );
                    $this->db->where('id', $data['note_id']);
                    $result = $this->db->update("prospect_has_notes", $prospectData);
                    $json_response['message'] = 'Updated';
                }
                if ($result) {
                    $json_response['status'] = 'success';
                    $json_response['reload'] = 'true';
                } else {
                    $json_response['status'] = 'error';
                    $json_response['message'] = 'Something will be wrong';
                }
            }
        } else {
            $json_response['message'] = validation_errors();
            $json_response['status'] = 'warning';
        }
        return $json_response;
    }

    function getShortlistedExperts($data)
    {

        $this->db->select('project_has_experts.*,experts.var_fname,experts.var_lname,experts.txt_bio,master_angle.var_name as angleName,
            currentCompany.var_company as currentCompanyName,
            currentCompany.var_position as currentPosition,
            currentCompany.var_geography as currentGeography',TRUE);
        $this->db->join('experts', 'experts.id = project_has_experts.fk_experts');
        $this->db->join('experts_has_employment_history as currentCompany', 'currentCompany.fk_experts = experts.id AND currentCompany.enum_present = "YES"','left');
        $this->db->join('master_angle', 'master_angle.id = project_has_experts.fk_angle','left');
        if ($data['geography'] != '') {
            $this->db->like('experts_has_employment_history.var_geography', $data['geography']);
        }
        if ($data['expertStatus'] != '') {
            $this->db->where('project_has_experts.enum_status', $data['expertStatus']);
        }
        $this->db->where('project_has_experts.fk_project', $data['projectId']);
        $this->db->group_by('project_has_experts.fk_experts');
        $this->db->order_by("ps_project_has_experts.enum_status <> 'EXPERTS_ATTACHED'", "ps_project_has_experts.enum_status ASC");
        $experts = $this->db->get('project_has_experts')->result_array();
        for ($i = 0; $i < count($experts); $i++) {
            $experts[$i]['notes'] = $this->getProjecthasExpertsNotes($experts[$i]['id']);
        }

        return $experts;
    }

    function getProjecthasExpertsNotes($ProjecthasExpertsId)
    {
        $this->db->select('project_has_experts_notes.*,
                           IF((' . TABLE_PREFIX . 'admin.var_name !=""),' . TABLE_PREFIX . 'admin.var_name,IF((' . TABLE_PREFIX . 'account_manager.var_name !=""),' . TABLE_PREFIX . 'account_manager.var_name,"")) as noteBy,
                           account_manager.var_name as ac_manager,
                           admin.var_name as admin');
        $this->db->join('account_manager', 'account_manager.id = project_has_experts_notes.fk_account_manager', 'left');
        $this->db->join('admin', 'admin.id = project_has_experts_notes.fk_admin', 'left');
        $this->db->order_by('project_has_experts_notes.id', 'DESC');
        $notesArray = $this->db->get_where('project_has_experts_notes', array('fk_project_has_experts' => $ProjecthasExpertsId))->result_array();
        return $notesArray;
    }

    function checkValidExperts($data, $json_response)
    {
        $compliance = [];

        for ($i = 0; $i < count($data['shortlist']); $i++) {
            $exp = $this->db->select('experts.dt_compliance_date,experts.id,CONCAT(' . TABLE_PREFIX . 'experts.var_fname," ",' . TABLE_PREFIX . 'experts.var_lname) as name')
                ->join('experts', 'experts.id = project_has_experts.fk_experts')
                ->where('project_has_experts.id', $data['shortlist'][$i])->get('project_has_experts')->row_array();

            if (strtotime($exp['dt_compliance_date']) < strtotime('-1 Year')) {
                array_push($compliance, $exp['name']);
            }
        }

        if (!empty($compliance)) {
            $json_response['type'] = 'TRUE';
            $json_response['status'] = 'success';
            $json_response['data'] = implode(',', $compliance);

        } else {
            $json_response['status'] = 'FALSE';
            $json_response['data'] = $compliance;
        }
        return $json_response;
    }

    function bioSend($data, $json_response)
    {

        for ($i = 0; $i < count($data['shortlist']); $i++) {
            $experts_biosend = array(
                'enum_status' => 'BIOS_SENT',
            );
            $this->db->where('id', $data['shortlist'][$i]);
            $result = $this->db->update('project_has_experts', $experts_biosend);
        }
        $this->db->trans_complete();

        $this->db->select('project_has_experts.id,project_has_experts.fk_experts,user.var_email,user.var_fname as userfname,user.var_lname as userlname,
                           experts.var_fname as expertsfname,experts.var_lname as expertslname,experts.txt_bio,
                           currentCompany.var_company as currentCompanyName,currentCompany.var_position as currentPosition,
                           account_manager.var_name as stafname,account_manager.var_email as staffemail,account_manager.bint_phone staffphone,
                           ');
        $this->db->join('experts', 'experts.id = project_has_experts.fk_experts');
        $this->db->join('experts_has_employment_history', 'experts_has_employment_history.fk_experts = project_has_experts.fk_experts','left');
        $this->db->join('experts_has_employment_history as currentCompany', 'currentCompany.fk_experts = project_has_experts.fk_experts AND currentCompany.enum_present = "YES"','left');
        $this->db->join('user', 'user.id = project_has_experts.fk_user');
        $this->db->join('client', 'client.id = project_has_experts.fk_client');
        $this->db->join('account_manager', 'account_manager.id = client.fk_assign_account_manager', 'left');
        $this->db->where_in('project_has_experts.id', $data['shortlist']);
        $this->db->group_by('project_has_experts.fk_experts');
        $data['expertsdata'] = $this->db->get('project_has_experts')->result_array();


        $mail_templet = $this->load->view('email-templates/client/bio-sent', $data, TRUE);

        /* Send email on successfull Experts Bio Send to Client */

        $mail_body = $mail_templet;

        $this->load->library('Mylibrary');
        $configs['to'] = $data['expertsdata'][0]['var_email'];
        $configs['subject'] = 'Experts shortlisted';
        $configs['mail_body'] = $mail_body;
        $sendMail = $this->mylibrary->sendMail($configs);


        if ($this->db->trans_status() !== FALSE) {
            $json_response['message'] = 'Experts Bio Send Successfully to Client!..';
            $json_response['status'] = 'success';
            $json_response['jscode'] = 'Project.experts_data_init()';
        } else {
            $json_response['status'] = 'error';
            $json_response['message'] = 'Something will be wrong';
        }
        return $json_response;
    }

    function updateTags($data, $json_response)
    {
        $this->db->where('fk_project', $data['id']);
        $this->db->delete($data['table']);

        if ($data['table'] == 'project_has_country') {

            if (!empty($data['value'])) {
                $country = explode(',', $data['value']);
                for ($i = 0; $i < count($country); $i++) {
                    $country[$i] = trim($country[$i]);
                    $countryData = $this->toval->idtorowarr('var_country_name', $country[$i], 'master_country');

                    if (!empty($countryData)) {
                        $countryId = $countryData['id'];
                    } else {
                        $countrydata = array(
                            'var_country_code' => $country[$i],
                            'var_country_name' => $country[$i],
                            'created_at' => date('Y-m-d H:i:s')
                        );
                        $this->db->insert('master_country', $countrydata);
                        $countryId = $this->db->insert_id();
                    }

                    $project_has_country = array(
                        'fk_project' => $data['id'],
                        'fk_country' => $countryId,
                        'created_at' => date('Y-m-d H:i:s')
                    );
                    $this->db->insert('project_has_country', $project_has_country);
                }
            }
        } else if ($data['table'] == 'project_has_regions') {
            if (!empty($data['value'])) {
                $regions = explode(',', $data['value']);
                for ($i = 0; $i < count($regions); $i++) {
                    $regions[$i] = trim($regions[$i]);
                    $regionsData = $this->toval->idtorowarr('var_regions_name', $regions[$i], 'master_regions');
                    if (!empty($regionsData)) {
                        $regionsId = $regionsData['id'];
                    } else {
                        $regionsdata = array(
                            'var_regions_name' => $regions[$i],
                            'created_at' => date('Y-m-d H:i:s')
                        );

                        $this->db->insert('master_regions', $regionsdata);
                        $regionsId = $this->db->insert_id();
                    }

                    $project_has_region = array(
                        'fk_project' => $data['id'],
                        'fk_regions' => $regionsId,
                        'created_at' => date('Y-m-d H:i:s')
                    );
                    $this->db->insert('project_has_regions', $project_has_region);
                }
            }
        } else if ($data['table'] == 'project_has_company') {
            if (!empty($data['value'])) {
                $company = explode(',', $data['value']);
                for ($i = 0; $i < count($company); $i++) {
                    $company[$i] = trim($company[$i]);

                    $companyData = $this->toval->idtorowarr('var_company_name', $company[$i], 'master_company');
                    if (!empty($companyData)) {
                        $companyId = $companyData['id'];
                    } else {
                        $companydata = array(
                            'var_company_name' => $company[$i],
                            'created_at' => date('Y-m-d H:i:s')
                        );
                        $this->db->insert('master_company', $companydata);
                        $companyId = $this->db->insert_id();
                    }

                    $project_has_company = array(
                        'fk_project' => $data['id'],
                        'fk_company' => $companyId,
                        'created_at' => date('Y-m-d H:i:s'),
                    );

                    $this->db->insert('project_has_company', $project_has_company);
                }
            }
        }

    }

    function addProjectExpertNote($data, $json_response)
    {
        $config = array(
            array('field' => 'notes', 'label' => 'Note', 'rules' => 'required'),
        );
        $this->form_validation->set_rules($config);
        if ($this->form_validation->run() !== FALSE) {
            if (!empty($data['notes'])) {
                $noteData = array(
                    'txt_note' => $data['notes'],
                    'fk_experts' => $data['expertId'],
                    'fk_client' => $data['clientId'],
                    'fk_user' => $data['userId'],
                    'fk_project' => $data['projectId'],
                    'fk_project_has_experts' => $data['projecthasExpertsId'],
                    'fk_admin' => ($this->user_type == 'ADMIN') ? $this->user_id : NULL,
                    'fk_account_manager' => ($this->user_type == 'KEY_ACCOUNT_MANAGER') ? $this->user_id : NULL,
                    'created_at' => date('Y-m-d H:i:s'),
                );

                if ($data['projecthasExpertsNoteId'] == '') {
                    $result = $this->db->insert("project_has_experts_notes", $noteData);
                } else {
                    $this->db->where('id', $data['projecthasExpertsNoteId']);
                    $result = $this->db->update('project_has_experts_notes', $noteData);
                }

                if ($result) {
                    $json_response['status'] = 'success';
                    $json_response['message'] = 'Note added Successfully!..';
                    $json_response['reload'] = "true";
                } else {
                    $json_response['status'] = 'error';
                    $json_response['message'] = 'Something will be wrong';
                }
            } else {
                $json_response['message'] = validation_errors();
                $json_response['status'] = 'warning';
            }
            return $json_response;
        }
    }


    function updateDeadlineDate($data,$json_response){
        if($data['value'] != ''){
            $value = DateTime::createFromFormat('d/m/Y',$data['value'])->format('Y-m-d');
        }else{
            $value = NULL;
        }

        $date_array = array(
            'dt_deadline' => $value,
        );

        $this->db->where('id',$data['id']);
        $this->db->update($data['table'],$date_array);
    }

    function updateAngle($data){
        if ($data['value'] != '') {
            $checkAngle = $this->toval->idtorowarr('var_name',$data['value'], 'master_angle');
            if (!empty($checkAngle)) {
                $angleId = $checkAngle['id'];
            } else {
                $angleData = array(
                    'var_name' => $data['value'],
                    'created_at' => date('Y-m-d H:i:s'),
                );
                $this->db->insert('master_angle', $angleData);
                $angleId = $this->db->insert_id();
            }

            $updateAngles = array(
                'fk_angle' => $angleId,
            );

            $this->db->where('id', $data['id']);
            $this->db->update($data['table'], $updateAngles);
        }
    }
}
