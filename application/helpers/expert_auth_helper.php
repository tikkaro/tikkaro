<?php

function isExpertLogin() {
    $CI = &get_instance();
    return $CI->expertauthlibrary->isLogin();
}

function getLoginExpertUserData() {
    $CI = &get_instance();
    return $CI->expertauthlibrary->getLoginUserData();
}

function getLoginExpertUserId() {
    $CI = &get_instance();
    return $CI->expertauthlibrary->getLoginUserId();
}

function doExpertLogin($userData) {
    $CI = &get_instance();
    return $CI->expertauthlibrary->doLogin($userData);
}

function checkExpertLoggedinUser() {
    $CI = &get_instance();
    return $CI->expertauthlibrary->checkLoggedinUser();
}

function getLoginExpertUserType() {
    $CI = &get_instance();
    return $CI->expertauthlibrary->getLoginUserType();
}
