<?php

function isUserLogin() {
    $CI = &get_instance();
    return $CI->userauthlibrary->isLogin();
}

function getLoginUserUserData() {
    $CI = &get_instance();
    return $CI->userauthlibrary->getLoginUserData();
}

function getLoginUserId() {
    $CI = &get_instance();
    return $CI->userauthlibrary->getLoginUserId();
}

function doUserLogin($userData) {
    $CI = &get_instance();
    return $CI->userauthlibrary->doLogin($userData);
}

function checkUserLoggedinUser() {
    $CI = &get_instance();
    return $CI->userauthlibrary->checkLoggedinUser();
}

function getLoginUserUserType() {
    $CI = &get_instance();
    return $CI->userauthlibrary->getLoginUserType();
}
