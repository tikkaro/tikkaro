<?php

function get_country($ip) {
    $CI = &get_instance();
    $CI->load->model('curl_function');
    $url = "http://api.wipmania.com/" . $ip . "?" . base_url();
    $result = $CI->curl_function->get($url);
    return $result;
}

function admin_url($url = '') {
    $CI = &get_instance();
    return $CI->config->config['admin_url'] . $url;
}

function customer_url($url = '') {
    $CI = &get_instance();
    return $CI->config->config['customer_url'] . $url;
}

function expert_url($url = '') {
    $CI = &get_instance();
    return $CI->config->config['expert_url'] . $url;
}

function user_url($url = '') {
    $CI = &get_instance();
    return $CI->config->config['user_url'] . $url;
}

function get_project_name() {
    $CI = &get_instance();
    return $CI->config->config['project_name'];
}

function get_skip($page_no = 1, $per_page = 24) {
    return (($page_no - 1) * $per_page);
}

function upload_single_image($file, $name, $path, $thumb = FALSE) {
    $CI = &get_instance();

    $return['error'] = '';
    $image_name = $name . '_' . time();

    $CI->load->helper('form');
    $config['upload_path'] = $path;
    $config['allowed_types'] = 'gif|jpg|png|jpeg|JPEG|PNG|JPG';
    $config['file_name'] = $image_name;

    $CI->load->library('upload', $config);
    $CI->upload->initialize($config);

    $CI->upload->set_allowed_types('gif|jpg|png|jpeg|JPEG|PNG|JPG|GIF');

    if (!$CI->upload->do_upload(key($file))) {
        $return['error'] = $CI->upload->display_errors();
    } else {
        $result = $CI->upload->data();
        $return['data'] = $result;
    }

    if ($thumb == TRUE && $return['error'] == '') {
        $CI->load->library('Mylibrary');
        $thumb_array = array(
            array('width' => '100', 'height' => '100', 'image_type' => 'SMALL'),
            array('width' => '250', 'height' => '250', 'image_type' => 'MEDIUM'));
        for ($i = 0; $i < count($thumb_array); $i++) {
            $imageinfo = getimagesize($result['full_path']);
            $thumbSize = $CI->mylibrary->calculateResizeImage($imageinfo[0], $imageinfo[1], $thumb_array[$i]['width'], $thumb_array[$i]['height']);

            $CI->load->library('image_lib');
            $conf['image_library'] = 'gd2';
            $conf['source_image'] = $path . $result['orig_name'];
            $conf['create_thumb'] = TRUE;
            $conf['maintain_ratio'] = TRUE;
            $conf['new_image'] = $result['orig_name'];
            $conf['thumb_marker'] = "_" . $thumb_array[$i]['image_type'];
            $conf['width'] = $thumbSize['width'];
            $conf['height'] = $thumbSize['height'];
            $CI->image_lib->clear();
            $CI->image_lib->initialize($conf);
            if (!$CI->image_lib->resize()) {
                $return['error'] = 'Thumb Not Created';
            }
        }
    }

    return $return;
}

function delete_single_image($fullPath, $fileName) {
    unlink($fullPath . '/' . $fileName);
}

function delete_image($array, $path) {
    $CI = &get_instance();
    $img = $CI->db->select($array['field'])->where('int_glcode', $array['id'])->get($array['table'])->row_array();
    $mainImg = $img[$array['field']];
    $expImg = explode('.', $mainImg);
    $imgdelete = $path . $mainImg;
    if (file_exists($imgdelete)) {
        unlink($imgdelete);
    }
    return TRUE;
}

function sanitize($string, $force_lowercase = true, $anal = false) {

    $string = htmlentities($string, ENT_QUOTES, 'UTF-8');
    $string = preg_replace('~&([a-z]{1,2})(acute|cedil|circ|grave|lig|orn|ring|slash|th|tilde|uml);~i', '$1', $string);
    $string = html_entity_decode($string, ENT_QUOTES, 'UTF-8');

    $strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
        "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
        "â€”", "â€“", ",", "<", ".", ">", "/", "?");
    $clean = trim(str_replace($strip, "", strip_tags($string)));
    $clean = preg_replace('/\s+/', "-", $clean);
    $clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean;
    return ($force_lowercase) ?
            (function_exists('mb_strtolower')) ?
            mb_strtolower($clean, 'UTF-8') :
            strtolower($clean) :
            $clean;
}

function sanitize_title($string) {

    $strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "=", "+", "[", "{", "]",
        "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
        "â€”", "â€“", ",", "<", ".", ">", "/", "?");
    $string = trim(str_replace($strip, "", strip_tags($string)));

    $url = $string;
    $url = preg_replace('~[^\\pL0-9_]+~u', '-', $url); // substitutes anything but letters, numbers and '_' with separator
    $url = trim($url, "-");
    $url = iconv("utf-8", "us-ascii//TRANSLIT", $url); // TRANSLIT does the whole job
    $url = strtolower($url);
    $url = preg_replace('~[^-a-z0-9_]+~', '', $url); // keep only letters, numbers, '_' and separator
    return $url;
}

function apply_lang($string, $delimiter = ' ') {
    $CI = &get_instance();
    $str_array = explode($delimiter, $string);
    $return_string = '';

    for ($i = 0; $i < count($str_array); $i++) {
        $return_string .= $CI->lang->line($str_array[$i]);
        if ($i < count($str_array)) {
            $return_string .= $delimiter;
        }
    }
    if ($return_string == '') {
        return $CI->lang->line($string);
    }
    return $return_string;
}

function sorttextlen($text, $limit) {
    if (strlen($text) < $limit) {
        $sort_text = mb_substr($text, 0, $limit);
    } else if (strlen($text) > $limit) {
        $sort_text = mb_substr($text, 0, $limit) . '...';
    }

    return $sort_text;
}

function shortenText($str, $len, $readMore = " ... ") {

    if (mb_strlen($str) > $len) {
        $str = mb_substr($str, 0, $len);
        //        $str = mb_substr($str, 0, mb_strrpos($str, " "));
        return html_entity_decode(mb_convert_encoding($str, 'HTML-ENTITIES', 'UTF-8'), ENT_QUOTES, 'UTF-8') . $readMore;
    } else
        return html_entity_decode(mb_convert_encoding($str, 'HTML-ENTITIES', 'UTF-8'), ENT_QUOTES, 'UTF-8');
}

function date_formate($date) {
    $date = date('M', strtotime($date)) . ' ' . date('j', strtotime($date)) . "'" . date('y', strtotime($date)) . ' at' . ' ' . date('h:i', strtotime($date));
    return $date;
}

function str_replace_first($from, $to, $subject) {
    $from = '/' . preg_quote($from, '/') . '/';

    return preg_replace($from, $to, $subject, 1);
}

/* ----   Start Function to get the client IP address ----- */

function get_ip() {
    $ipaddress = '';
    if (getenv('HTTP_CLIENT_IP')) {
        $ipaddress = getenv('HTTP_CLIENT_IP');
    } else if (getenv('HTTP_X_FORWARDED_FOR')) {
        $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
    } else if (getenv('HTTP_X_FORWARDED')) {
        $ipaddress = getenv('HTTP_X_FORWARDED');
    } else if (getenv('HTTP_FORWARDED_FOR')) {
        $ipaddress = getenv('HTTP_FORWARDED_FOR');
    } else if (getenv('HTTP_FORWARDED')) {
        $ipaddress = getenv('HTTP_FORWARDED');
    } else if (getenv('REMOTE_ADDR')) {
        $ipaddress = getenv('REMOTE_ADDR');
    } else {
        $ipaddress = 'UNKNOWN';
    }
    return $ipaddress;
}

/* ---- End Function to get the client IP address ----- */

function getLoginUserData() {
    $CI = &get_instance();
    if ($CI->session->userdata['valid_user']) {
        $result = $CI->session->userdata['valid_user'];
    } elseif ($CI->session->userdata['valid_dealer']) {
        $result = $CI->session->userdata['valid_dealer'];
    } elseif ($CI->session->userdata['valid_admin']) {
        $result = $CI->session->userdata['valid_admin'];
    }
    return $result;
}

function get_img($path, $image, $multiple_img = FALSE) {
    $path = trim($path);
    $image = trim($image);
    if ($multiple_img == TRUE) {
        if (!empty($image)) {
            $img = explode(',', $image);
            if (file_exists($path . $img[0])) {
                $data = base_url() . $path . $img[0];
            } else {
                $data = base_url() . NO_IMAGE;
            }
        } else {
            $data = base_url() . NO_IMAGE;
        }
    } else {
        if (!empty($image) && !empty($path)) {
            if (file_exists($path . $image)) {
                $data = base_url() . $path . $image;
            } else {
                $data = base_url() . NO_IMAGE;
            }
        } else {
            $data = base_url() . NO_IMAGE;
        }
    }
    return $data;
}

function getLoginUserType($userData) {
    $CI = &get_instance();
    if ($CI->session->userdata['valid_dealer'] && $CI->uri->segment(1) == 'dealer') {
        $result = $CI->session->userdata['valid_dealer'];
    } elseif ($CI->session->userdata['valid_user'] && $CI->uri->segment(1) == 'user') {
        $result = $CI->session->userdata['valid_user'];
    } elseif ($CI->session->userdata['valid_admin'] && $CI->uri->segment(1) == 'admin') {
        $result = $CI->session->userdata['valid_admin'];
    }
    return $result['user_type'];
}

function getbaseURL($url = '') {
    $CI = &get_instance();
    if ($CI->session->userdata['valid_dealer'] && $CI->uri->segment(1) == 'dealer') {
        return dealer_url($url);
    } elseif ($CI->session->userdata['valid_user'] && $CI->uri->segment(1) == 'user') {
        return user_url($url);
    } elseif ($CI->session->userdata['valid_admin'] && $CI->uri->segment(1) == 'admin') {
        return admin_url($url);
    } else {
        return base_url($url);
    }
}

function tz_date($date, $formate = 'Y-m-d H:i:s', $zone = null) {
    return date($formate, strtotime($date));
}

function timezone_offset_string($offset) {
    return sprintf("%s%02d:%02d", ($offset >= 0) ? '+' : '-', abs($offset / 3600), abs($offset % 3600));
}

function getTimezoneOfset($timeZone, $isString = FALSE) {
    $offset = timezone_offset_get(new DateTimeZone($timeZone), new DateTime());
    if ($isString === TRUE) {
        $offset = timezone_offset_string($offset);
    }
    return $offset;
}

function getTimeZone() {
    $CI = &get_instance();
    $settings = $CI->authlibrary->getStoreSetting();
    $settings = $settings[0];
    return ($settings['var_time_zone']) ? $settings['var_time_zone'] : "UTC";
}

function date_tz($format = 'Y-m-d H:i:s') {
    return convert_date_from_utc(date('Y-m-d H:i:s'), getTimeZone(), $format);
}

function array_insert($array, $index, $val) {
    $size = count($array); //because I am going to use this more than one time
    if (!is_int($index) || $index < 0 || $index > $size) {
        return -1;
    } else {
        $temp = array_slice($array, 0, $index);
        $temp[] = $val;
        return array_merge($temp, array_slice($array, $index, $size));
    }
}

function escapeJavaScriptText($string) {
    return str_replace("\n", '\n', str_replace('"', '\"', addcslashes(str_replace("\r", '', (string) $string), "\0..\37'\\")));
}

function addForeignKey($sourceTable, $sourceField, $targetTable, $targetField = 'id', $onDelete = 'RESTRICT', $onUpdate = 'RESTRICT') {
    $CI = &get_instance();
    $CI->db->query('ALTER TABLE `prosapient`.`ps_' . $sourceTable . '` ADD INDEX `ps_' . $sourceTable . '_' . $sourceField . '` (`' . $sourceField . '`);');
    $CI->db->query('ALTER TABLE `ps_' . $sourceTable . '` ADD CONSTRAINT `ps_' . $sourceTable . '_' . $sourceField . '` FOREIGN KEY (`' . $sourceField . '`) REFERENCES `prosapient`.`ps_' . $targetTable . '`(`' . $targetField . '`) ON DELETE ' . $onDelete . ' ON UPDATE ' . $onUpdate . '; ');
}

function dropForeignKey($sourceTable, $sourceField, $targetTable, $targetField = 'id') {
    $CI = &get_instance();
    $CI->db->query('ALTER TABLE ps_' . $sourceTable . ' DROP FOREIGN KEY ps_' . $sourceTable . '_' . $sourceField . ';');
    $CI->db->query('ALTER TABLE ps_' . $sourceTable . ' DROP INDEX ps_' . $sourceTable . '_' . $sourceField . ';');
}

//ALTER TABLE ps_news DROP FOREIGN KEY ps_news_fk_category;

function convertNumber($number) {
    return preg_replace('/[^\\d.]+/', '', $number);
}

function clientLable($clientLable) {

    $client_status = json_decode(CLIENT_STATUS);

    if ($clientLable == "LOW") {
        $status = '<span class="label label-sm label-info ">' . $client_status->LOW . '</span>';
    } elseif ($clientLable == 'MEDIUM') {
        $status = '<span class="label label-sm label-success ">' . $client_status->MEDIUM . '</span>';
    } else {
        $status = '<span class="label label-sm label-danger ">' . $client_status->HIGH . '</span>';
    }

    return $status;
}

function projectLable($projectLable) {

    $projectStatus = json_decode(PROJECT_STATUS);

    if ($projectLable == "WAITING") {
        $status = '<span class="label label-sm label-info">' . $projectStatus->WAITING . '</span>';
    } elseif ($projectLable == 'ACTIVE') {
        $status = '<span class="label label-sm label-success">' . $projectStatus->ACTIVE . '</span>';
    } else if ($projectLable == 'CLOSED') {
        $status = '<span class="label label-sm label-danger">' . $projectStatus->CLOSED . '</span>';
    } else if ($projectLable == 'PRIORITY') {
        $status = '<span class="label label-sm label-default">' . $projectStatus->PRIORITY . '</span>';
    } else if ($projectLable == 'SCHEDULING') {
        $status = '<span class="label label-sm label-warning">' . $projectStatus->SCHEDULING . '</span>';
    }

    return $status;
}

function getProjectStatus($status) {
    $ProjectStatus = '';
    $projectStatus = json_decode(PROJECT_STATUS);
    foreach ($projectStatus as $key => $value) {
        if ($status == $key) {
            $ProjectStatus = $value;
        }
    }
    return $ProjectStatus;
}

function getShortlistedStatus($status) {
    $ListedStatus = '';
    $shortListedStatus = json_decode(SHORTLISTED_STATUS);
    foreach ($shortListedStatus as $key => $value) {
        if ($status == $key) {
            $ListedStatus = $value;
        }
    }
    return $ListedStatus;
}

function age_diff($fromdate, $todate) {
    if (trim($fromdate) != '0000-00-00' && trim($todate) != '0000-00-00' && date('Y-m-d', strtotime($fromdate)) <= date('Y-m-d', strtotime($todate))) {
        $datediff = (new DateTime(date('Y-m-d', strtotime($todate))))->diff((new DateTime(date('Y-m-d', strtotime($fromdate)))));

        if ($datediff->y == 0 && $datediff->m == 0) {
            if ($datediff->d > 1) {
                $result = $datediff->d . ' days';
            } else if ($datediff->d == 1 || $datediff->d == 0) {
                $result = '1 day';
            }
        } else if ($datediff->y == 0 && $datediff->m > 0) {
            if ($datediff->m > 1) {
                $result = $datediff->m . ' months';
            } else if ($datediff->m == 1 || $datediff->m == 0) {
                $result = '1 month';
            }
        } else {
            if ($datediff->y > 1) {
                $result = $datediff->y . ' years ';
            } else {
                $result = $datediff->y . ' year';
            }
        }
    } else {
        $result = '-';
    }
    return $result;
}

function time_ago($datetime, $todate, $seconds = FALSE) {
    if (is_numeric($datetime)) {
        $timestamp = $datetime;
    } else {
        $timestamp = strtotime($datetime);
    }
    if (is_numeric($todate)) {
        $to_timestamp = $todate;
    } else {
        $to_timestamp = strtotime($todate);
    }
    $diff = $to_timestamp - $timestamp;

    $min = 60;
    $hour = 60 * 60;
    $day = 60 * 60 * 24;
    $month = $day * 30;
    $year = $month * 12;
    $timeago = '';

    if ($diff < 60) { //Under a min
        if ($seconds) {
            $timeago .= $diff . " seconds";
        }
    } elseif ($diff < $hour) { //Under an hour
        $timeago .= round($diff / $min) . " mins";
    } elseif ($diff < $day) { //Under a day
        $timeago .= round($diff / $hour) . " hours";
    } elseif ($diff < $month) { //Under a day
        $timeago .= round($diff / $day) . " days";
    } elseif ($diff < $year) { //Under a day
        $timeago .= round($diff / $month) . " months";
    } else {
        if (round($diff / $year) > 1) {
            $timeago .= round($diff / $year) . " years";
        } else {
            $timeago .= round($diff / $year) . " year";
        }
    }

    return $timeago;
}

function dateDifference($date_1, $date_2, $days = FALSE) {
    $datetime1 = date_create($date_1);
    $datetime2 = date_create($date_2);
    $differenceFormat = '';
    $interval = date_diff($datetime1, $datetime2);

    if ($interval->y > 0) {
        if ($interval->y > 1) {
            $differenceFormat .= '%y years';
        } else {
            $differenceFormat .= '1 year';
        }
    }

    if ($interval->m > 0) {
        if ($interval->m > 1) {
            if ($interval->y > 0) {
                $differenceFormat .= ' and %m months';
            } else {
                $differenceFormat .= '%m months';
            }
        } else {
            $differenceFormat .= '1 month';
        }
    }
    if ($days) {
        if ($interval->d > 0) {
            if ($interval->d > 1) {
                if ($interval->m > 0) {
                    $differenceFormat .= ' and %d days';
                } else {
                    $differenceFormat .= '%d days';
                }
            } else {
                $differenceFormat .= '1 day';
            }
        }
    }

    return $interval->format($differenceFormat);
}

function getPastYears($startFromYear = FALSE, $cnt = 50) {
    if ($startFromYear) {
        $cur_year = $startFromYear;
    } else {
        $cur_year = date('Y');
    }
    for ($i = 0; $i <= $cnt; $i++) {
        $years[] = $cur_year--;
    }
    return $years;
}

function getMonths($full = FALSE) {

    $months = array(
        '01' => ($full) ? 'January' : 'Jan',
        '02' => ($full) ? 'February' : 'Feb',
        '03' => ($full) ? 'March' : 'Mar',
        '04' => ($full) ? 'April' : 'Apr',
        '05' => ($full) ? 'May' : 'May',
        '06' => ($full) ? 'June' : 'Jun',
        '07' => ($full) ? 'July' : 'Jul',
        '08' => ($full) ? 'August' : 'Aug',
        '09' => ($full) ? 'September' : 'Sep',
        '10' => ($full) ? 'Octomber' : 'Oct',
        '11' => ($full) ? 'November' : 'Nov',
        '12' => ($full) ? 'December' : 'Dec');

    return $months;
}