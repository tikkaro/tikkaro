<?php

function clientMenu($activeClass, $clientid) {
    if ($activeClass == 'overview') {
        $aciveOverview = 'class="active"';
    } else if ($activeClass == 'rules') {
        $aciveRules = 'class="active"';
    } else if ($activeClass == 'notes') {
        $aciveNotes = 'class="active"';
    } else if ($activeClass == 'project') {
        $aciveProject = 'class="active"';
    } else if ($activeClass == 'officer') {
        $aciveOfficer = 'class="active"';
    } else if ($activeClass == 'history') {
        $aciveHistory = 'class="active"';
    } else if ($activeClass == 'callhistory') {
        $activecallHistroy = 'class="active"';
    }


    $html = '<ul class="nav nav-tabs">
                <li ' . $aciveOverview . '>
                    <a href="' . admin_url() . 'client/overview/' . $clientid . '"> Overview </a>
                </li>
                <li ' . $aciveRules . '>
                    <a href="' . admin_url() . 'client/rules/' . $clientid . '"> Client Rules </a>
                </li>
                <li ' . $aciveNotes . '>
                    <a href="' . admin_url() . 'client/notes/' . $clientid . '"> Notes </a>
                </li>
                <li ' . $aciveProject . '>
                    <a href="' . admin_url() . 'client/project/' . $clientid . '"> Project History </a>
                </li>
                <li ' . $aciveOfficer . '>
                    <a href="' . admin_url() . 'client/compliance-officer/' . $clientid . '"> Compliance Officer </a>
                </li>
               
                 <li ' . $activecallHistroy . '>
                    <a href="' . admin_url() . 'client/client-call-history/' . $clientid . '"> Call History </a>
                </li>
            </ul>';

    return $html;
}

function clientUserMenu($activeClass, $userid) {
    if ($activeClass == 'userbio') {
        $aciveUserBio = 'class="active"';
    } else if ($activeClass == 'usernotes') {
        $aciveUserNotes = 'class="active"';
    } else if ($activeClass == 'callhistory') {
        $aciveCalls = 'class="active"';
    } else if ($activeClass == 'userproject') {
        $aciveProject = 'class="active"';
    }

    $html = '<ul class="nav nav-tabs">
                <li ' . $aciveUserBio . '>
                    <a href="' . admin_url() . 'client/user-bio/' . $userid . '"> User Bio </a>
                </li>
                <li ' . $aciveUserNotes . '>
                    <a href="' . admin_url() . 'client/user-notes/' . $userid . '"> User Notes </a>
                </li>
               
            </ul>';

    return $html;
}

function projectMenu($activeClass, $projectId) {

    if ($activeClass == 'overview') {
        $aciveOverview = 'class="active"';
    } else if ($activeClass == 'shortlist') {
        $aciveShortlist = 'class="active"';
    } else if ($activeClass == 'notes') {
        $aciveNotes = 'class="active"';
    } else if ($activeClass == 'prospects') {
        $aciveProspect = 'class="active"';
    } else if ($activeClass == 'calls') {
        $aciveCalls = 'class="active"';
    } else if ($activeClass == 'rules') {
        $aciveRules = 'class="active"';
    } else if ($activeClass == 'interest') {
        $aciveInterest = 'class="active"';
    }

    $html = '<ul class="nav nav-tabs">
                <li ' . $aciveOverview . '>
                    <a href="' . admin_url('project/overview/' . $projectId) . '"> Overview </a>
                </li>
                <li ' . $aciveNotes . '>
                    <a href="' . admin_url('project/notes/' . $projectId) . '"> Notes </a>
                </li>
                <li ' . $aciveShortlist . '>
                    <a href="' . admin_url('project/expert-shortlist/' . $projectId) . '"> Expert Shortlist </a>
                </li>
                <li ' . $aciveProspect . '>
                    <a href="' . admin_url('project/expert-prospects/' . $projectId) . '"> Prospects </a>
                </li>
                <li ' . $aciveCalls . '>
                    <a href="' . admin_url('project/calls/' . $projectId) . '"> Calls </a>
                </li>
                <li ' . $aciveRules . '>
                    <a href="' . admin_url('project/compliance-rules/' . $projectId) . '"> Compliance Rules </a>
                </li>
               
            </ul>';

    return $html;
}

function expertsMenu($activeClass, $expertId ,$getExperts,$email) {
    if ($activeClass == 'bio') {
        $aciveBio = 'class="active"';
    } else if ($activeClass == 'email') {
        $aciveEmail = 'class="active"';
    } else if ($activeClass == 'notes') {
        $aciveNotes = 'class="active"';
    } else if ($activeClass == 'projects') {
        $aciveProject = 'class="active"';
    } else if ($activeClass == 'addtoshortlist') {
        $aciveAddtoshortlist = 'class="active"';
    } else if ($activeClass == 'call_history') {
        $aciveCallHistory = 'class="active"';
    }
    if(!empty($getExperts)){
        $backtosearch = admin_url().'find-experts/?clientId='.$getExperts['clientId'].'&projectId='.$getExperts['projectId'].'&expertsName='.$getExperts['expertsName'].'&position='.$getExperts['position'].'&geoGraphy='.$getExperts['geoGraphy'].'&expretise='.$getExperts['expretise'].'&currentCompany='.$getExperts['currentCompany'].'&formerCompany='.$getExperts['formerCompany'].'';
        $search = '<li ' . $aciveAddtoshortlist . '>
                    <a href="'. $backtosearch.'"> Back to Search </a>
                </li>';

        $addtoShorlist = '<li ' . $aciveAddtoshortlist . '>
                    <a href="javascript:;"> Add to Shortlist </a>
                </li>';
        $biourl = admin_url().'experts/bio/' . $expertId . '?clientId='.$getExperts['clientId'].'&projectId='.$getExperts['projectId'].'&expertsName='.$getExperts['expertsName'].'&position='.$getExperts['position'].'&geoGraphy='.$getExperts['geoGraphy'].'&expretise='.$getExperts['expretise'].'&currentCompany='.$getExperts['currentCompany'].'&formerCompany='.$getExperts['formerCompany'].'';
        $projecturl = admin_url().'experts/projects/' . $expertId . '?clientId='.$getExperts['clientId'].'&projectId='.$getExperts['projectId'].'&expertsName='.$getExperts['expertsName'].'&position='.$getExperts['position'].'&geoGraphy='.$getExperts['geoGraphy'].'&expretise='.$getExperts['expretise'].'&currentCompany='.$getExperts['currentCompany'].'&formerCompany='.$getExperts['formerCompany'].'';
        $noteurl = admin_url().'experts/notes/' . $expertId . '?clientId='.$getExperts['clientId'].'&projectId='.$getExperts['projectId'].'&expertsName='.$getExperts['expertsName'].'&position='.$getExperts['position'].'&geoGraphy='.$getExperts['geoGraphy'].'&expretise='.$getExperts['expretise'].'&currentCompany='.$getExperts['currentCompany'].'&formerCompany='.$getExperts['formerCompany'].'';
    }else{
        $biourl = admin_url().'experts/bio/' . $expertId . '';
        $projecturl = admin_url().'experts/projects/' . $expertId . '';
        $noteurl = admin_url().'experts/notes/' . $expertId . '';
    }

    if($email != ''){
        $mailExperts = '<li ' . $aciveEmail . '>
                    <a href="mailto:'.$email.'"> Email </a>
                </li>';
    }
    $html = '<ul class="nav nav-tabs">
                <li ' . $aciveBio . '>
                    <a href="' . $biourl. '"> Bio </a>
                </li>
                '.$mailExperts.'
                <li ' . $aciveProject . '>
                    <a href="' . $projecturl. '"> Projects </a>
                </li>
                <li ' . $aciveNotes . '>
                    <a href="' . $noteurl . '"> Notes </a>
                </li>
                 '.$addtoShorlist.'
                 <li ' . $aciveCallHistory . '>
                   <a href="' . admin_url() . 'experts/call-history/' . $expertId . '"> Call History </a>
                </li>
                '.$search.'
            </ul>';

    return $html;
}
