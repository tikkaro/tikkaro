<?php

function isCustomerLogin() {
    $CI = &get_instance();
    return $CI->customerauthlibrary->isLogin();
}

function getLoginCustomerUserData() {
    $CI = &get_instance();
    return $CI->customerauthlibrary->getLoginUserData();
}

function getLoginCustomerUserId() {
    $CI = &get_instance();
    return $CI->customerauthlibrary->getLoginUserId();
}

function doCustomerLogin($userData) {
    $CI = &get_instance();
    return $CI->customerauthlibrary->doLogin($userData);
}

function checkCustomerLoggedinUser() {
    $CI = &get_instance();
    return $CI->customerauthlibrary->checkLoggedinUser();
}

function getLoginCustomerUserType() {
    $CI = &get_instance();
    return $CI->customerauthlibrary->getLoginUserType();
}
