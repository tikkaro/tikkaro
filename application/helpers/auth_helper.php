<?php

function setUserToken()
{
    $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
    $token = '';
    for ($i = 0; $i < 10; $i++) {
        $token .= $characters[mt_rand(0, 61)];
    }
    return $token;
}

function isLogin()
{
    $CI = &get_instance();
    return $CI->authLibrary->isLogin();
}

function getLoginUserData()
{
    $CI = &get_instance();
    return $CI->authLibrary->getLoginUserData();
}

function getLoginUserId()
{
    $CI = &get_instance();
    return $CI->authLibrary->getLoginUserId();
}

function isIpBan()
{
    $CI = &get_instance();
    return $CI->authLibrary->isIpBan();
}

function getBanIp()
{
    $CI = &get_instance();
    return $CI->authLibrary->getBanIp();
}

function doLogin($userData)
{
    $CI = &get_instance();
    return $CI->authLibrary->doLogin($userData);
}

function checkLoggedinUser()
{
    $CI = &get_instance();
    return $CI->authLibrary->checkLoggedinUser();

}
function getLoginUserType()
{
    $CI = &get_instance();
    return $CI->authLibrary->getLoginUserType();

}