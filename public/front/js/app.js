/**
 Core script to handle the entire theme and core functions
 **/
var App = function () {

    var handleTabs = function () {
        $('ul.tabs').tabs();
        $('.modal').modal({ready: function (modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
                $('ul.tabs li a.active').trigger('click');
            }, });

    };

    var handleSelect = function () {
        $('select').material_select();
    }

    return {
        //main function to initiate the theme
        init: function () {
            handleTabs();
            handleSelect();
        }
    };

}();