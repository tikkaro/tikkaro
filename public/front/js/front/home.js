var Home = function () {

    var General = function () {
        $('body').on('click', '#global-search', function () {
            $('#services-stores-popup').modal('open');
        });

        $('body').on('click', '#closebtn', function () {
            $('#services-stores-popup').modal('close');
        });

    };

    return {
        init: function () {
            General();
        }
    };
}();