var Custom = function () {

    var General = function () {
        //Login and forgot password form
        $('body').on('click', '#forgotpass', function () {
            $('#frmlogin').hide();
            $('#frmforgotpass').show();
        });
        $('body').on('click', '#cancel-btn', function () {
            $('#frmlogin').show();
            $('#frmforgotpass').hide();
        });
        $('body').on('click', '.loginlinks', function () {
            $('.userlinks').toggle();
        });
    };

    return {
        init: function () {
            General();
        }
    };
}();