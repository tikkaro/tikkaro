var Stores = function () {

    var General = function () {
        $('body').on('click', '.choose-service', function () {
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
            } else {
                $(this).addClass('active');
            }
        });

        $('body').on('click', '.choose-time', function () {
            $('#choose-time').modal('open');
        });
    };

    return {
        init: function () {
            General();
        }
    };
}();