var Login = function () {
    var handleSignup = function () {

        var form = $('.signup-form');
        var rules = {
            fname: {required: true},
            lname: {required: true},
            username: {required: true},
            email: {email: true, required: true},
            password: {required: true},
            repassword: {required: true, equalTo: "#register_password"},
            phone: {required: true, number: true, maxlength: 10},
            dob: {required: true},
            address: {required: true},
            tnc: {required: true}
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form);
            return false;
        });
        $('.signup-form').keypress(function (e) {
            e.stopImmediatePropagation();
            if (e.which == 13) {
                if ($('.signup-form').validate().form()) {
                    $('.signup-form').submit();
                }
                return false;
            }
        });
    }


    var handleLogin = function () {
//        alert('sad');
        $('#frmlogin').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            rules: {
                username: {required: true},
                password: {required: true}
            },
            messages: {
                username: {
                    required: "Mobile OR Email is required."
                },
                password: {
                    required: "Password is required."
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit
                $('.alert-danger', $('.frmlogin')).show();
            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                        .closest('.input-field').addClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.input-field').removeClass('has-error');
                label.remove();
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element.closest('.input-icon'));
            },
            submitHandler: function (form) {
                var username = $("#username").val();
                var password = $("#password").val();
                var data = $(form).serialize();

                $.ajax({
                    type: "post",
                    url: userurl + "account/login",
                    data: {username: username, password: password},
                    success: function (response) {
                        output = JSON.parse(response);
                        if (output.status == 'success') {
                            $('.alert-success').fadeIn(1000, function () {
                                $(".alert-success").html(output.message).fadeOut(5000);
                            });
                            location.href = baseurl;
                        } else if (output.status == 'error') {
                            $('.alert-danger').fadeIn(1000, function () {
                                $(".alert-danger").html(output.message).fadeOut(5000);
                            });
                        } else if (output.status == 'warning') {
                            $('.alert-warning').fadeIn(1000, function () {
                                $(".alert-warning").html(output.message).fadeOut(5000);
                            });
                        }
                    }
                });
            }
        });

        $('#frmlogin').keypress(function (e) {
            e.stopImmediatePropagation();
            if (e.which == 13) {
                if ($('#frmlogin').validate().form()) {
                    $('#frmlogin').submit();
                }
                return false;
            }
        });
    }

    var handleForgetPassword = function () {
        $('.forget-form').validate({
            errorElement: 'span', //default input error message container
            errorClass: 'help-block', // default input error message class
            focusInvalid: false, // do not focus the last invalid input
            ignore: "",
            rules: {
                email: {
                    required: true,
                    email: true
                }
            },
            messages: {
                email: {
                    required: "Email is required."
                }
            },
            invalidHandler: function (event, validator) { //display error alert on form submit   

            },
            highlight: function (element) { // hightlight error inputs
                $(element)
                        .closest('.form-group').addClass('has-error'); // set error class to the control group
            },
            success: function (label) {
                label.closest('.form-group').removeClass('has-error');
                label.remove();
            },
            errorPlacement: function (error, element) {
                error.insertAfter(element.closest('.input-icon'));
            },
            submitHandler: function () {
                var forgetemail = $('#forgetemail').val();
                $.ajax({
                    type: "POST",
                    url: baseurl + "account/forgot_password",
                    data: {"forgetemail": forgetemail},
                    success: function (response) {
                        if (response == 'success') {
                            $('.alert-danger', $('.forget-form')).hide();
                            $('.alert-success').children('span').html('Password Sent To your Email.');
                            $('.alert-success', $('.forget-form')).show();
                            $('.login-form').show();
                            location.href = baseurl;
                        }
                        else {
                            $('.alert-danger').children('span').html('Invalid Email address.');
                            $('.alert-danger', $('.forget-form')).show();
                            $('.alert-success', $('.forget-form')).hide();
                        }
                    }
                });
            }
        });

        $('.forget-form input').keypress(function (e) {
            e.stopImmediatePropagation();
            if (e.which == 13) {
                if ($('.forget-form').validate().form()) {
                    $('.forget-form').submit();
                }
                return false;
            }
        });

        jQuery('#forget-password').click(function () {
            jQuery('.login-form').hide();
            jQuery('.forget-form').show();
        });

        jQuery('#back-btn').click(function () {
            jQuery('.login-form').show();
            jQuery('.forget-form').hide();
        });

    }

    var handleResetPassword = function () {

        var form = $('#resetpwd_form');
        var rules = {
            password: {
                required: true
            },
            repassword: {
                required: true,
                equalTo: "#pwd"
            }
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form);
            return false;
        });
    }

    var handleSetPassword = function () {

        var form = $('#setPassword_frm');
        var rules = {
            password: {
                required: true
            },
            re_password: {
                required: true,
                equalTo: "#password"
            }
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form);
            return false;
        });
    }

    return {
        //main function to initiate the module
        init: function () {
            handleLogin();
            handleForgetPassword();
            handleResetPassword();
        },
        signup_init: function () {
            handleSignup();
        },
        set_pass_init: function () {
            handleSetPassword();
        }
    };

}();