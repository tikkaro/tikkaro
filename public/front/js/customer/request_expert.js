var Request_expert = function () {

    var General = function () {

    };

    var HandleSendMail = function () {
        var form = $('#frm-request-expert');
        var rules = {
            projectOnline: {required: true},
            peopleType: {required: true},
            IndustryExpert: {required: true},
            Geographies: {required: true},
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form);
            return false;
        });
    }

    return {
        init: function () {
            General();
            HandleSendMail();
        }
    };
}();