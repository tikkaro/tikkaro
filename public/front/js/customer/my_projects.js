var My_projects = function () {

    var general = function () {

        $('body').on('blur', '.ProjectDetails', function () {
            var that = this;

            if ($(this).data('edit-field') == 'ProjectDesc') {
                ajaxcall(customerurl + 'my-projects/editProject', {
                    id: $(this).data('id'),
                    value: $(this).val(),
                    editType: 'ProjectDetails',
                    editField: $(this).data('edit-field')
                }, function (output) {
                    output = JSON.parse(output);
                    $(that).attr('data-value', $(that).val());
                });
            }
        });

        $('body').on('click', '.my-projects-panel', function () {
            var projectDetailId = $(this).data('project-id');
            if ($(projectDetailId).is(':hidden')) {
                // $('.projectDetailsArea').slideUp('500');
                $(projectDetailId).slideDown('500');
            } else {
                $(projectDetailId).slideUp('500');
            }
        });
    };

    return {
        init: function () {
            general();
        }
    };
}();