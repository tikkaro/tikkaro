var Call_schedule = function () {

    var handleCallScheduling = function () {
        $('body').on('click','.bookbtn',function(){
            var bookId = $(this).attr('data-id');
            var data = {bookId:bookId};
            var url = baseurl + 'scheduling/clientSelectTimeAvailability';
            ajaxcall(url,data,function (output) {
                output = JSON.parse(output);
                if (output.message != '') {
                    Toastr.init(output.status, output.message, '');
                }

            });
        })
    };

    return {
        init: function () {
            handleCallScheduling();
        },
    };
}
();