var Profile = function () {

    var profile_info = function () {

        var form = $('#admin_infor');
        var rules = {
            email: {required: true, email: true},
            // uname: {required: true},
            name: {required: true},
            // lname: {required: true},
            dob: {required: true},
            // mnumber: {required: true, number: true},
            address: {required: true}
        };
        handleFormValidate(form, rules);
    };

    var change_pass = function () {
        var form = $('#change_password');
        var rules = {
            old_pwd: {required: true},
            new_pwd: {required: true},
            conf_pwd: {required: true, equalTo: "#new_pwd"}
        };
        handleFormValidate(form, rules);
    };

    var change_image = function () {

        // $('.profilePicture').change(function (e) {
        var form = $('#change_picture');
        var rules = {
            // 'profilePicture': {required: true}
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form, true);
        });
        // });
    };
    return {
        //main function to initiate the module
        init: function () {
            profile_info();
            change_pass();
            change_image();
        },
    };
}();
