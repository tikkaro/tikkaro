var accountmanagerDatatables = null;

var Staff = function () {

    var handleDatatable = function () {
        accountmanagerDatatables = getDataTable('#accountmanager_list', adminurl + 'staff/getAccountManagerList', {
            dataTable: {
                "columnDefs": [{
                    "searchable": false,
                    "targets": [-1]
                }]
            }
        });
    };

    var dateformat = function () {
        $('#var_Manage_Dob').datepicker({
            format: 'yyyy-mm-dd',
            autoclose: true
        });
    };

    var addAccount = function () {
        var form = $('#addAccountManagerFrm');
        var rules = {
            var_usrname: {required: true},
            var_email: {required: true, email: true},
            var_password: {required: true},
            var_manage_dob: {required: true},
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form, true);
            return false;
        });
    }

    var editAccount = function () {

        var form = $('#editAccountManagerFrm');
        var rules = {
            var_usrname: {required: true},
            var_email: {required: true, email: true},
            var_manage_dob: {required: true},
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form, true);
            return false;
        });
    }


    return {
        init: function () {
            handleDatatable();
            handleDelete();
        },
        Add_init: function () {
            addAccount();
            dateformat();
        },
        Edit_init: function () {
            editAccount();
            dateformat();
        }
    };
}();
