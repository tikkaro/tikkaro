var Resetpassword = function () {

    var handleUserResetPassword = function(){
        var form = $('#UserResetPass');
        var rules = {
            'password': {required: true},
            'confirmpass': {required: true,equalTo:'#password'},
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form);
        });
    }
    return {
        //main function to initiate the module
        reset_user_pass: function(){
            handleUserResetPassword();
        }
    };

} ();