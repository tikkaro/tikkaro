var clientDatatables = null;
var overviewDatatables = null;
var projectDatatables = null;
var notesDatatables = null;
var usernotesDatatables = null;
var clientcallDatatables = null;
var userprojectDatatables = null;
var Client = function () {

    var handleDatatable = function () {
        clientDatatables = getDataTable('#client_list', adminurl + 'client/getClientList', {
            dataTable: {
                "columnDefs": [{
                    "searchable": false,
                    "targets": [-1,7]
                }]
            }
        });
    };
    // var handleDatatable = function () {
    //     clientDatatables = getDataTable('#client_list', adminurl + 'client/getClientList', {
    //         dataTable: {
    //             "columnDefs": [{
    //                 "searchable": false,
    //                 "targets": [-1]
    //             }]
    //         }
    //     });
    // };
    var addClient = function () {
        var form = $('#addClientFrm');
        var rules = {
            company_name: {required: true},
            team: {required: true},
            // phone: {number: true},
            ac_manager: {required: true},
            set_priority: {required: true}
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form, true);
            return false;
        });
    }


    var handleOverviewDatatables = function () {

        if (typeof clientId !== 'undefine' && clientId != '') {
            overviewDatatables = getDataTable('#overview_lists', adminurl + 'client/getOverviewList/' + clientId, {
                dataTable: {
                    "columnDefs": [{
                        "searchable": false,
                        "targets": [-1]
                    }]
                }
            });
        }

    };
    var handleProjectDatatables = function () {
        if (typeof clientId !== 'undefine' && clientId != '') {
            projectDatatables = getDataTable('#project_list', adminurl + 'client/getProjectList/' + clientId, {
                dataTable: {
                    "columnDefs": [{
                        "searchable": false,
                        "targets": [-1]
                    }]
                }
            });
        }
    };
    var handleClientCallDatatables = function () {
        clientcallDatatables = getDataTable('#client_call_list', adminurl + 'client/getClientCallList', {
            dataTable: {
                "columnDefs": [{
                    "searchable": false,
                    "targets": [-1]
                }]
            }
        });
    };
    var handleUserCallDatatables = function () {
        clientcallDatatables = getDataTable('#user_call_list', adminurl + 'client/getUserCallList', {
            dataTable: {
                "columnDefs": [{
                    "searchable": false,
                    "targets": [-1]
                }]
            }
        });
    };
    var handleNotesDatatables = function () {

        if (typeof clientId !== 'undefine' && clientId != '') {
            notesDatatables = getDataTable('#notes_list', adminurl + 'client/getNotesList/' + clientId, {
                dataTable: {
                    "columnDefs": [{
                        "searchable": false,
                        "targets": [-1]
                    }]
                }
            });
        }

    };
    var handleUserNotesDatatables = function () {
        if (typeof userID !== 'undefined' && userID != '') {
            usernotesDatatables = getDataTable('#user_notes_list', adminurl + 'client/getUserNotesList/' + userID, {
                dataTable: {
                    "columnDefs": [{
                        "searchable": false,
                        "targets": [-1]
                    }]
                }
            });
        }

    };
    var handleUserProjectDatatables = function () {
        usernotesDatatables = getDataTable('#user_project_list', adminurl + 'client/getUserProjectList', {
            dataTable: {
                "columnDefs": [{
                    "searchable": false,
                    "targets": [-1]
                }]
            }
        });
    };
    var handleRules = function () {
        $('body').on('click', '.addNewRules', function () {
            var value = $(".company_rules").val();
            var Cusvalue = $(".company_Cusrules").val();
            var client_id = $(this).attr('data-client-id');
            var assign_account_id = $(this).attr('data-assign-account-id');
            var data = {
                'optionvlaue': value,
                'textvalue': Cusvalue,
                'clientid': client_id,
                'assign_account_id': assign_account_id
            };
            var url = adminurl + 'client/addRules';

            if (value != '' || Cusvalue != '') {
                ajaxcall(url, data, function (output) {
                    handleAjaxResponse(output);
                });
            } else {
                Toastr.init('error', 'Please select or enter new rule', '');
            }
        });
    };
    var addUser = function () {
        var form = $('#addUserFrm');
        var rules = {
            'var_fname': {required: true},
            'var_lname': {required: true},
            'var_email': {email: true},
            'var_password': {required: true},
            // 'var_dob': {required: true},
            // 'bint_phone': {number: true},
            // 'var_position': {required: true},
            // 'user_address': {required: true},
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form, true);
        });
    };


    var addProject = function () {
        var form = $('#addClientProjectFrm');
        var rules = {
            'user_id': {required: true},
            'var_project': {required: true},
            'project_description': {required: true},
            'dt_initiate': {required: true},
            'dt_deadline': {required: true},
            'var_target_company': {required: true},
            'client_anonymous': {required: true},
            'target_anonymous': {required: true},
            'var_country_interest': {required: true},
            'var_companies_interest': {required: true},
            'var_geography_interest': {required: true},
            'var_industry': {required: true},
            'priority': {required: true},
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form, true);
        });
    };

    var handleHistory = function () {
        $('#sub_type').on('change', function () {
            if (this.value == '1') {
                $("#unit_block").show();
                $("#pay").hide();
            } else {
                $("#pay").show();
                $("#unit_block").hide();
            }
        });
    };


    var handleNote = function () {
        $('body').on('click', '.btn-edit-note', function () {

            var noteId = $(this).attr('data-note-id');
            var userId = $(this).attr('data-user-id');
            var clientId = $(this).attr('data-client-id');
            $('.note_id').val(noteId);
            $('.user_note').val('');
            $('.user_id').val(userId);
            $('.client_id').val(clientId);
            if (typeof noteId !== 'undefined' && noteId != '') {
                ajaxcall(adminurl + 'client/getNoteById', {noteId: noteId}, function (output) {
                    output = JSON.parse(output);
                    var notes = output.txt_note;
                    $('.user_note').val(notes);
                    $('.user_id').val(output.fk_user);
                    $('.client_id').val(output.fk_client);
                    $('.note_id').val(noteId);
                    $('.modal-title').text('Edit Notes');
                    $('#add_notes_model').modal('show');
                });
            } else {
                $('.modal-title').text('Add Notes');
                $('#add_notes_model').modal('show');
            }

        });
    };

    var addUserNote = function () {
        var form = $('#add_note_frm');
        var rules = {
            'notes': {required: true},
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form);
        });
    };

    var handleClientNote = function () {
        $('body').on('click', '.edit_client_notes', function () {
            var clientId = $(this).attr('data-client-id');
            var assignAcManager = $(this).attr('data-assign-ac-manager');
            var noteId = $(this).attr('data-note-id');
            $('#fk_client').val(clientId);
            $('#fk_assign_ac_manager').val(assignAcManager);
            $('#note_id').val(noteId);
            $('.client_notes').val('');
            if (typeof noteId !== 'undefined') {
                ajaxcall(adminurl + 'client/getClientNoteById', {noteId: noteId}, function (output) {
                    output = JSON.parse(output);
                    var notes = output.txt_note;
                    $('.client_notes').val(notes);
                    $('#fk_client').val(output.fk_client);
                    $('#fk_assign_ac_manager').val(output.fk_assign_account_manager);
                    $('#note_id').val(noteId);
                    $('.modal-title').text('Edit Notes');
                    $('#add_client_notes').modal('show');
                });
            } else {
                $('.modal-title').text('Add Notes');
                $('#add_client_notes').modal('show');
            }

        });
    }

    var addClientNote = function () {
        var form = $('#add_client_notes_frm');
        var rules = {
            'notes': {required: true},
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form);

        });
    };

    var handleOfficer = function () {
        $('.edit_complaince_officer').on('click', function () {
            var id = $(this).attr('data-id');
            var client_id = $(this).attr('data-client-id');
            var account_id = $(this).attr('data-account-id');
            $('#edit_compliance_modal').modal('show');
            $('#fk_client').val(client_id);
            $('#fk_assign_account').val(account_id);
            $('#officer_id').val(id);
            if (id != '') {
                var data = {'officer_id': id, 'client_id': client_id, 'account_id': account_id};
                var url = adminurl + 'client/getOfficer';
                ajaxcall(url, data, function (output) {
                    output = JSON.parse(output);
                    $('#name').val(output.var_name);
                    $('#email').val(output.var_email);
                    $('#phone').val(output.bint_phone);
                });
            }
        });
    }


    var editOfficer = function () {
        var form = $('#edit_compliance');
        var rules = {
            'name': {required: true},
            'email': {required: true, email: true},
            'phone': {required: true},
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form, true);
        });
    }

    var addSubscriptionGeneral = function () {
        $('body').on('change', '.subscription_type', function () {
            var subType = $(this).val();
            console.log(subType);
            if (subType != '' && subType == 'UNIT-BLOCK') {
                $('#dt_end_date').rules("remove");
                $('#subcription_time').rules("add", {required: true});
                $('#no_of_unit').rules("add", {required: true, number: true});
                $('.pay-as-you-div').addClass('hide');
                $('.unit-block-div').removeClass('hide');
            } else {
                $('#subcription_time').rules("remove");
                $('#no_of_unit').rules("remove");
                $('#dt_end_date').rules("add", {required: true});
                $('.pay-as-you-div').removeClass('hide');
                $('.pay-as-you-div').removeClass('hide');
                $('.unit-block-div').addClass('hide');
            }
        });

        $('#add_subscription_model').on('hidden.bs.modal', function () {
            $(this).find("input,textarea").val('').end();

        });
    }

    var addSubcription = function () {
        var form = $('#add_subscription_frm');
        var rules = {
            // 'subscription_type': {required: true},
            'dt_start_date': {required: true},
            'subcription_time': {required: true},
            'no_of_unit': {required: true, number: true},
            'price_per_unit': {required: true, number: true}
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form);
        });
    }


    var userResetpass = function () {
        var checkComplete = true;
        $('body').on('click', '.resetpassword', function () {
            var id = $(this).attr('data-id');
            var email = $(this).attr('data-email');
            console.log(checkComplete);
            if (checkComplete) {
                checkComplete = false;
                ajaxcall(adminurl + 'client/userResetpassword', {id: id, email: email}, function (output) {
                    handleAjaxResponse(output);
                    setTimeout(function () {
                        checkComplete = true;
                    }, 1000);

                });
            }

        })
    }


    return {
        init: function () {
            handleDatatable();
            handleDelete();

        },
        addClient: function () {
            addClient();
        },
        overview_init: function () {
            addSubcription();
            addSubscriptionGeneral();
            handleOverviewDatatables();
        },
        project_init: function () {
            handleProjectDatatables();
        },
        add_project: function () {
            addProject();
        },
        notes_init: function () {
            handleNotesDatatables();
            handleClientNote();
            addClientNote();
            handleDelete();
        },
        user_reset_pass: function () {
            userResetpass();
        },
        user_notes_init: function () {
            handleNote();
            addUserNote();
            handleDelete();
            handleUserNotesDatatables();
        },
        rules_init: function () {
            handleRules();
            handleDelete();
        },
        addUser: function () {
            addUser();
        },

        officer: function () {
            handleOfficer();
            editOfficer();
        },
        history_init: function () {
            handleHistory();
        },
        call_history: function () {
            handleClientCallDatatables();
        },
        user_call_history: function () {
            handleUserCallDatatables();
        },
        user_project_list: function () {
            handleUserProjectDatatables();
        },

    };
}();