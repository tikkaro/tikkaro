var expertsDatatables = null;
var notesDatatables = null;
var projectsDatatables = null;
var employeementDatatables = null;
var educationHistoryDatatables = null;
var callhistoryDatatables = null;

var Experts = function () {

    var handleDatatable = function () {
        expertsDatatables = getDataTable('#experts_list', adminurl + 'experts/getExpertsList', {
            dataTable: {
                "columnDefs": [{
                    "searchable": false,
                    "targets": [-1]
                }]
            }
        });
    };
    var handleNotesDatatables = function () {
        if (typeof expertId !== 'undefined' && expertId != '') {
            notesDatatables = getDataTable('#notes_list', adminurl + 'experts/getNotesList/' + expertId, {
                dataTable: {
                    "columnDefs": [{
                        "searchable": false,
                        "targets": [-1]
                    }]
                }
            });
        }

    };

    var handleExpertsNote = function () {
        $('body').on('click', '.expert-notes', function () {
            var noteId = $(this).attr('note-id');
            var expertId = $(this).attr('expert-id');

            $('.expert_id').val(expertId);
            if (typeof noteId !== 'undefined' && noteId != '') {
                ajaxcall(adminurl + 'experts/getExpertNoteById', {noteId: noteId}, function (output) {
                    output = JSON.parse(output);
                    $('.note_id').val(noteId);
                    var notes = output.txt_note;
                    $('.exp_note').val(notes);
                    $('.modal-title').text('Edit Notes');
                    $('#edit_notes_model').modal('show');
                });
            } else {
                $('.note_id').val('');
                $('.exp_note').val('');
                $('.modal-title').text('Add Notes');
                $('#edit_notes_model').modal('show');
            }

        });
    };

    var addExpertNote = function () {
        var form = $('#expert_note_frm');
        var rules = {
            'notes': {required: true},
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form);
        });
    };

    var handleprojectsDatatable = function () {
        projectsDatatables = getDataTable('#projectList', adminurl + 'Experts/getProjectList/' + expertId, {
            dataTable: {
                "columnDefs": [{
                    "searchable": false,
                    "targets": [-1]
                }]
            }
        });
    };
    // var handleEmployeementDatatable = function () {
    //     if (typeof expertId !== 'undefined' && expertId != '') {
    //         employeementDatatables = getDataTable('#employeement_history', adminurl + 'Experts/getEmployeementList/' + expertId, {
    //             dataTable: {
    //                 "columnDefs": [{
    //                         "searchable": false,
    //                         "targets": [-1]
    //                     }]
    //             }
    //         });
    //     }
    //
    // };

    // var handleEducationDatatable = function () {
    //     if (typeof expertId !== 'undefined' && expertId != '') {
    //         educationHistoryDatatables = getDataTable('#education_history', adminurl + 'Experts/getEducationList/' + expertId, {
    //             dataTable: {
    //                 "columnDefs": [{
    //                         "searchable": false,
    //                         "targets": [-1]
    //                     }]
    //             }
    //         });
    //     }
    //
    // };
    var handleCallHistoryDatatable = function () {
        callhistoryDatatables = getDataTable('#expert_call_list', adminurl + 'Experts/getCallHistoryList', {
            dataTable: {
                "columnDefs": [{
                    "searchable": false,
                    "targets": [-1]
                }]
            }
        });
    };

    var findexperts = function () {

        $('.goToProject').on('click', function () {
            var project = $('.project').val();
            if (project === null) {
                project = '';
            }
            if (project == '') {
                Toastr.init('warning', 'Please select Project', '');
            } else {
                var url = adminurl + 'project/overview/' + project;
                window.open(url);
            }
        })

        if (clientId != '' && projectId != '') {
            setTimeout(function () {
                $('.experts_search').trigger('click');
            }, 1000);
        }

        if ((clientId != '' && projectId != '') && (expertsName != '' || position != '' || geoGraphy != '' || expretise != '' || currentCompany != '' || formerCompany != '')) {
            setTimeout(function () {
                $('.expertsCustomeSearch').trigger('click');
            }, 1000);
        }

        $('body').on('change', '.client', function () {
            var client_id = $(this).val();
            ajaxcall(adminurl + 'find_experts/clientProject', {'client_id': client_id}, function (output) {
                var output = JSON.parse(output);

                if (output.length === 0) {
                    $('.project').html('');
                    Toastr.init('warning', 'No Project Found for this client', '');
                } else {
                    var html = '<option value=""></option>';
                    for (var i = 0; i < output.length; i++) {
                        html += '<option value=' + output[i].id + ' data-user-id=' + output[i].fk_user + '>' + output[i].var_project_name + '</option>'
                    }
                    $('.project').html(html);
                }
            })
        })


        $('.experts_search').on('click', function () {
            var client = $('.client').val();
            var project = $('.project').val();
            var user = $('.project option:selected').attr('data-user-id');
            var assign_account = $('.client option:selected').attr('data-account-id')
            var data = {'client_id': client, project_id: project, user_id: user, assign_account: assign_account};
            var url = adminurl + 'find_experts/expertFind';
            if (project === null) {
                project = '';
            }
            if (client != '' && project != '') {
                findExprts(data, url);
            } else {
                Toastr.init('warning', 'Please select Client and Project', '');
            }
        });

        $('body').on('click', '.expertsCustomeSearch', function () {
            var client = $('.client').val();
            var project = $('.project').val();
            var expertname = $('#experts_name').val();
            var geography = $('#experts_geography').val();
            var expretise = $('#expertise').val();
            var position = $('#experts_position').val();
            var currentcompany = $('#current_company').val();
            var formercompany = $('#formar_company').val();
            var user = $('.project option:selected').attr('data-user-id');
            var assign_account = $('.client option:selected').attr('data-account-id')
            var data = {
                'client_id': client,
                'project_id': project,
                'user_id': user,
                'assign_account': assign_account,
                'expertName': expertname,
                'geography': geography,
                'expretise': expretise,
                'position': position,
                'currentCompany': currentcompany,
                'formerCompany': formercompany
            };
            var url = adminurl + 'find_experts/expertFind';

            findExprts(data, url);
        })


        function findExprts(data, url) {
            ajaxcall(url, data, function (output) {
                $('.experts_fliter').show();
                $('.findexperts').html(output);
            });
        }
    }

    var addtoShortlist = function () {
        $('body').on('click', '.addtoShortlist', function () {
            var data = {
                client_id: $(this).data('client-id'),
                project_id: $(this).data('project-id'),
                user_id: $(this).data('user-id'),
                experts_id: $(this).data('experts-id'),
                assign_account_id: $(this).data('assign-account-id')
            };
            var url = adminurl + 'find_experts/addShortlist';
            ajaxcall(url, data, function (output) {
                handleAjaxResponse(output);
                setTimeout(function () {
                    $('.expertsCustomeSearch').trigger('click');
                }, 1000);
            })
        })
    }

    var expertsAdd = function () {
        var form = $('#addExpertsFrm');
        var rules = {
            var_fname: {required: true},
            var_lname: {required: true},
//            var_companyname: {required: true},
            var_email: {email: true},
//            var_country: {required: true},
//            var_ratepersurvey: {required: true},
//            'var_college[]': {required: true},
//            'var_certificates[]': {required: true},
//            'var_position[]': {required: true},
//            'txt_resposibility[]': {required: true},
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form, true);
            return false;
        });

        $('body').on('click', '.addmoreEdution', function () {
            var education = $('.eduction_history').html();
            $('.addEdution').append(education);
            $('.date-picker').datepicker();
        });

        $('body').on('click', '.eductionRemove', function () {
            $(this).parents('.eduction').remove();
        })

        $('body').on('click', '.employeeRemove', function () {
            $(this).parents('.employment').remove();
        })

        var count = 0;
        $('body').on('click', '.addmoreEmployment', function () {
            count++;
            var employee = $('.employment_history').html().replace(/\@/g, count);
            $('.addEmployment').append(employee);
            $('.date-picker').datepicker();
        })
    }

    var overviewTags = function () {
        $('body').on('click', '.cusComapnine', function () {
            $(this).hide();
            $(this).parent('.control-label').next('.Tagstoshow').show();
        });
        $('body').on('click', '.cancleTags', function () {
            $(this).parents('.Tagstoshow').hide();
            $(this).parents('.col-md-8').find('.cusComapnine').show();
        });

        $('body').on('click', '.updateTags', function () {
            var that = $(this);
            var value = $(this).parents('.Tagstoshow').find('input').val();
            var id = $(this).attr('data-id');
            var table = $(this).attr('data-table');
            ajaxcall(adminurl + 'experts/updateTags', {value: value, id: id, table: table}, function (output) {
                if (value == '') {
                    that.parents('.col-md-8').find('.cusComapnine').html('<span style="color: #DD1144"><i>Empty</i></span>');
                } else {
                    value = value.replace(/\,/g, ' , ');
                    that.parents('.col-md-8').find('.cusComapnine').html(value);
                }
                that.parents('.col-md-8').find('.cusComapnine').show();
                that.parents('.Tagstoshow').hide();
            });
        });

        $('body').on('click','.expertsComplianceDate',function () {
            $(this).hide();
            $(this).parent('.control-label').next('.dateToshow').show();
        });

        $('body').on('click', '.cancelDates', function () {
            $(this).parents('.dateToshow').hide();
            $(this).parents('.col-md-8').find('.expertsComplianceDate').show();
        });

        $('body').on('click', '.updateDates', function () {
            var that = $(this);
            var value = $(this).parents('.dateToshow').find('input').val();
            var id = $(this).attr('data-id');
            var table = $(this).attr('data-table');
            ajaxcall(adminurl + 'experts/updateComplianceDate', {value: value, id: id, table: table}, function (output) {
                if (value == '') {
                    that.parents('.col-md-8').find('.expertsComplianceDate').html('<span style="color: #DD1144"><i>Compliance training incomplete</i></span>');
                } else {
                    that.parents('.col-md-8').find('.expertsComplianceDate').html(value);
                }
                that.parents('.col-md-8').find('.expertsComplianceDate').show();
                that.parents('.dateToshow').hide();
            });
        });
    }

    var expertBioEdit = function () {
        var form = $('#edit_bio_frm');
        var rules = {
            var_fname: {required: true},
            var_lname: {required: true},
            var_companyname: {required: true},
            var_email: {required: true, email: true},
            var_country: {required: true},
            var_ratepersurvey: {required: true},
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form, true);
            return false;
        });
    }

    var handleExpertEducation = function () {
        $('body').on('click', '.edit_education', function () {
            var eduId = $(this).attr('data-id');
            if (typeof eduId != 'undefined' && eduId != '') {
                $('#edit_education_frm .modal-title').text('Edit Education');
                ajaxcall(adminurl + 'experts/getEducationHistoryById', {id: eduId}, function (output) {
                    $('#edit_bio_education_model').modal('show');
                    $('#edit_bio_education_model').html(output);
                    ComponentsDateTimePickers.init();
                    expertEducationEdit();
                });
            } else {
                $('#edit_education_frm .modal-title').text('Add Education');
                $('#edit_education_frm').find("input[type=text]").val("");
                $('#edit_bio_education_model').modal('show');
            }
        });

        $('#add_bio_employeement_model').on('hidden.bs.modal', function (e) {
            $(this)
                .find("input,textarea,select")
                .val('')
                .end()
                .find("input[type=checkbox], input[type=radio]")
                .prop("checked", "")
                .end();
            $('.to').show();

        })
    }

    var expertEducationEdit = function () {
        var form = $('#edit_education_frm');
        var rules = {
            'var_college': {required: true},
            'from': {required: true},
            'to': {required: true},
            'var_certificates': {required: true},
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form);
            return false;
        });

    }


    var getExpertEmployment = function () {
        $('body').on('click', '.edit', function () {
            var histId = $(this).attr('data-id');
            ajaxcall(adminurl + 'experts/getEmployHistoryById', {histId: histId}, function (output) {
                output = JSON.parse(output);
                console.log(output);
                $('.history_id').val(histId);
                $('.var_company').val(output.var_company);
                $('.var_position').val(output.var_position);
                $('.var_geography').val(output.var_geography);
                $('.txt_resposibility').text(output.txt_responsibilities);
                $('.history_from').val(output.dt_from_respective_date);
                if (output.enum_present == 'YES') {
                    $('.to').hide();
                    $('.present').show();
                    $('#check1').attr('checked', 'checked');
                    $.uniform.update();
                } else {
                    $('#check1').removeAttr('checked');
                    $.uniform.update();
                    $('.present').hide();
                    $('.to').show();
                    if (output.dt_to_respective_date != '') {
                        $('.history_to').val(output.dt_to_respective_date);
                    }

                }
                $('#edit_bio_employeement_model').modal('show');
            });

        });


    }

    var editExpertEmployment = function () {
        var form = $('#edit_employeement_frm');
        var rules = {
            'var_company': {required: true},
            'var_position': {required: true},
            'var_geography': {required: true},
            'txt_resposibility': {required: true},
            'history_from': {required: true},
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form, true);
            return false;
        });

    }


    var expertEmploymentAdd = function () {
        var form = $('#add_employeement_frm');
        var rules = {
            'var_company': {required: true},
            'var_position': {required: true},
            'var_geography': {required: true},
            'txt_resposibility': {required: true},
            'history_from': {required: true},
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form, true);
            return false;
        });
    }

    var handleExperience = function () {
        $('body').on('click', '.checkbox6', function () {
            if ($(this).is(':checked')) {
                $(this).closest('.form-group').prev('.respectiveDates').find(".checkVal").val('1');
                $(this).closest('.form-group').prev('.respectiveDates').find(".present").show();
                $(this).closest('.form-group').prev('.respectiveDates').find(".to").hide();
            } else {
                $(this).closest('.form-group').prev('.respectiveDates').find(".checkVal").val('0');
                $(this).closest('.form-group').prev('.respectiveDates').find(".to").show();
                $(this).closest('.form-group').prev('.respectiveDates').find(".present").hide();
            }

        });
    };


    var handleExpertBlock = function () {

        $('body').on('click', '.block', function () {
            var id = $(this).attr('data-id');
            $('#block_model').modal('show');
            $('#block_frm').attr('action', adminurl + 'experts/blockExperts');
            $('#expertBlockId').attr('value', id);
        });

        $('body').on('click', '.unblock', function () {
            var id = $(this).attr('data-id');
            $('#unblock_experts').modal('show');
            $('#experts_unblock').attr('data-id', id);
        });

        $('body').on('click', '#experts_unblock', function () {
            var id = $(this).attr('data-id');
            ajaxcall(adminurl + 'experts/unblockExperts', {'id': id}, function (output) {
                handleAjaxResponse(output);
            })
        });

        $('.checkbox1').on('click', function () {
            if ($(this).is(':checked')) {
                $('.from').attr("disabled", "disabled");
                $('.to').attr("disabled", "disabled");
            } else {
                $('.from').removeAttr('disabled');
                $('.to').removeAttr('disabled');
            }
        });

        $('body').on('click','.resetPassword',function(){
            var id = $(this).attr('data-id');
            $('#experts_reset_password').modal('show');
            $('.expertsResetPassword').attr('data-id',id);
        });

        $('body').on('click','.expertsResetPassword',function(){
            var id = $(this).attr('data-id');

            ajaxcall(adminurl + 'experts/resetPassword', {'id': id}, function (output) {
                handleAjaxResponse(output);
            })
        })
    };


    var addBExpertBlock = function () {
        var form = $('#block_frm');
        var rules = {
            txt_reason: {
                required: true,
            }
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form);
            return false;
        });
    }

    return {
        init: function () {
            handleDatatable();
            handleExpertBlock();
            addBExpertBlock();
            expertBioEdit();
            handleExpertEducation();
            expertEducationEdit();
            getExpertEmployment();
            editExpertEmployment();
            expertEmploymentAdd();
            handleExperience();
            overviewTags();

        },
        notes_init: function () {
            handleNotesDatatables();
            handleExpertsNote();
            addExpertNote();
            handleDelete();

        },
        projects_init: function () {
            handleprojectsDatatable();
        },
        findexperts: function () {
            findexperts();
            addtoShortlist();
        },
        expertsadd: function () {
            expertsAdd();
            handleExperience();
            $(".select2, .select2-multiple").select2({
                width: null
            });
        },
        call_history: function () {
            handleCallHistoryDatatable();
        }

    };
}();