var teamDatatables = null;

var Team = function () {

    var handleDatatable = function () {
        teamDatatables = getDataTable('#team_list', adminurl + 'team/getTeamList', {dataTable: {"columnDefs": [{"searchable": false, "targets": [-1]}]}});
    };

    var addTeam = function () {
        var form = $('#addTeamFrm');
        var rules = {
            var_team: {required: true},
            // var_discription: {required: true}
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form, true);
            return false;
        });
    }
    var editTeam = function () {
        var form = $('#editTeamFrm');
        var rules = {
            var_team: {required: true},
            // var_discription: {required: true}
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form, true);
            return false;
        });
    }




    return {
        init: function () {
            handleDatatable();
            handleDelete();
        },
        add_team: function () {
            addTeam();
        },
        edit_team: function () {
            editTeam();
        }
    };
}();