var projectsDatatables = null;
var Dashboard = function () {

    var handleprojectsDeadlineDatatable = function () {
        projectsDatatables = getDataTable('#deadLineprojectList', adminurl + 'dashboard/deadLineNearProjects',
            {dataTable:
                {
                    "columnDefs": [{"searchable": false, "targets": [-1]}],
                    "language": { "emptyTable": "No project deadlines within the next 10 days"},
                },

            });
    };
    
    return {
        //main function to initiate the module
        init: function () {
            handleprojectsDeadlineDatatable();
        },
    };
}();
