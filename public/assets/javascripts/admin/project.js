var projectDatatables = null;
var projectCallDatatables = null;
var projectNotesDatatables = null;
var Project = function () {

    var handleDatatable = function () {
        projectDatatables = getDataTable('#projectList', adminurl + 'project/getProjectList', {
            dataTable: {
                "columnDefs": [{
                    "searchable": false,
                    "targets": [-1]
                }],
                "order": [
                    [9, "desc"]
                ],
            }

        });

        $('.project_custome_search').keyup(function () {
            var column = $(this).attr('data-column');
            projectDatatables.setAjaxParam('projectName', $('input[name="project_name"]').val());
            projectDatatables.setAjaxParam('clientName', $('input[name="client_name"]').val());
            projectDatatables.getDataTable().draw();
        });
        $('.project_custome_search').change(function () {
            var column = $(this).attr('data-column');
            projectDatatables.setAjaxParam('clientValue', $(this).val());
            projectDatatables.getDataTable().draw();
        });
    };


    var handleOverviewDatatables = function () {
        projectCallDatatables = getDataTable('#callList', adminurl + 'project/getProjectCallList', {
            dataTable: {
                "columnDefs": [{
                    "searchable": false,
                    "targets": [-1]
                }]
            }
        });
    };
    var handleNotesDatatables = function () {
        if (typeof projectId !== 'undefined' && projectId != '') {
            projectNotesDatatables = getDataTable('#noteList', adminurl + 'project/getNotesList/' + projectId, {
                dataTable: {
                    "columnDefs": [{
                        "searchable": false,
                        "targets": [-1]
                    }],
                }
            });
        }

    };

    var addProject = function () {

        var form = $('#addClientProjectFrm');
        var rules = {
            'var_project': {required: true},
            'project_description': {required: true},
            'var_industry': {required: true},
            'user_id': {required: true},
            'client_id': {required: true},
            'var_number_call': {
                number: true,
                maxlength: 5
            },
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form);
        });
    };

    var generalProject = function () {


        /* Project has company*/

        // var companyUrl = adminurl + 'project/getCompany';

        //
        // /* Project has regions*/
        //


        /* Project has country */
        // var countryUrl = adminurl + 'project/getCountry';


        /*Get User base on client selection*/

        $('body').on('change', '.client_select', function () {
            var clientId = $(this).val();
            var clientAssignManager = $('option:selected', this).attr('assign-ac-manager');
            $('.clientAssignAcManager').val(clientAssignManager);
            ajaxcall(adminurl + 'project/getUserByClientId', {clientId: clientId}, function (output) {
                output = JSON.parse(output);
                var html = ' <option value=""></option>';
                for (var i = 0; i < output.length; i++) {
                    html += '<option value="' + output[i].id + '">' + output[i].var_fname + ' ' + output[i].var_lname + '</option>';
                }
                $('#user_id').html(html);
            });
        });
    }


    var handleProjectNote = function () {

        $('body').on('click', '.btn-edit-note', function () {
            var clientId = $(this).attr('data-client-id');
            var projectId = $(this).attr('data-project-id');
            $('.client_id').val(clientId);
            $('.project_id').val(projectId);
            var noteId = $(this).attr('data-note-id');
            $('.note_id').val('');
            $('.project_note').val('');
            if (typeof noteId !== 'undefined' && noteId != '') {
                ajaxcall(adminurl + 'project/getProjectNoteById', {id: noteId}, function (output) {
                    output = JSON.parse(output);
                    var notes = output.txt_note;
                    $('.project_note').val(notes);
                    $('.note_id').val(noteId);
                    $('.modal-title').text('Edit Notes');
                    $('#add_project_notes_model').modal('show');
                });
            } else {
                $('.modal-title').text('Add Notes');
                $('#add_project_notes_model').modal('show');
            }

        });
    };
    var addProjectNote = function () {
        var form = $('#add_project_note_frm');
        var rules = {
            'notes': {required: true},
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form);
        });
    };


    var handleRules = function () {

        $('body').on('click', '.addNewRules', function () {
            var value = $(".company_rules").val();
            var Cusvalue = $(".company_Cusrules").val();
            if (value != '') {
                var html = '<div class="row"><div class="form-group"><label class="control-label col-md-3">' + value + '</label><a href="javascript:;" class=removeRules><i class="fa fa-close"></i></a><div class="clearfix"></div></div></div>';
                $('.companyRules').append(html);
            }
            if (Cusvalue != '') {
                var html = '<div class="row"><div class="form-group"><label class="control-label col-md-3">' + Cusvalue + '</label><a href="javascript:;" class=removeRules><i class="fa fa-close"></i></a><div class="clearfix"></div></div></div>';
                $('.companyRules').append(html);
            }

            $(".company_Cusrules").val('');
            $(".company_rules").val('');
            $(".company_rules").select2();

        });

        $('body').on('click', '.removeRules', function () {
            $(this).closest('.row').remove();
        });


    }

    var handleInterest = function () {

        $('body').on('click', '.addInterest', function () {
            var value = $(".Interest").val();

            if (value != '') {
                var html = '<div class="row"><div class="form-group"><label class="control-label col-md-3">' + value + '</label><a href="javascript:;" class=removeRules><i class="fa fa-close"></i></a><div class="clearfix"></div></div></div>';
                $('.angleInterest').append(html);
            }

            $(".Interest").val('');
            $(".Interest").select2();

        });

        $('body').on('click', '.removeRules', function () {
            $(this).closest('.row').remove();
        });


    }

    var handleExperts = function () {
        var url = adminurl + 'project/shortListedExperts';
        var data = {'projectId': projectId};
        shortListedExperts(data, url);

        var shorlistExpert = [];
        $('body').on('click', '.checkSelect', function () {
            var project_expert = $(this).val();
            if ($(this).is(':checked')) {
                shorlistExpert.push(project_expert);
                $(this).parents('.panel-default').addClass('panel-highlight');
            } else {
                if ($.inArray(project_expert, shorlistExpert) !== -1) {
                    shorlistExpert.splice($.inArray(project_expert, shorlistExpert), 1);
                }
                $(this).parents('.panel-default').removeClass('panel-highlight');
            }
        });

        $('body').on('click', '.sendBio', function () {

            var sendBio = ($(this).attr('data-status') == '1') ? true : false;

            if (shorlistExpert.length === 0) {
                Toastr.init('warning', 'Please select at least one experts', '');
            } else {
                if (sendBio == false) {
                    var url = adminurl + 'project/checkValidExperts';
                    var data = {shortlist: shorlistExpert};
                    ajaxcall(url, data, function (output) {
                        output = JSON.parse(output);
                        if (output.type == 'TRUE') {
                            var appendHrml = '';
                            console.log(output.data);

                            appendHrml += '<p>' + output.data + ' is currently not compliant.</p>';

                            appendHrml += '<p>are you sure you want to continue?</p>';
                            $('.appendHrml').html(appendHrml);
                            $('#compliance_list').modal('show');
                        } else {
                            sendBio == true;
                            bioSend(shorlistExpert);
                        }
                    })
                }
                if (sendBio == true) {
                    bioSend(shorlistExpert);
                }
            }
        });

        function bioSend(shorlistExpert) {
            var url = adminurl + 'project/sendBio';
            var data = {shortlist: shorlistExpert};
            ajaxcall(url, data, function (output) {
                $('#compliance_list').modal('hide');
                handleAjaxResponse(output);
            });
        }

        $('body').on('click', '.project_experts_notes', function () {

            var expertsId = $(this).attr('data-id');

            $('.expert_id').val(expertsId);
            $('#project_experts_notes_model').modal('show');
        })

        $('.expertshortlist_search').on('click', function () {
            var geography = $('#geography').val();
            var expertStatus = $('.projectStatus').val();
            var url = adminurl + 'project/shortListedExperts';
            if (geography != '' || expertStatus != '') {
                var data = {'projectId': projectId, geography: geography, expertStatus: expertStatus};
                shortListedExperts(data, url)
            } else {
                var data = {'projectId': projectId};
                shortListedExperts(data, url)
            }
        });

        function shortListedExperts(data, url) {

            ajaxcall(url, data, function (output) {
                $('.expertShortlisted').html(output);
                $('.checkSelect').uniform();
                handleAutoComplete();
                handleInlineAutoComplete();
            });
        }
    }

    var handleAllExpertsData = function(){
        var url = adminurl + 'project/shortListedExperts';
        var data = {'projectId': projectId};
        shortListedExperts(data, url);

        function shortListedExperts(data, url) {

            ajaxcall(url, data, function (output) {
                $('.expertShortlisted').html(output);
                $('.checkSelect').uniform();
                handleAutoComplete();
                handleInlineAutoComplete();
            });
        }
    }
    /* Prospect and prospect note manages */

    var handleReadmore = function () {
        $('body').on('click', ".readmore", function () {
            $(this).parent().next('.read_more').css("display", "block");
            $(this).closest('.read_less').css("display", "none");
            $(this).css("display", "none");
        });
        $('body').on('click', ".readless", function () {
            $(this).closest(".read_more").css("display", "none");
            $(this).parent().prev(".read_less").css("display", "block");
            $(".readmore").css("display", "block");
        });
    }

    var handleProspectNotes = function () {

        $('body').on('click', '.add_prospect', function () {
            var prospectId = $(this).attr('prospect-id');
            $('#prospect_id').val(prospectId);
            var noteId = $(this).attr('note-id');
            $('.prospect_notes').text('');
            if (typeof noteId !== 'undefined' && noteId != '') {
                ajaxcall(adminurl + 'project/getProspectNoteById', {noteId: noteId}, function (output) {
                    output = JSON.parse(output);
                    var notes = output.txt_note;
                    $('.prospect_notes').text(notes);
                    $('#note_id').val(noteId);
                    $('#add_prospect_notes .modal-title').text('Edit Prospect Notes');
                    $('#add_prospect_notes').modal('show');
                });
            } else {
                $('#add_prospect_notes .modal-title').text('Add Prospect Notes');
                $('#add_prospect_notes').modal('show');
            }
        });
    }

    var addProspectNote = function () {
        var form = $('#add_prospect_notes_frm');
        var rules = {
            'notes': {required: true},
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form);
        });
    };

    var addProspect = function () {
        var form = $('#add_prospect_frm');
        var rules = {
            'prospect_email': {email: true},
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form);
        });
    };

    // var dynamicTags = function(){
    //    alert('fa');
    //     var country = new Bloodhound({
    //         datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
    //         queryTokenizer: Bloodhound.tokenizers.whitespace,
    //         prefetch: {
    //             url: adminurl +'project/getCountry',
    //             filter: function(list) {
    //                 return $.map(list, function(country) {
    //                     return { name: country }; });
    //             }
    //         }
    //     });
    //     country.initialize();
    //
    //     $('#var_country_interest').tagsinput({
    //         typeaheadjs: {
    //             name: 'countryname',
    //             displayKey: 'name',
    //             valueKey: 'name',
    //             source: country.ttAdapter()
    //         }
    //     });
    //
    //     var regions = new Bloodhound({
    //         datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
    //         queryTokenizer: Bloodhound.tokenizers.whitespace,
    //         prefetch: {
    //             url: adminurl +'project/getRegions',
    //             filter: function(list) {
    //                 return $.map(list, function(regions) {
    //                     return { name: regions }; });
    //             }
    //         }
    //     });
    //     regions.initialize();
    //
    //     $('#var_regions_interest').tagsinput({
    //         typeaheadjs: {
    //             name: 'regionsname',
    //             displayKey: 'name',
    //             valueKey: 'name',
    //             source: regions.ttAdapter()
    //         }
    //     });
    //
    //     var company = new Bloodhound({
    //         datumTokenizer: Bloodhound.tokenizers.obj.whitespace('name'),
    //         queryTokenizer: Bloodhound.tokenizers.whitespace,
    //         prefetch: {
    //             url: adminurl +'project/getCompany',
    //             filter: function(list) {
    //                 return $.map(list, function(company) {
    //                     return { name: company }; });
    //             }
    //         }
    //     });
    //     company.initialize();
    //
    //     $('#var_companies_interest').tagsinput({
    //         typeaheadjs: {
    //             name: 'companyname',
    //             displayKey: 'name',
    //             valueKey: 'name',
    //             source: company.ttAdapter()
    //         }
    //     });
    // }


    var overviewTags = function () {
        $('body').on('click', '.cusComapnine', function () {
            $(this).hide();
            $(this).parent('.control-label').next('.Tagstoshow').show();
        });
        $('body').on('click', '.cancleTags', function () {
            $(this).parents('.Tagstoshow').hide();
            $(this).parents('.col-md-8').find('.cusComapnine').show();
        });

        $('body').on('click', '.updateTags', function () {
            var that = $(this);
            var value = $(this).parents('.Tagstoshow').find('input').val();
            var id = $(this).attr('data-id');
            var table = $(this).attr('data-table');
            ajaxcall(adminurl + 'project/updateTags', {value: value, id: id, table: table}, function (output) {

                if (value == '') {
                    that.parents('.col-md-8').find('.cusComapnine').html('<span style="color: #DD1144"><i>Empty</i></span>');
                } else {
                    value = value.replace(/\,/g, ' , ');
                    that.parents('.col-md-8').find('.cusComapnine').html(value);
                }
                that.parents('.col-md-8').find('.cusComapnine').show();
                that.parents('.Tagstoshow').hide();
            });
        });

        $('body').on('click','.expertsComplianceDate',function () {
            $(this).hide();
            $(this).parent('.control-label').next('.dateToshow').show();
        });

        $('body').on('click', '.cancelDates', function () {
            $(this).parents('.dateToshow').hide();
            $(this).parents('.col-md-8').find('.expertsComplianceDate').show();
        });

        $('body').on('click', '.updateDates', function () {
            var that = $(this);
            var value = $(this).parents('.dateToshow').find('input').val();
            var id = $(this).attr('data-id');
            var table = $(this).attr('data-table');
            ajaxcall(adminurl + 'project/updateDeadlineDate', {value: value, id: id, table: table}, function (output) {
                if (value == '') {
                    that.parents('.col-md-8').find('.expertsComplianceDate').html('<span style="color: #DD1144"><i>Compliance training incomplete</i></span>');
                } else {
                    that.parents('.col-md-8').find('.expertsComplianceDate').html(value);
                }
                that.parents('.col-md-8').find('.expertsComplianceDate').show();
                that.parents('.dateToshow').hide();
            });
        });


    }


    var ProjecthasExpertsNotes = function () {
        $('body').on('click', '.project_has_experts_notes', function () {
            var projecthasExperts = $(this).attr('data-project-has-experts-id');
            var projectId = $(this).attr('data-project-id');
            var clientId = $(this).attr('data-client-id');
            var userId = $(this).attr('data-user-id');
            var accountId = $(this).attr('data-account-manager-id');
            var expertsId = $(this).attr('data-experts-id');
            var projecthasExpertsNotesId = $(this).attr('data-project-has-experts-notes');

            $('.projecthasExpertsId').val(projecthasExperts);
            $('.projectId').val(projectId);
            $('.clientId').val(clientId);
            $('.userId').val(userId);
            $('.accountId').val(accountId);
            $('.expertId').val(expertsId);
            if (projecthasExpertsNotesId != '') {
                ajaxcall(adminurl + 'project/getProjecthasExpertsNotes', {noteId: projecthasExpertsNotesId}, function (output) {
                    $('#project_experts_notes_model').modal('show');
                    output = JSON.parse(output);
                    var notes = output.txt_note;
                    $('.project_exp_note').text(notes);
                    $('.projecthasExpertsNoteId').val(projecthasExpertsNotesId);
                    $('#project_experts_notes_model .modal-title').text('Edit Notes');
                });
            } else {
                $('#project_experts_notes_model').modal('show');
            }
        });
    }

    var handleAutoComplete = function () {

        var anglesofIntrest = new Bloodhound({
            datumTokenizer: function(d) { return Bloodhound.tokenizers.whitespace(d.name); },
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            limit: 10,
            prefetch: {
                url: adminurl+'project/getAngles',
                filter: function(list) {
                    console.log(list);
                    return $.map(list, function(angle) {
                        return { name: angle }; });
                }
            }
        });

        anglesofIntrest.initialize();

        if (App.isRTL()) {
            $('#angle_of_intrest').attr("dir", "rtl");
        }
        $('#angle_of_intrest').typeahead(null, {
            name: 'angle_of_intrest',
            displayKey: 'name',
            hint: (App.isRTL() ? false : true),
            source: anglesofIntrest.ttAdapter()
        });

        $('.angle_of_intrest').typeahead(null, {
            name: 'angle_of_intrest',
            displayKey: 'name',
            hint: (App.isRTL() ? false : true),
            source: anglesofIntrest.ttAdapter()
        });
    }

    function handleInlineAutoComplete(){
        $('body').on('click', '.angleOfIntrest', function () {
            $(this).hide();
            $(this).parent('.control-lable').next('.Angletoshow').show();
        });
        $('body').on('click', '.cancleTags', function () {
            $(this).parents('.Angletoshow').hide();
            $(this).parents('.col-md-2').find('.angleOfIntrest').show();
        });

        $('body').on('click', '.updateTags', function () {
            var that = $(this);
            var value = $(this).parents('.Angletoshow').find('input[name=angle_of_intrest]').val();
            var id = $(this).attr('data-id');
            var table = $(this).attr('data-table');
            ajaxcall(adminurl + 'project/updateAngle', {value: value, id: id,table:table}, function (output) {

                if (value == '') {
                    that.parents('.col-md-2').find('.angleOfIntrest').html("Empty");
                } else {
                    that.parents('.col-md-2').find('.angleOfIntrest').html(value);
                }
                that.parents('.col-md-2').find('.angleOfIntrest').show();
                that.parents('.Angletoshow').hide();
            });
        });
    }

    var addProjectExpertNote = function () {
        var form = $('#project_expert_note_frm');
        var rules = {
            'notes': {required: true},
        };
        handleFormValidate(form, rules, function (form) {
            handleAjaxFormSubmit(form);
        });
    };

    return {
        init: function () {
            handleDatatable();
        },
        add_project: function () {
            // dynamicTags();
            addProject();
            generalProject();
        },
        overview: function () {
            // dynamicTags();
            overviewTags();
        },
        call_init: function () {
            handleOverviewDatatables();
        },
        notes_init: function () {
            handleNotesDatatables();
            handleProjectNote();
            addProjectNote();
            handleDelete();
        },
        rules_init: function () {
            handleRules();
        },
        interest_init: function () {
            handleInterest();
        },
        experts_init: function () {
            handleReadmore();
            ProjecthasExpertsNotes();
            addProjectExpertNote();
            handleExperts();
        },
        experts_data_init: function(){
            handleAllExpertsData();
        },
        prospect_init: function () {
            handleAutoComplete();
            handleInlineAutoComplete();
            handleReadmore();
            handleProspectNotes();
            addProspect();
            addProspectNote();

        }
    };
}();