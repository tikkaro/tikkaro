var callDatatables = null;


var Call = function () {

    var handleDatatable = function () {
        callDatatables = getDataTable('#call_list', adminurl + 'calls/getCallList', {dataTable: {"columnDefs": [{"searchable": false, "targets": [-1]}]}});
    };

    return {
        init: function () {
            handleDatatable();
        }
    };
}();